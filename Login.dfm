object LoginFm: TLoginFm
  Left = 533
  Top = 237
  Width = 242
  Height = 157
  Caption = 'LOGIN'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object lbl1: TLabel
    Left = 26
    Top = 32
    Width = 50
    Height = 13
    Caption = 'UserName'
  end
  object lbl2: TLabel
    Left = 30
    Top = 59
    Width = 46
    Height = 13
    Caption = 'Password'
  end
  object IdEdit: TcxTextEdit
    Left = 90
    Top = 28
    Style.BorderColor = clWindowFrame
    Style.BorderStyle = ebs3D
    Style.HotTrack = False
    TabOrder = 0
    Width = 121
  end
  object CancelButton: TcxButton
    Left = 119
    Top = 90
    Width = 75
    Height = 25
    Caption = 'CANCEL'
    ModalResult = 2
    TabOrder = 3
  end
  object LoginButton: TcxButton
    Left = 38
    Top = 90
    Width = 75
    Height = 25
    Caption = 'LOGIN'
    ModalResult = 1
    TabOrder = 2
    OnClick = LoginButtonClick
  end
  object PasswordEdit: TEdit
    Left = 90
    Top = 55
    Width = 121
    Height = 20
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Wingdings'
    Font.Style = []
    ParentFont = False
    PasswordChar = 'l'
    TabOrder = 1
  end
end
