unit InstansiDropDown;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, dxSkinsCore, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, SDEngine, dxSkinBlack,
  dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinValentine, dxSkinXmas2008Blue, Menus,
  cxContainer, cxTextEdit, StdCtrls, cxButtons, ExtCtrls, cxButtonEdit,
  UCrpeClasses, UCrpe32;

type
  TInstansiDropDownFm = class(TForm)
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    BarangQ: TSDQuery;
    LPBDs: TDataSource;
    Panel1: TPanel;
    cxButTambah: TcxButton;
    cxSearchText: TcxTextEdit;
    Label1: TLabel;
    Crpe1: TCrpe;
    KodeQ: TSDQuery;
    KodeQAngka: TStringField;
    KodeQKode: TStringField;
    BarangQKode: TStringField;
    BarangQNama: TStringField;
    cxGrid1DBTableView1Kode: TcxGridDBColumn;
    cxGrid1DBTableView1Nama: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxSearchTextEnter(Sender: TObject);
    procedure cxSearchTextPropertiesChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cxButTambahClick(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    kode,nama:string;
    OriSQL:string;
    jumlah:integer;
  end;

var
  InstansiDropDownFm: TInstansiDropDownFm;

implementation

uses MasterBarang, MenuUtama, StrUtils, MasterInstansi;

{$R *.dfm}

procedure TInstansiDropDownFm.FormCreate(Sender: TObject);
begin
  BarangQ.Open;
end;

procedure TInstansiDropDownFm.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  kode:=BarangQKode.AsString;
  nama:=barangqnama.asstring;
  ModalResult:=mrOK;
end;

procedure TInstansiDropDownFm.cxSearchTextEnter(Sender: TObject);
begin
  cxSearchText.Text:='';
end;

procedure TInstansiDropDownFm.cxSearchTextPropertiesChange(Sender: TObject);
begin
  BarangQ.Close;
  BarangQ.SQL.Text:='select * from barang where nama like '+QuotedStr('%'+cxSearchText.Text+'%');
  BarangQ.Open;
  if BarangQ.RecordCount=0 then
    cxButTambah.Enabled:=True
  else
    cxButTambah.Enabled:=False;
end;

procedure TInstansiDropDownFm.FormShow(Sender: TObject);
begin
  OriSQL:=BarangQ.SQL.Text;
  cxButTambah.Enabled:=False;
  cxSearchText.SetFocus;
  BarangQ.Open;
end;

procedure TInstansiDropDownFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  BarangQ.SQL.Text:=OriSQL;
end;

procedure TInstansiDropDownFm.cxButTambahClick(Sender: TObject);
begin
  MasterInstansiFm := TMasterInstansiFm.create(self);
end;

procedure TInstansiDropDownFm.cxButton1Click(Sender: TObject);
var temp:string;
i:integer;
begin
end;

end.
