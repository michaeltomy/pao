unit LaporanPenjualan;

{$I cxVer.inc}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DemoBasicMain, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd,
  dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd,
  dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPSCore, ActnList, ImgList, Menus, ComCtrls,
  ToolWin, StdCtrls, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridCustomView, cxGrid, cxClasses,
  cxEditRepositoryItems, dxPScxCommon, dxPScxGridLnk, XPMan, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary,
  dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin,
  dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver, dxSkinSpringTime,
  dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, dxSkinscxPCPainter, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxGridLayoutViewLnk, dxSkinsdxBarPainter, DB,
  cxDBData, cxGridBandedTableView, cxGridDBBandedTableView, SDEngine,
  ExtCtrls, cxCalendar, cxGridDBTableView;

type
  TLaporanPenjualanFm = class(TDemoBasicMainForm)
    edrepMain: TcxEditRepository;
    edrepCenterText: TcxEditRepositoryTextItem;
    edrepRightText: TcxEditRepositoryTextItem;
    StyleRepository: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    cxStyle14: TcxStyle;
    tvssDevExpress: TcxGridTableViewStyleSheet;
    dxComponentPrinterLink1: TdxGridReportLink;
    SDQuery1: TSDQuery;
    DataSource1: TDataSource;
    Panel1: TPanel;
    Panel2: TPanel;
    cxGrid: TcxGrid;
    tvPlanets: TcxGridTableView;
    tvPlanetsNAME: TcxGridColumn;
    tvPlanetsNO: TcxGridColumn;
    tvPlanetsORBITS: TcxGridColumn;
    tvPlanetsDISTANCE: TcxGridColumn;
    tvPlanetsPERIOD: TcxGridColumn;
    tvPlanetsDISCOVERER: TcxGridColumn;
    tvPlanetsDATE: TcxGridColumn;
    tvPlanetsRADIUS: TcxGridColumn;
    cxGridDBBandedTableView1: TcxGridDBBandedTableView;
    cxGridLevel1: TcxGridLevel;
    DateTimePicker1: TDateTimePicker;
    DateTimePicker2: TDateTimePicker;
    Button1: TButton;
    MasterQ: TSDQuery;
    MasterDs: TDataSource;
    MasterUs: TSDUpdateSQL;
    PelangganQ: TSDQuery;
    PegawaiQ: TSDQuery;
    RealisasiAnjemUs: TSDUpdateSQL;
    MasterQKode: TStringField;
    MasterQTanggal: TDateTimeField;
    MasterQPelanggan: TStringField;
    MasterQDiskon: TCurrencyField;
    MasterQGrandTotal: TCurrencyField;
    MasterQJumlahDibayar: TCurrencyField;
    MasterQStatus: TStringField;
    MasterQKeterangan: TMemoField;
    MasterQPemeriksa: TStringField;
    MasterQPenjual: TStringField;
    MasterQCreateDate: TDateTimeField;
    MasterQCreateBy: TStringField;
    MasterQTglEntry: TDateTimeField;
    MasterQOperator: TStringField;
    MasterQDeleted: TBooleanField;
    cxGridDBBandedTableView1Kode: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1Tanggal: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1Pelanggan: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1Diskon: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1GrandTotal: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1JumlahDibayar: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1Status: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1Keterangan: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1Pemeriksa: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1Penjual: TcxGridDBBandedColumn;
    PelangganQKode: TStringField;
    PelangganQNama: TStringField;
    PelangganQAlamat: TStringField;
    PelangganQKota: TStringField;
    PelangganQTelp: TStringField;
    PelangganQHP: TStringField;
    PelangganQTglLahir: TDateTimeField;
    PelangganQFoto: TBlobField;
    PelangganQAktif: TBooleanField;
    PelangganQCreateDate: TDateTimeField;
    PelangganQCreateBy: TStringField;
    PelangganQTglEntry: TDateTimeField;
    PelangganQOperator: TStringField;
    PelangganQDeleted: TBooleanField;
    PegawaiQKode: TStringField;
    PegawaiQNama: TStringField;
    PegawaiQAlamat: TStringField;
    PegawaiQTglLahir: TDateTimeField;
    PegawaiQAktif: TBooleanField;
    PegawaiQCreateDate: TDateTimeField;
    PegawaiQCreateBy: TStringField;
    PegawaiQTglEntry: TDateTimeField;
    PegawaiQOperator: TStringField;
    PegawaiQDeleted: TBooleanField;
    MasterQNamaPelanggan: TStringField;
    MasterQNamaPemeriksa: TStringField;
    MasterQNamaPenjual: TStringField;
    BarangQ: TSDQuery;
    SDQuery1KodePenjualan: TStringField;
    SDQuery1KodeBarang: TStringField;
    SDQuery1Quantity: TFloatField;
    SDQuery1HargaBeli: TCurrencyField;
    SDQuery1HargaJual: TCurrencyField;
    SDQuery1Diskon: TCurrencyField;
    SDQuery1SubTotal: TCurrencyField;
    SDQuery1Deleted: TBooleanField;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridDBTableView1KodePenjualan: TcxGridDBColumn;
    cxGridDBTableView1KodeBarang: TcxGridDBColumn;
    cxGridDBTableView1Quantity: TcxGridDBColumn;
    cxGridDBTableView1HargaBeli: TcxGridDBColumn;
    cxGridDBTableView1HargaJual: TcxGridDBColumn;
    cxGridDBTableView1Diskon: TcxGridDBColumn;
    cxGridDBTableView1SubTotal: TcxGridDBColumn;
    cxGridDBTableView1Deleted: TcxGridDBColumn;
    BarangQKode: TStringField;
    BarangQBarcode: TStringField;
    BarangQNama: TStringField;
    BarangQKategori: TStringField;
    BarangQTipe: TStringField;
    BarangQKeterangan: TMemoField;
    BarangQJumlah: TFloatField;
    BarangQSatuan: TStringField;
    BarangQInsentifJual: TCurrencyField;
    BarangQInsentifPeriksa: TCurrencyField;
    BarangQHargaJual: TCurrencyField;
    BarangQHargaBeli: TCurrencyField;
    BarangQAktif: TBooleanField;
    BarangQCreateDate: TDateTimeField;
    BarangQCreateBy: TStringField;
    BarangQTglEntry: TDateTimeField;
    BarangQOperator: TStringField;
    BarangQDeleted: TBooleanField;
    SDQuery1NamaBarang: TStringField;
    MasterQDP: TCurrencyField;
    MasterQKodePenjualan: TStringField;
    MasterQKodeBarang: TStringField;
    MasterQQuantity: TFloatField;
    MasterQHargaBeli: TCurrencyField;
    MasterQHargaJual: TCurrencyField;
    MasterQDiskon_1: TCurrencyField;
    MasterQSubTotal: TCurrencyField;
    MasterQDeleted_1: TBooleanField;
    cxGridDBBandedTableView1Quantity: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1HargaJual: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1SubTotal: TcxGridDBBandedColumn;
    MasterQNamaBarang: TStringField;
    MasterQInsentifJual: TCurrencyField;
    MasterQInsentifPeriksa: TCurrencyField;
    cxGridDBBandedTableView1NamaBarang: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1InsentifJual: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1InsentifPeriksa: TcxGridDBBandedColumn;
    procedure actFullExpandExecute(Sender: TObject);
    procedure actFullCollapseExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure actShowDemoDescriptionExecute(Sender: TObject);
  private
    procedure CustomizeColumns;
  end;

var
  LaporanPenjualanFm: TLaporanPenjualanFm;

implementation

{$R *.dfm}
uses DateUtils;

procedure TLaporanPenjualanFm.actFullExpandExecute(Sender: TObject);
begin
  inherited;
  cxGridDBBandedTableView1.DataController.Groups.FullExpand;
  //tvPlanets.DataController.Groups.FullExpand;
end;

procedure TLaporanPenjualanFm.actFullCollapseExecute(Sender: TObject);
begin
  inherited;
  cxGridDBBandedTableView1.DataController.Groups.FullCollapse;
  //tvPlanets.DataController.Groups.FullCollapse;
end;

procedure TLaporanPenjualanFm.CustomizeColumns;
const
  cDistance = 3;
  cPeriod = 4;
  cRadius = 7;
var
  I: Integer;
begin
  DecimalSeparator := '.';
  with tvPlanets do
  for I := 0 to ColumnCount - 1 do
    if I in [cDistance, cRadius] then
      Columns[I].DataBinding.ValueTypeClass := TcxIntegerValueType
    else
      if I in [cPeriod] then
      Columns[I].DataBinding.ValueTypeClass := TcxFloatValueType
      else
       Columns[I].DataBinding.ValueTypeClass := TcxStringValueType;
end;

procedure TLaporanPenjualanFm.FormCreate(Sender: TObject);
begin
  inherited;
  CustomizeColumns;
end;

procedure TLaporanPenjualanFm.FormShow(Sender: TObject);
begin
  tvPlanets.DataController.Groups.FullExpand;
  DateTimePicker1.DateTime:=Today;
DateTimePicker2.DateTime:=Today;
PelangganQ.Open;
Pegawaiq.Open;
barangq.Open;
SDQuery1.Open;

end;

procedure TLaporanPenjualanFm.Button1Click(Sender: TObject);
var total:currency;
begin
  inherited;
  MasterQ.Close;
  MasterQ.ParamByName('text1').AsDate:= DateTimePicker1.Date;
  MasterQ.ParamByName('text2').AsDate:=DateTimePicker2.Date;
  MasterQ.Open;
end;

procedure TLaporanPenjualanFm.actShowDemoDescriptionExecute(Sender: TObject);
begin
  //inherited;

end;

end.
