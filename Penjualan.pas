unit Penjualan;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxCalendar,
  cxCurrencyEdit, cxCalc, dxSkinscxPCPainter, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxGridCustomView, cxGrid,
  cxDropDownEdit, cxLabel, UCrpeClasses, UCrpe32, cxCheckBox, cxRadioGroup,
  cxDBEdit, cxDBLookupComboBox, cxMemo;

type
  TPenjualanFm = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    MasterQ: TSDQuery;
    MasterDs: TDataSource;
    StatusBar: TStatusBar;
    MasterUS: TSDUpdateSQL;
    KodeQ: TSDQuery;
    ExitBtn: TcxButton;
    DeleteBtn: TcxButton;
    SaveBtn: TcxButton;
    KodeEdit: TcxButtonEdit;
    lbl1: TLabel;
    KodeQkode: TStringField;
    DetailPenjualanQ: TSDQuery;
    DataSource1: TDataSource;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    BarangQ: TSDQuery;
    DetailPenjualanUS: TSDUpdateSQL;
    BarangUS: TSDUpdateSQL;
    cxLabel1: TcxLabel;
    cxLabel3: TcxLabel;
    cxButton1: TcxButton;
    cxLabel2: TcxLabel;
    PegawaiQ: TSDQuery;
    cxLabel5: TcxLabel;
    cxLabel6: TcxLabel;
    cxButtonEdit1: TcxButtonEdit;
    PelangganQ: TSDQuery;
    cxCheckBox1: TcxCheckBox;
    cxLabel7: TcxLabel;
    cxCurrencyEdit2: TcxCurrencyEdit;
    Kode2Q: TSDQuery;
    PembayaranQ: TSDQuery;
    PembayaranUS: TSDUpdateSQL;
    DataSource2: TDataSource;
    Kode2QDataSource: TDataSource;
    Kode2Qkode: TStringField;
    cxGrid2: TcxGrid;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2Level1: TcxGridLevel;
    DataSource3: TDataSource;
    ViewQ: TSDQuery;
    SDQuery2: TSDQuery;
    MasterQKode: TStringField;
    MasterQTanggal: TDateTimeField;
    MasterQPelanggan: TStringField;
    MasterQGrandTotal: TCurrencyField;
    MasterQStatus: TStringField;
    MasterQKeterangan: TMemoField;
    MasterQPemeriksa: TStringField;
    MasterQPenjual: TStringField;
    MasterQCreateDate: TDateTimeField;
    MasterQCreateBy: TStringField;
    MasterQTglEntry: TDateTimeField;
    MasterQOperator: TStringField;
    MasterQDeleted: TBooleanField;
    DetailPenjualanQKodePenjualan: TStringField;
    DetailPenjualanQKodeBarang: TStringField;
    DetailPenjualanQQuantity: TFloatField;
    DetailPenjualanQHargaBeli: TCurrencyField;
    DetailPenjualanQHargaJual: TCurrencyField;
    DetailPenjualanQDiskon: TCurrencyField;
    DetailPenjualanQDeleted: TBooleanField;
    cxDBDateEdit1: TcxDBDateEdit;
    MasterQNamaPelanggan: TStringField;
    PelangganQKode: TStringField;
    PelangganQNama: TStringField;
    PelangganQAlamat: TStringField;
    PelangganQKota: TStringField;
    PelangganQTelp: TStringField;
    PelangganQHP: TStringField;
    PelangganQTglLahir: TDateTimeField;
    PelangganQFoto: TBlobField;
    PelangganQAktif: TBooleanField;
    PelangganQCreateDate: TDateTimeField;
    PelangganQCreateBy: TStringField;
    PelangganQTglEntry: TDateTimeField;
    PelangganQOperator: TStringField;
    PelangganQDeleted: TBooleanField;
    DetailPenjualanQSubTotal: TCurrencyField;
    DetailPenjualanQBarcodeBarang: TStringField;
    BarangQKode: TStringField;
    BarangQBarcode: TStringField;
    BarangQNama: TStringField;
    BarangQKategori: TStringField;
    BarangQTipe: TStringField;
    BarangQKeterangan: TMemoField;
    BarangQJumlah: TFloatField;
    BarangQSatuan: TStringField;
    BarangQInsentifJual: TCurrencyField;
    BarangQInsentifPeriksa: TCurrencyField;
    BarangQHargaJual: TCurrencyField;
    BarangQAktif: TBooleanField;
    BarangQCreateDate: TDateTimeField;
    BarangQCreateBy: TStringField;
    BarangQTglEntry: TDateTimeField;
    BarangQOperator: TStringField;
    BarangQDeleted: TBooleanField;
    DetailPenjualanQNamaBarang: TStringField;
    DetailPenjualanQTipeBarang: TStringField;
    DetailPenjualanQSatuanBarang: TStringField;
    cxGrid1DBTableView1KodeBarang: TcxGridDBColumn;
    cxGrid1DBTableView1Quantity: TcxGridDBColumn;
    cxGrid1DBTableView1HargaJual: TcxGridDBColumn;
    cxGrid1DBTableView1Diskon: TcxGridDBColumn;
    cxGrid1DBTableView1SubTotal: TcxGridDBColumn;
    cxGrid1DBTableView1BarcodeBarang: TcxGridDBColumn;
    cxGrid1DBTableView1NamaBarang: TcxGridDBColumn;
    cxGrid1DBTableView1TipeBarang: TcxGridDBColumn;
    cxGrid1DBTableView1SatuanBarang: TcxGridDBColumn;
    DetailPenjualanQStok: TFloatField;
    DetailPenjualanQHargaBeliBarang: TCurrencyField;
    DetailPenjualanQHargaJualBarang: TCurrencyField;
    cxDBCurrencyEdit1: TcxDBCurrencyEdit;
    cxLabel4: TcxLabel;
    cxCurrencyEdit3: TcxCurrencyEdit;
    cxLabel8: TcxLabel;
    cxDBCurrencyEdit2: TcxDBCurrencyEdit;
    MasterQDiskon: TCurrencyField;
    MasterQJumlahDibayar: TCurrencyField;
    ViewQKode: TStringField;
    ViewQTanggal: TDateTimeField;
    ViewQNama: TStringField;
    ViewQGrandTotal: TCurrencyField;
    ViewQStatus: TStringField;
    ViewQJumlahDibayar: TCurrencyField;
    PembayaranQKode: TStringField;
    PembayaranQTanggal: TDateTimeField;
    PembayaranQPelanggan: TStringField;
    PembayaranQPenjualan: TStringField;
    PembayaranQNominal: TCurrencyField;
    PembayaranQKeterangan: TMemoField;
    PembayaranQCreateDate: TDateTimeField;
    PembayaranQCreateBy: TStringField;
    PembayaranQOperator: TStringField;
    PembayaranQTglEntry: TDateTimeField;
    cxGrid2DBTableView1Kode: TcxGridDBColumn;
    cxGrid2DBTableView1Tanggal: TcxGridDBColumn;
    cxGrid2DBTableView1Nama: TcxGridDBColumn;
    cxGrid2DBTableView1GrandTotal: TcxGridDBColumn;
    cxGrid2DBTableView1Status: TcxGridDBColumn;
    cxGrid2DBTableView1JumlahDibayar: TcxGridDBColumn;
    MasterQDP: TCurrencyField;
    cxLabel9: TcxLabel;
    cxButtonEdit2: TcxButtonEdit;
    cxLabel10: TcxLabel;
    cxButtonEdit3: TcxButtonEdit;
    PegawaiQKode: TStringField;
    PegawaiQNama: TStringField;
    PegawaiQAlamat: TStringField;
    PegawaiQTglLahir: TDateTimeField;
    PegawaiQAktif: TBooleanField;
    PegawaiQCreateDate: TDateTimeField;
    PegawaiQCreateBy: TStringField;
    PegawaiQTglEntry: TDateTimeField;
    PegawaiQOperator: TStringField;
    PegawaiQDeleted: TBooleanField;
    SDQuery1: TSDQuery;
    StringField1: TStringField;
    StringField2: TStringField;
    StringField3: TStringField;
    DateTimeField1: TDateTimeField;
    BooleanField1: TBooleanField;
    DateTimeField2: TDateTimeField;
    StringField4: TStringField;
    DateTimeField3: TDateTimeField;
    StringField5: TStringField;
    BooleanField2: TBooleanField;
    MasterQNamaPemeriksa: TStringField;
    MasterQNamaPenjual: TStringField;
    BarangQHargaBeli: TCurrencyField;
    MasterQHPP: TCurrencyField;
    Crpe1: TCrpe;
    MasterQNamaInstansi: TStringField;
    MasterQNoRefJamKesMa: TStringField;
    cxLabel11: TcxLabel;
    cxLabel12: TcxLabel;
    cxDBTextEdit2: TcxDBTextEdit;
    cxButton2: TcxButton;
    cxLabel13: TcxLabel;
    cxDBMemo1: TcxDBMemo;
    InstansiQ: TSDQuery;
    InstansiQKode: TStringField;
    InstansiQNama: TStringField;
    InstansiDS: TDataSource;
    cxButtonEdit4: TcxButtonEdit;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ExitBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure KodeEditEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SaveBtnClick(Sender: TObject);
    procedure DeleteBtnClick(Sender: TObject);
    procedure SearchBtnClick(Sender: TObject);
    procedure MasterVGridKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MasterVGridBarangEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridJumlahEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure MasterVGridHargaSatuanEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxGrid1DBTableView1Column1PropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure SDQuery1BeforePost(DataSet: TDataSet);
    procedure cxGrid1Exit(Sender: TObject);
    procedure cxGrid1DBTableView1Column6PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxGrid1DBTableView1Column8PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure SDQuery1AfterPost(DataSet: TDataSet);
    procedure DetailQAfterPost(DataSet: TDataSet);
    procedure DetailQBeforePost(DataSet: TDataSet);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxGrid1DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SaveBtnKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxButton1Click(Sender: TObject);
    procedure cxGrid1DBTableView1Column7PropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure KodeEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxCheckBox1PropertiesChange(Sender: TObject);
    procedure cxGrid2DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure DetailPenjualanQAfterDelete(DataSet: TDataSet);
    procedure cxGrid1DBTableView1QuantityPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxGrid1DBTableView1KodeBarangPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxGrid1DBTableView1BarcodeBarangPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxGrid1DBTableView1NamaBarangPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxGrid1DBTableView1HargaJualPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxGrid1DBTableView1DiskonPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDBCurrencyEdit1PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDBCurrencyEdit1Exit(Sender: TObject);
    procedure cxButtonEdit2PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxButtonEdit3PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButtonEdit4PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent);
  end;

var
  PenjualanFm: TPenjualanFm;

  MasterOriSQL: string;
  BarangOriSQL, DetailPenjualanOriSQL:string;
implementation

uses MenuUtama, DropDown, DM, StrUtils, DateUtils, BarangDropDown,
  KartuGaransi, MasterPelanggan, InstansiDropDown;

{$R *.dfm}

{ TMasterArmadaFm }

constructor TPenjualanFm.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
end;

procedure TPenjualanFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TPenjualanFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TPenjualanFm.ExitBtnClick(Sender: TObject);
begin
  Release;
end;

procedure TPenjualanFm.FormCreate(Sender: TObject);
begin
  DMFm:=TDMFm.Create(self);
  MasterOriSQL:=MasterQ.SQL.Text;
  DetailPenjualanOriSQL:=DetailPenjualanQ.SQL.Text;
  BarangOriSQL:=BarangQ.SQL.Text;
  KodeEdit.Properties.MaxLength:=MasterQKode.Size;
end;

procedure TPenjualanFm.cxButtonEdit1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
if abuttonindex=0 then
begin
  PelangganQ.Close;
  PelangganQ.Open;
  DropDownFm:=TDropdownfm.Create(Self,PelangganQ);
  if DropDownFm.ShowModal=MrOK then
  begin
    MasterQPelanggan.AsString:=PelangganQKode.AsString;
    cxButtonEdit1.Text:=PelangganQNama.AsString;
  end;
  DropDownFm.Release;
end
else if abuttonindex=1 then
begin
  MasterPelangganFm:=TMasterPelangganFm.create(self);
end;
end;

procedure TPenjualanFm.KodeEditEnter(Sender: TObject);
begin
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  SaveBtn.Enabled:=false;
  DeleteBtn.Enabled:=false;
  MasterQ.Close;
end;

procedure TPenjualanFm.FormShow(Sender: TObject);
begin
  KodeEdit.SetFocus;
  ViewQ.Open;
end;

procedure TPenjualanFm.KodeEditExit(Sender: TObject);
begin
  if KodeEdit.Text<>'' then
  begin
    //kodeedit.Text:=DMfm.Fill(kodeedit.Text,10,'0',true);
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
    
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
      cxDBDateEdit1.Date:=Today;
      MasterQJumlahDibayar.AsInteger:=0;
      MasterQDiskon.AsCurrency:=0;
      DetailPenjualanQ.Close;
      DetailPenjualanQ.ParamByName('text').AsString:='a';
      DetailPenjualanQ.Open;
      cxGrid1.Enabled:=true;
    end
    else
    begin
      DetailPenjualanQ.Close;
      DetailPenjualanQ.ParamByName('text').AsString:=KodeEdit.Text;
      DetailPenjualanQ.Open;
      DetailPenjualanQ.Edit;
      PembayaranQ.Close;
      PembayaranQ.ParamByName('text').AsString:=KodeEdit.Text;
      pembayaranq.Open;
      masterq.Edit;
      cxButtonEdit1.Text:=MasterQNamaPelanggan.asstring;
      cxbuttonedit2.Text:=masterqnamapemeriksa.AsString;
      cxbuttonedit3.text:=masterqnamapenjual.asstring;
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      cxGrid1.Enabled:=false;
      DeleteBtn.Enabled:=True;
      cxGrid1.Enabled:=true;
      if MasterQStatus.AsString='LUNAS' then
      begin
        cxCheckBox1.Checked:=true;
        cxCurrencyEdit2.Visible:=false;
      end;
    end;
    SaveBtn.Enabled:=True;
    //MasterVGrid.Enabled:=True;
  end;
end;

procedure TPenjualanFm.KodeEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    if key=VK_F6 then
  begin
     SaveBtn.Click;
  end
  else if key=VK_F7 then
  begin
    DeleteBtn.Click;
  end
  else if key=VK_F8 then
  begin
    ExitBtn.Click;
  end
  else if key=VK_F5 then
  begin
    cxButtonEdit1PropertiesButtonClick(self,0);
  end
  else if key=VK_F4 then
  begin
    //ShowMessage('asd');
    cxGrid1DBTableView1.NavigatorButtons.Insert.Click;
    cxGrid1.SetFocus;
  end;
  if (Key=VK_escape) then
  begin
    ExitBtn.Click;
  end;
end;

procedure TPenjualanFm.SaveBtnClick(Sender: TObject);
var kodeprint:string;
begin
    if StatusBar.Panels[0].Text= 'Mode : Entry' then
    begin
      MasterQ.Edit;
      KodeQ.Close;
      KodeQ.Open;
      if KodeQ.IsEmpty then
      begin
        MasterQKode.AsString:=DMFm.Fill('1',10,'0',True);
      end
      else
      begin
        MasterQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
      end;

      MasterQGrandTotal.AsInteger:=strtoint(cxlabel2.Caption);
      KodeQ.Close;
      KodeEdit.Text:=MasterQKode.AsString;
      if cxCheckBox1.Checked then
      begin
        Masterqstatus.AsString:='LUNAS';
        MasterQDP.AsCurrency:=MasterQGrandTotal.AsCurrency;
        MasterQJumlahDibayar.AsCurrency:=MasterQGrandTotal.AsCurrency-MasterQDiskon.AsCurrency;
      end
      else
      begin
        MasterQDP.AsCurrency:=cxCurrencyEdit2.EditValue;
        MasterQStatus.AsString:='BELUM LUNAS';
        MasterQJumlahDibayar.AsCurrency:=cxCurrencyEdit2.EditValue;
      end;

    end
    else
    begin
      if cxCheckBox1.Checked then
      begin
        MasterQDP.AsCurrency:=cxCurrencyEdit2.EditValue;
        MasterQJumlahDibayar.AsCurrency:=MasterQGrandTotal.AsCurrency-MasterQDiskon.AsCurrency;
      end
      else
      begin
        MasterQJumlahDibayar.AsCurrency:=MasterQJumlahDibayar.AsCurrency-MasterQDP.AsCurrency+cxCurrencyEdit2.EditValue;
        MasterQDP.AsCurrency:=cxCurrencyEdit2.EditValue;
      end;
      if MasterQJumlahDibayar.AsCurrency>=MasterQGrandTotal.AsCurrency then MasterQStatus.AsString:='LUNAS';
    end;
    MasterQ.Post;
    MenuUtamaFm.Database1.StartTransaction;
    try
      MasterQ.ApplyUpdates;
      DetailPenjualanQ.ApplyUpdates;
      //PembayaranQ.ApplyUpdates;
      MenuUtamaFm.Database1.Commit;
      MasterQ.CommitUpdates;
      DetailPenjualanQ.CommitUpdates;
      //PembayaranQ.CommitUpdates;
      kodeprint:=MasterQKode.AsString;
      ShowMessage('Penyimpanan Berhasil');

      if StatusBar.Panels[0].Text= 'Mode : Entry' then
      begin
        if MessageDlg('Cetak Nota' + ', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then
        begin
          Crpe1.Refresh;
          Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'Invoice.rpt';
          Crpe1.ParamFields[0].CurrentValue:=kodeprint;
          Crpe1.Connect.UserID:=menuutamafm.DataBase1.Params.Values['USER NAME'];
          Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
          Crpe1.Connect.ServerName:=LeftStr(MenuutamaFm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);
          Crpe1.Execute;
        end;
        if MessageDlg('Buat Kartu Garansi' + ', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then
        begin
          KartuGaransiFm:=TKartuGaransiFm.create(self,'');
          KartuGaransiFm.showmodal;
        end;
      end;

    except
    on E : Exception do
    begin
              ShowMessage(E.ClassName+' error raised, with message : '+E.Message);

      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      //PembayaranQ.RollbackUpdates;
      DetailPenjualanQ.RollbackUpdates;
      end;
    end;
    SaveBtn.Enabled:=false;
    cxCheckBox1.Checked:=false;
    cxCurrencyEdit3.EditValue:=null;
    cxCurrencyEdit2.EditValue:=0;
    cxButtonEdit1.Text:='';
    ViewQ.Close;
    ViewQ.Open;
    viewq.Refresh;
    barangq.Refresh;
    
end;

procedure TPenjualanFm.DeleteBtnClick(Sender: TObject);
begin
  if MessageDlg('Hapus Penjualan '+KodeEdit.Text+' - '+MasterQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     MenuUtamaFm.Database1.StartTransaction;
     try
       SDQuery1.SQL.Text:='delete from detailpenjualan where kodejual='+MasterQKode.AsString;
       SDQuery1.ExecSQL;
       SDQuery1.SQL.Text:='delete from pembayaran where kodejual='+MasterQKode.AsString;
       SDQuery1.ExecSQL;
       MasterQ.Delete;
       //DetailPenjualanQ.ApplyUpdates;
       //PembayaranQ.ApplyUpdates;
       MasterQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;
       MessageDlg('Penjualan telah dihapus.',mtInformation,[mbOK],0);
     except
       MenuUtamaFm.Database1.Rollback;
       MasterQ.RollbackUpdates;
       Pembayaranq.RollbackUpdates;
       DetailPenjualanQ.RollbackUpdates;
       MessageDlg('Penjualan pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
     end;
     DeleteBtn.Enabled:=false;
     KodeEdit.SetFocus;
  end;
end;

procedure TPenjualanFm.SearchBtnClick(Sender: TObject);
begin
    cxGrid1DBTableView1.NavigatorButtons.Insert.Click;
    cxGrid1.SetFocus;
    cxGrid1DBTableView1.Columns[0].Focused:=true;
end;

procedure TPenjualanFm.MasterVGridKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
if key=VK_F6 then
begin
   SaveBtn.Click;
end
else if key=VK_F7 then
begin
  DeleteBtn.Click;
end
else if key=VK_F8 then
begin
  ExitBtn.Click;
end
  else if key=VK_F4 then
  begin
    cxGrid1DBTableView1.NavigatorButtons.Insert.Click;
    cxGrid1.SetFocus;
  end;
end;

procedure TPenjualanFm.MasterVGridBarangEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    DropDownFm:=TDropdownfm.Create(Self,BarangQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      {Masterqbarang.AsString:=barangqkode.asstring;
      masterqsupplier.asstring:=barangqsupplier.asstring;
      masterqnama.AsString:=barangqnama.asstring;
      masterqsatuan.asstring:=barangqsatuan.asstring;
      masterqjenis.AsString:=barangqjenis.asstring;
      MasterVGrid.SetFocus;}
    end;
    DropDownFm.Release;
end;

procedure TPenjualanFm.MasterVGridJumlahEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
  //MasterQJumlah.AsInteger:=DisplayValue;
  //MasterQGrandTotal.AsInteger:=MasterQJumlah.AsInteger * MasterQHargaSatuan.AsInteger;
end;

procedure TPenjualanFm.MasterVGridHargaSatuanEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin

  //MasterQHargaSatuan.AsCurrency:=DisplayValue;
  //MasterQGrandTotal.AsInteger:=MasterQJumlah.AsInteger * MasterQHargaSatuan.AsInteger;

end;

procedure TPenjualanFm.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  //KodeEdit.Text:=SDQuery1Kode.AsString;
  //KodeEditExit(self);
  masterq.Close;
  //MasterQ.SQL.Text:= 'select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+SDQuery1Kode.AsString+'%');
  //masterq.ParamByName('text').AsString:=SDQuery1Kode.AsString;
  masterq.Open;
  SaveBtn.Enabled:=true;
  DeleteBtn.Enabled:=true;
end;

procedure TPenjualanFm.cxGrid1DBTableView1Column1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  //ShowMessage(inttostr(abuttonindex));
    DetailPenjualanQ.Open;
    DetailPenjualanQ.Insert;
    BarangDropDownFm:=TBarangDropdownfm.Create(Self);
    if DropDownFm.ShowModal=MrOK then
    begin
      DetailPenjualanQKodeBarang.AsString:=BarangDropDownFm.kode;
    end;
    BarangDropDownFm.Release;
    //cxGrid1DBTableView1Column7.Focused:=true;
end;

procedure TPenjualanFm.SDQuery1BeforePost(DataSet: TDataSet);
var i : integer;
begin
    
end;

procedure TPenjualanFm.cxGrid1Exit(Sender: TObject);
begin
  cxGrid1DBTableView1.NavigatorButtons.Post.Click;
  cxButton1.SetFocus;
  //SaveBtn.SetFocus;
end;

procedure TPenjualanFm.cxGrid1DBTableView1Column6PropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
  //DetailQHargaSatuan.AsString:=DisplayValue;
  //DetailQSubTotal.AsInteger:=DetailQHargaSatuan.AsInteger * DetailQJumlah.AsInteger - DetailQDiskon.AsInteger;
  //cxLabel1.Caption:=inttostr();
end;

procedure TPenjualanFm.cxGrid1DBTableView1Column8PropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
  DetailPenjualanQDiskon.AsCurrency:=DisplayValue;
  DetailPenjualanQSubTotal.AsFloat:=DetailPenjualanQHargaJual.AsCurrency * DetailPenjualanQQuantity.AsFloat;

end;

procedure TPenjualanFm.SDQuery1AfterPost(DataSet: TDataSet);
var i,temp:integer;
begin

end;

procedure TPenjualanFm.DetailQAfterPost(DataSet: TDataSet);
var i:integer;
temp,temp2:double;
begin
    temp2:=0;
    temp:=0;
    DetailPenjualanQ.First;
    for i:= 1 to DetailPenjualanQ.RecordCount do
    begin
      //ShowMessage(DetailQSubTotal.AsString);
      temp:=temp+ DetailPenjualanQSubTotal.Asfloat;
      temp2:=temp2 + (DetailPenjualanQHargaBeliBarang.AsFloat*DetailPenjualanQQuantity.asfloat);
      DetailPenjualanQ.Next;
    end;
    cxCurrencyEdit3.Text:=FloatToStr(temp);
    Masterq.edit;
    MasterQGrandTotal.AsCurrency:=cxCurrencyEdit3.Value;
    MasterQHPP.AsCurrency:=temp2;
    //ShowMessage(MasterQHPP.AsString);
    cxCurrencyEdit3.SetFocus;
    cxlabel2.Caption:=floattostr(temp);
    //cxCurrencyEdit1.SetFocus;
    {BarangQ.Edit;
    //ShowMessage(BarangQStok.AsString);
    BarangQStok.AsInteger:=BarangQStok.AsInteger-DetailQJumlah.AsInteger;
    //ShowMessage(BarangQStok.AsString);
    BarangQ.Post;}
    DetailPenjualanQ.First;
      cxGrid1DBTableView1.NavigatorButtons.Post.Click;
  cxGrid1DBTableView1.NavigatorButtons.Insert.Click;
  cxGrid1.SetFocus;
  cxGrid1DBTableView1.Columns[7].Focused:=true;
end;

procedure TPenjualanFm.DetailQBeforePost(DataSet: TDataSet);
begin
    if KodeEdit.Text='INSERT BARU' then
    begin
      KodeQ.Open;
      if KodeQ.IsEmpty then
      begin
        DetailPenjualanQKodePenjualan.AsString:=DMFm.Fill('1',10,'0',True);
      end
      else
      begin
        DetailPenjualanQKodePenjualan.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
      end;
    end
    else
    begin
      DetailPenjualanQKodePenJualan.AsString:=KodeEdit.Text;
    end;
    DetailPenjualanQHargaBeli.AsCurrency:=DetailPenjualanQHargaBeliBarang.AsCurrency;
      DetailPenjualanQHargaJual.AsCurrency:=DetailPenjualanQHargaJualBarang.AsCurrency;
      
    //ShowMessage(SDQuery1KodeJual.AsString);
end;

procedure TPenjualanFm.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
  var temp:boolean;
begin
    if key=VK_F6 then
  begin
     SaveBtn.Click;
  end
  else if key=VK_F7 then
  begin
    DeleteBtn.Click;
  end
  else if key=VK_F8 then
  begin
    ExitBtn.Click;
  end
  else if key=VK_F5 then
  begin
    cxButtonEdit1PropertiesButtonClick(self,0);
  end
  else if key=VK_F4 then
  begin
    //ShowMessage('asd');
    cxGrid1DBTableView1.NavigatorButtons.Insert.Click;
    cxGrid1.SetFocus;
  end;
end;

procedure TPenjualanFm.cxGrid1DBTableView1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    if key=VK_F6 then
  begin
     SaveBtn.Click;
  end
  else if key=VK_F7 then
  begin
    DeleteBtn.Click;
  end
  else if key=VK_F8 then
  begin
    ExitBtn.Click;
  end
  else if key=VK_F5 then
  begin
    cxButtonEdit1PropertiesButtonClick(self,0);
  end
  else if key=VK_F4 then
  begin
    //ShowMessage('asd');
    cxGrid1DBTableView1.NavigatorButtons.Insert.Click;
    cxGrid1.SetFocus;
  end;
end;

procedure TPenjualanFm.SaveBtnKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_F6 then
  begin
     SaveBtn.Click;
  end
  else if key=VK_F7 then
  begin
    DeleteBtn.Click;
  end
  else if key=VK_F8 then
  begin
    ExitBtn.Click;
  end
  else if key=VK_F5 then
  begin
    cxButtonEdit1PropertiesButtonClick(self,0);
  end
  else if key=VK_F4 then
  begin
    //ShowMessage('asd');
    cxGrid1DBTableView1.NavigatorButtons.Insert.Click;
    cxGrid1.SetFocus;
  end;
end;

procedure TPenjualanFm.cxButton1Click(Sender: TObject);
begin
  cxGrid1DBTableView1.NavigatorButtons.Insert.Click;
  cxGrid1.SetFocus;
end;

procedure TPenjualanFm.cxGrid1DBTableView1Column7PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  BarangQ.Close;
  BarangQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,BarangQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      DetailPenjualanQ.Insert;
      DetailPenjualanQ.Edit;
      DetailPenjualanQKodebarang.AsString:=barangqkode.asstring;
      DetailPenjualanQDiskon.AsInteger:=0;
      DetailPenjualanQKodePenJualan.AsString:=MasterQKode.AsString;
    end;
    DropDownFm.Release;
    cxGrid1DBTableView1.Columns[5].Focused:=true;
end;

procedure TPenjualanFm.KodeEditPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  if kodeedit.Text ='' then kodeedit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  MasterQ.Open;
  MasterQ.Insert;
  MasterQ.Edit;
  KodeEditExit(sender);
  DetailPenjualanQ.Close;
  DetailPenjualanQ.SQL.Text:=DetailPenjualanOriSQL;
  DetailPenjualanQ.Open;
  cxButtonEdit1.Text:='';
  cxCurrencyEdit2.Value:=0;
  cxCurrencyEdit3.Value:=0;
  {cxGrid1.SetFocus;
  cxGrid1DBTableView1.NavigatorButtons.Post.Click;
  cxGrid1DBTableView1.NavigatorButtons.Insert.Click;
  cxGrid1.SetFocus;
  cxGrid1DBTableView1.Columns[0].Focused:=true;   }
end;

procedure TPenjualanFm.cxCheckBox1PropertiesChange(Sender: TObject);
begin
  if cxCheckBox1.Checked then
  begin
    cxLabel7.Visible:=false;
    cxCurrencyEdit2.Visible:=false;
  end
  else
  begin
    cxlabel7.Visible:=true;
    cxCurrencyEdit2.Visible:=true;
  end;
end;

procedure TPenjualanFm.cxGrid2DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  KodeEdit.Text:=ViewQKode.AsString;
  KodeEditExit(self);
  cxCurrencyEdit2.EditValue:=MasterQDP.AsCurrency;
  cxCurrencyEdit3.EditValue:=MasterQGrandTotal.AsCurrency-MasterQDiskon.AsCurrency;
  //cxCurrencyEdit3.SetFocus;
end;

procedure TPenjualanFm.DetailPenjualanQAfterDelete(DataSet: TDataSet);
var  i :integer;
temp,temp2:double;
begin
    temp:=0;
    DetailPenjualanQ.First;
    for i:= 1 to DetailPenjualanQ.RecordCount do
    begin
      //ShowMessage(DetailQSubTotal.AsString);
      temp:=temp+ DetailPenjualanQSubTotal.AsCurrency;
      temp2:=temp2 + (DetailPenjualanQHargaBeliBarang.AsFloat*DetailPenjualanQQuantity.asfloat);
      DetailPenjualanQ.Next;
    end;
    cxCurrencyEdit3.Text:=FloatToStr(temp);
    Masterq.edit;
    MasterQGrandTotal.AsCurrency:=cxCurrencyEdit3.Value;
    MasterQHPP.AsCurrency:=temp2;
    cxCurrencyEdit3.SetFocus;
    cxlabel2.Caption:=floattostr(temp);
end;

procedure TPenjualanFm.cxGrid1DBTableView1QuantityPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
  if DisplayValue> DetailPenjualanQStok.AsFloat then
  begin
    ShowMessage('stok tidak cukup');
    DetailPenjualanQQuantity.AsFloat:=0;
  end
  else
  begin
    DetailPenjualanQQuantity.AsFloat:=DisplayValue;
    DetailPenjualanQSubTotal.AsFloat:=DetailPenjualanQHargaJualBarang.AsCurrency * DetailPenjualanQQuantity.AsFloat - DetailPenjualanQDiskon.AsFloat;
    DetailPenjualanQHargaBeli.AsCurrency:=DetailPenjualanQHargaBeli.AsCurrency;
    DetailPenjualanQHargaJual.AsCurrency:=DetailPenjualanQHargaJual.AsCurrency;
  end;
end;

procedure TPenjualanFm.cxGrid1DBTableView1KodeBarangPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
  DetailPenjualanQKodeBarang.AsString:=DisplayValue;
  DetailPenjualanQHargaBeli.AsCurrency:=DetailPenjualanQHargaBeliBarang.AsCurrency;
  DetailPenjualanQHargaJual.AsCurrency:=DetailpenjualanQHargaJualBarang.AsCurrency;
end;

procedure TPenjualanFm.cxGrid1DBTableView1BarcodeBarangPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
  DetailPenjualanQBarcodeBarang.AsString:=DisplayValue;
  DetailPenjualanQHargaBeli.AsCurrency:=DetailPenjualanQHargaBeliBarang.AsCurrency;
  DetailPenjualanQHargaJual.AsCurrency:=DetailpenjualanQHargaJualBarang.AsCurrency;
  //ShowMessage(DetailPenjualanQHargaBeli.AsString);
end;

procedure TPenjualanFm.cxGrid1DBTableView1NamaBarangPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
  DetailPenjualanQNamaBarang.AsString:=DisplayValue;
  DetailPenjualanQHargaBeli.AsCurrency:=DetailPenjualanQHargaBeliBarang.AsCurrency;
  DetailPenjualanQHargaJual.AsCurrency:=DetailpenjualanQHargaJualBarang.AsCurrency;

end;

procedure TPenjualanFm.cxGrid1DBTableView1HargaJualPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
  DetailPenjualanQHargaJual.AsFloat:=DisplayValue;
  DetailPenjualanQSubTotal.AsFloat:=DetailPenjualanQHargaJualBarang.AsCurrency * DetailPenjualanQQuantity.AsFloat - DetailPenjualanQDiskon.AsFloat;
end;

procedure TPenjualanFm.cxGrid1DBTableView1DiskonPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
  DetailPenjualanQDiskon.AsFloat:=DisplayValue;
  DetailPenjualanQSubTotal.AsFloat:=DetailPenjualanQHargaJualBarang.AsCurrency * DetailPenjualanQQuantity.AsFloat - DetailPenjualanQDiskon.AsFloat;
end;

procedure TPenjualanFm.cxDBCurrencyEdit1PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
begin
  MasterQGrandTotal.AsCurrency:=cxCurrencyEdit3.Value-cxDBCurrencyEdit1.Value;
end;

procedure TPenjualanFm.cxDBCurrencyEdit1Exit(Sender: TObject);
begin
SaveBtn.SetFocus;
end;

procedure TPenjualanFm.cxButtonEdit2PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
    PegawaiQ.Close;
    PegawaiQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,PegawaiQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQPemeriksa.AsString:=PegawaiQKode.AsString;
      cxButtonEdit2.Text:=PegawaiQNama.AsString;
    end;
    DropDownFm.Release;
end;

procedure TPenjualanFm.cxButtonEdit3PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
    PegawaiQ.Close;
    PegawaiQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,PegawaiQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQPenjual.AsString:=PegawaiQKode.AsString;
      cxButtonEdit3.Text:=PegawaiQNama.AsString;
    end;
    DropDownFm.Release;
end;

procedure TPenjualanFm.cxButton2Click(Sender: TObject);
begin
  KartuGaransiFm:=TKartuGaransiFm.create(self,MasterQPelanggan.AsString);
  KartuGaransiFm.showmodal;
end;

procedure TPenjualanFm.cxButtonEdit4PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
    InstansiQ.Close;
    InstansiQ.Open;
    InstansiDropDownFm:=TInstansiDropdownfm.Create(Self);
    if InstansiDropDownFm.ShowModal=MrOK then
    begin
      MasterQNamaInstansi.AsString:=instansidropdownfm.kode;
      cxButtonEdit4.Text:=instansidropdownfm.nama;
    end;
    InstansiDropDownFm.Release;
end;

end.
