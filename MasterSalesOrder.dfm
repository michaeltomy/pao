object MasterSalesOrderFm: TMasterSalesOrderFm
  Left = 658
  Top = 52
  Width = 607
  Height = 258
  Caption = 'Master Sales Order'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 591
    Height = 48
    Align = alTop
    TabOrder = 0
    object lbl1: TLabel
      Left = 8
      Top = 13
      Width = 25
      Height = 13
      Caption = 'Kode'
    end
    object KodeEdit: TcxButtonEdit
      Left = 40
      Top = 10
      Properties.Buttons = <
        item
          Caption = '+'
          Default = True
          Kind = bkText
        end>
      Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      TabOrder = 0
      OnEnter = KodeEditEnter
      OnExit = KodeEditExit
      OnKeyDown = KodeEditKeyDown
      Width = 121
    end
    object SearchBtn: TcxButton
      Left = 168
      Top = 10
      Width = 57
      Height = 21
      Caption = 'Search'
      TabOrder = 1
      OnClick = SearchBtnClick
    end
  end
  object pnl2: TPanel
    Left = 0
    Top = 150
    Width = 591
    Height = 51
    Align = alBottom
    TabOrder = 2
    object ExitBtn: TcxButton
      Left = 172
      Top = 10
      Width = 75
      Height = 25
      Caption = 'EXIT'
      TabOrder = 0
      OnClick = ExitBtnClick
    end
    object DeleteBtn: TcxButton
      Left = 91
      Top = 10
      Width = 75
      Height = 25
      Caption = 'DELETE'
      TabOrder = 1
      OnClick = DeleteBtnClick
    end
    object SaveBtn: TcxButton
      Left = 8
      Top = 10
      Width = 75
      Height = 25
      Caption = 'SAVE'
      TabOrder = 2
      OnClick = SaveBtnClick
    end
  end
  object MasterVGrid: TcxDBVerticalGrid
    Left = 0
    Top = 48
    Width = 591
    Height = 102
    Align = alClient
    OptionsBehavior.GoToNextCellOnTab = True
    OptionsData.CancelOnExit = False
    OptionsData.Appending = False
    OptionsData.Deleting = False
    OptionsData.DeletingConfirmation = False
    OptionsData.Inserting = False
    TabOrder = 1
    TabStop = False
    DataController.DataSource = MasterDs
    Version = 1
    object MasterVGridTgl: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Tgl'
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object MasterVGridPelanggan: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Pelanggan'
      ID = 1
      ParentID = -1
      Index = 1
      Version = 1
    end
    object MasterVGridTglOrder: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'TglOrder'
      ID = 2
      ParentID = -1
      Index = 2
      Version = 1
    end
    object MasterVGridJenisBarang: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'JenisBarang'
      ID = 3
      ParentID = -1
      Index = 3
      Version = 1
    end
    object MasterVGridHarga: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Harga'
      ID = 4
      ParentID = -1
      Index = 4
      Version = 1
    end
    object MasterVGridRute: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Rute'
      ID = 5
      ParentID = -1
      Index = 5
      Version = 1
    end
    object MasterVGridKeterangan: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Keterangan'
      ID = 6
      ParentID = -1
      Index = 6
      Version = 1
    end
    object MasterVGridStatus: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Status'
      ID = 7
      ParentID = -1
      Index = 7
      Version = 1
    end
    object MasterVGridTglFollowUp: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'TglFollowUp'
      ID = 8
      ParentID = -1
      Index = 8
      Version = 1
    end
    object MasterVGridMuat: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Muat'
      ID = 9
      ParentID = -1
      Index = 9
      Version = 1
    end
    object MasterVGridBongkar: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Bongkar'
      ID = 10
      ParentID = -1
      Index = 10
      Version = 1
    end
    object MasterVGridPanjang: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Panjang'
      ID = 11
      ParentID = -1
      Index = 11
      Version = 1
    end
    object MasterVGridBerat: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Berat'
      ID = 12
      ParentID = -1
      Index = 12
      Version = 1
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 201
    Width = 591
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object MasterQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select * from masterso')
    UpdateObject = MasterUS
    Left = 313
    Top = 9
    object MasterQKodenota: TStringField
      FieldName = 'Kodenota'
      Required = True
    end
    object MasterQTgl: TDateTimeField
      FieldName = 'Tgl'
      Required = True
    end
    object MasterQPelanggan: TStringField
      FieldName = 'Pelanggan'
      Required = True
      Size = 10
    end
    object MasterQTglOrder: TDateTimeField
      FieldName = 'TglOrder'
    end
    object MasterQJenisBarang: TStringField
      FieldName = 'JenisBarang'
      Size = 100
    end
    object MasterQHarga: TCurrencyField
      FieldName = 'Harga'
      Required = True
    end
    object MasterQRute: TStringField
      FieldName = 'Rute'
      Required = True
      Size = 10
    end
    object MasterQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 100
    end
    object MasterQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Required = True
    end
    object MasterQCreateBy: TStringField
      FieldName = 'CreateBy'
      Required = True
      Size = 10
    end
    object MasterQOperator: TStringField
      FieldName = 'Operator'
      Required = True
      Size = 10
    end
    object MasterQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Required = True
    end
    object MasterQStatus: TIntegerField
      FieldName = 'Status'
      Required = True
    end
    object MasterQTglFollowUp: TDateTimeField
      FieldName = 'TglFollowUp'
    end
    object MasterQMuat: TStringField
      FieldName = 'Muat'
      Required = True
      Size = 50
    end
    object MasterQBongkar: TStringField
      FieldName = 'Bongkar'
      Required = True
      Size = 50
    end
    object MasterQPanjang: TFloatField
      FieldName = 'Panjang'
      Required = True
    end
    object MasterQBerat: TFloatField
      FieldName = 'Berat'
      Required = True
    end
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 380
    Top = 14
  end
  object MasterUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kodenota, Tgl, Pelanggan, TglOrder, JenisBarang, Harga, R' +
        'ute, Keterangan, CreateDate, CreateBy, Operator, TglEntry, Statu' +
        's, TglFollowUp, Panjang, Berat'
      'from masterso'
      'where'
      '  Kodenota = :OLD_Kodenota')
    ModifySQL.Strings = (
      'update masterso'
      'set'
      '  Kodenota = :Kodenota,'
      '  Tgl = :Tgl,'
      '  Pelanggan = :Pelanggan,'
      '  TglOrder = :TglOrder,'
      '  JenisBarang = :JenisBarang,'
      '  Harga = :Harga,'
      '  Rute = :Rute,'
      '  Keterangan = :Keterangan,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  TglEntry = :TglEntry,'
      '  Status = :Status,'
      '  TglFollowUp = :TglFollowUp,'
      '  Panjang = :Panjang,'
      '  Berat = :Berat'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into masterso'
      
        '  (Kodenota, Tgl, Pelanggan, TglOrder, JenisBarang, Harga, Rute,' +
        ' Keterangan, CreateDate, CreateBy, Operator, TglEntry, Status, T' +
        'glFollowUp, Panjang, Berat)'
      'values'
      
        '  (:Kodenota, :Tgl, :Pelanggan, :TglOrder, :JenisBarang, :Harga,' +
        ' :Rute, :Keterangan, :CreateDate, :CreateBy, :Operator, :TglEntr' +
        'y, :Status, :TglFollowUp, :Panjang, :Berat)'
      '')
    DeleteSQL.Strings = (
      'delete from masterso'
      'where'
      '  Kodenota = :OLD_Kodenota')
    Left = 452
    Top = 18
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from masterso order by kode desc')
    Left = 329
    Top = 71
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
end
