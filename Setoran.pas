unit Setoran;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxCalendar,
  cxCurrencyEdit, cxCalc, dxSkinscxPCPainter, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxGridCustomView, cxGrid,
  cxDropDownEdit, cxLabel;

type
  TSetoranFm = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    MasterQ: TSDQuery;
    MasterDs: TDataSource;
    MasterVGrid: TcxDBVerticalGrid;
    StatusBar: TStatusBar;
    MasterUS: TSDUpdateSQL;
    KodeQ: TSDQuery;
    ExitBtn: TcxButton;
    DeleteBtn: TcxButton;
    SaveBtn: TcxButton;
    KodeEdit: TcxButtonEdit;
    lbl1: TLabel;
    KodeQkode: TStringField;
    SDQuery1: TSDQuery;
    DataSource1: TDataSource;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    MasterVGridTanggal: TcxDBEditorRow;
    MasterVGridDeskripsi: TcxDBEditorRow;
    MasterVGridBiaya: TcxDBEditorRow;
    cxGrid1DBTableView1Kode: TcxGridDBColumn;
    cxGrid1DBTableView1Tanggal: TcxGridDBColumn;
    cxGrid1DBTableView1Biaya: TcxGridDBColumn;
    cxGrid1DBTableView1Deskripsi: TcxGridDBColumn;
    MasterQKode: TStringField;
    MasterQTanggal: TDateTimeField;
    MasterQDeskripsi: TStringField;
    MasterQNominal: TCurrencyField;
    MasterQCreateDate: TDateTimeField;
    MasterQCreateBy: TStringField;
    MasterQOperator: TStringField;
    MasterQTglEntry: TDateTimeField;
    MasterQDeleted: TBooleanField;
    SDQuery1Kode: TStringField;
    SDQuery1Tanggal: TDateTimeField;
    SDQuery1Deskripsi: TStringField;
    SDQuery1Nominal: TCurrencyField;
    SDQuery1CreateDate: TDateTimeField;
    SDQuery1CreateBy: TStringField;
    SDQuery1Operator: TStringField;
    SDQuery1TglEntry: TDateTimeField;
    SDQuery1Deleted: TBooleanField;
    procedure FormCreate(Sender: TObject);
    procedure KodeEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure KodeEditEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SaveBtnClick(Sender: TObject);
    procedure DeleteBtnClick(Sender: TObject);
    procedure MasterVGridKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure MasterQAfterInsert(DataSet: TDataSet);
    procedure MasterQBeforePost(DataSet: TDataSet);
   
  private
    { Private declarations }
  public
    { Public declarations }
//    constructor Create(aOwner: TComponent);
  end;

var
  SetoranFm: TSetoranFm;

  MasterOriSQL: string;
  BarangOriSQL : string;
implementation

uses MenuUtama, DropDown, DM;

{$R *.dfm}

{ TMasterArmadaFm }



procedure TSetoranFm.FormCreate(Sender: TObject);
begin
  DMFm:=TDMFm.Create(self);
  MasterOriSQL:=MasterQ.SQL.Text;
  KodeEdit.Properties.MaxLength:=MasterQKode.Size;
end;

procedure TSetoranFm.KodeEditPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  KodeEdit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  MasterQ.Open;
  MasterQ.Append;
  KodeEditExit(sender);
  MasterVGrid.SetFocus;
  //MasterVGrid.FocusRow(MasterVGridBarang);
end;

procedure TSetoranFm.KodeEditEnter(Sender: TObject);
begin
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
  MasterVGrid.Enabled:=false;
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  SaveBtn.Enabled:=false;
  DeleteBtn.Enabled:=false;
  MasterQ.Close;
end;

procedure TSetoranFm.FormShow(Sender: TObject);
begin
   KodeEdit.SetFocus;
end;

procedure TSetoranFm.KodeEditExit(Sender: TObject);
begin
  if KodeEdit.Text<>'' then
  begin
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
      DMfm.GetDateQ.Open;
  MasterQTanggal.AsDateTime:=dmfm.GetDateQNow.AsDateTime;
  dmfm.GetDateQ.Close;
      //MasterQAktif.AsBoolean :=false;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      DeleteBtn.Enabled:=True;
    end;
    SaveBtn.Enabled:=True;
    MasterVGrid.Enabled:=True;
  end;
end;

procedure TSetoranFm.KodeEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_F5) then
  begin
    KodeEditPropertiesButtonClick(sender,0);
  end;
  if (Key=VK_escape) then
  begin
    ExitBtn.Click;
  end;
end;

procedure TSetoranFm.SaveBtnClick(Sender: TObject);
begin
  if StatusBar.Panels[0].Text= 'Mode : Entry' then
  begin
    MasterQ.Edit;
    KodeQ.Open;
    if KodeQ.IsEmpty then
    begin
      MasterQKode.AsString:=DMFm.Fill('1',10,'0',True);
    end
    else
    begin
      MasterQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
    end;
    MasterQ.Post;
    KodeQ.Close;
    KodeEdit.Text:=MasterQKode.AsString;
  end;
  MenuUtamaFm.Database1.StartTransaction;
  try
    MasterQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    ShowMessage('Setoran dengan Kode ' + KodeEdit.Text + ' telah disimpan');
    KodeEdit.SetFocus;
  except
    MenuUtamaFm.Database1.Rollback;
    MasterQ.RollbackUpdates;
    ShowMessage('Penyimpanan Gagal');
  end;
  SDQuery1.Close;
  SDQuery1.Open;
end;

procedure TSetoranFm.DeleteBtnClick(Sender: TObject);
begin
  if MessageDlg('Hapus Setoran '+KodeEdit.Text+' - '+MasterQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     MenuUtamaFm.Database1.StartTransaction;
     try
       MasterQ.Delete;
       MasterQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;
       MessageDlg('Setoran telah dihapus.',mtInformation,[mbOK],0);
     except
       MenuUtamaFm.Database1.Rollback;
       MasterQ.RollbackUpdates;
       MessageDlg('Setoran pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
     end;
     KodeEdit.SetFocus;
     SDQuery1.Close;
     SDQuery1.Open;
  end;
end;

procedure TSetoranFm.MasterVGridKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if key=VK_F6 then
begin
   SaveBtn.Click;
end
else if key=VK_F7 then
begin
  DeleteBtn.Click;
end
else if key=VK_F8 then
begin
  ExitBtn.Click;
end;
end;

procedure TSetoranFm.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_F6 then
  begin
     SaveBtn.Click;
  end
  else if key=VK_F7 then
  begin
    DeleteBtn.Click;
  end
  else if key=VK_F8 then
  begin
    ExitBtn.Click;
  end
  else if key=VK_F5 then
  begin
    KodeEditPropertiesButtonClick(self,0);
  end;
end;

procedure TSetoranFm.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  KodeEdit.Text:=SDQuery1Kode.AsString;
  //KodeEditExit(self);
  masterq.Close;
  MasterQ.SQL.Text:= 'select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+SDQuery1Kode.AsString+'%');
  //masterq.ParamByName('text').AsString:=SDQuery1Kode.AsString;
  masterq.Open;
  DeleteBtn.Enabled:=true;
end;

procedure TSetoranFm.MasterQAfterInsert(DataSet: TDataSet);
begin
  MasterQCreateBy.AsString:=User;
end;

procedure TSetoranFm.MasterQBeforePost(DataSet: TDataSet);
begin
  MasterQOperator.AsString:=User;
end;

end.
