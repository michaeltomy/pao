object PengeluaranFm: TPengeluaranFm
  Left = 209
  Top = 245
  Width = 935
  Height = 396
  AutoSize = True
  Caption = 'Pengeluaran'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 919
    Height = 48
    Align = alTop
    TabOrder = 0
    object lbl1: TLabel
      Left = 8
      Top = 13
      Width = 25
      Height = 13
      Caption = 'Kode'
    end
    object KodeEdit: TcxButtonEdit
      Left = 40
      Top = 10
      Properties.Buttons = <
        item
          Caption = '+'
          Default = True
          Kind = bkText
        end>
      Properties.OnButtonClick = KodeEditPropertiesButtonClick
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      TabOrder = 0
      OnEnter = KodeEditEnter
      OnExit = KodeEditExit
      OnKeyDown = KodeEditKeyDown
      Width = 121
    end
    object cxLabel1: TcxLabel
      Left = 344
      Top = 8
      Caption = 'History Pengeluaran'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -21
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = []
      Style.IsFontAssigned = True
    end
  end
  object pnl2: TPanel
    Left = 0
    Top = 288
    Width = 919
    Height = 51
    Align = alBottom
    TabOrder = 2
    object ExitBtn: TcxButton
      Left = 172
      Top = 10
      Width = 75
      Height = 25
      Caption = 'EXIT [F8]'
      TabOrder = 1
    end
    object DeleteBtn: TcxButton
      Left = 91
      Top = 10
      Width = 75
      Height = 25
      Caption = 'DELETE [F7]'
      TabOrder = 2
      OnClick = DeleteBtnClick
    end
    object SaveBtn: TcxButton
      Left = 8
      Top = 10
      Width = 75
      Height = 25
      Caption = 'SAVE [F6]'
      TabOrder = 0
      OnClick = SaveBtnClick
    end
    object cxLabel2: TcxLabel
      Left = 264
      Top = 8
      Caption = 'New [F5]'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -21
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = []
      Style.IsFontAssigned = True
    end
  end
  object MasterVGrid: TcxDBVerticalGrid
    Left = 0
    Top = 48
    Width = 345
    Height = 240
    Align = alLeft
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    OptionsView.RowHeaderWidth = 113
    OptionsBehavior.GoToNextCellOnEnter = True
    OptionsBehavior.GoToNextCellOnTab = True
    OptionsData.CancelOnExit = False
    OptionsData.Appending = False
    OptionsData.Deleting = False
    OptionsData.DeletingConfirmation = False
    OptionsData.Inserting = False
    ParentFont = False
    TabOrder = 1
    TabStop = False
    OnKeyDown = MasterVGridKeyDown
    DataController.DataSource = MasterDs
    Version = 1
    object MasterVGridTanggal: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxDateEditProperties'
      Properties.EditProperties.SaveTime = False
      Properties.EditProperties.ShowTime = False
      Properties.DataBinding.FieldName = 'Tanggal'
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object MasterVGridBiaya: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Biaya'
      ID = 1
      ParentID = -1
      Index = 1
      Version = 1
    end
    object MasterVGridDeskripsi: TcxDBEditorRow
      Height = 42
      Properties.DataBinding.FieldName = 'Deskripsi'
      ID = 2
      ParentID = -1
      Index = 2
      Version = 1
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 339
    Width = 919
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object cxGrid1: TcxGrid
    Left = 349
    Top = 48
    Width = 570
    Height = 240
    Align = alRight
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
    object cxGrid1DBTableView1: TcxGridDBTableView
      OnCellDblClick = cxGrid1DBTableView1CellDblClick
      DataController.DataSource = DataSource1
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.FocusCellOnTab = True
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      object cxGrid1DBTableView1Kode: TcxGridDBColumn
        DataBinding.FieldName = 'Kode'
      end
      object cxGrid1DBTableView1Tanggal: TcxGridDBColumn
        DataBinding.FieldName = 'Tanggal'
      end
      object cxGrid1DBTableView1Biaya: TcxGridDBColumn
        DataBinding.FieldName = 'Biaya'
      end
      object cxGrid1DBTableView1Deskripsi: TcxGridDBColumn
        DataBinding.FieldName = 'Deskripsi'
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object MasterQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select * from pengeluaran')
    UpdateObject = MasterUS
    Left = 201
    Top = 9
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQTanggal: TDateTimeField
      FieldName = 'Tanggal'
    end
    object MasterQDeskripsi: TStringField
      FieldName = 'Deskripsi'
      Size = 250
    end
    object MasterQBiaya: TCurrencyField
      FieldName = 'Biaya'
    end
    object MasterQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object MasterQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object MasterQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object MasterQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object MasterQDeleted: TBooleanField
      FieldName = 'Deleted'
    end
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 244
    Top = 6
  end
  object MasterUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, Tanggal, Deskripsi, Biaya, CreateDate, CreateBy, Tg' +
        'lEntry, Operator, Deleted'#13#10'from pengeluaran'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update pengeluaran'
      'set'
      '  Kode = :Kode,'
      '  Tanggal = :Tanggal,'
      '  Deskripsi = :Deskripsi,'
      '  Biaya = :Biaya,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  TglEntry = :TglEntry,'
      '  Operator = :Operator,'
      '  Deleted = :Deleted'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into pengeluaran'
      
        '  (Kode, Tanggal, Deskripsi, Biaya, CreateDate, CreateBy, TglEnt' +
        'ry, Operator, Deleted)'
      'values'
      
        '  (:Kode, :Tanggal, :Deskripsi, :Biaya, :CreateDate, :CreateBy, ' +
        ':TglEntry, :Operator, :Deleted)')
    DeleteSQL.Strings = (
      'delete from pengeluaran'
      'where'
      '  Kode = :OLD_Kode')
    Left = 268
    Top = 10
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from pengeluaran order by kode desc')
    Left = 169
    Top = 7
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object SDQuery1: TSDQuery
    AutoRefresh = True
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from pengeluaran order by kode desc'
      '')
    Left = 473
    Top = 151
    object SDQuery1Kode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object SDQuery1Tanggal: TDateTimeField
      FieldName = 'Tanggal'
    end
    object SDQuery1Biaya: TCurrencyField
      FieldName = 'Biaya'
    end
    object SDQuery1Deskripsi: TStringField
      FieldName = 'Deskripsi'
      Size = 250
    end
  end
  object DataSource1: TDataSource
    DataSet = SDQuery1
    Left = 508
    Top = 150
  end
end
