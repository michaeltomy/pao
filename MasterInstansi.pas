unit MasterInstansi;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxCalendar,
  cxCurrencyEdit, cxCalc;

type
  TMasterInstansiFm = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    MasterQ: TSDQuery;
    MasterDs: TDataSource;
    MasterVGrid: TcxDBVerticalGrid;
    StatusBar: TStatusBar;
    MasterUS: TSDUpdateSQL;
    KodeQ: TSDQuery;
    ExitBtn: TcxButton;
    DeleteBtn: TcxButton;
    SaveBtn: TcxButton;
    KodeEdit: TcxButtonEdit;
    lbl1: TLabel;
    SearchBtn: TcxButton;
    KodeQkode: TStringField;
    KategoriQ: TSDQuery;
    KategoriQKode: TStringField;
    KategoriQNamaKategori: TStringField;
    KategoriQMinStok: TFloatField;
    KategoriQMaxStok: TFloatField;
    KategoriQDeleted: TBooleanField;
    MasterQKode: TStringField;
    MasterQNama: TStringField;
    MasterVGridNama: TcxDBEditorRow;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ExitBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure KodeEditEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SaveBtnClick(Sender: TObject);
    procedure DeleteBtnClick(Sender: TObject);
    procedure SearchBtnClick(Sender: TObject);
    procedure MasterVGridKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent);
  end;

var
  MasterInstansiFm: TMasterInstansiFm;

  MasterOriSQL: string;
implementation

uses MenuUtama, DropDown, DM, KategoriBarangDropDown, BarangDropDown,
  InstansiDropDown;

{$R *.dfm}

{ TMasterArmadaFm }

constructor TMasterInstansiFm.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
end;

procedure TMasterInstansiFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TMasterInstansiFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TMasterInstansiFm.ExitBtnClick(Sender: TObject);
begin
  Release;
end;

procedure TMasterInstansiFm.FormCreate(Sender: TObject);
begin
  DMFm:=TDMFm.Create(self);
  MasterOriSQL:=MasterQ.SQL.Text;
  KodeEdit.Properties.MaxLength:=MasterQKode.Size;
end;

procedure TMasterInstansiFm.cxButtonEdit1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  KodeEdit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  MasterQ.Open;
  MasterQ.Append;
  KodeEditExit(sender);
  MasterVGrid.SetFocus;
end;

procedure TMasterInstansiFm.KodeEditEnter(Sender: TObject);
begin
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
  MasterVGrid.Enabled:=false;
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  SaveBtn.Enabled:=false;
  DeleteBtn.Enabled:=false;
  MasterQ.Close;
end;

procedure TMasterInstansiFm.FormShow(Sender: TObject);
begin
  KodeEdit.SetFocus;
end;

procedure TMasterInstansiFm.KodeEditExit(Sender: TObject);
begin
  if KodeEdit.Text<>'' then
  begin
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
      //MasterQAktif.AsBoolean :=false;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      DeleteBtn.Enabled:=True;
      MasterQ.Edit;
    end;
    SaveBtn.Enabled:=True;
    MasterVGrid.Enabled:=True;
  end;
end;

procedure TMasterInstansiFm.KodeEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_F5) then
  begin
    cxButtonEdit1PropertiesButtonClick(sender,0);
    //MasterQ.SQL.Text:=MasterOriSQL;
    //DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    //if DropDownFm.ShowModal=MrOK then
    //begin
    //  KodeEdit.Text:=DropDownFm.kode;
    //  KodeEditExit(Sender);
    //  MasterVGrid.SetFocus;
    //end;
    //DropDownFm.Release;
  end;
  if (Key=VK_escape) then
  begin
    ExitBtn.Click;
  end;
end;

procedure TMasterInstansiFm.SaveBtnClick(Sender: TObject);
begin
  if StatusBar.Panels[0].Text= 'Mode : Entry' then
  begin
    MasterQ.Edit;
    KodeQ.Open;
    if KodeQ.IsEmpty then
    begin
      MasterQKode.AsString:=DMFm.Fill('1',10,'0',True);
    end
    else
    begin
      MasterQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
    end;
    KodeQ.Close;
    KodeEdit.Text:=MasterQKode.AsString;
  end;
  MasterQ.Post;
  MenuUtamaFm.Database1.StartTransaction;
  try
    MasterQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    ShowMessage('Instansi dengan Kode ' + KodeEdit.Text + ' telah disimpan');
    KodeEdit.SetFocus;
  except
    MenuUtamaFm.Database1.Rollback;
    MasterQ.RollbackUpdates;
    ShowMessage('Penyimpanan Gagal');
  end;
end;

procedure TMasterInstansiFm.DeleteBtnClick(Sender: TObject);
begin
  if MessageDlg('Hapus Instansi '+KodeEdit.Text+' - '+MasterQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     MenuUtamaFm.Database1.StartTransaction;
     try
       MasterQ.Delete;
       MasterQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;
       MessageDlg('Instansi telah dihapus.',mtInformation,[mbOK],0);
     except
       MenuUtamaFm.Database1.Rollback;
       MasterQ.RollbackUpdates;
       MessageDlg('Instansi pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
     end;
     KodeEdit.SetFocus;
  end;
end;

procedure TMasterInstansiFm.SearchBtnClick(Sender: TObject);
begin

    InstansiDropDownFm:=TInstansiDropdownfm.Create(Self);
    if InstansiDropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=InstansiDropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    InstansiDropDownFm.Release;
end;

procedure TMasterInstansiFm.MasterVGridKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
if key=VK_F6 then
begin
   SaveBtn.Click;
end
else if key=VK_F7 then
begin
  DeleteBtn.Click;
end
else if key=VK_F8 then
begin
  ExitBtn.Click;
end;
end;

procedure TMasterInstansiFm.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if key=VK_F6 then
begin
   SaveBtn.Click;
end
else if key=VK_F7 then
begin
  DeleteBtn.Click;
end
else if key=VK_F8 then
begin
  ExitBtn.Click;
end;
end;

end.
