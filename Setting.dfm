object SettingFm: TSettingFm
  Left = 212
  Top = 134
  Width = 323
  Height = 217
  Caption = 'Setting'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object cxLabel1: TcxLabel
    Left = 8
    Top = 16
    Caption = 'Servis'
    ParentFont = False
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -16
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = []
    Style.IsFontAssigned = True
  end
  object cxLabel2: TcxLabel
    Left = 8
    Top = 56
    Caption = 'Pajak'
    ParentFont = False
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -16
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = []
    Style.IsFontAssigned = True
  end
  object cxLabel3: TcxLabel
    Left = 8
    Top = 96
    Caption = 'BCA'
    ParentFont = False
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -16
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = []
    Style.IsFontAssigned = True
  end
  object cxLabel4: TcxLabel
    Left = 8
    Top = 136
    Caption = 'Visa/Master'
    ParentFont = False
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -16
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = []
    Style.IsFontAssigned = True
  end
  object cxButton1: TcxButton
    Left = 200
    Top = 24
    Width = 97
    Height = 49
    Caption = 'SAVE'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
    OnClick = cxButton1Click
  end
  object cxButton2: TcxButton
    Left = 200
    Top = 96
    Width = 97
    Height = 49
    Caption = 'RESET'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
  end
  object cxDBCEServis: TcxDBCurrencyEdit
    Left = 120
    Top = 16
    DataBinding.DataField = 'Servis'
    DataBinding.DataSource = SettingDs
    ParentFont = False
    Properties.DisplayFormat = '0.00, %'
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -16
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = []
    Style.IsFontAssigned = True
    TabOrder = 6
    Width = 65
  end
  object cxDBCEPajak: TcxDBCurrencyEdit
    Left = 120
    Top = 56
    DataBinding.DataField = 'Pajak'
    DataBinding.DataSource = SettingDs
    ParentFont = False
    Properties.DisplayFormat = '0.00, %'
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -16
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = []
    Style.IsFontAssigned = True
    TabOrder = 7
    Width = 65
  end
  object cxDBCEBCA: TcxDBCurrencyEdit
    Left = 120
    Top = 96
    DataBinding.DataField = 'BCA'
    DataBinding.DataSource = SettingDs
    ParentFont = False
    Properties.DisplayFormat = '0.00, %'
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -16
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = []
    Style.IsFontAssigned = True
    TabOrder = 8
    Width = 65
  end
  object cxDBCEVisaMaster: TcxDBCurrencyEdit
    Left = 120
    Top = 136
    DataBinding.DataField = 'VisaMaster'
    DataBinding.DataSource = SettingDs
    ParentFont = False
    Properties.DisplayFormat = '0.00, %'
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -16
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = []
    Style.IsFontAssigned = True
    TabOrder = 9
    Width = 65
  end
  object SettingQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'SELECT * FROM Setting')
    UpdateObject = SettingUS
    Left = 224
    Top = 152
    object SettingQJumlahMeja: TIntegerField
      FieldName = 'JumlahMeja'
    end
    object SettingQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object SettingQPajak: TFloatField
      FieldName = 'Pajak'
    end
    object SettingQServis: TFloatField
      FieldName = 'Servis'
    end
    object SettingQBCA: TFloatField
      FieldName = 'BCA'
    end
    object SettingQVisaMaster: TFloatField
      FieldName = 'VisaMaster'
    end
  end
  object SettingUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      'select JumlahMeja, Kode, Pajak, Servis, BCA, VisaMaster'
      'from Setting'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update Setting'
      'set'
      '  JumlahMeja = :JumlahMeja,'
      '  Kode = :Kode,'
      '  Pajak = :Pajak,'
      '  Servis = :Servis,'
      '  BCA = :BCA,'
      '  VisaMaster = :VisaMaster'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into Setting'
      '  (JumlahMeja, Kode, Pajak, Servis, BCA, VisaMaster)'
      'values'
      '  (:JumlahMeja, :Kode, :Pajak, :Servis, :BCA, :VisaMaster)')
    DeleteSQL.Strings = (
      'delete from Setting'
      'where'
      '  Kode = :OLD_Kode')
    Left = 256
    Top = 152
  end
  object SettingDs: TDataSource
    DataSet = SettingQ
    Left = 192
    Top = 152
  end
end
