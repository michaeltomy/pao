unit Invoice;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxCalendar,
  cxCurrencyEdit, cxCalc, cxRadioGroup, cxLabel, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxDBData, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxGroupBox, cxDropDownEdit, UCrpeClasses,
  UCrpe32, cxMemo;

type
  TInvoiceFm = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    StatusBar: TStatusBar;
    MasterUS: TSDUpdateSQL;
    SaveBtn: TcxButton;
    PenerimaanQ: TSDQuery;
    ExitBtn: TcxButton;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    cxLabel3: TcxLabel;
    cxLabel4: TcxLabel;
    cxLabel5: TcxLabel;
    cxLabel6: TcxLabel;
    MasterQ: TSDQuery;
    MasterDs: TDataSource;
    cxDBVerticalGrid1: TcxDBVerticalGrid;
    KodeQ: TSDQuery;
    Crpe1: TCrpe;
    KodeQkode: TStringField;
    SupplierQ: TSDQuery;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    ViewQ: TSDQuery;
    ViewDs: TDataSource;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    ViewQKode: TStringField;
    ViewQTanggal: TDateTimeField;
    ViewQSupplier: TStringField;
    ViewQPenerimaanBahan: TStringField;
    ViewQNominal: TCurrencyField;
    ViewQCreateDate: TDateTimeField;
    ViewQCreateBy: TStringField;
    ViewQOperator: TStringField;
    ViewQTglEntry: TDateTimeField;
    ViewQnamasupplier: TStringField;
    cxGrid1DBTableView1Kode: TcxGridDBColumn;
    cxGrid1DBTableView1Tanggal: TcxGridDBColumn;
    cxGrid1DBTableView1Nominal: TcxGridDBColumn;
    cxGrid1DBTableView1namasupplier: TcxGridDBColumn;
    PenerimaanQKode: TStringField;
    PenerimaanQTanggal: TDateTimeField;
    PenerimaanQNoUrut: TIntegerField;
    PenerimaanQSupplier: TStringField;
    PenerimaanQHargaTotal: TCurrencyField;
    PenerimaanQPenerima: TStringField;
    PenerimaanQCreateDate: TDateTimeField;
    PenerimaanQCreateBy: TStringField;
    PenerimaanQOperator: TStringField;
    PenerimaanQTglEntry: TDateTimeField;
    PenerimaanQStatus: TStringField;
    PenerimaanQJumlahTerbayar: TCurrencyField;
    MasterQHargaTotal: TCurrencyField;
    MasterQJumlahTerbayar: TCurrencyField;
    MasterQsisa: TCurrencyField;
    cxDBVerticalGrid1Tanggal: TcxDBEditorRow;
    cxDBVerticalGrid1Supplier: TcxDBEditorRow;
    cxDBVerticalGrid1PenerimaanBahan: TcxDBEditorRow;
    cxDBVerticalGrid1Nominal: TcxDBEditorRow;
    cxDBVerticalGrid1HargaTotal: TcxDBEditorRow;
    cxDBVerticalGrid1JumlahTerbayar: TcxDBEditorRow;
    cxDBVerticalGrid1sisa: TcxDBEditorRow;
    SupplierQKode: TStringField;
    SupplierQNama: TStringField;
    SupplierQAlamat: TStringField;
    SupplierQKota: TStringField;
    SupplierQTelp: TStringField;
    SupplierQPIC: TStringField;
    SupplierQTelpPIC: TStringField;
    MasterQNamaSupplier: TStringField;
    cxDBVerticalGrid1NamaSupplier: TcxDBEditorRow;
    MasterQKode: TStringField;
    MasterQTanggal: TDateTimeField;
    MasterQSupplier: TStringField;
    MasterQPenerimaanBahan: TStringField;
    MasterQNominal: TCurrencyField;
    MasterQCreateDate: TDateTimeField;
    MasterQCreateBy: TStringField;
    MasterQOperator: TStringField;
    MasterQTglEntry: TDateTimeField;
    Panel1: TPanel;
    cxDateEdit2: TcxDateEdit;
    cxDateEdit1: TcxDateEdit;
    cxLabel7: TcxLabel;
    MasterQDiskon: TCurrencyField;
    cxDBVerticalGrid1Diskon: TcxDBEditorRow;
    procedure CreateParams(var Params: TCreateParams);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ExitBtnClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SaveBtnClick(Sender: TObject);
    procedure cxDBVerticalGrid1JumlahPembayaranEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxDBVerticalGrid1DiskonEditPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
    procedure MasterQAfterInsert(DataSet: TDataSet);
    procedure MasterQBeforePost(DataSet: TDataSet);
    procedure cxDateEdit1PropertiesChange(Sender: TObject);
    procedure cxDateEdit2PropertiesChange(Sender: TObject);
private
    { Private declarations }
  public
    { Public declarations }
    kodejual,supp:string;

  end;

var
  InvoiceFm: TInvoiceFm;

  MasterOriSQL,ViewOriSQL: string;

implementation

uses MenuUtama, DropDown, DM, StrUtils, Pembayaran;

{$R *.dfm}

{ TMasterArmadaFm }

procedure TInvoiceFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TInvoiceFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  //ModalResult:=mrOk;
end;

procedure TInvoiceFm.ExitBtnClick(Sender: TObject);
begin
//ModalResult:=mrOk;

end;

procedure TInvoiceFm.FormShow(Sender: TObject);
var i : integer;
begin
  if kodejual<>'' then
  begin
    masterq.Close;
    masterq.Open;
    masterq.Insert;
    masterq.Edit;
    kodeq.Open;
    KodeQ.Refresh;
    MasterQTanggal.AsDateTime:=now();
    MasterQpenerimaanbahan.AsString:=kodejual;
    MasterQSupplier.AsString:=supp;
    MasterQDiskon.AsCurrency:=0;
    MasterQsisa.AsCurrency:=MasterQHargaTotal.AsCurrency-MasterQJumlahTerbayar.AsCurrency;
    cxDateEdit1.Date:=now-1;
    cxdateedit2.Date:=now;
    viewq.close;
    viewq.parambyname('text1').AsDate:=cxDateEdit1.Date;
    viewq.ParamByName('text2').AsDate:=cxDateEdit2.Date+1;
    viewq.open;
    cxDBVerticalGrid1.SetFocus;
    cxDBVerticalGrid1Nominal.Focused:=true;
  end;
end;

procedure TInvoiceFm.SaveBtnClick(Sender: TObject);
var kodeprint : String;
thn, bln, tgl : word;
temp : TDateTime;
begin
  MenuUtamaFm.Database1.StartTransaction;
  try
    KodeQ.Open;
    KodeQ.Refresh;
    if KodeQ.IsEmpty then
    begin
      MasterQKode.AsString:=DMFm.Fill('1',10,'0',True);
    end
    else
    begin
      MasterQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
    end;
    MasterQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    ShowMessage('Simpan Sukses');
    PembayaranFm.ViewQ.Refresh;
    kodeprint:=MasterQKode.asstring;
  except
    MenuUtamaFm.Database1.Rollback;
    MasterQ.RollbackUpdates;
    ShowMessage('Penyimpanan Gagal');
  end;
  //masterq.Open;
  viewq.Refresh;
end;


procedure TInvoiceFm.cxDBVerticalGrid1JumlahPembayaranEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin    {
  MasterQJumlahPembayaran.AsInteger:=DisplayValue;
  if (MasterQJumlahPembayaran.AsInteger=MasterQharga.AsInteger) and (MasterQdiskonpaket.AsInteger=10) then MasterQDiskon.AsInteger:=strtoint(leftstr(MasterQharga.AsString,length(MasterQharga.AsString)-1));
  MasterQJumlahTerima.AsInteger:=MasterQJumlahPembayaran.AsInteger-MasterQDiskon.AsInteger;
}end;

procedure TInvoiceFm.cxDBVerticalGrid1DiskonEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
//masterqdiskon.AsInteger:=DisplayValue;
//MasterQJumlahTerima.AsInteger:=MasterQJumlahPembayaran.AsInteger-MasterQDiskon.AsInteger;
end;

procedure TInvoiceFm.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+viewQKode.AsString+'%'));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
    end;
    SaveBtn.Enabled:=True;
    cxDBVerticalGrid1.Enabled:=true;
end;

procedure TInvoiceFm.cxGrid1DBTableView1DblClick(Sender: TObject);
begin
  masterq.Close;
  masterq.ParamByName('text').AsString:=ViewQKode.AsString;
  masterq.Open;
  masterq.Edit;
end;

procedure TInvoiceFm.MasterQAfterInsert(DataSet: TDataSet);
begin
  masterqcreateby.AsString:=user;
end;

procedure TInvoiceFm.MasterQBeforePost(DataSet: TDataSet);
begin
  Masterqoperator.asstring:=user;
end;

procedure TInvoiceFm.cxDateEdit1PropertiesChange(Sender: TObject);
begin
    viewq.close;
    viewq.parambyname('text1').AsDate:=cxDateEdit1.Date;
    viewq.ParamByName('text2').AsDate:=cxDateEdit2.Date+1;
    viewq.open;
end;

procedure TInvoiceFm.cxDateEdit2PropertiesChange(Sender: TObject);
begin
    viewq.close;
    viewq.parambyname('text1').AsDate:=cxDateEdit1.Date;
    viewq.ParamByName('text2').AsDate:=cxDateEdit2.Date+1;
    viewq.open;
end;

end.
