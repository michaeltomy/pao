object DaftarBarangFm: TDaftarBarangFm
  Left = 193
  Top = 151
  Width = 1092
  Height = 616
  Caption = 'Daftar Barang'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  PrintScale = poPrintToFit
  Visible = True
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 549
    Width = 1076
    Height = 29
    Align = alBottom
    TabOrder = 0
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 41
    Width = 1076
    Height = 508
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGridViewRepository1DBBandedTableView1
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1076
    Height = 41
    Align = alTop
    TabOrder = 2
    object cxTextEdit1: TcxTextEdit
      Left = 16
      Top = 8
      Properties.OnChange = cxTextEdit1PropertiesChange
      TabOrder = 0
      Width = 121
    end
    object cxComboBox1: TcxComboBox
      Left = 152
      Top = 8
      Properties.Items.Strings = (
        'Nama'
        'Kode')
      TabOrder = 1
      Text = 'Nama'
      Width = 121
    end
    object Button1: TButton
      Left = 312
      Top = 8
      Width = 75
      Height = 25
      Caption = 'UPDATE'
      TabOrder = 2
      OnClick = Button1Click
    end
  end
  object DataSource1: TDataSource
    DataSet = MasterQ
    Left = 296
    Top = 372
  end
  object cxGridViewRepository1: TcxGridViewRepository
    Left = 432
    Top = 384
    object cxGridViewRepository1DBBandedTableView1: TcxGridDBBandedTableView
      DragMode = dmAutomatic
      Navigator.Buttons.PriorPage.Visible = True
      Navigator.Buttons.Prior.Visible = True
      Navigator.Buttons.Insert.Visible = True
      Navigator.Buttons.Append.Visible = True
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = True
      Navigator.Buttons.Post.Visible = True
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.InfoPanel.Visible = True
      Navigator.Visible = True
      DataController.DataSource = DataSource1
      DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoSortByDisplayText, dcoFocusTopRowAfterSorting, dcoGroupsAlwaysExpanded, dcoImmediatePost, dcoInsertOnNewItemRowFocusing]
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      Filtering.ColumnFilteredItemsList = True
      OptionsBehavior.ImmediateEditor = False
      OptionsBehavior.IncSearch = True
      OptionsBehavior.ShowLockedStateImageOptions.BestFit = lsimImmediate
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Appending = True
      OptionsSelection.HideFocusRectOnExit = False
      OptionsView.CellEndEllipsis = True
      OptionsView.ColumnAutoWidth = True
      OptionsView.Indicator = True
      OptionsView.BandCaptionsInColumnAlternateCaption = True
      OptionsView.BandHeaderEndEllipsis = True
      Styles.Selection = cxStyle1
      Bands = <
        item
          Width = 90
        end>
      object cxGridViewRepository1DBBandedTableView1Column1: TcxGridDBBandedColumn
        DataBinding.FieldName = 'Kode'
        Width = 114
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1Column2: TcxGridDBBandedColumn
        DataBinding.FieldName = 'Nama'
        Width = 322
        Position.BandIndex = 0
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1Column3: TcxGridDBBandedColumn
        DataBinding.FieldName = 'HargaBeli'
        Width = 172
        Position.BandIndex = 0
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1Column4: TcxGridDBBandedColumn
        DataBinding.FieldName = 'HargaJual'
        Width = 165
        Position.BandIndex = 0
        Position.ColIndex = 3
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1Column5: TcxGridDBBandedColumn
        DataBinding.FieldName = 'Jumlah'
        Width = 117
        Position.BandIndex = 0
        Position.ColIndex = 4
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1Column6: TcxGridDBBandedColumn
        DataBinding.FieldName = 'Satuan'
        Width = 74
        Position.BandIndex = 0
        Position.ColIndex = 5
        Position.RowIndex = 0
      end
      object cxGridViewRepository1DBBandedTableView1Column7: TcxGridDBBandedColumn
        DataBinding.FieldName = 'MinimumStok'
        Width = 98
        Position.BandIndex = 0
        Position.ColIndex = 6
        Position.RowIndex = 0
      end
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 552
    Top = 400
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 640
    Top = 392
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor]
      Color = clHighlight
    end
  end
  object MasterQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from barang'
      '')
    UpdateObject = SDUpdateSQL1
    Left = 345
    Top = 313
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQNama: TStringField
      FieldName = 'Nama'
      Required = True
      Size = 50
    end
    object MasterQHargaBeli: TCurrencyField
      FieldName = 'HargaBeli'
    end
    object MasterQHargaJual: TCurrencyField
      FieldName = 'HargaJual'
    end
    object MasterQJumlah: TFloatField
      FieldName = 'Jumlah'
    end
    object MasterQSatuan: TStringField
      FieldName = 'Satuan'
      Size = 50
    end
    object MasterQMinimumStok: TIntegerField
      FieldName = 'MinimumStok'
    end
  end
  object SDUpdateSQL1: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, Nama, HargaBeli, HargaJual, Jumlah, Satuan, Minimum' +
        'Stok'#13#10'from barang'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update barang'
      'set'
      '  Kode = :Kode,'
      '  Nama = :Nama,'
      '  HargaBeli = :HargaBeli,'
      '  HargaJual = :HargaJual,'
      '  Jumlah = :Jumlah,'
      '  Satuan = :Satuan,'
      '  MinimumStok = :MinimumStok'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into barang'
      
        '  (Kode, Nama, HargaBeli, HargaJual, Jumlah, Satuan, MinimumStok' +
        ')'
      'values'
      
        '  (:Kode, :Nama, :HargaBeli, :HargaJual, :Jumlah, :Satuan, :Mini' +
        'mumStok)')
    DeleteSQL.Strings = (
      'delete from barang'
      'where'
      '  Kode = :OLD_Kode')
    Left = 536
    Top = 296
  end
end
