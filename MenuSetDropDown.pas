unit MenuSetDropDown;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, SDEngine, Grids, DBGrids,
  cxGridBandedTableView, cxGridDBBandedTableView, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxGridCustomPopupMenu,
  cxGridPopupMenu;

type
  TMenuSetDropDownFm = class(TForm)
    pnl1: TPanel;
    DataSource1: TDataSource;
    MenuSetQ: TSDQuery;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    MenuSetQKodeMenuSet: TStringField;
    MenuSetQKodeMenu: TStringField;
    MenuSetQJumlah: TStringField;
    MenuSetQMenuSet: TStringField;
    MenuSetQDetailMenuSet: TStringField;
    cxGrid1DBTableView1Jumlah: TcxGridDBColumn;
    cxGrid1DBTableView1MenuSet: TcxGridDBColumn;
    cxGrid1DBTableView1DetailMenuSet: TcxGridDBColumn;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure dbgrd1DblClick(Sender: TObject);
    procedure cxGridViewRepository1DBBandedTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxGridViewRepository1DBBandedTableView1KeyDown(
      Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    kode:string;
  end;

var
  MenuSetDropDownFm: TMenuSetDropDownFm;

implementation

{$R *.dfm}

{ TDropDownFm }

procedure TMenuSetDropDownFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TMenuSetDropDownFm.dbgrd1DblClick(Sender: TObject);
begin
  kode:=MenuSetQKodeMenuSet.AsString;
  ModalResult:=mrOk;
end;

procedure TMenuSetDropDownFm.cxGridViewRepository1DBBandedTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  kode:=MenuSetQKodeMenuSet.AsString;
  ModalResult:=mrOk;
end;

procedure TMenuSetDropDownFm.cxGridViewRepository1DBBandedTableView1KeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
if Key=13 then
begin
  kode:=MenuSetQKodeMenuSet.AsString;
  ModalResult:=mrOk;
end;
end;

procedure TMenuSetDropDownFm.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  kode:=MenuSetQKodeMenuSet.AsString;
  ModalResult:=mrOk;
end;

procedure TMenuSetDropDownFm.FormShow(Sender: TObject);
begin
  MenuSetQ.open;
end;

end.
