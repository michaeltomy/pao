unit PembayaranDropDown;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, SDEngine, Grids, DBGrids,
  cxGridBandedTableView, cxGridDBBandedTableView, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxGridCustomPopupMenu,
  cxGridPopupMenu, cxContainer, cxLabel, cxTextEdit, cxMaskEdit,
  cxDropDownEdit, cxCalendar, Menus, StdCtrls, cxButtons;

type
  TPembayaranDropDownFm = class(TForm)
    pnl1: TPanel;
    DataSource1: TDataSource;
    KategoriBarangQ: TSDQuery;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    PegawaiQ: TSDQuery;
    PegawaiQKode: TStringField;
    PegawaiQNama: TStringField;
    PegawaiQAlamat: TStringField;
    PegawaiQKota: TStringField;
    PegawaiQTelp: TStringField;
    PegawaiQTglLahir: TDateTimeField;
    PegawaiQJabatan: TStringField;
    PegawaiQAktif: TBooleanField;
    SupplierQ: TSDQuery;
    SupplierQKode: TStringField;
    SupplierQNama: TStringField;
    SupplierQAlamat: TStringField;
    SupplierQKota: TStringField;
    SupplierQTelp: TStringField;
    SupplierQPIC: TStringField;
    SupplierQTelpPIC: TStringField;
    KategoriBarangQKode: TStringField;
    KategoriBarangQTanggal: TDateTimeField;
    KategoriBarangQNoUrut: TIntegerField;
    KategoriBarangQSupplier: TStringField;
    KategoriBarangQHargaTotal: TCurrencyField;
    KategoriBarangQPenerima: TStringField;
    KategoriBarangQCreateDate: TDateTimeField;
    KategoriBarangQCreateBy: TStringField;
    KategoriBarangQOperator: TStringField;
    KategoriBarangQTglEntry: TDateTimeField;
    KategoriBarangQStatus: TStringField;
    KategoriBarangQNamaSupplier: TStringField;
    KategoriBarangQNamaPenerima: TStringField;
    cxGrid1DBTableView1Kode: TcxGridDBColumn;
    cxGrid1DBTableView1Tanggal: TcxGridDBColumn;
    cxGrid1DBTableView1NoUrut: TcxGridDBColumn;
    cxGrid1DBTableView1HargaTotal: TcxGridDBColumn;
    cxGrid1DBTableView1Status: TcxGridDBColumn;
    cxGrid1DBTableView1NamaSupplier: TcxGridDBColumn;
    cxGrid1DBTableView1NamaPenerima: TcxGridDBColumn;
    cxButton1: TcxButton;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure dbgrd1DblClick(Sender: TObject);
    procedure cxGridViewRepository1DBBandedTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxGridViewRepository1DBBandedTableView1KeyDown(
      Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    kode:string;
  end;

var
  PembayaranDropDownFm: TPembayaranDropDownFm;

implementation

{$R *.dfm}

{ TDropDownFm }

procedure TPembayaranDropDownFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TPembayaranDropDownFm.dbgrd1DblClick(Sender: TObject);
begin
  kode:=KategoriBarangQ.Fields[0].AsString;
  ModalResult:=mrOk;
end;

procedure TPembayaranDropDownFm.cxGridViewRepository1DBBandedTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  kode:=KategoriBarangQ.Fields[0].AsString;
  ModalResult:=mrOk;
end;

procedure TPembayaranDropDownFm.cxGridViewRepository1DBBandedTableView1KeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
if Key=13 then
begin
  kode:=KategoriBarangQ.Fields[0].AsString;
  ModalResult:=mrOk;
end;
end;

procedure TPembayaranDropDownFm.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  kode:=KategoriBarangQ.Fields[0].AsString;
  ModalResult:=mrOk;
end;

procedure TPembayaranDropDownFm.FormShow(Sender: TObject);
begin
  PegawaiQ.Open;
  
  KategoriBarangQ.Open;
end;

end.
