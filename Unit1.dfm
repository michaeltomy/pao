object Form1: TForm1
  Left = 194
  Top = 124
  Width = 870
  Height = 500
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object frxReport1: TfrxReport
    Version = '4.1.38'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41200.976376921300000000
    ReportOptions.LastChange = 41200.976376921300000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 160
    Top = 112
    Datasets = <
      item
        DataSet = frxDBDataset1
        DataSetName = 'frxDBDataset1'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      object MasterData1: TfrxMasterData
        Height = 18.897650000000000000
        Top = 139.842610000000000000
        Width = 793.701300000000000000
        DataSet = frxDBDataset1
        DataSetName = 'frxDBDataset1'
        RowCount = 0
        object Memo2: TfrxMemoView
          Left = 30.236240000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          DataField = 'Panjang'
          DataSet = frxDBDataset1
          DataSetName = 'frxDBDataset1'
          DisplayFormat.DecimalSeparator = '.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDBDataset1."Panjang"]')
          ParentFont = False
        end
      end
      object ReportTitle1: TfrxReportTitle
        Height = 60.472480000000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        object Memo1: TfrxMemoView
          Left = 351.496290000000000000
          Top = 15.118120000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = '.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            'ARMADA')
          ParentFont = False
        end
      end
    end
  end
  object frxDBDataset1: TfrxDBDataset
    UserName = 'frxDBDataset1'
    CloseDataSource = False
    DataSet = SDQuery1
    Left = 296
    Top = 120
  end
  object SDQuery1: TSDQuery
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from armada')
    Left = 424
    Top = 240
    object SDQuery1Kode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object SDQuery1PlatNo: TStringField
      FieldName = 'PlatNo'
      Required = True
      Size = 10
    end
    object SDQuery1Panjang: TFloatField
      FieldName = 'Panjang'
      Required = True
    end
    object SDQuery1Kapasitas: TFloatField
      FieldName = 'Kapasitas'
      Required = True
    end
    object SDQuery1Aktif: TBooleanField
      FieldName = 'Aktif'
      Required = True
    end
    object SDQuery1Keterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object SDQuery1CreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object SDQuery1CreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object SDQuery1Operator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object SDQuery1TglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object SDQuery1Sopir: TStringField
      FieldName = 'Sopir'
      Required = True
      Size = 10
    end
  end
  object DataSource1: TDataSource
    DataSet = SDQuery1
    Left = 456
    Top = 200
  end
end
