unit PengeluaranBahanDropDown;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, SDEngine, Grids, DBGrids,
  cxGridBandedTableView, cxGridDBBandedTableView, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxGridCustomPopupMenu,
  cxGridPopupMenu, cxContainer, cxLabel, cxTextEdit, cxMaskEdit,
  cxDropDownEdit, cxCalendar;

type
  TPengeluaranBahanDropDownFm = class(TForm)
    pnl1: TPanel;
    DataSource1: TDataSource;
    KategoriBarangQ: TSDQuery;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    PegawaiQ: TSDQuery;
    PegawaiQKode: TStringField;
    PegawaiQNama: TStringField;
    PegawaiQAlamat: TStringField;
    PegawaiQKota: TStringField;
    PegawaiQTelp: TStringField;
    PegawaiQTglLahir: TDateTimeField;
    PegawaiQJabatan: TStringField;
    PegawaiQAktif: TBooleanField;
    Panel1: TPanel;
    cxDateEdit2: TcxDateEdit;
    cxDateEdit1: TcxDateEdit;
    cxLabel1: TcxLabel;
    KategoriBarangQKode: TStringField;
    KategoriBarangQTanggal: TDateTimeField;
    KategoriBarangQPIC: TStringField;
    KategoriBarangQKeperluan: TStringField;
    KategoriBarangQCreateDate: TDateTimeField;
    KategoriBarangQCreateBy: TStringField;
    KategoriBarangQOperator: TStringField;
    KategoriBarangQTglEntry: TDateTimeField;
    cxGrid1DBTableView1Kode: TcxGridDBColumn;
    cxGrid1DBTableView1Tanggal: TcxGridDBColumn;
    cxGrid1DBTableView1PIC: TcxGridDBColumn;
    cxGrid1DBTableView1Keperluan: TcxGridDBColumn;
    cxGrid1DBTableView1CreateDate: TcxGridDBColumn;
    cxGrid1DBTableView1CreateBy: TcxGridDBColumn;
    cxGrid1DBTableView1Operator: TcxGridDBColumn;
    cxGrid1DBTableView1TglEntry: TcxGridDBColumn;
    KategoriBarangQNamaPIC: TStringField;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure dbgrd1DblClick(Sender: TObject);
    procedure cxGridViewRepository1DBBandedTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxGridViewRepository1DBBandedTableView1KeyDown(
      Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure cxDateEdit1PropertiesChange(Sender: TObject);
    procedure cxDateEdit2PropertiesChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    kode:string;
  end;

var
  PengeluaranBahanDropDownFm: TPengeluaranBahanDropDownFm;

implementation

{$R *.dfm}

{ TDropDownFm }

procedure TPengeluaranBahanDropDownFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TPengeluaranBahanDropDownFm.dbgrd1DblClick(Sender: TObject);
begin
  kode:=KategoriBarangQ.Fields[0].AsString;
  ModalResult:=mrOk;
end;

procedure TPengeluaranBahanDropDownFm.cxGridViewRepository1DBBandedTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  kode:=KategoriBarangQ.Fields[0].AsString;
  ModalResult:=mrOk;
end;

procedure TPengeluaranBahanDropDownFm.cxGridViewRepository1DBBandedTableView1KeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
if Key=13 then
begin
  kode:=KategoriBarangQ.Fields[0].AsString;
  ModalResult:=mrOk;
end;
end;

procedure TPengeluaranBahanDropDownFm.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  kode:=KategoriBarangQ.Fields[0].AsString;
  ModalResult:=mrOk;
end;

procedure TPengeluaranBahanDropDownFm.FormShow(Sender: TObject);
begin
  PegawaiQ.Open;
  cxDateEdit1.Date:=now-1;
  cxdateedit2.Date:=now;
  kategoribarangq.Close;
  Kategoribarangq.ParamByName('text1').AsDatetime:=cxDateEdit1.Date;
  kategoribarangq.ParamByName('text2').AsDatetime:=cxdateedit2.Date+1;
  KategoriBarangQ.Open;
end;

procedure TPengeluaranBahanDropDownFm.cxDateEdit1PropertiesChange(
  Sender: TObject);
begin
  kategoribarangq.Close;
  Kategoribarangq.ParamByName('text1').AsDatetime:=cxDateEdit1.Date;
  kategoribarangq.ParamByName('text2').AsDatetime:=cxdateedit2.Date+1;
  KategoriBarangQ.Open;
end;

procedure TPengeluaranBahanDropDownFm.cxDateEdit2PropertiesChange(
  Sender: TObject);
begin
  kategoribarangq.Close;
  Kategoribarangq.ParamByName('text1').AsDatetime:=cxDateEdit1.Date;
  kategoribarangq.ParamByName('text2').AsDatetime:=cxdateedit2.Date+1;
  KategoriBarangQ.Open;
end;

end.
