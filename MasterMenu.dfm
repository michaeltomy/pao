object MasterMenuFm: TMasterMenuFm
  Left = 341
  Top = 151
  AutoScroll = False
  BorderIcons = [biSystemMenu]
  Caption = 'Master Menu'
  ClientHeight = 413
  ClientWidth = 609
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 609
    Height = 48
    Align = alTop
    TabOrder = 0
    object lbl1: TLabel
      Left = 8
      Top = 13
      Width = 25
      Height = 13
      Caption = 'Kode'
    end
    object KodeEdit: TcxButtonEdit
      Left = 40
      Top = 10
      TabStop = False
      Properties.Buttons = <
        item
          Caption = '+'
          Default = True
          Kind = bkText
        end>
      Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      TabOrder = 0
      OnEnter = KodeEditEnter
      OnExit = KodeEditExit
      OnKeyDown = KodeEditKeyDown
      Width = 121
    end
    object SearchBtn: TcxButton
      Left = 168
      Top = 10
      Width = 57
      Height = 21
      Caption = 'Search'
      TabOrder = 1
      TabStop = False
      OnClick = SearchBtnClick
    end
  end
  object pnl2: TPanel
    Left = 0
    Top = 343
    Width = 609
    Height = 51
    Align = alBottom
    TabOrder = 1
    object ExitBtn: TcxButton
      Left = 172
      Top = 10
      Width = 75
      Height = 25
      Caption = 'EXIT'
      TabOrder = 2
      Visible = False
      OnClick = ExitBtnClick
    end
    object DeleteBtn: TcxButton
      Left = 91
      Top = 10
      Width = 75
      Height = 25
      Caption = 'DELETE'
      TabOrder = 0
      TabStop = False
      OnClick = DeleteBtnClick
    end
    object SaveBtn: TcxButton
      Left = 8
      Top = 10
      Width = 75
      Height = 25
      Caption = 'SAVE'
      TabOrder = 1
      OnClick = SaveBtnClick
    end
    object cxLabel1: TcxLabel
      Left = 336
      Top = 2
      Caption = '* required fields'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clRed
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = []
      Style.IsFontAssigned = True
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 394
    Width = 609
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 345
    Height = 295
    Align = alLeft
    TabOrder = 3
    object MasterVGrid: TcxDBVerticalGrid
      Left = 1
      Top = 1
      Width = 343
      Height = 160
      Align = alTop
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      OptionsView.RowHeaderWidth = 146
      OptionsBehavior.GoToNextCellOnTab = True
      OptionsData.CancelOnExit = False
      OptionsData.Appending = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Inserting = False
      ParentFont = False
      TabOrder = 0
      OnEnter = MasterVGridEnter
      OnExit = MasterVGridExit
      DataController.DataSource = MasterDs
      Version = 1
      object MasterVGridNamaMenu: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'NamaMenu'
        ID = 0
        ParentID = -1
        Index = 0
        Version = 1
      end
      object MasterVGridInisial: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'Inisial'
        ID = 10
        ParentID = -1
        Index = 1
        Version = 1
      end
      object MasterVGridHarga: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'Harga'
        ID = 1
        ParentID = -1
        Index = 2
        Version = 1
      end
      object MasterVGridNamaKategori: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'NamaKategori'
        ID = 2
        ParentID = -1
        Index = 3
        Version = 1
      end
      object MasterVGridKeterangan: TcxDBEditorRow
        Height = 56
        Properties.DataBinding.FieldName = 'Keterangan'
        ID = 3
        ParentID = -1
        Index = 4
        Version = 1
      end
    end
    object cxGrid2: TcxGrid
      Left = 1
      Top = 161
      Width = 343
      Height = 133
      Align = alClient
      TabOrder = 1
      OnExit = cxGrid1Exit
      object cxGridDBTableView1: TcxGridDBTableView
        Navigator.Buttons.OnButtonClick = cxGrid1DBTableView1NavigatorButtonsButtonClick
        Navigator.Buttons.First.Visible = False
        Navigator.Buttons.PriorPage.Visible = False
        Navigator.Buttons.Prior.Visible = False
        Navigator.Buttons.Next.Visible = False
        Navigator.Buttons.NextPage.Visible = False
        Navigator.Buttons.Last.Visible = False
        Navigator.Buttons.Insert.Visible = False
        Navigator.Buttons.Append.Visible = False
        Navigator.Buttons.Edit.Visible = True
        Navigator.Buttons.Post.Visible = True
        Navigator.Buttons.Cancel.Visible = True
        Navigator.Buttons.SaveBookmark.Visible = False
        Navigator.Buttons.GotoBookmark.Visible = False
        Navigator.Buttons.Filter.Visible = False
        Navigator.Visible = True
        DataController.DataModeController.SmartRefresh = True
        DataController.DataSource = DataSource1
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        NewItemRow.Visible = True
        OptionsBehavior.AlwaysShowEditor = True
        OptionsBehavior.FocusCellOnTab = True
        OptionsBehavior.FocusFirstCellOnNewRecord = True
        OptionsBehavior.GoToNextCellOnEnter = True
        OptionsData.Appending = True
        OptionsData.DeletingConfirmation = False
        OptionsSelection.MultiSelect = True
        OptionsView.GroupByBox = False
        object cxGridDBTableView1Pertanyaan: TcxGridDBColumn
          DataBinding.FieldName = 'Pertanyaan'
          Width = 333
        end
      end
      object cxGridLevel1: TcxGridLevel
        GridView = cxGridDBTableView1
        Options.DetailTabsPosition = dtpLeft
      end
    end
  end
  object Panel2: TPanel
    Left = 345
    Top = 48
    Width = 264
    Height = 295
    Align = alClient
    TabOrder = 4
    object cxGrid1: TcxGrid
      Left = 1
      Top = 1
      Width = 262
      Height = 293
      Align = alClient
      TabOrder = 0
      OnExit = cxGrid1Exit
      object cxGrid1DBTableView1: TcxGridDBTableView
        Navigator.Buttons.OnButtonClick = cxGrid1DBTableView1NavigatorButtonsButtonClick
        Navigator.Buttons.First.Visible = False
        Navigator.Buttons.PriorPage.Visible = False
        Navigator.Buttons.Prior.Visible = False
        Navigator.Buttons.Next.Visible = False
        Navigator.Buttons.NextPage.Visible = False
        Navigator.Buttons.Last.Visible = False
        Navigator.Buttons.Insert.Visible = False
        Navigator.Buttons.Append.Visible = False
        Navigator.Buttons.Edit.Visible = True
        Navigator.Buttons.Post.Visible = True
        Navigator.Buttons.Cancel.Visible = True
        Navigator.Buttons.SaveBookmark.Visible = False
        Navigator.Buttons.GotoBookmark.Visible = False
        Navigator.Buttons.Filter.Visible = False
        Navigator.Visible = True
        DataController.DataModeController.SmartRefresh = True
        DataController.DataSource = DataSource2
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        NewItemRow.Visible = True
        OptionsBehavior.AlwaysShowEditor = True
        OptionsBehavior.FocusCellOnTab = True
        OptionsBehavior.FocusFirstCellOnNewRecord = True
        OptionsBehavior.GoToNextCellOnEnter = True
        OptionsData.Appending = True
        OptionsData.DeletingConfirmation = False
        OptionsSelection.MultiSelect = True
        OptionsView.GroupByBox = False
        object cxGrid1DBTableView1Bahan: TcxGridDBColumn
          DataBinding.FieldName = 'NamaBahan'
          PropertiesClassName = 'TcxButtonEditProperties'
          Properties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.OnButtonClick = cxGrid1DBTableView1BahanPropertiesButtonClick
          Width = 118
        end
        object cxGrid1DBTableView1Quantity: TcxGridDBColumn
          DataBinding.FieldName = 'Quantity'
        end
        object cxGrid1DBTableView1Satuan: TcxGridDBColumn
          DataBinding.FieldName = 'Satuan'
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
        Options.DetailTabsPosition = dtpLeft
      end
    end
  end
  object MasterQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select * from menu')
    UpdateObject = MasterUS
    Left = 161
    Top = 113
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQNamaMenu: TStringField
      FieldName = 'NamaMenu'
      Size = 200
    end
    object MasterQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object MasterQHarga: TCurrencyField
      FieldName = 'Harga'
    end
    object MasterQKategori: TStringField
      FieldName = 'Kategori'
      Size = 10
    end
    object MasterQNamaKategori: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaKategori'
      LookupDataSet = KategoriQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Kategori'
      Size = 200
      Lookup = True
    end
    object MasterQInisial: TStringField
      FieldName = 'Inisial'
      Size = 100
    end
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 404
    Top = 6
  end
  object MasterUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, NamaMenu, Keterangan, Harga, Kategori, Inisial'#13#10'fro' +
        'm menu'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update menu'
      'set'
      '  Kode = :Kode,'
      '  NamaMenu = :NamaMenu,'
      '  Keterangan = :Keterangan,'
      '  Harga = :Harga,'
      '  Kategori = :Kategori,'
      '  Inisial = :Inisial'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into menu'
      '  (Kode, NamaMenu, Keterangan, Harga, Kategori, Inisial)'
      'values'
      '  (:Kode, :NamaMenu, :Keterangan, :Harga, :Kategori, :Inisial)')
    DeleteSQL.Strings = (
      'delete from menu'
      'where'
      '  Kode = :OLD_Kode')
    Left = 444
    Top = 10
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from menu order by kode desc')
    Left = 129
    Top = 119
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object DetailQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterDelete = DetailQAfterDelete
    SQL.Strings = (
      'select * from detailmenu where menu= :text')
    UpdateObject = SDUpdateSQL1
    Left = 201
    Top = 113
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object DetailQMenu: TStringField
      FieldName = 'Menu'
      Size = 10
    end
    object DetailQPertanyaan: TStringField
      FieldName = 'Pertanyaan'
      Required = True
      Size = 250
    end
  end
  object DataSource1: TDataSource
    DataSet = DetailQ
    Left = 484
    Top = 6
  end
  object Kode2Q: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select top 1 kode from detailmenu order by kode desc')
    Left = 321
    Top = 9
    object Kode2Qkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object SDUpdateSQL1: TSDUpdateSQL
    RefreshSQL.Strings = (
      'select Menu, Pertanyaan'#13#10'from detailmenu'
      'where'
      '  Menu = :OLD_Menu and'
      '  Pertanyaan = :OLD_Pertanyaan')
    ModifySQL.Strings = (
      'update detailmenu'
      'set'
      '  Menu = :Menu,'
      '  Pertanyaan = :Pertanyaan'
      'where'
      '  Menu = :OLD_Menu and'
      '  Pertanyaan = :OLD_Pertanyaan')
    InsertSQL.Strings = (
      'insert into detailmenu'
      '  (Menu, Pertanyaan)'
      'values'
      '  (:Menu, :Pertanyaan)')
    DeleteSQL.Strings = (
      'delete from detailmenu'
      'where'
      '  Menu = :OLD_Menu and'
      '  Pertanyaan = :OLD_Pertanyaan')
    Left = 524
    Top = 10
  end
  object BahanQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Active = True
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select * from bahan')
    Left = 241
    Top = 73
    object BahanQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object BahanQNamaBahan: TStringField
      FieldName = 'NamaBahan'
      Size = 200
    end
    object BahanQJumlah: TFloatField
      FieldName = 'Jumlah'
    end
    object BahanQSatuan: TStringField
      FieldName = 'Satuan'
      Size = 50
    end
    object BahanQLokasi: TStringField
      FieldName = 'Lokasi'
      Size = 50
    end
    object BahanQKategori: TStringField
      FieldName = 'Kategori'
      Size = 10
    end
  end
  object PenggunaanQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterDelete = DetailQAfterDelete
    SQL.Strings = (
      'select * from penggunaanbahan where menu= :text')
    UpdateObject = SDUpdateSQL2
    Left = 233
    Top = 113
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object PenggunaanQMenu: TStringField
      FieldName = 'Menu'
      Required = True
      Size = 10
    end
    object PenggunaanQBahan: TStringField
      FieldName = 'Bahan'
      Required = True
      Size = 10
    end
    object PenggunaanQQuantity: TFloatField
      FieldName = 'Quantity'
    end
    object PenggunaanQNamaBahan: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaBahan'
      LookupDataSet = BahanQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NamaBahan'
      KeyFields = 'Bahan'
      Size = 200
      Lookup = True
    end
    object PenggunaanQSatuan: TStringField
      FieldKind = fkLookup
      FieldName = 'Satuan'
      LookupDataSet = BahanQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Satuan'
      KeyFields = 'Bahan'
      Size = 10
      Lookup = True
    end
  end
  object Kode3Q: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select top 1 kode from penggunaanbahan order by kode desc')
    Left = 313
    Top = 57
    object StringField3: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object DataSource2: TDataSource
    DataSet = PenggunaanQ
    Left = 636
    Top = 14
  end
  object SDUpdateSQL2: TSDUpdateSQL
    RefreshSQL.Strings = (
      'select Menu, Bahan, Quantity'#13#10'from penggunaanbahan'
      'where'
      '  Menu = :OLD_Menu and'
      '  Bahan = :OLD_Bahan')
    ModifySQL.Strings = (
      'update penggunaanbahan'
      'set'
      '  Menu = :Menu,'
      '  Bahan = :Bahan,'
      '  Quantity = :Quantity'
      'where'
      '  Menu = :OLD_Menu and'
      '  Bahan = :OLD_Bahan')
    InsertSQL.Strings = (
      'insert into penggunaanbahan'
      '  (Menu, Bahan, Quantity)'
      'values'
      '  (:Menu, :Bahan, :Quantity)')
    DeleteSQL.Strings = (
      'delete from penggunaanbahan'
      'where'
      '  Menu = :OLD_Menu and'
      '  Bahan = :OLD_Bahan')
    Left = 660
    Top = 10
  end
  object DeleteDetailStandardQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from detailmenu where menu=:text')
    UpdateObject = UpdateDeleteDetailStandardUS
    Left = 264
    Top = 312
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object DeleteDetailStandardQPrep: TStringField
      FieldName = 'Prep'
      Required = True
      Size = 10
    end
    object DeleteDetailStandardQBahan: TStringField
      FieldName = 'Bahan'
      Required = True
      Size = 10
    end
    object DeleteDetailStandardQQuantity: TFloatField
      FieldName = 'Quantity'
    end
  end
  object UpdateDeleteDetailStandardUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      'select Prep, Bahan, Quantity'#13#10'from detailprep'
      'where'
      '  Prep = :OLD_Prep and'
      '  Bahan = :OLD_Bahan')
    ModifySQL.Strings = (
      'update detailprep'
      'set'
      '  Prep = :Prep,'
      '  Bahan = :Bahan,'
      '  Quantity = :Quantity'
      'where'
      '  Prep = :OLD_Prep and'
      '  Bahan = :OLD_Bahan')
    InsertSQL.Strings = (
      'insert into detailprep'
      '  (Prep, Bahan, Quantity)'
      'values'
      '  (:Prep, :Bahan, :Quantity)')
    DeleteSQL.Strings = (
      'delete from detailprep'
      'where'
      '  Prep = :OLD_Prep and'
      '  Bahan = :OLD_Bahan')
    Left = 384
    Top = 312
  end
  object ExecuteDeleteDetailStandardQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from detailmenu where menu=:text')
    Left = 312
    Top = 312
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object ExecuteDeleteDetailStandardQPrep: TStringField
      FieldName = 'Prep'
      Required = True
      Size = 10
    end
    object ExecuteDeleteDetailStandardQBahan: TStringField
      FieldName = 'Bahan'
      Required = True
      Size = 10
    end
    object ExecuteDeleteDetailStandardQQuantity: TFloatField
      FieldName = 'Quantity'
    end
  end
  object KategoriQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from kategori')
    Left = 193
    Top = 79
    object KategoriQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object KategoriQNama: TStringField
      FieldName = 'Nama'
      Size = 200
    end
  end
end
