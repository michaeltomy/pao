unit PelangganDropDown;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, SDEngine, Grids, DBGrids,
  cxGridBandedTableView, cxGridDBBandedTableView, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxGridCustomPopupMenu,
  cxGridPopupMenu, cxContainer, cxTextEdit, StdCtrls, Menus, cxButtons;

type
  TPelangganDropDownFm = class(TForm)
    pnl1: TPanel;
    DataSource1: TDataSource;
    PelangganQ: TSDQuery;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    Panel1: TPanel;
    cxSearchText: TcxTextEdit;
    Label1: TLabel;
    cxButTambah: TcxButton;
    PelangganQKode: TStringField;
    PelangganQNama: TStringField;
    PelangganQAlamat: TStringField;
    PelangganQKota: TStringField;
    PelangganQTelp: TStringField;
    PelangganQHP: TStringField;
    PelangganQTglLahir: TDateTimeField;
    PelangganQFoto: TBlobField;
    PelangganQAktif: TBooleanField;
    PelangganQCreateDate: TDateTimeField;
    PelangganQCreateBy: TStringField;
    PelangganQTglEntry: TDateTimeField;
    PelangganQOperator: TStringField;
    PelangganQDeleted: TBooleanField;
    cxGrid1DBTableView1Nama: TcxGridDBColumn;
    cxGrid1DBTableView1Alamat: TcxGridDBColumn;
    cxGrid1DBTableView1Kota: TcxGridDBColumn;
    cxGrid1DBTableView1Telp: TcxGridDBColumn;
    cxGrid1DBTableView1HP: TcxGridDBColumn;
    cxGrid1DBTableView1TglLahir: TcxGridDBColumn;
    cxGrid1DBTableView1Foto: TcxGridDBColumn;
    cxGrid1DBTableView1Aktif: TcxGridDBColumn;
    cxGrid1DBTableView1Kode: TcxGridDBColumn;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure dbgrd1DblClick(Sender: TObject);
    procedure cxGridViewRepository1DBBandedTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxGridViewRepository1DBBandedTableView1KeyDown(
      Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxSearchTextPropertiesChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cxSearchTextEnter(Sender: TObject);
    procedure cxButTambahClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    kode:string;
    OriSQL:string;
  end;

var
  PelangganDropDownFm: TPelangganDropDownFm;

implementation

uses SysConst, MasterPelanggan;

{$R *.dfm}

{ TDropDownFm }

procedure TPelangganDropDownFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TPelangganDropDownFm.dbgrd1DblClick(Sender: TObject);
begin
  kode:=PelangganQ.Fields[0].AsString;
  ModalResult:=mrOk;
end;

procedure TPelangganDropDownFm.cxGridViewRepository1DBBandedTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  kode:=PelangganQ.Fields[0].AsString;
  ModalResult:=mrOk;
end;

procedure TPelangganDropDownFm.cxGridViewRepository1DBBandedTableView1KeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
if Key=13 then
begin
  kode:=PelangganQ.Fields[0].AsString;
  ModalResult:=mrOk;
end;
end;

procedure TPelangganDropDownFm.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  kode:=PelangganQ.Fields[0].AsString;
  ModalResult:=mrOk;
end;

procedure TPelangganDropDownFm.cxSearchTextPropertiesChange(
  Sender: TObject);
begin
  PelangganQ.Close;
  PelangganQ.SQL.Text:='select * from pelanggan where nama like '+QuotedStr('%'+cxSearchText.Text+'%');
  PelangganQ.ExecSQL;
  PelangganQ.Open;
  if PelangganQ.RecordCount=0 then
    cxButTambah.Enabled:=True
  else
    cxButTambah.Enabled:=False;
end;

procedure TPelangganDropDownFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  PelangganQ.SQL.Text:=OriSQL;
end;

procedure TPelangganDropDownFm.cxSearchTextEnter(Sender: TObject);
begin
  cxSearchText.Text:='';
end;

procedure TPelangganDropDownFm.cxButTambahClick(Sender: TObject);
begin
  MasterPelangganFm := TMasterPelangganFm.create(self);
end;

procedure TPelangganDropDownFm.FormShow(Sender: TObject);
begin
  OriSQL:=PelangganQ.SQL.Text;
  cxButTambah.Enabled:=False;
  cxSearchText.SetFocus;
  PelangganQ.Open;
end;

end.
