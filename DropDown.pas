unit DropDown;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, SDEngine, Grids, DBGrids,
  cxGridBandedTableView, cxGridDBBandedTableView, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxGridCustomPopupMenu,
  cxGridPopupMenu, cxContainer, cxMaskEdit, cxDropDownEdit, cxTextEdit;

type
  TDropDownFm = class(TForm)
    pnl1: TPanel;
    DataSource1: TDataSource;
    cxGridViewRepository1: TcxGridViewRepository;
    cxGridViewRepository1DBBandedTableView1: TcxGridDBBandedTableView;
    cxGridPopupMenu1: TcxGridPopupMenu;
    cxGrid1: TcxGrid;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxGrid1Level1: TcxGridLevel;
    Panel1: TPanel;
    cxComboBox1: TcxComboBox;
    cxTextEdit1: TcxTextEdit;
    cxStyle2: TcxStyle;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormCreate(Sender: TObject);
    procedure dbgrd1DblClick(Sender: TObject);
    procedure cxGridViewRepository1DBBandedTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxGridViewRepository1DBBandedTableView1DblClick(
      Sender: TObject);
    procedure cxGridViewRepository1DBBandedTableView1EditKeyDown(
      Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
      AEdit: TcxCustomEdit; var Key: Word; Shift: TShiftState);
    procedure cxGridViewRepository1DBBandedTableView1KeyDown(
      Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure cxTextEdit1PropertiesChange(Sender: TObject);
    procedure cxComboBox1PropertiesChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    q :TSDQuery;
  public
    { Public declarations }
    kode:string;
    constructor Create(aOwner: TComponent;Query1:TSDQuery);overload;
  end;

var
  DropDownFm: TDropDownFm;
  MasterOriSQL: String;

implementation

{$R *.dfm}

{ TDropDownFm }

constructor TDropDownFm.Create(aOwner: TComponent; Query1: TSDQuery);
begin
  inherited Create(aOwner);
  //query1.SQL.Text:='select * from barang';
  q:=Query1;
  MasterOriSQL:=q.SQL.Text;
end;

procedure TDropDownFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TDropDownFm.FormCreate(Sender: TObject);
var i : integer;
begin
  DataSource1.DataSet:=q;
  cxGridViewRepository1DBBandedTableView1.DataController.DataSource:=DataSource1;
  q.Open;
  for i:=0 to DataSource1.DataSet.FieldCount-1 do
  begin
    if DataSource1.DataSet.Fields[i].Visible=true then
    cxGridViewRepository1DBBandedTableView1.CreateColumn.DataBinding.FieldName:=DataSource1.DataSet.Fields[i].FieldName;
    cxGridViewRepository1DBBandedTableView1.GetColumnByFieldName(DataSource1.DataSet.Fields[i].FieldName).Options.Editing:=true;
    if DataSource1.DataSet.Fields[i].FieldName='Nama' then cxGridViewRepository1DBBandedTableView1.GetColumnByFieldName(DataSource1.DataSet.Fields[i].FieldName).Width:=40
    else if DataSource1.DataSet.Fields[i].FieldName='Stok' then  cxGridViewRepository1DBBandedTableView1.GetColumnByFieldName(DataSource1.DataSet.Fields[i].FieldName).Width:=10
    else
    cxGridViewRepository1DBBandedTableView1.GetColumnByFieldName(DataSource1.DataSet.Fields[i].FieldName).Width:=20;
  end;
  cxGrid1Level1.GridView:=cxGridViewRepository1DBBandedTableView1;
end;

procedure TDropDownFm.dbgrd1DblClick(Sender: TObject);
begin
  kode:=q.Fields[0].AsString;
  ModalResult:=mrOk;
end;

procedure TDropDownFm.cxGridViewRepository1DBBandedTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  kode:=q.Fields[0].AsString;
  ModalResult:=mrOk;
end;

procedure TDropDownFm.cxGridViewRepository1DBBandedTableView1DblClick(
  Sender: TObject);
begin
  kode:=q.Fields[0].AsString;
  ModalResult:=mrOk;
end;

procedure TDropDownFm.cxGridViewRepository1DBBandedTableView1EditKeyDown(
  Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
  AEdit: TcxCustomEdit; var Key: Word; Shift: TShiftState);
begin
if key=13 then
begin
  kode:=q.Fields[0].AsString;
  ModalResult:=mrOk;
end;
end;

procedure TDropDownFm.cxGridViewRepository1DBBandedTableView1KeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
if key=13 then
begin
  kode:=q.Fields[0].AsString;
  ModalResult:=mrOk;
end;
end;

procedure TDropDownFm.cxTextEdit1PropertiesChange(Sender: TObject);
begin
  if cxComboBox1.ItemIndex=0 then
  begin
    q.Close;
    q.SQL.Text:= 'select * from ('+ MasterOriSQL +')x where x.nama like '+ QuotedStr('%'+cxTextEdit1.Text+'%');
    q.Open;
  end
  {else if cxComboBox1.ItemIndex=1 then
  begin
    q.Close;
    q.SQL.Text:= 'select * from ('+ MasterOriSQL +')x where x.supplier like '+ QuotedStr('%'+cxTextEdit1.Text+'%');
    q.Open;
  end
  else if cxComboBox1.ItemIndex=2 then
  begin
    q.Close;
    q.SQL.Text:= 'select * from ('+ MasterOriSQL +')x where x.jenis like '+ QuotedStr('%'+cxTextEdit1.Text+'%');
    q.Open;
  end;    }
  //q.SQL.Text:=MasterOriSQL;
end;

procedure TDropDownFm.cxComboBox1PropertiesChange(Sender: TObject);
begin
cxTextEdit1.Clear;
end;

procedure TDropDownFm.FormShow(Sender: TObject);
begin
  cxtextedit1.SetFocus;
end;

end.
