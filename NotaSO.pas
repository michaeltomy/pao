unit NotaSO;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxDBData, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxClasses, cxGridLevel, cxGrid, cxPC, cxCheckBox,
  cxLookAndFeels, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, cxCalendar, cxCurrencyEdit, cxCalc;

type
  TNotaSOFm = class(TForm)
    pnl2: TPanel;
    MasterQ: TSDQuery;
    MasterDs: TDataSource;
    MasterUS: TSDUpdateSQL;
    KodeQ: TSDQuery;
    ExitBtn: TcxButton;
    SaveBtn: TcxButton;
    pnl1: TPanel;
    pnl3: TPanel;
    pnl4: TPanel;
    pnl5: TPanel;
    MasterVGrid: TcxDBVerticalGrid;
    pnl6: TPanel;
    cxDBVerticalGrid1: TcxDBVerticalGrid;
    lbl1: TLabel;
    pnl7: TPanel;
    pnl8: TPanel;
    pnl9: TPanel;
    pnl10: TPanel;
    pnl11: TPanel;
    cxDBVerticalGrid2: TcxDBVerticalGrid;
    pnl12: TPanel;
    cxCheckBox1: TcxCheckBox;
    cxCheckBox2: TcxCheckBox;
    cxCheckBox3: TcxCheckBox;
    cxDBVerticalGrid3: TcxDBVerticalGrid;
    cxDBVerticalGrid1Kodenota: TcxDBEditorRow;
    cxDBVerticalGrid1Tgl: TcxDBEditorRow;
    cxDBVerticalGrid3TglFollowUp: TcxDBEditorRow;
    cxDBVerticalGrid2Keterangan: TcxDBEditorRow;
    KodeQKode: TStringField;
    KodeQNama: TStringField;
    KodeQAlamat: TStringField;
    KodeQNoTelp: TStringField;
    Kode2Q: TSDQuery;
    Kode2Qkodenota: TStringField;
    MasterQKodenota: TStringField;
    MasterQTgl: TDateTimeField;
    MasterQPelanggan: TStringField;
    MasterQTglOrder: TDateTimeField;
    MasterQJenisBarang: TStringField;
    MasterQHarga: TCurrencyField;
    MasterQRute: TStringField;
    MasterQKeterangan: TStringField;
    MasterQCreateDate: TDateTimeField;
    MasterQCreateBy: TStringField;
    MasterQOperator: TStringField;
    MasterQTglEntry: TDateTimeField;
    MasterQStatus: TIntegerField;
    MasterQTglFollowUp: TDateTimeField;
    MasterQPanjang: TFloatField;
    MasterQBerat: TFloatField;
    MasterQKode: TStringField;
    MasterQNama: TStringField;
    MasterQAlamat: TStringField;
    MasterQNoTelp: TStringField;
    MasterQCreateDate_1: TDateTimeField;
    MasterQCreateBy_1: TStringField;
    MasterQOperator_1: TStringField;
    MasterQTglEntry_1: TDateTimeField;
    MasterQKode_1: TStringField;
    MasterQMuat: TStringField;
    MasterQBongkar: TStringField;
    MasterQHarga_1: TCurrencyField;
    MasterQCreateDate_2: TDateTimeField;
    MasterQCreateBy_2: TStringField;
    MasterQOperator_2: TStringField;
    MasterQTglEntry_2: TDateTimeField;
    MasterQJarak: TIntegerField;
    MasterQPoin: TFloatField;
    MasterQBorongan: TCurrencyField;
    MasterVGridPelanggan: TcxDBEditorRow;
    MasterVGridTglOrder: TcxDBEditorRow;
    MasterVGridJenisBarang: TcxDBEditorRow;
    MasterVGridHarga: TcxDBEditorRow;
    MasterVGridRute: TcxDBEditorRow;
    MasterVGridPanjang: TcxDBEditorRow;
    MasterVGridBerat: TcxDBEditorRow;
    MasterVGridNama: TcxDBEditorRow;
    MasterVGridAlamat: TcxDBEditorRow;
    MasterVGridNoTelp: TcxDBEditorRow;
    MasterVGridMuat: TcxDBEditorRow;
    MasterVGridBongkar: TcxDBEditorRow;
    MasterVGridHarga_1: TcxDBEditorRow;
    RuteQ: TSDQuery;
    RuteQKode: TStringField;
    RuteQMuat: TStringField;
    RuteQBongkar: TStringField;
    RuteQHarga: TCurrencyField;
    RuteQJarak: TIntegerField;
    RuteQPoin: TFloatField;
    RuteQBorongan: TCurrencyField;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ExitBtnClick(Sender: TObject);
    procedure SaveBtnClick(Sender: TObject);
    procedure MasterQAfterInsert(DataSet: TDataSet);
    procedure MasterQBeforePost(DataSet: TDataSet);
    procedure cxCheckBox3Click(Sender: TObject);
    procedure cxCheckBox1Click(Sender: TObject);
    procedure cxCheckBox2Click(Sender: TObject);
    procedure MasterVGridKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MasterVGridPelangganEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxDBVerticalGrid1KodenotaEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure MasterVGridRuteEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridPelangganEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
  private
    { Private declarations }
    kodenota:string;
  public
    { Public declarations }
    constructor Create(aOwner: TComponent;kd:string);
  end;

var
  NotaSOFm: TNotaSOFm;
  MasterOriSQL: string;
implementation

uses MenuUtama, DropDown, DM;

{$R *.dfm}

{ TMasterArmadaFm }

constructor TNotaSOFm.Create(aOwner: TComponent;kd:string);
begin
  inherited Create(aOwner);
  kodenota:='';
  DMFm:=TDMFm.Create(self);
  MasterOriSQL:=MasterQ.SQL.Text;
  MasterQ.Open;
  MasterQ.Append;
end;

procedure TNotaSOFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TNotaSOFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TNotaSOFm.ExitBtnClick(Sender: TObject);
var clear:boolean;
begin
  clear := true;
  if sender = ExitBtn then
  begin
    if (messagedlg('Anda yakin mau Batal ?',mtConfirmation,[mbYes,mbNo],0)=IDNo) then clear:=false;
  end;
  if clear then
  begin
    Release;
  end;
end;

procedure TNotaSOFm.SaveBtnClick(Sender: TObject);
begin
  MasterQ.Open;
  MasterQ.Edit;
  if MasterQKodenota.AsString='insert' then
  begin
    Kode2Q.Open;
    if Kode2Q.IsEmpty then
    begin
      MasterQKodenota.AsString:=DMFm.Fill('1',10,'0',True);
    end
    else
    begin
      MasterQKodenota.AsString:=DMFm.Fill(InttoStr(StrtoInt(Kode2QKodenota.AsString)+1),10,'0',True);
    end;
    Kode2Q.Close;
  end;
  if cxCheckBox1.Checked then
  begin
    MasterQStatus.AsInteger:=2;
  end
  else if cxCheckBox2.Checked then
  begin
    MasterQStatus.AsInteger:=0;
  end
  else if cxCheckBox3.Checked then
  begin
    MasterQStatus.AsInteger:=1;
  end;
  MasterQ.Post;
  MenuUtamaFm.Database1.StartTransaction;
  try
    MasterQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    ShowMessage('Pesanan Penjualan dengan Kode ' + MasterQKodenota.AsString + ' telah disimpan');
    if kodenota='' then TNotaSOFm.Create(Self.Parent,'');
    ExitBtnClick(sender);
  except
    MenuUtamaFm.Database1.Rollback;
    MasterQ.RollbackUpdates;
    ShowMessage('Penyimpanan Gagal');
  end;
end;

procedure TNotaSOFm.MasterQAfterInsert(DataSet: TDataSet);
begin
  DMFm.GetDateQ.Open;
  MasterQCreateDate.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  MasterQCreateBy.AsString:=User;
  DMFm.GetDateQ.Close;
end;

procedure TNotaSOFm.MasterQBeforePost(DataSet: TDataSet);
begin
  DMFm.GetDateQ.Open;
  MasterQTglEntry.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  MasterQOperator.AsString:=User;
  DMFm.GetDateQ.Close;
end;

procedure TNotaSOFm.cxCheckBox3Click(Sender: TObject);
begin
  if cxCheckBox3.Checked then
  begin
    cxCheckBox1.Checked:=false;
    cxCheckBox2.Checked:=false;
    MasterQ.Open;
    MasterQ.Edit;
    MasterQStatus.AsInteger:=1;
  end;
end;

procedure TNotaSOFm.cxCheckBox1Click(Sender: TObject);
begin
  if cxCheckBox1.Checked then
  begin
    cxCheckBox3.Checked:=false;
    cxCheckBox2.Checked:=false;
    MasterQ.Open;
    MasterQ.Edit;
    MasterQStatus.AsInteger:=2;
  end;
end;

procedure TNotaSOFm.cxCheckBox2Click(Sender: TObject);
begin
  if cxCheckBox2.Checked then
  begin
    cxCheckBox1.Checked:=false;
    cxCheckBox3.Checked:=false;
    MasterQ.Open;
    MasterQ.Edit;
    MasterQStatus.AsInteger:=0;
  end;
end;

procedure TNotaSOFm.MasterVGridKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_F5) and (MasterVGrid.FocusedRow.Index=0) then
  begin
    KodeQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,KodeQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQPelanggan.AsString:=KodeQKode.AsString;
      MasterQKode.AsString:=KodeQKode.AsString;
      MasterQNama.AsString:=KodeQNama.AsString;
      MasterQAlamat.AsString:=KodeQAlamat.AsString;
      MasterQNoTelp.AsString:=KodeQNoTelp.AsString;
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
  end;
end;

procedure TNotaSOFm.MasterVGridPelangganEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    //KodeQ.SQL.Text:=MasterOriSQL;
    KodeQ.Close;
    KodeQ.ParamByName('text').AsString:='';
    KodeQ.ExecSQL;
    KodeQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,KodeQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQPelanggan.AsString:=KodeQKode.AsString;
      MasterQKode.AsString:=KodeQKode.AsString;
      MasterQNama.AsString:=KodeQNama.AsString;
      MasterQAlamat.AsString:=KodeQAlamat.AsString;
      MasterQNoTelp.AsString:=KodeQNoTelp.AsString;
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TNotaSOFm.cxDBVerticalGrid1KodenotaEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  if AButtonIndex=1 then
  begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      if MasterQStatus.AsInteger=0 then
      begin
        cxCheckBox2.Checked:=True;
        cxCheckBox2.OnClick(self);
      end
      else if MasterQStatus.AsInteger=1 then
      begin
        cxCheckBox3.Checked:=True;
        cxCheckBox3.OnClick(self);
      end
      else
      begin
        cxCheckBox1.Checked:=True;
        cxCheckBox1.OnClick(self);
      end;
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
  end
  else if AButtonIndex=0 then
  begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQ.ClearFields;
      MasterQKodenota.AsString:='insert';
      DMFm.GetDateQ.Open;
      MasterQTgl.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
      DMFm.GetDateQ.Close;
      MasterVGrid.SetFocus;
  end;
end;

procedure TNotaSOFm.MasterVGridRuteEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    RuteQ.Close;
    ruteQ.ExecSQL;
    RuteQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,RuteQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQRute.AsString:=RuteQKode.AsString;
      MasterQKode_1.Asstring:=RuteQKode.asstring;
      MasterQMuat.AsString:=RuteQMuat.AsString;
      MasterQBongkar.AsString:=RuteQBongkar.AsString;
      MasterQHarga_1.AsString:=RuteQHarga.AsString;
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

procedure TNotaSOFm.MasterVGridPelangganEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
    KodeQ.Close;
    KodeQ.ParamByName('text').AsString:=VarToStr(displayvalue);
    KodeQ.ExecSQL;
    KodeQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,KodeQ);
    if DropDownFm.ShowModal=MrOK then
    begin
//      MasterQ.Open;
      MasterQ.Edit;
      MasterQPelanggan.AsString:=KodeQKode.AsString;
      MasterQKode.AsString:=KodeQKode.AsString;
      MasterQNama.AsString:=KodeQNama.AsString;
      MasterQAlamat.AsString:=KodeQAlamat.AsString;
      MasterQNoTelp.AsString:=KodeQNoTelp.AsString;
      //MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

end.
