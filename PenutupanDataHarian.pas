unit PenutupanDataHarian;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxCalendar,
  cxCurrencyEdit, cxCalc, cxRadioGroup, cxMemo, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxGridCustomView, cxGrid, cxLabel, cxCheckBox, cxListBox,
  cxDBEdit, cxDropDownEdit, cxGridCardView, cxGridCustomLayoutView,
  cxGridDBCardView, UCrpeClasses, UCrpe32, dxPSGlbl, dxPSUtl, dxPSEngn,
  dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns,
  dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils,
  dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon, dxPScxPageControlProducer,
  dxPScxGridLnk, dxPScxGridLayoutViewLnk, dxPScxEditorProducers,
  dxPScxExtEditorProducers, dxSkinsdxBarPainter, dxPSContainerLnk,
  dxPSCore, dxPScxCommon;
const
  InputBoxMessage = WM_USER + 200;
type
  TPenutupanDataHarianFm = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    StatusBar: TStatusBar;
    lbl1: TLabel;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyleRepository2: TcxStyleRepository;
    cxStyle2: TcxStyle;
    Panel1: TPanel;
    Crpe1: TCrpe;
    cxDateEdit1: TcxDateEdit;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    Label1: TLabel;
    InfoTransaksiQ: TSDQuery;
    DetailTransaksiQ: TSDQuery;
    TutupTransaksiExecuteQ: TSDQuery;
    DetailTransaksiDs: TDataSource;
    InfoTransaksiQTotalBruto: TCurrencyField;
    InfoTransaksiQTotalDiskon: TCurrencyField;
    InfoTransaksiQTunai: TCurrencyField;
    InfoTransaksiQDebit: TCurrencyField;
    InfoTransaksiQBCA: TCurrencyField;
    InfoTransaksiQVISA: TCurrencyField;
    InfoTransaksiQPending: TCurrencyField;
    InfoTransaksiQCancel: TCurrencyField;
    cxButton1: TcxButton;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    DetailTransaksiQKode: TStringField;
    DetailTransaksiQNomorMeja: TStringField;
    DetailTransaksiQNamaMenu: TStringField;
    DetailTransaksiQQuantity: TFloatField;
    DetailTransaksiQHargaSatuan: TCurrencyField;
    cxGrid1DBTableView1Kode: TcxGridDBColumn;
    cxGrid1DBTableView1NomorMeja: TcxGridDBColumn;
    cxGrid1DBTableView1NamaMenu: TcxGridDBColumn;
    cxGrid1DBTableView1Quantity: TcxGridDBColumn;
    cxGrid1DBTableView1HargaSatuan: TcxGridDBColumn;
    PenjualanQ: TSDQuery;
    PenjualanQKode: TStringField;
    PenjualanQTanggal: TDateTimeField;
    PenjualanQNomorMeja: TStringField;
    PenjualanQCaraPembayaran: TStringField;
    PenjualanQNomorKartu: TStringField;
    PenjualanQKasir: TStringField;
    PenjualanQPramusaji: TStringField;
    PenjualanQGrandTotal: TCurrencyField;
    PenjualanQPembayaran: TCurrencyField;
    PenjualanQKembali: TCurrencyField;
    PenjualanQStatus: TStringField;
    PenjualanQCreateDate: TDateTimeField;
    PenjualanQCreateBy: TStringField;
    PenjualanQOperator: TStringField;
    PenjualanQTglEntry: TDateTimeField;
    PenjualanQDiskon: TCurrencyField;
    PenutupanPenjualanUS: TSDUpdateSQL;
    InfoTransaksiQVIP: TCurrencyField;
    SettingQ: TSDQuery;
    SettingQKode: TStringField;
    SettingQPajak: TFloatField;
    SettingQServis: TFloatField;
    SettingQBCA: TFloatField;
    SettingQVisaMaster: TFloatField;
    PenutupanPenjualanQ: TSDQuery;
    PenutupanPenjualanQKode: TStringField;
    PenutupanPenjualanQTanggalPenutupan: TDateTimeField;
    PenutupanPenjualanQCreateBy: TStringField;
    PenutupanPenjualanQCreateDate: TDateTimeField;
    KodeQ: TSDQuery;
    KodeQkode: TStringField;
    CekPPQ: TSDQuery;
    CekPPQKode: TStringField;
    CekPPQTanggalPenutupan: TDateTimeField;
    CekPPQCreateBy: TStringField;
    CekPPQCreateDate: TDateTimeField;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    Panel2: TPanel;
    GroupBox1: TGroupBox;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    cxLabel3: TcxLabel;
    cxLabel4: TcxLabel;
    cxLabel5: TcxLabel;
    cxLabel6: TcxLabel;
    cxLabel7: TcxLabel;
    cxLabel8: TcxLabel;
    cxLabel9: TcxLabel;
    cxLabel10: TcxLabel;
    cxLabel11: TcxLabel;
    cxLabel12: TcxLabel;
    cxLabel13: TcxLabel;
    cxLabel14: TcxLabel;
    cxLabel15: TcxLabel;
    cxLabel16: TcxLabel;
    lblPenjualanKotor: TcxLabel;
    lblDiskon: TcxLabel;
    lblPPDiskon: TcxLabel;
    lblServis: TcxLabel;
    lblPajak: TcxLabel;
    lblGTotalPenjualan: TcxLabel;
    lblTunai: TcxLabel;
    lblDebit: TcxLabel;
    lblVisaMaster: TcxLabel;
    lblBCADisc: TcxLabel;
    lblVisaMasterDisc: TcxLabel;
    lblPMemberVIP: TcxLabel;
    lblVoucher: TcxLabel;
    lblOrderTidakBayar: TcxLabel;
    lblOrderBatal: TcxLabel;
    cxLabel33: TcxLabel;
    cxLabel34: TcxLabel;
    lblBCA: TcxLabel;
    cxLabel35: TcxLabel;
    cxLabel36: TcxLabel;
    cxLabel17: TcxLabel;
    cxCurrencyEdit1: TcxCurrencyEdit;
    cxCurrencyEdit2: TcxCurrencyEdit;
    dxComponentPrinter1Link2: TdxCustomContainerReportLink;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure cxDateEdit1PropertiesChange(Sender: TObject);
    procedure cxCurrencyEdit1PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxCurrencyEdit2PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
  private
    { Private declarations }
    procedure InputBoxSetPasswordChar(var Msg: TMessage); message InputBoxMessage;
  public
    { Public declarations }
    constructor Create(aOwner: TComponent; mj:string ;baru: boolean); overload;
    constructor Create(aOwner: TComponent;kd:string); overload;
  end;

var
  PenutupanDataHarianFm: TPenutupanDataHarianFm;
  meja,kode:string;
  br :boolean;
  pagemenu,maxpage, maxpagekat, maxpageorder:integer;

  KategoriOriSQL,MenuOriSQL, OrderOriSQL, MasterOriSQL, DetailOriSQL: string;
implementation

uses MenuUtama, DropDown, DM, 
  BarangDropDown, PrepDropDown, DateUtils, PenerimaanBahanDropDown,
  PengeluaranBahanDropDown, PersiapanDropDown,
  StrUtils, Math;

{$R *.dfm}

{ TMasterArmadaFm }

procedure TPenutupanDataHarianFm.InputBoxSetPasswordChar(var Msg: TMessage);
var
  hInputForm, hEdit, hButton: HWND;
begin
  hInputForm := Screen.Forms[0].Handle;
  if (hInputForm <> 0) then
  begin
    hEdit := FindWindowEx(hInputForm, 0, 'TEdit', nil);
    {
      // Change button text:
      hButton := FindWindowEx(hInputForm, 0, 'TButton', nil);
      SendMessage(hButton, WM_SETTEXT, 0, Integer(PChar('Cancel')));
    }
    SendMessage(hEdit, EM_SETPASSWORDCHAR, Ord('*'), 0);
  end;
end;

constructor TPenutupanDataHarianFm.Create(aOwner: TComponent;mj:string; baru:boolean);
begin

end;

constructor TPenutupanDataHarianFm.Create(aOwner: TComponent;kd:string);
begin
  inherited Create(aOwner);
  kode:=kd;
  br:=false;
end;

procedure TPenutupanDataHarianFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TPenutupanDataHarianFm.cxDateEdit1PropertiesChange(
  Sender: TObject);
var temp,servis,pajak,gt:currency;
begin
  CekPPQ.Close;
  CekPPQ.ParamByName('text').AsDate:=cxDateEdit1.Date;
  CekPPQ.Open;
  if CekPPQ.RecordCount<1 then
  begin
    InfoTransaksiQ.Close;
    InfoTransaksiQ.ParamByName('text').AsDate:=cxDateEdit1.Date;
    InfoTransaksiQ.Open;
    DetailTransaksiQ.Close;
    DetailTransaksiQ.ParamByName('text').AsDate:=cxDateEdit1.Date;
    DetailTransaksiQ.Open;
    lblPenjualanKotor.Caption:=FloatToStrF(InfoTransaksiQTotalBruto.AsCurrency,ffCurrency,10,0);
    lblDiskon.Caption:=FloatToStrF(InfoTransaksiQTotalDiskon.AsCurrency,ffCurrency,10,0);
    lblTunai.Caption:=FloatToStrF(InfoTransaksiQTunai.AsCurrency,ffCurrency,10,0);
    lblDebit.Caption:=FloatToStrF(InfoTransaksiQDebit.AsCurrency,ffCurrency,10,0);
    lblBCA.Caption:=FloatToStrF(InfoTransaksiQBCA.AsCurrency,ffCurrency,10,0);
    lblVisaMaster.Caption:=FloatToStrF(InfoTransaksiQVISA.AsCurrency,ffCurrency,10,0);
    lblPMemberVIP.Caption:=FloatToStrF(InfoTransaksiQVIP.AsCurrency,ffCurrency,10,0);
    lblOrderTidakBayar.Caption:=FloatToStrF(InfoTransaksiQPending.AsCurrency,ffCurrency,10,0);
    lblOrderBatal.Caption:=FloatToStrF(InfoTransaksiQCancel.AsCurrency,ffCurrency,10,0);
    temp:=InfoTransaksiQTotalBruto.AsCurrency-InfoTransaksiQTotalDiskon.AsCurrency;
    lblPPDiskon.Caption:=FloatToStrF(temp,ffCurrency,10,0);
    servis:=(SettingQServis.AsFloat/100)*temp;
    lblServis.Caption:=FloatToStrF(servis,ffCurrency,10,0);
    pajak:=(SettingQPajak.AsFloat/100)*temp;
    lblPajak.Caption:=FloatToStrF(pajak,ffCurrency,10,0);
    gt:=temp+servis+pajak;
    lblGTotalPenjualan.Caption:=FloatToStrF(gt,ffCurrency,10,0);
    temp:=InfoTransaksiQBCA.AsCurrency*((100-cxCurrencyEdit1.Value)/100);
    lblBCADisc.Caption:=FloatToStrF(temp,ffCurrency,10,0);
    temp:=InfoTransaksiQVISA.AsCurrency*((100-cxCurrencyEdit2.Value)/100);
    lblVisaMasterDisc.Caption:=FloatToStrF(temp,ffCurrency,10,0);
  end
  else
  begin
    ShowMessage('Transaksi pada tanggal ini sudah ditutup.');
    cxDateEdit1.Clear;
  end;
 // FloatToStrF(sisa,ffCurrency,10,0)
end;

procedure TPenutupanDataHarianFm.cxCurrencyEdit1PropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
var temp:currency;
begin
  temp:=InfoTransaksiQBCA.AsCurrency*((100-DisplayValue)/100);
  lblBCADisc.Caption:=FloatToStrF(temp,ffCurrency,10,0);
end;

procedure TPenutupanDataHarianFm.cxCurrencyEdit2PropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
var temp:currency;
begin
  temp:=InfoTransaksiQVISA.AsCurrency*((100-DisplayValue)/100);
  lblVisaMasterDisc.Caption:=FloatToStrF(temp,ffCurrency,10,0);
end;

procedure TPenutupanDataHarianFm.cxButton3Click(Sender: TObject);
begin
  close;
  release;
end;

procedure TPenutupanDataHarianFm.cxButton1Click(Sender: TObject);
var recount,totaldelete,i:integer;
    tempdelete:single;
    lastdelete:string;
begin
  PenjualanQ.Close;
  PenjualanQ.ParamByName('text').AsDate:=cxDateEdit1.date;
  PenjualanQ.Open;
  recount:=PenjualanQ.RecordCount;
  tempdelete:=recount*0.6;
  totaldelete:=Floor(tempdelete);
  PenjualanQ.First;
  lastdelete:=DMFm.Fill(InttoStr(StrtoInt(PenjualanQKode.AsString)+totaldelete-1),10,'0',True);
//  ShowMessage('total : '+IntToStr(totaldelete)+' kode awal : '+PenjualanQKode.AsString+' kode akhir : '+lastdelete);
  if MessageDlg('Tutup Transaksi Tanggal '+DateToStr(cxDateEdit1.Date)+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then
  begin
    TutupTransaksiExecuteQ.SQL.Text:='DELETE FROM Penjualan WHERE KODE>='+QuotedStr(PenjualanQKode.AsString)+' AND KODE<='+QuotedStr(lastdelete);
    TutupTransaksiExecuteQ.ExecSQL;
    KodeQ.Open;
    PenutupanPenjualanQ.Open;
    PenutupanPenjualanQ.Edit;
    if KodeQ.IsEmpty then
    begin
      PenutupanPenjualanQKode.AsString:=DMFm.Fill('1',10,'0',True);
    end
    else
    begin
      PenutupanPenjualanQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
    end;
    KodeQ.Close;
    PenutupanPenjualanQTanggalPenutupan.AsDateTime:=cxDateEdit1.Date;
    PenutupanPenjualanQCreateBy.AsString:=MenuUtamaFm.UserQKode.AsString;
    PenutupanPenjualanQ.Post;
    MenuUtamaFm.Database1.StartTransaction;
    try
      PenutupanPenjualanQ.ApplyUpdates;
      MenuUtamaFm.Database1.Commit;
      PenutupanPenjualanQ.CommitUpdates;
      ShowMessage('Penutupan Transaksi '+DateToStr(cxDateEdit1.Date)+' Berhasil');
    except
      on E : Exception do begin
        ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
        MenuUtamaFm.Database1.Rollback;
        PenutupanPenjualanQ.RollbackUpdates;
        ShowMessage('Penutupan Transaksi Gagal');
      end;
    end;
  end;
  cxDateEdit1.Clear;
end;

procedure TPenutupanDataHarianFm.FormShow(Sender: TObject);
begin
  SettingQ.Open;
  cxCurrencyEdit1.Value:=SettingQBCA.AsFloat;
  cxCurrencyEdit2.Value:=SettingQVisaMaster.AsFloat;
end;

procedure TPenutupanDataHarianFm.cxButton4Click(Sender: TObject);
begin
  dxComponentPrinter1Link1.Preview();
  dxComponentPrinter1Link2.Preview();
end;

end.
