object MasterRuteFm: TMasterRuteFm
  Left = 511
  Top = 352
  Width = 607
  Height = 258
  Caption = 'Master Rute'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 591
    Height = 48
    Align = alTop
    TabOrder = 0
    object lbl1: TLabel
      Left = 8
      Top = 13
      Width = 25
      Height = 13
      Caption = 'Kode'
    end
    object KodeEdit: TcxButtonEdit
      Left = 40
      Top = 10
      Properties.Buttons = <
        item
          Caption = '+'
          Default = True
          Kind = bkText
        end>
      Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      TabOrder = 0
      OnEnter = KodeEditEnter
      OnExit = KodeEditExit
      OnKeyDown = KodeEditKeyDown
      Width = 121
    end
    object SearchBtn: TcxButton
      Left = 168
      Top = 10
      Width = 57
      Height = 21
      Caption = 'Search'
      TabOrder = 1
      OnClick = SearchBtnClick
    end
  end
  object pnl2: TPanel
    Left = 0
    Top = 150
    Width = 591
    Height = 51
    Align = alBottom
    TabOrder = 2
    object ExitBtn: TcxButton
      Left = 172
      Top = 10
      Width = 75
      Height = 25
      Caption = 'EXIT'
      TabOrder = 0
      OnClick = ExitBtnClick
    end
    object DeleteBtn: TcxButton
      Left = 91
      Top = 10
      Width = 75
      Height = 25
      Caption = 'DELETE'
      TabOrder = 1
      OnClick = DeleteBtnClick
    end
    object SaveBtn: TcxButton
      Left = 8
      Top = 10
      Width = 75
      Height = 25
      Caption = 'SAVE'
      TabOrder = 2
      OnClick = SaveBtnClick
    end
  end
  object MasterVGrid: TcxDBVerticalGrid
    Left = 0
    Top = 48
    Width = 591
    Height = 102
    Align = alClient
    OptionsBehavior.GoToNextCellOnTab = True
    OptionsData.CancelOnExit = False
    OptionsData.Appending = False
    OptionsData.Deleting = False
    OptionsData.DeletingConfirmation = False
    OptionsData.Inserting = False
    TabOrder = 1
    TabStop = False
    DataController.DataSource = MasterDs
    Version = 1
    object MasterVGridMuat: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Muat'
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object MasterVGridBongkar: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Bongkar'
      ID = 1
      ParentID = -1
      Index = 1
      Version = 1
    end
    object MasterVGridHarga: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Harga'
      ID = 2
      ParentID = -1
      Index = 2
      Version = 1
    end
    object MasterVGridJarak: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Jarak'
      ID = 3
      ParentID = -1
      Index = 3
      Version = 1
    end
    object MasterVGridPoin: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Poin'
      ID = 4
      ParentID = -1
      Index = 4
      Version = 1
    end
    object MasterVGridBorongan: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Borongan'
      ID = 5
      ParentID = -1
      Index = 5
      Version = 1
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 201
    Width = 591
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object MasterQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select * from rute')
    UpdateObject = MasterUS
    Left = 313
    Top = 9
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQMuat: TStringField
      FieldName = 'Muat'
      Required = True
      Size = 50
    end
    object MasterQBongkar: TStringField
      FieldName = 'Bongkar'
      Required = True
      Size = 50
    end
    object MasterQHarga: TCurrencyField
      FieldName = 'Harga'
      Required = True
    end
    object MasterQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Required = True
    end
    object MasterQCreateBy: TStringField
      FieldName = 'CreateBy'
      Required = True
      Size = 10
    end
    object MasterQOperator: TStringField
      FieldName = 'Operator'
      Required = True
      Size = 10
    end
    object MasterQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Required = True
    end
    object MasterQJarak: TIntegerField
      FieldName = 'Jarak'
    end
    object MasterQPoin: TFloatField
      FieldName = 'Poin'
    end
    object MasterQBorongan: TCurrencyField
      FieldName = 'Borongan'
    end
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 380
    Top = 14
  end
  object MasterUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, Muat, Bongkar, Harga, CreateDate, CreateBy, Operato' +
        'r, TglEntry, Jarak, Poin, Borongan'
      'from rute'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update rute'
      'set'
      '  Kode = :Kode,'
      '  Muat = :Muat,'
      '  Bongkar = :Bongkar,'
      '  Harga = :Harga,'
      '  Jarak = :Jarak,'
      '  Poin = :Poin,'
      '  Borongan = :Borongan,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  TglEntry = :TglEntry'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into rute'
      
        '  (Kode, Muat, Bongkar, Harga, CreateDate, CreateBy, TglEntry, J' +
        'arak, Poin, Borongan)'
      'values'
      
        '  (:Kode, :Muat, :Bongkar, :Harga, :CreateDate, :CreateBy, :TglE' +
        'ntry, :Jarak, :Poin, :Borongan)')
    DeleteSQL.Strings = (
      'delete from rute'
      'where'
      '  Kode = :OLD_Kode')
    Left = 452
    Top = 18
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from rute order by kode desc')
    Left = 329
    Top = 71
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
end
