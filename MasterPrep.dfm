object MasterPrepFm: TMasterPrepFm
  Left = 914
  Top = 419
  AutoScroll = False
  BorderIcons = [biSystemMenu]
  Caption = 'Master Prep'
  ClientHeight = 264
  ClientWidth = 685
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 685
    Height = 48
    Align = alTop
    TabOrder = 0
    object lbl1: TLabel
      Left = 8
      Top = 13
      Width = 25
      Height = 13
      Caption = 'Kode'
    end
    object KodeEdit: TcxButtonEdit
      Left = 40
      Top = 10
      TabStop = False
      Properties.Buttons = <
        item
          Caption = '+'
          Default = True
          Kind = bkText
        end>
      Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      TabOrder = 0
      OnEnter = KodeEditEnter
      OnExit = KodeEditExit
      OnKeyDown = KodeEditKeyDown
      Width = 121
    end
    object SearchBtn: TcxButton
      Left = 168
      Top = 10
      Width = 57
      Height = 21
      Caption = 'Search'
      TabOrder = 1
      TabStop = False
      OnClick = SearchBtnClick
    end
  end
  object pnl2: TPanel
    Left = 0
    Top = 194
    Width = 685
    Height = 51
    Align = alBottom
    TabOrder = 2
    object ExitBtn: TcxButton
      Left = 172
      Top = 10
      Width = 75
      Height = 25
      Caption = 'EXIT'
      TabOrder = 2
      Visible = False
      OnClick = ExitBtnClick
    end
    object DeleteBtn: TcxButton
      Left = 91
      Top = 10
      Width = 75
      Height = 25
      Caption = 'DELETE'
      TabOrder = 0
      TabStop = False
      OnClick = DeleteBtnClick
    end
    object SaveBtn: TcxButton
      Left = 8
      Top = 10
      Width = 75
      Height = 25
      Caption = 'SAVE'
      TabOrder = 1
      OnClick = SaveBtnClick
    end
    object cxLabel1: TcxLabel
      Left = 336
      Top = 2
      Caption = '* required fields'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clRed
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = []
      Style.IsFontAssigned = True
    end
  end
  object MasterVGrid: TcxDBVerticalGrid
    Left = 0
    Top = 48
    Width = 377
    Height = 146
    Align = alLeft
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    OptionsView.RowHeaderWidth = 146
    OptionsBehavior.GoToNextCellOnTab = True
    OptionsData.CancelOnExit = False
    OptionsData.Appending = False
    OptionsData.Deleting = False
    OptionsData.DeletingConfirmation = False
    OptionsData.Inserting = False
    ParentFont = False
    TabOrder = 1
    OnEnter = MasterVGridEnter
    OnExit = MasterVGridExit
    DataController.DataSource = MasterDs
    Version = 1
    object MasterVGridBahan: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = MasterVGridBahanEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'Bahan'
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object MasterVGridNamaBahan: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NamaBahan'
      ID = 1
      ParentID = 0
      Index = 0
      Version = 1
    end
    object MasterVGridStok: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Stok'
      ID = 2
      ParentID = 0
      Index = 1
      Version = 1
    end
    object MasterVGridSatuan: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Satuan'
      ID = 3
      ParentID = 0
      Index = 2
      Version = 1
    end
    object MasterVGridJumlah: TcxDBEditorRow
      Properties.Caption = 'Jumlah (porsi)'
      Properties.DataBinding.FieldName = 'Jumlah'
      ID = 9
      ParentID = -1
      Index = 1
      Version = 1
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 245
    Width = 685
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object cxGrid1: TcxGrid
    Left = 377
    Top = 48
    Width = 308
    Height = 146
    Align = alClient
    TabOrder = 4
    OnExit = cxGrid1Exit
    object cxGrid1DBTableView1: TcxGridDBTableView
      Navigator.Buttons.OnButtonClick = cxGrid1DBTableView1NavigatorButtonsButtonClick
      Navigator.Buttons.First.Visible = False
      Navigator.Buttons.PriorPage.Visible = False
      Navigator.Buttons.Prior.Visible = False
      Navigator.Buttons.Next.Visible = False
      Navigator.Buttons.NextPage.Visible = False
      Navigator.Buttons.Last.Visible = False
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Append.Visible = False
      Navigator.Buttons.Edit.Visible = True
      Navigator.Buttons.Post.Visible = True
      Navigator.Buttons.Cancel.Visible = True
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.Visible = True
      DataController.DataModeController.SmartRefresh = True
      DataController.DataSource = DataSource1
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      NewItemRow.Visible = True
      OptionsBehavior.AlwaysShowEditor = True
      OptionsBehavior.FocusCellOnTab = True
      OptionsBehavior.FocusFirstCellOnNewRecord = True
      OptionsBehavior.GoToNextCellOnEnter = True
      OptionsData.Appending = True
      OptionsData.DeletingConfirmation = False
      OptionsSelection.MultiSelect = True
      OptionsView.GroupByBox = False
      object cxGrid1DBTableView1NamaBahan: TcxGridDBColumn
        DataBinding.FieldName = 'NamaBahan'
        PropertiesClassName = 'TcxButtonEditProperties'
        Properties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.OnButtonClick = cxGrid1DBTableView1NamaBahanPropertiesButtonClick
        Width = 136
      end
      object cxGrid1DBTableView1Quantity: TcxGridDBColumn
        DataBinding.FieldName = 'Quantity'
      end
      object cxGrid1DBTableView1Satuan: TcxGridDBColumn
        DataBinding.FieldName = 'Satuan'
        Width = 76
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
      Options.DetailTabsPosition = dtpLeft
    end
  end
  object MasterQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select * from prep')
    UpdateObject = MasterUS
    Left = 97
    Top = 73
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQBahan: TStringField
      FieldName = 'Bahan'
      Required = True
      Size = 10
    end
    object MasterQNamaBahan: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaBahan'
      LookupDataSet = BahanQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NamaBahan'
      KeyFields = 'Bahan'
      Size = 200
      Lookup = True
    end
    object MasterQStok: TFloatField
      FieldKind = fkLookup
      FieldName = 'Stok'
      LookupDataSet = BahanQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Jumlah'
      KeyFields = 'Bahan'
      Lookup = True
    end
    object MasterQSatuan: TStringField
      FieldKind = fkLookup
      FieldName = 'Satuan'
      LookupDataSet = BahanQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Satuan'
      KeyFields = 'Bahan'
      Lookup = True
    end
    object MasterQJumlah: TFloatField
      FieldName = 'Jumlah'
    end
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 404
    Top = 6
  end
  object MasterUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      'select Kode, Bahan, Jumlah'#13#10'from prep'
      'where'
      '  Kode = :OLD_Kode and'
      '  Bahan = :OLD_Bahan')
    ModifySQL.Strings = (
      'update prep'
      'set'
      '  Kode = :Kode,'
      '  Bahan = :Bahan,'
      '  Jumlah = :Jumlah'
      'where'
      '  Kode = :OLD_Kode and'
      '  Bahan = :OLD_Bahan')
    InsertSQL.Strings = (
      'insert into prep'
      '  (Kode, Bahan, Jumlah)'
      'values'
      '  (:Kode, :Bahan, :Jumlah)')
    DeleteSQL.Strings = (
      'delete from prep'
      'where'
      '  Kode = :OLD_Kode and'
      '  Bahan = :OLD_Bahan')
    Left = 444
    Top = 10
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from prep order by kode desc')
    Left = 225
    Top = 63
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object DetailQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterDelete = DetailQAfterDelete
    SQL.Strings = (
      'select * from detailprep where prep= :text')
    UpdateObject = SDUpdateSQL1
    Left = 273
    Top = 65
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object DetailQPrep: TStringField
      FieldName = 'Prep'
      Required = True
      Size = 10
    end
    object DetailQBahan: TStringField
      FieldName = 'Bahan'
      Required = True
      Size = 10
    end
    object DetailQQuantity: TFloatField
      FieldName = 'Quantity'
    end
    object DetailQNamaBahan: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaBahan'
      LookupDataSet = BahanQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NamaBahan'
      KeyFields = 'Bahan'
      Size = 200
      Lookup = True
    end
    object DetailQSatuan: TStringField
      FieldKind = fkLookup
      FieldName = 'Satuan'
      LookupDataSet = BahanQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Satuan'
      KeyFields = 'Bahan'
      Size = 50
      Lookup = True
    end
  end
  object DataSource1: TDataSource
    DataSet = DetailQ
    Left = 484
    Top = 6
  end
  object DetailStandardQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select * from detailbarang')
    Left = 617
    Top = 9
    object StringField1: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object StringField2: TStringField
      FieldName = 'NamaJenis'
      Required = True
      Size = 50
    end
    object MemoField1: TMemoField
      FieldName = 'Tipe'
      Required = True
      BlobType = ftMemo
    end
    object MemoField2: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object DetailStandardQKodeBarang: TStringField
      FieldName = 'KodeBarang'
      Required = True
      Size = 10
    end
    object DetailStandardQKodeJenisKendaraan: TStringField
      FieldName = 'KodeJenisKendaraan'
      Required = True
      Size = 10
    end
  end
  object Kode2Q: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select top 1 kode from detailstandard order by kode desc')
    Left = 321
    Top = 9
    object Kode2Qkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object SDUpdateSQL1: TSDUpdateSQL
    RefreshSQL.Strings = (
      'select Prep, Bahan, Quantity'#13#10'from detailprep'
      'where'
      '  Prep = :OLD_Prep and'
      '  Bahan = :OLD_Bahan')
    ModifySQL.Strings = (
      'update detailprep'
      'set'
      '  Prep = :Prep,'
      '  Bahan = :Bahan,'
      '  Quantity = :Quantity'
      'where'
      '  Prep = :OLD_Prep and'
      '  Bahan = :OLD_Bahan')
    InsertSQL.Strings = (
      'insert into detailprep'
      '  (Prep, Bahan, Quantity)'
      'values'
      '  (:Prep, :Bahan, :Quantity)')
    DeleteSQL.Strings = (
      'delete from detailprep'
      'where'
      '  Prep = :OLD_Prep and'
      '  Bahan = :OLD_Bahan')
    Left = 524
    Top = 10
  end
  object BahanQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select * from bahan')
    Left = 161
    Top = 73
    object BahanQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object BahanQNamaBahan: TStringField
      FieldName = 'NamaBahan'
      Size = 200
    end
    object BahanQJumlah: TFloatField
      FieldName = 'Jumlah'
    end
    object BahanQSatuan: TStringField
      FieldName = 'Satuan'
      Size = 50
    end
    object BahanQLokasi: TStringField
      FieldName = 'Lokasi'
      Size = 50
    end
    object BahanQKategori: TStringField
      FieldName = 'Kategori'
      Size = 10
    end
  end
  object DeleteDetailStandardQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from detailprep where prep=:text')
    UpdateObject = UpdateDeleteDetailStandardUS
    Left = 528
    Top = 184
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object DeleteDetailStandardQPrep: TStringField
      FieldName = 'Prep'
      Required = True
      Size = 10
    end
    object DeleteDetailStandardQBahan: TStringField
      FieldName = 'Bahan'
      Required = True
      Size = 10
    end
    object DeleteDetailStandardQQuantity: TFloatField
      FieldName = 'Quantity'
    end
  end
  object ExecuteDeleteDetailStandardQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from detailprep where prep=:text')
    Left = 592
    Top = 184
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object ExecuteDeleteDetailStandardQPrep: TStringField
      FieldName = 'Prep'
      Required = True
      Size = 10
    end
    object ExecuteDeleteDetailStandardQBahan: TStringField
      FieldName = 'Bahan'
      Required = True
      Size = 10
    end
    object ExecuteDeleteDetailStandardQQuantity: TFloatField
      FieldName = 'Quantity'
    end
  end
  object UpdateDeleteDetailStandardUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      'select Prep, Bahan, Quantity'#13#10'from detailprep'
      'where'
      '  Prep = :OLD_Prep and'
      '  Bahan = :OLD_Bahan')
    ModifySQL.Strings = (
      'update detailprep'
      'set'
      '  Prep = :Prep,'
      '  Bahan = :Bahan,'
      '  Quantity = :Quantity'
      'where'
      '  Prep = :OLD_Prep and'
      '  Bahan = :OLD_Bahan')
    InsertSQL.Strings = (
      'insert into detailprep'
      '  (Prep, Bahan, Quantity)'
      'values'
      '  (:Prep, :Bahan, :Quantity)')
    DeleteSQL.Strings = (
      'delete from detailprep'
      'where'
      '  Prep = :OLD_Prep and'
      '  Bahan = :OLD_Bahan')
    Left = 560
    Top = 184
  end
end
