object KwitansiJamkesDropDownFm: TKwitansiJamkesDropDownFm
  Left = 160
  Top = 171
  Width = 1086
  Height = 498
  Caption = 'Kwitansi Drop Down'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid1: TcxGrid
    Left = 0
    Top = 0
    Width = 1070
    Height = 460
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object cxGrid1DBTableView1: TcxGridDBTableView
      OnCellDblClick = cxGrid1DBTableView1CellDblClick
      DataController.DataSource = LPBDs
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.IncSearch = True
      OptionsData.Editing = False
      OptionsView.GroupByBox = False
      object cxGrid1DBTableView1Kode: TcxGridDBColumn
        DataBinding.FieldName = 'Kode'
      end
      object cxGrid1DBTableView1Tanggal: TcxGridDBColumn
        DataBinding.FieldName = 'Tanggal'
      end
      object cxGrid1DBTableView1TerimaDari: TcxGridDBColumn
        DataBinding.FieldName = 'TerimaDari'
        Width = 201
      end
      object cxGrid1DBTableView1Uang: TcxGridDBColumn
        DataBinding.FieldName = 'Uang'
      end
      object cxGrid1DBTableView1GunaPembayaran: TcxGridDBColumn
        DataBinding.FieldName = 'GunaPembayaran'
        Width = 415
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object MasterQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from kwitansijamkesmas')
    Left = 216
    Top = 368
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQTanggal: TDateTimeField
      FieldName = 'Tanggal'
    end
    object MasterQTerimaDari: TStringField
      FieldName = 'TerimaDari'
      Size = 250
    end
    object MasterQUang: TCurrencyField
      FieldName = 'Uang'
    end
    object MasterQJumlahUang: TMemoField
      FieldName = 'JumlahUang'
      BlobType = ftMemo
    end
    object MasterQGunaPembayaran: TMemoField
      FieldName = 'GunaPembayaran'
      BlobType = ftMemo
    end
    object MasterQKeteranganKwitansi: TMemoField
      FieldName = 'KeteranganKwitansi'
      BlobType = ftMemo
    end
    object MasterQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object MasterQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object MasterQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object MasterQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
  end
  object LPBDs: TDataSource
    DataSet = MasterQ
    Left = 248
    Top = 368
  end
  object PelangganQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from pelanggan')
    Left = 112
    Top = 328
  end
end
