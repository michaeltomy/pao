unit TransferMasuk;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxCalendar,
  cxCurrencyEdit, cxCalc, dxSkinscxPCPainter, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxGridCustomView, cxGrid,
  cxDropDownEdit, cxLabel, UCrpeClasses, UCrpe32, cxCheckBox, cxDBEdit;

type
  TTransferMasukFm = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    MasterQ: TSDQuery;
    MasterDs: TDataSource;
    StatusBar: TStatusBar;
    MasterUS: TSDUpdateSQL;
    KodeQ: TSDQuery;
    ExitBtn: TcxButton;
    DeleteBtn: TcxButton;
    SaveBtn: TcxButton;
    KodeEdit: TcxButtonEdit;
    lbl1: TLabel;
    KodeQkode: TStringField;
    DetailTransferQ: TSDQuery;
    DataSource1: TDataSource;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    BarangQ: TSDQuery;
    DetailTransferUS: TSDUpdateSQL;
    BarangUS: TSDUpdateSQL;
    cxCurrencyEdit1: TcxCurrencyEdit;
    cxLabel1: TcxLabel;
    cxButton1: TcxButton;
    SDQuery1: TSDQuery;
    StringField1: TStringField;
    StringField2: TStringField;
    CurrencyField1: TCurrencyField;
    CurrencyField2: TCurrencyField;
    StringField3: TStringField;
    cxLabel5: TcxLabel;
    cxLabel6: TcxLabel;
    TokoQ: TSDQuery;
    cxLabel8: TcxLabel;
    cxGrid2: TcxGrid;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2Level1: TcxGridLevel;
    DataSource3: TDataSource;
    ViewQ: TSDQuery;
    cxLabel4: TcxLabel;
    SDQuery1Jumlah: TFloatField;
    cxDBComboBox1: TcxDBComboBox;
    cxDBDateEdit1: TcxDBDateEdit;
    cxLabel2: TcxLabel;
    TokoQKode: TStringField;
    TokoQNamaToko: TStringField;
    TokoQAlamat: TStringField;
    TokoQKota: TStringField;
    TokoQTelp: TStringField;
    TokoQAktif: TBooleanField;
    TokoQCreateDate: TDateTimeField;
    TokoQCreateBy: TStringField;
    TokoQTglEntry: TDateTimeField;
    TokoQOperator: TStringField;
    TokoQDeleted: TBooleanField;
    PegawaiQ: TSDQuery;
    DetailTransferQKodeTransferMasuk: TStringField;
    DetailTransferQKodeBarang: TStringField;
    DetailTransferQQuantity: TFloatField;
    DetailTransferQHargaPokok: TCurrencyField;
    DetailTransferQSubTotal: TCurrencyField;
    DetailTransferQKeterangan: TMemoField;
    MasterQKode: TStringField;
    MasterQTanggal: TDateTimeField;
    MasterQTokoAsal: TStringField;
    MasterQPenanggungJawab: TStringField;
    MasterQGrandTotal: TCurrencyField;
    MasterQTerbayar: TCurrencyField;
    MasterQStatus: TStringField;
    MasterQCreateDate: TDateTimeField;
    MasterQCreateBy: TStringField;
    MasterQTglEntry: TDateTimeField;
    MasterQOperator: TStringField;
    MasterQDeleted: TBooleanField;
    MasterQNamaTokoAsal: TStringField;
    MasterQNamaPenanggungJawab: TStringField;
    PegawaiQKode: TStringField;
    PegawaiQNama: TStringField;
    PegawaiQAlamat: TStringField;
    PegawaiQTglLahir: TDateTimeField;
    PegawaiQAktif: TBooleanField;
    PegawaiQCreateDate: TDateTimeField;
    PegawaiQCreateBy: TStringField;
    PegawaiQTglEntry: TDateTimeField;
    PegawaiQOperator: TStringField;
    PegawaiQDeleted: TBooleanField;
    cxButtonEdit1: TcxButtonEdit;
    cxButtonEdit2: TcxButtonEdit;
    BarangQKode: TStringField;
    BarangQBarcode: TStringField;
    BarangQNama: TStringField;
    BarangQKategori: TStringField;
    BarangQTipe: TStringField;
    BarangQKeterangan: TMemoField;
    BarangQJumlah: TFloatField;
    BarangQSatuan: TStringField;
    BarangQInsentifJual: TCurrencyField;
    BarangQInsentifPeriksa: TCurrencyField;
    BarangQHargaJual: TCurrencyField;
    BarangQAktif: TBooleanField;
    BarangQCreateDate: TDateTimeField;
    BarangQCreateBy: TStringField;
    BarangQTglEntry: TDateTimeField;
    BarangQOperator: TStringField;
    BarangQDeleted: TBooleanField;
    cxGrid1DBTableView1KodeBarang: TcxGridDBColumn;
    cxGrid1DBTableView1Quantity: TcxGridDBColumn;
    cxGrid1DBTableView1HargaPokok: TcxGridDBColumn;
    cxGrid1DBTableView1SubTotal: TcxGridDBColumn;
    cxGrid1DBTableView1Keterangan: TcxGridDBColumn;
    DetailTransferQNamaBarang: TStringField;
    DetailTransferQBarcodeBarang: TStringField;
    DetailTransferQKategoriBarang: TStringField;
    DetailTransferQTipeBarang: TStringField;
    cxGrid1DBTableView1NamaBarang: TcxGridDBColumn;
    cxGrid1DBTableView1BarcodeBarang: TcxGridDBColumn;
    cxGrid1DBTableView1TipeBarang: TcxGridDBColumn;
    DetailTransferQSatuan: TStringField;
    cxGrid1DBTableView1Satuan: TcxGridDBColumn;
    ViewQTanggal: TDateTimeField;
    ViewQTokoAsal: TStringField;
    ViewQPenanggungJawab: TStringField;
    ViewQgrandtotal: TCurrencyField;
    cxGrid2DBTableView1Tanggal: TcxGridDBColumn;
    cxGrid2DBTableView1TokoAsal: TcxGridDBColumn;
    cxGrid2DBTableView1PenanggungJawab: TcxGridDBColumn;
    cxGrid2DBTableView1grandtotal: TcxGridDBColumn;
    ViewQKode: TStringField;
    cxGrid2DBTableView1Kode: TcxGridDBColumn;
    BarangQHargaBeli: TCurrencyField;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ExitBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure KodeEditEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SaveBtnClick(Sender: TObject);
    procedure DeleteBtnClick(Sender: TObject);
    procedure SearchBtnClick(Sender: TObject);
    procedure MasterVGridKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MasterVGridBarangEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridJumlahEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure MasterVGridHargaSatuanEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxGrid1DBTableView1Column1PropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure SDQuery1BeforePost(DataSet: TDataSet);
    procedure cxGrid1Exit(Sender: TObject);
    procedure cxGrid1DBTableView1Column6PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxGrid1DBTableView1Column8PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure SDQuery1AfterPost(DataSet: TDataSet);
    procedure DetailQAfterPost(DataSet: TDataSet);
    procedure DetailQBeforePost(DataSet: TDataSet);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxGrid1DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SaveBtnKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxButton1Click(Sender: TObject);
    procedure cxGrid1DBTableView1Column7PropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure KodeEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxGrid1DBTableView1Column5PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxGrid2DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxGrid1DBTableView1Column7PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure DetailTransferQAfterDelete(DataSet: TDataSet);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxButtonEdit2PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxGrid1DBTableView1KodeBarangPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxGrid1DBTableView1QuantityPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxGrid1DBTableView1HargaPokokPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent);
  end;

var
  TransferMasukFm: TTransferMasukFm;

  MasterOriSQL: string;
  BarangOriSQL, DetailPembelianOriSQL:string;
implementation

uses MenuUtama, DropDown, DM, StrUtils, DateUtils, TokoDropDown,
  PegawaiDropDown, BarangDropDown;

{$R *.dfm}

{ TMasterArmadaFm }

constructor TTransferMasukFm.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
end;

procedure TTransferMasukFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TTransferMasukFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TTransferMasukFm.ExitBtnClick(Sender: TObject);
begin
  Release;
end;

procedure TTransferMasukFm.FormCreate(Sender: TObject);
begin
  MasterOriSQL:=MasterQ.SQL.Text;
  BarangOriSQL:=BarangQ.SQL.Text;
  KodeEdit.Properties.MaxLength:=MasterQKode.Size;
  TokoQ.Open;
  PegawaiQ.Open;
end;

procedure TTransferMasukFm.KodeEditEnter(Sender: TObject);
begin
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  SaveBtn.Enabled:=false;
  DeleteBtn.Enabled:=false;
  MasterQ.Close;
end;

procedure TTransferMasukFm.FormShow(Sender: TObject);
begin
  KodeEdit.SetFocus;
end;

procedure TTransferMasukFm.KodeEditExit(Sender: TObject);
begin
  if KodeEdit.Text<>'' then
  begin
    //kodeedit.Text:=DMfm.Fill(kodeedit.Text,10,'0',true);
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
      cxDBDateEdit1.Date:=Today;
      DetailTransferQ.Close;
      DetailTransferQ.ParamByName('text').AsString:='a';
      DetailTransferQ.Open;
      cxGrid1.Enabled:=true;
    end
    else
    begin
      DetailTransferQ.Close;
      DetailTransferQ.ParamByName('text').AsString:=KodeEdit.Text;
      DetailTransferQ.Open;
      DetailTransferQ.Edit;
      masterq.Edit;
      cxButtonEdit1.Text:=MasterQNamaTokoAsal.AsString;
      cxButtonEdit2.Text:=MasterQNamaPenanggungJawab.AsString;
      cxCurrencyEdit1.Text:=MasterQGrandTotal.AsString;
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      DeleteBtn.Enabled:=True;
      cxGrid1.Enabled:=true;
    end;
    SaveBtn.Enabled:=True;
    cxButtonedit1.Enabled:=true;
    cxbuttonedit2.enabled:=true;
    cxdbdateedit1.Enabled:=true;
    cxdbcombobox1.Enabled:=true;
  end;
end;

procedure TTransferMasukFm.KodeEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    if key=VK_F6 then
  begin
     SaveBtn.Click;
  end
  else if key=VK_F7 then
  begin
    DeleteBtn.Click;
  end
  else if key=VK_F8 then
  begin
    ExitBtn.Click;
  end
  else if key=VK_F5 then
  begin
    KodeEditPropertiesButtonClick(self,0);
  end
  else if key=VK_F4 then
  begin
    //ShowMessage('asd');
    cxGrid1DBTableView1.NavigatorButtons.Insert.Click;
    cxGrid1.SetFocus;
  end;
  if (Key=VK_escape) then
  begin
    ExitBtn.Click;
  end;
end;

procedure TTransferMasukFm.SaveBtnClick(Sender: TObject);
var kodeprint:string;
begin

    if StatusBar.Panels[0].Text= 'Mode : Entry' then
    begin
      MasterQ.Edit;
      KodeQ.Close;
      KodeQ.Open;
      if KodeQ.IsEmpty then
      begin
        MasterQKode.AsString:=DMFm.Fill('1',10,'0',True);
      end
      else
      begin
        MasterQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
      end;
      MasterQGrandTotal.AsInteger:=strtoint(cxlabel2.Caption);
      KodeQ.Close;
      KodeEdit.Text:=MasterQKode.AsString;
    end
    else
    begin
      MasterQGrandTotal.AsCurrency:=cxCurrencyEdit1.Value;
    end;
    MasterQGrandTotal.AsInteger:=strtoint(cxCurrencyEdit1.EditValue);
    MasterQ.Post;
    MenuUtamaFm.Database1.StartTransaction;
    try
      MasterQ.ApplyUpdates;
      DetailTransferQ.ApplyUpdates;
      MenuUtamaFm.Database1.Commit;
      MasterQ.CommitUpdates;
      DetailTransferQ.CommitUpdates;
      ShowMessage('Penyimpanan Berhasil');
    except
    on E : Exception
      do
      begin
        ShowMessage(E.ClassName+' error raised, with message : '+E.Message);
        MenuUtamaFm.Database1.Rollback;
        MasterQ.RollbackUpdates;
        DetailTransferQ.RollbackUpdates;
      end;
    end;
    SaveBtn.Enabled:=false;
    cxCurrencyEdit1.EditValue:=null;
    ViewQ.Close;
    ViewQ.Open;
    viewq.Refresh;
    barangq.Refresh;

end;

procedure TTransferMasukFm.DeleteBtnClick(Sender: TObject);
begin
  if MessageDlg('Hapus Transfer Masuk '+KodeEdit.Text+' - '+MasterQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     MenuUtamaFm.Database1.StartTransaction;
     try
       SDQuery1.SQL.Text:='delete from detailtransfermasuk where kodetransfermasuk='+MasterQKode.AsString;
       SDQuery1.ExecSQL;
       MasterQ.Delete;
       MasterQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;
       MessageDlg('Transfer Masuk telah dihapus.',mtInformation,[mbOK],0);
     except
       MenuUtamaFm.Database1.Rollback;
       MasterQ.RollbackUpdates;
       DetailTransferQ.RollbackUpdates;
       MessageDlg('Transfer Masuk pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
     end;
     DeleteBtn.Enabled:=false;
     KodeEdit.SetFocus;
    viewq.Refresh;
  end;
end;

procedure TTransferMasukFm.SearchBtnClick(Sender: TObject);
begin
    cxGrid1DBTableView1.NavigatorButtons.Insert.Click;
    cxGrid1.SetFocus;
    cxGrid1DBTableView1.Columns[0].Focused:=true;
end;

procedure TTransferMasukFm.MasterVGridKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
if key=VK_F6 then
begin
   SaveBtn.Click;
end
else if key=VK_F7 then
begin
  DeleteBtn.Click;
end
else if key=VK_F8 then
begin
  ExitBtn.Click;
end
  else if key=VK_F4 then
  begin
    cxGrid1DBTableView1.NavigatorButtons.Insert.Click;
    cxGrid1.SetFocus;
  end;
end;

procedure TTransferMasukFm.MasterVGridBarangEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    DropDownFm:=TDropdownfm.Create(Self,BarangQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      {Masterqbarang.AsString:=barangqkode.asstring;
      masterqsupplier.asstring:=barangqsupplier.asstring;
      masterqnama.AsString:=barangqnama.asstring;
      masterqsatuan.asstring:=barangqsatuan.asstring;
      masterqjenis.AsString:=barangqjenis.asstring;
      MasterVGrid.SetFocus;}
    end;
    DropDownFm.Release;
end;

procedure TTransferMasukFm.MasterVGridJumlahEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
  //MasterQJumlah.AsInteger:=DisplayValue;
  //MasterQGrandTotal.AsInteger:=MasterQJumlah.AsInteger * MasterQHargaSatuan.AsInteger;
end;

procedure TTransferMasukFm.MasterVGridHargaSatuanEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin

  //MasterQHargaSatuan.AsCurrency:=DisplayValue;
  //MasterQGrandTotal.AsInteger:=MasterQJumlah.AsInteger * MasterQHargaSatuan.AsInteger;

end;

procedure TTransferMasukFm.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  //KodeEdit.Text:=SDQuery1Kode.AsString;
  //KodeEditExit(self);
  masterq.Close;
  //MasterQ.SQL.Text:= 'select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+SDQuery1Kode.AsString+'%');
  //masterq.ParamByName('text').AsString:=SDQuery1Kode.AsString;
  masterq.Open;
  SaveBtn.Enabled:=true;
  DeleteBtn.Enabled:=true;
end;

procedure TTransferMasukFm.cxGrid1DBTableView1Column1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  //ShowMessage(inttostr(abuttonindex));
    DetailTransferQ.Open;
    DetailTransferQ.Insert;
    DropDownFm:=TDropdownfm.Create(Self,BarangQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      DetailTransferQKodeBarang.AsString:=barangqkode.asstring;
      //DetailTransferQSatuan.AsString:=BarangQSatuan.AsString;
    end;
    DropDownFm.Release;
    //cxGrid1DBTableView1Column7.Focused:=true;
end;

procedure TTransferMasukFm.SDQuery1BeforePost(DataSet: TDataSet);
var i : integer;
begin
    
end;

procedure TTransferMasukFm.cxGrid1Exit(Sender: TObject);
begin
  cxGrid1DBTableView1.NavigatorButtons.Post.Click;
  cxButton1.SetFocus;
  //SaveBtn.SetFocus;
end;

procedure TTransferMasukFm.cxGrid1DBTableView1Column6PropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
  DetailTransferQQuantity.AsFloat:=DisplayValue;
  DetailTransferQSubTotal.AsCurrency:=DetailTransferQHargaPokok.AsCurrency*DetailTransferQQuantity.AsFloat;
  //DetailQHargaSatuan.AsString:=DisplayValue;
  //DetailQSubTotal.AsInteger:=DetailQHargaSatuan.AsInteger * DetailQJumlah.AsInteger - DetailQDiskon.AsInteger;
  //cxLabel1.Caption:=inttostr();
end;

procedure TTransferMasukFm.cxGrid1DBTableView1Column8PropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
  //DetailPembelianQDiskon.AsString:=DisplayValue;
  //DetailPembelianQSubTotal.AsInteger:=DetailPembelianQHargaSatuan.AsInteger * DetailPembelianQJumlah.AsInteger - DetailPembelianQDiskon.AsInteger;

end;

procedure TTransferMasukFm.SDQuery1AfterPost(DataSet: TDataSet);
var i,temp:integer;
begin

end;

procedure TTransferMasukFm.DetailQAfterPost(DataSet: TDataSet);
var temp,i:integer;
begin
    temp:=0;
    DetailTransferQ.First;
    for i:= 1 to DetailTransferQ.RecordCount do
    begin
      //ShowMessage(DetailQSubTotal.AsString);
      temp:=temp+ DetailTransferQSubTotal.AsInteger;
      DetailTransferQ.Next;
    end;
    cxCurrencyEdit1.Text:=inttostr(temp);
    cxlabel2.Caption:=inttostr(temp);
    cxCurrencyEdit1.SetFocus;
    {BarangQ.Edit;
    //ShowMessage(BarangQStok.AsString);
    BarangQStok.AsInteger:=BarangQStok.AsInteger-DetailQJumlah.AsInteger;
    //ShowMessage(BarangQStok.AsString);
    BarangQ.Post;}
    DetailTransferQ.First;
      cxGrid1DBTableView1.NavigatorButtons.Post.Click;
  cxGrid1DBTableView1.NavigatorButtons.Insert.Click;
  cxGrid1.SetFocus;
  cxGrid1DBTableView1.Columns[7].Focused:=true;
end;

procedure TTransferMasukFm.DetailQBeforePost(DataSet: TDataSet);
begin
    if KodeEdit.Text='INSERT BARU' then
    begin
      KodeQ.Open;
      if KodeQ.IsEmpty then
      begin
        DetailTransferQKodeTransferMasuk.AsString:=DMFm.Fill('1',10,'0',True);
      end
      else
      begin
        DetailTransferQKodeTransferMasuk.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
      end;
    end
    else
    begin
      DetailTransferQKodeTransferMasuk.AsString:=KodeEdit.Text;
    end;
    //ShowMessage(SDQuery1KodeJual.AsString);
end;

procedure TTransferMasukFm.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
  var temp:boolean;
begin
    if key=VK_F6 then
  begin
     SaveBtn.Click;
  end
  else if key=VK_F7 then
  begin
    DeleteBtn.Click;
  end
  else if key=VK_F8 then
  begin
    ExitBtn.Click;
  end
  else if key=VK_F5 then
  begin
    KodeEditPropertiesButtonClick(self,0);
  end
  else if key=VK_F4 then
  begin
    //ShowMessage('asd');
    cxGrid1DBTableView1.NavigatorButtons.Insert.Click;
    cxGrid1.SetFocus;
  end;
end;

procedure TTransferMasukFm.cxGrid1DBTableView1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    if key=VK_F6 then
  begin
     SaveBtn.Click;
  end
  else if key=VK_F7 then
  begin
    DeleteBtn.Click;
  end
  else if key=VK_F8 then
  begin
    ExitBtn.Click;
  end
  else if key=VK_F5 then
  begin
    KodeEditPropertiesButtonClick(self,0);
  end
  else if key=VK_F4 then
  begin
    //ShowMessage('asd');
    cxGrid1DBTableView1.NavigatorButtons.Insert.Click;
    cxGrid1.SetFocus;
  end;
end;

procedure TTransferMasukFm.SaveBtnKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
           if key=VK_F6 then
  begin
     SaveBtn.Click;
  end
  else if key=VK_F7 then
  begin
    DeleteBtn.Click;
  end
  else if key=VK_F8 then
  begin
    ExitBtn.Click;
  end
  else if key=VK_F5 then
  begin
    //cxButtonEdit1PropertiesButtonClick(self,0);
  end
  else if key=VK_F4 then
  begin
    //ShowMessage('asd');
    cxGrid1DBTableView1.NavigatorButtons.Insert.Click;
    cxGrid1.SetFocus;
  end;
end;

procedure TTransferMasukFm.cxButton1Click(Sender: TObject);
begin
  cxGrid1DBTableView1.NavigatorButtons.Insert.Click;
  cxGrid1.SetFocus;
end;

procedure TTransferMasukFm.cxGrid1DBTableView1Column7PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  BarangQ.Close;
  BarangQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,BarangQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      DetailTransferQ.Insert;
      DetailTransferQ.Edit;
      DetailTransferQKodebarang.AsString:=barangqkode.asstring;
      //DetailPembelianQDiskon.AsInteger:=0;
      DetailTransferQKodeTransferMasuk.AsString:=MasterQKode.AsString;
    end;
    DropDownFm.Release;
    cxGrid1DBTableView1.Columns[5].Focused:=true;
end;

procedure TTransferMasukFm.KodeEditPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  if kodeedit.Text ='' then kodeedit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  KodeEditExit(sender);
end;

procedure TTransferMasukFm.cxGrid1DBTableView1Column5PropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
  {if DisplayValue> DetailPembelianQStok.AsInteger then
  begin
    ShowMessage('stok tidak cukup');
    DetailPembelianQJumlah.AsInteger:=0;
  end
  else
  begin
    DetailPembelianQJumlah.AsInteger:=DisplayValue;
    DetailPembelianQSubTotal.AsInteger:=DetailPembelianQHargaSatuan.AsInteger * DetailPembelianQJumlah.AsInteger - DetailPembelianQDiskon.AsInteger;
  end; }
end;

procedure TTransferMasukFm.cxGrid2DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  KodeEdit.Text:=ViewQKode.AsString;
  KodeEditExit(self);
  cxCurrencyEdit1.SetFocus;
end;

procedure TTransferMasukFm.cxGrid1DBTableView1Column7PropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
  DetailTransferQHargaPokok.AsCurrency:=DisplayValue;
  DetailTransferQSubTotal.AsCurrency:=DetailTransferQHargaPokok.AsCurrency*DetailTransferQQuantity.AsFloat;

end;

procedure TTransferMasukFm.DetailTransferQAfterDelete(DataSet: TDataSet);
var temp, i : integer;
begin
    temp:=0;
    DetailTransferQ.First;
    for i:= 1 to DetailTransferQ.RecordCount do
    begin
      temp:=temp+ DetailTransferQSubTotal.AsInteger;
      DetailTransferQ.Next;
    end;
    cxCurrencyEdit1.Text:=inttostr(temp);
    cxlabel2.Caption:=inttostr(temp);
    cxCurrencyEdit1.SetFocus;
end;

procedure TTransferMasukFm.cxButtonEdit1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  TokoDropDownFm:=TTokoDropDownFm.create(self);
  if TokoDropDownFm.showmodal=mrOK then
  begin
    MasterQ.edit;
    MasterQTokoAsal.AsString:=TokoDropDownFm.kode;
    cxButtonEdit1.Text:=MasterQNamaTokoAsal.AsString;
  end;
  TokoDropDownFm.release;
end;

procedure TTransferMasukFm.cxButtonEdit2PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  PegawaiDropDownFm:=TPegawaiDropDownFm.create(self);
  if PegawaiDropDownFm.showmodal=mrOK then
  begin
    MasterQ.edit;
    MasterQPenanggungJawab.AsString:=PegawaiDropDownFm.kode;
    cxButtonEdit2.Text:=MasterQNamaPenanggungJawab.asstring;
  end;
  PegawaiDropDownFm.release;
end;

procedure TTransferMasukFm.cxGrid1DBTableView1KodeBarangPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  BarangDropDownFm:=TBarangDropDownFm.create(self);
  if BarangDropDownFm.showmodal=mrOK then
  begin
    DetailTransferQ.Edit;
    DetailTransferQKodeBarang.AsString:=Barangdropdownfm.kode;
  end;
  BarangDropDownFm.release;
end;

procedure TTransferMasukFm.cxGrid1DBTableView1QuantityPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
  DetailTransferQQuantity.AsFloat:=DisplayValue;
  DetailTransferQSubTotal.AsCurrency:=DetailTransferQQuantity.AsFloat*DetailTransferQHargaPokok.AsCurrency;
end;

procedure TTransferMasukFm.cxGrid1DBTableView1HargaPokokPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
  DetailTransferQHargaPokok.AsFloat:=DisplayValue;
  DetailTransferQSubTotal.AsCurrency:=DetailTransferQQuantity.AsFloat*DetailTransferQHargaPokok.AsCurrency;

end;

end.
