object SettingNotifikasiFm: TSettingNotifikasiFm
  Left = 482
  Top = 193
  Width = 402
  Height = 364
  Caption = 'Setting Notifikasi'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar: TStatusBar
    Left = 0
    Top = 307
    Width = 386
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object cxButton1: TcxButton
    Left = 152
    Top = 264
    Width = 75
    Height = 25
    Caption = 'SAVE'
    TabOrder = 1
    OnClick = cxButton1Click
  end
  object cxGroupBox2: TcxGroupBox
    Left = 16
    Top = 96
    Caption = 'Parameter Notifikasi'
    TabOrder = 2
    Height = 153
    Width = 345
    object cxLabel2: TcxLabel
      Left = 10
      Top = 32
      Caption = 'Minimum Stok'
    end
  end
  object cxDBSpinEdit2: TcxDBSpinEdit
    Left = 208
    Top = 124
    DataBinding.DataField = 'InactiveCustomer'
    DataBinding.DataSource = DataSource1
    TabOrder = 3
    Width = 121
  end
  object DataSource1: TDataSource
    Left = 280
    Top = 8
  end
  object SDUpdateSQL1: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, WaktuPengecekan, InactiveCustomer, PilihKendaraan, ' +
        'SuratJalan'#13#10'from Notifikasi'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update Notifikasi'
      'set'
      '  Kode = :Kode,'
      '  WaktuPengecekan = :WaktuPengecekan,'
      '  InactiveCustomer = :InactiveCustomer,'
      '  PilihKendaraan = :PilihKendaraan,'
      '  SuratJalan = :SuratJalan'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into Notifikasi'
      
        '  (Kode, WaktuPengecekan, InactiveCustomer, PilihKendaraan, Sura' +
        'tJalan)'
      'values'
      
        '  (:Kode, :WaktuPengecekan, :InactiveCustomer, :PilihKendaraan, ' +
        ':SuratJalan)')
    DeleteSQL.Strings = (
      'delete from Notifikasi'
      'where'
      '  Kode = :OLD_Kode')
    Left = 328
    Top = 8
  end
end
