object NotaSJFm: TNotaSJFm
  Left = 484
  Top = 65
  Width = 642
  Height = 680
  Caption = 'SURAT JALAN'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object pnl2: TPanel
    Left = 0
    Top = 591
    Width = 626
    Height = 51
    Align = alBottom
    TabOrder = 0
    object ExitBtn: TcxButton
      Left = 91
      Top = 10
      Width = 75
      Height = 25
      Caption = 'EXIT'
      TabOrder = 0
      OnClick = ExitBtnClick
    end
    object SaveBtn: TcxButton
      Left = 10
      Top = 10
      Width = 75
      Height = 25
      Caption = 'SAVE'
      TabOrder = 1
      OnClick = SaveBtnClick
    end
  end
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 626
    Height = 79
    Align = alTop
    TabOrder = 1
    object lbl1: TLabel
      Left = 69
      Top = 19
      Width = 231
      Height = 37
      Caption = 'SURAT JALAN'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -32
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object pnl6: TPanel
      Left = 381
      Top = 1
      Width = 244
      Height = 77
      Align = alRight
      Caption = 'pnl6'
      TabOrder = 0
      object cxDBVerticalGrid1: TcxDBVerticalGrid
        Left = 1
        Top = 1
        Width = 242
        Height = 75
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        OptionsView.RowHeaderWidth = 85
        OptionsData.CancelOnExit = False
        OptionsData.Appending = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Inserting = False
        ParentFont = False
        TabOrder = 0
        DataController.DataSource = MasterDs
        Version = 1
        object cxDBVerticalGrid1Kodenota: TcxDBEditorRow
          Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
          Properties.EditProperties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end
            item
              Caption = '+'
              Kind = bkText
            end>
          Properties.DataBinding.FieldName = 'Kodenota'
          ID = 3
          ParentID = -1
          Index = 0
          Version = 1
        end
        object cxDBVerticalGrid1Tgl: TcxDBEditorRow
          Properties.DataBinding.FieldName = 'Tgl'
          ID = 4
          ParentID = -1
          Index = 1
          Version = 1
        end
      end
    end
  end
  object pnl3: TPanel
    Left = 0
    Top = 79
    Width = 626
    Height = 512
    Align = alClient
    TabOrder = 2
    object pnl4: TPanel
      Left = 1
      Top = 450
      Width = 624
      Height = 61
      Align = alBottom
      Caption = 'pnl4'
      TabOrder = 0
      object cxDBVerticalGrid2: TcxDBVerticalGrid
        Left = 1
        Top = 1
        Width = 622
        Height = 59
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        OptionsData.CancelOnExit = False
        OptionsData.Appending = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Inserting = False
        ParentFont = False
        TabOrder = 0
        DataController.DataSource = MasterDs
        Version = 1
        object cxDBVerticalGrid2Laporan: TcxDBEditorRow
          Height = 50
          Properties.DataBinding.FieldName = 'Laporan'
          ID = 0
          ParentID = -1
          Index = 0
          Version = 1
        end
      end
    end
    object pnl5: TPanel
      Left = 1
      Top = 1
      Width = 624
      Height = 449
      Align = alClient
      Caption = 'pnl5'
      TabOrder = 1
      object pnl7: TPanel
        Left = 1
        Top = 372
        Width = 622
        Height = 76
        Align = alBottom
        Caption = 'pnl7'
        TabOrder = 0
        object pnl8: TPanel
          Left = 1
          Top = 1
          Width = 314
          Height = 74
          Align = alLeft
          Caption = 'pnl8'
          TabOrder = 0
          object pnl10: TPanel
            Left = 1
            Top = 1
            Width = 312
            Height = 23
            Align = alTop
            Caption = 'Kontak Muat'
            TabOrder = 0
          end
          object cxDBVerticalGrid3: TcxDBVerticalGrid
            Left = 1
            Top = 24
            Width = 312
            Height = 49
            Align = alClient
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            OptionsData.CancelOnExit = False
            OptionsData.Appending = False
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsData.Inserting = False
            ParentFont = False
            TabOrder = 1
            DataController.DataSource = MasterDs
            Version = 1
            object cxDBVerticalGrid3NamaMuat: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'NamaMuat'
              ID = 0
              ParentID = -1
              Index = 0
              Version = 1
            end
            object cxDBVerticalGrid3TelpMuat: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'TelpMuat'
              ID = 1
              ParentID = -1
              Index = 1
              Version = 1
            end
          end
        end
        object pnl9: TPanel
          Left = 315
          Top = 1
          Width = 306
          Height = 74
          Align = alClient
          Caption = 'pnl9'
          TabOrder = 1
          object pnl11: TPanel
            Left = 1
            Top = 1
            Width = 304
            Height = 23
            Align = alTop
            Caption = 'Kontak Bongkar'
            TabOrder = 0
          end
          object cxDBVerticalGrid4: TcxDBVerticalGrid
            Left = 1
            Top = 24
            Width = 304
            Height = 49
            Align = alClient
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            OptionsView.RowHeaderWidth = 112
            OptionsData.CancelOnExit = False
            OptionsData.Appending = False
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsData.Inserting = False
            ParentFont = False
            TabOrder = 1
            DataController.DataSource = MasterDs
            Version = 1
            object cxDBVerticalGrid4NamaBongkar: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'NamaBongkar'
              ID = 0
              ParentID = -1
              Index = 0
              Version = 1
            end
            object cxDBVerticalGrid4TelpBongkar: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'TelpBongkar'
              ID = 1
              ParentID = -1
              Index = 1
              Version = 1
            end
          end
        end
      end
      object pnl12: TPanel
        Left = 1
        Top = 1
        Width = 622
        Height = 371
        Align = alClient
        Caption = 'pnl12'
        TabOrder = 1
        object pnl13: TPanel
          Left = 1
          Top = 275
          Width = 620
          Height = 95
          Align = alBottom
          Caption = 'pnl13'
          TabOrder = 0
          object pnl14: TPanel
            Left = 1
            Top = 1
            Width = 618
            Height = 24
            Align = alTop
            Caption = 'Surat Jalan Eksternal'
            TabOrder = 0
          end
          object cxDBVerticalGrid5: TcxDBVerticalGrid
            Left = 1
            Top = 25
            Width = 618
            Height = 69
            Align = alClient
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            OptionsData.CancelOnExit = False
            OptionsData.Appending = False
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsData.Inserting = False
            ParentFont = False
            TabOrder = 1
            DataController.DataSource = MasterDs
            Version = 1
            object cxDBVerticalGrid5xNama: TcxDBEditorRow
              Properties.Caption = 'Nama'
              Properties.DataBinding.FieldName = 'xNama'
              ID = 0
              ParentID = -1
              Index = 0
              Version = 1
            end
            object cxDBVerticalGrid5xTgl: TcxDBEditorRow
              Properties.Caption = 'Tgl'
              Properties.DataBinding.FieldName = 'xTgl'
              ID = 1
              ParentID = -1
              Index = 1
              Version = 1
            end
            object cxDBVerticalGrid5xNoSJ: TcxDBEditorRow
              Properties.Caption = 'NoSJ'
              Properties.DataBinding.FieldName = 'xNoSJ'
              ID = 2
              ParentID = -1
              Index = 2
              Version = 1
            end
          end
        end
        object cxDBVerticalGrid6: TcxDBVerticalGrid
          Left = 1
          Top = 1
          Width = 386
          Height = 274
          Align = alLeft
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          OptionsData.CancelOnExit = False
          OptionsData.Appending = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Inserting = False
          ParentFont = False
          TabOrder = 1
          DataController.DataSource = MasterDs
          Version = 1
          object cxDBVerticalGrid6NoSO: TcxDBEditorRow
            Properties.DataBinding.FieldName = 'NoSO'
            ID = 9
            ParentID = -1
            Index = 0
            Version = 1
          end
          object cxDBVerticalGrid6Rute: TcxDBEditorRow
            Properties.DataBinding.FieldName = 'Rute'
            ID = 10
            ParentID = -1
            Index = 1
            Version = 1
          end
          object cxDBVerticalGrid6Pelanggan: TcxDBEditorRow
            Properties.DataBinding.FieldName = 'Pelanggan'
            ID = 11
            ParentID = -1
            Index = 2
            Version = 1
          end
          object cxDBVerticalGrid6Sopir: TcxDBEditorRow
            Properties.DataBinding.FieldName = 'Sopir'
            ID = 14
            ParentID = -1
            Index = 3
            Version = 1
          end
          object cxDBVerticalGrid6Crew: TcxDBEditorRow
            Properties.DataBinding.FieldName = 'Crew'
            ID = 15
            ParentID = -1
            Index = 4
            Version = 1
          end
          object cxDBVerticalGrid6Keterangan: TcxDBEditorRow
            Properties.DataBinding.FieldName = 'Keterangan'
            ID = 16
            ParentID = -1
            Index = 5
            Version = 1
          end
          object cxDBVerticalGrid6muat: TcxDBEditorRow
            Properties.DataBinding.FieldName = 'muat'
            ID = 36
            ParentID = -1
            Index = 6
            Version = 1
          end
          object cxDBVerticalGrid6bongkar: TcxDBEditorRow
            Properties.DataBinding.FieldName = 'bongkar'
            ID = 37
            ParentID = -1
            Index = 7
            Version = 1
          end
          object cxDBVerticalGrid6nama: TcxDBEditorRow
            Properties.DataBinding.FieldName = 'nama'
            ID = 38
            ParentID = -1
            Index = 8
            Version = 1
          end
          object cxDBVerticalGrid6nama_1: TcxDBEditorRow
            Properties.DataBinding.FieldName = 'nama_1'
            ID = 39
            ParentID = -1
            Index = 9
            Version = 1
          end
        end
        object pnl16: TPanel
          Left = 387
          Top = 1
          Width = 234
          Height = 274
          Align = alClient
          Caption = 'pnl16'
          TabOrder = 2
          object cxDBVerticalGrid7: TcxDBVerticalGrid
            Left = 1
            Top = 1
            Width = 232
            Height = 272
            Align = alClient
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            OptionsView.RowHeaderWidth = 85
            OptionsData.CancelOnExit = False
            OptionsData.Appending = False
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsData.Inserting = False
            ParentFont = False
            TabOrder = 0
            DataController.DataSource = MasterDs
            Version = 1
            object cxDBVerticalGrid7Armada: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'Armada'
              ID = 47
              ParentID = -1
              Index = 0
              Version = 1
            end
            object cxDBVerticalGrid7Ekor: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'Ekor'
              ID = 48
              ParentID = -1
              Index = 1
              Version = 1
            end
            object cxDBVerticalGrid7KirKepala: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'KirKepala'
              ID = 52
              ParentID = -1
              Index = 2
              Version = 1
            end
            object cxDBVerticalGrid7KirEkor: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'KirEkor'
              ID = 53
              ParentID = -1
              Index = 3
              Version = 1
            end
            object cxDBVerticalGrid7STNK: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'STNK'
              ID = 54
              ParentID = -1
              Index = 4
              Version = 1
            end
            object cxDBVerticalGrid7Pajak: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'Pajak'
              ID = 55
              ParentID = -1
              Index = 5
              Version = 1
            end
            object cxDBVerticalGrid7Tali: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'Tali'
              ID = 56
              ParentID = -1
              Index = 6
              Version = 1
            end
            object cxDBVerticalGrid7Rantai: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'Rantai'
              ID = 57
              ParentID = -1
              Index = 7
              Version = 1
            end
            object cxDBVerticalGrid7Kayu: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'Kayu'
              ID = 58
              ParentID = -1
              Index = 8
              Version = 1
            end
          end
        end
      end
    end
  end
  object MasterQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select m.*, r.muat, r.bongkar, p.nama, s.nama'
      'from MasterSJ m, Rute r, Pelanggan p, armada a, sopir s')
    UpdateObject = MasterUS
    Left = 68
    Top = 32
    object MasterQKodenota: TStringField
      FieldName = 'Kodenota'
      Required = True
    end
    object MasterQTgl: TDateTimeField
      FieldName = 'Tgl'
      Required = True
    end
    object MasterQNoSO: TStringField
      FieldName = 'NoSO'
      Required = True
    end
    object MasterQRute: TStringField
      FieldName = 'Rute'
      Required = True
      Size = 10
    end
    object MasterQPelanggan: TStringField
      FieldName = 'Pelanggan'
      Required = True
      Size = 10
    end
    object MasterQArmada: TStringField
      FieldName = 'Armada'
      Required = True
      Size = 10
    end
    object MasterQEkor: TStringField
      FieldName = 'Ekor'
      Size = 10
    end
    object MasterQSopir: TStringField
      FieldName = 'Sopir'
      Required = True
      Size = 10
    end
    object MasterQCrew: TStringField
      FieldName = 'Crew'
      Size = 10
    end
    object MasterQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 100
    end
    object MasterQKirKepala: TBooleanField
      FieldName = 'KirKepala'
      Required = True
    end
    object MasterQKirEkor: TBooleanField
      FieldName = 'KirEkor'
      Required = True
    end
    object MasterQSTNK: TBooleanField
      FieldName = 'STNK'
      Required = True
    end
    object MasterQPajak: TBooleanField
      FieldName = 'Pajak'
      Required = True
    end
    object MasterQTali: TIntegerField
      FieldName = 'Tali'
      Required = True
    end
    object MasterQRantai: TIntegerField
      FieldName = 'Rantai'
      Required = True
    end
    object MasterQKayu: TIntegerField
      FieldName = 'Kayu'
      Required = True
    end
    object MasterQxNama: TStringField
      FieldName = 'xNama'
      Size = 50
    end
    object MasterQxTgl: TDateTimeField
      FieldName = 'xTgl'
    end
    object MasterQxNoSJ: TStringField
      FieldName = 'xNoSJ'
    end
    object MasterQNamaMuat: TStringField
      FieldName = 'NamaMuat'
      Size = 50
    end
    object MasterQTelpMuat: TStringField
      FieldName = 'TelpMuat'
      Size = 12
    end
    object MasterQNamaBongkar: TStringField
      FieldName = 'NamaBongkar'
      Size = 50
    end
    object MasterQTelpBongkar: TStringField
      FieldName = 'TelpBongkar'
      Size = 12
    end
    object MasterQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object MasterQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object MasterQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object MasterQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object MasterQLaporan: TStringField
      FieldName = 'Laporan'
      Size = 100
    end
    object MasterQmuat: TStringField
      FieldName = 'muat'
      Required = True
      Size = 50
    end
    object MasterQbongkar: TStringField
      FieldName = 'bongkar'
      Required = True
      Size = 50
    end
    object MasterQnama: TStringField
      FieldName = 'nama'
      Required = True
      Size = 50
    end
    object MasterQnama_1: TStringField
      FieldName = 'nama_1'
      Required = True
      Size = 50
    end
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 113
    Top = 31
  end
  object MasterUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kodenota, Tgl, NoUrut, Pelanggan, TglOrder, JenisBarang, ' +
        'Harga, Rute, Keterangan, CreateDate, CreateBy, Operator, TglEntr' +
        'y, Status, NoSuratJalan, TglFollowUp, Muat, Bongkar'
      'from MasterSO'
      'where'
      '  Kodenota = :OLD_Kodenota')
    ModifySQL.Strings = (
      'update MasterSO'
      'set'
      '  Kodenota = :Kodenota,'
      '  Tgl = :Tgl,'
      '  NoUrut = :NoUrut,'
      '  Pelanggan = :Pelanggan,'
      '  TglOrder = :TglOrder,'
      '  JenisBarang = :JenisBarang,'
      '  Harga = :Harga,'
      '  Rute = :Rute,'
      '  Keterangan = :Keterangan,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  TglEntry = :TglEntry,'
      '  Status = :Status,'
      '  NoSuratJalan = :NoSuratJalan,'
      '  TglFollowUp = :TglFollowUp,'
      '  Muat = :Muat,'
      '  Bongkar = :Bongkar'
      'where'
      '  Kodenota = :OLD_Kodenota')
    InsertSQL.Strings = (
      'insert into MasterSO'
      
        '  (Kodenota, Tgl, NoUrut, Pelanggan, TglOrder, JenisBarang, Harg' +
        'a, Rute, Keterangan, CreateDate, CreateBy, Operator, TglEntry, S' +
        'tatus, NoSuratJalan, TglFollowUp, Muat, Bongkar)'
      'values'
      
        '  (:Kodenota, :Tgl, :NoUrut, :Pelanggan, :TglOrder, :JenisBarang' +
        ', :Harga, :Rute, :Keterangan, :CreateDate, :CreateBy, :Operator,' +
        ' :TglEntry, :Status, :NoSuratJalan, :TglFollowUp, :Muat, :Bongka' +
        'r)')
    DeleteSQL.Strings = (
      'delete from MasterSO'
      'where'
      '  Kodenota = :OLD_Kodenota')
    Left = 152
    Top = 30
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from armada order by kode desc')
    Left = 28
    Top = 32
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
end
