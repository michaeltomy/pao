object KodeHargaBeliFm: TKodeHargaBeliFm
  Left = 209
  Top = 245
  AutoScroll = False
  Caption = 'Kode Harga Beli'
  ClientHeight = 358
  ClientWidth = 731
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 731
    Height = 48
    Align = alTop
    TabOrder = 0
  end
  object pnl2: TPanel
    Left = 0
    Top = 288
    Width = 731
    Height = 51
    Align = alBottom
    TabOrder = 1
    object ExitBtn: TcxButton
      Left = 172
      Top = 10
      Width = 75
      Height = 25
      Caption = 'EXIT [F8]'
      TabOrder = 1
    end
    object DeleteBtn: TcxButton
      Left = 91
      Top = 10
      Width = 75
      Height = 25
      Caption = 'DELETE [F7]'
      TabOrder = 2
      Visible = False
    end
    object SaveBtn: TcxButton
      Left = 8
      Top = 10
      Width = 75
      Height = 25
      Caption = 'SAVE [F6]'
      TabOrder = 0
      OnClick = SaveBtnClick
    end
    object cxLabel2: TcxLabel
      Left = 264
      Top = 8
      Caption = 'New [F5]'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -21
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = []
      Style.IsFontAssigned = True
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 339
    Width = 731
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 48
    Width = 731
    Height = 240
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    object cxGrid1DBTableView1: TcxGridDBTableView
      Navigator.Visible = True
      OnCellDblClick = cxGrid1DBTableView1CellDblClick
      DataController.DataSource = MasterDs
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      NewItemRow.Visible = True
      OptionsBehavior.FocusCellOnTab = True
      OptionsBehavior.FocusFirstCellOnNewRecord = True
      OptionsBehavior.IncSearch = True
      OptionsData.Appending = True
      OptionsView.GroupByBox = False
      object cxGrid1DBTableView1Angka: TcxGridDBColumn
        DataBinding.FieldName = 'Angka'
      end
      object cxGrid1DBTableView1Kode: TcxGridDBColumn
        DataBinding.FieldName = 'Kode'
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object MasterQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from kodehargabeli')
    UpdateObject = MasterUS
    Left = 201
    Top = 9
    object MasterQAngka: TStringField
      FieldName = 'Angka'
      Required = True
      Size = 50
    end
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 50
    end
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 244
    Top = 6
  end
  object MasterUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      'select Angka, Kode'#13#10'from kodehargabeli'
      'where'
      '  Angka = :OLD_Angka and'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update kodehargabeli'
      'set'
      '  Angka = :Angka,'
      '  Kode = :Kode'
      'where'
      '  Angka = :OLD_Angka and'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into kodehargabeli'
      '  (Angka, Kode)'
      'values'
      '  (:Angka, :Kode)')
    DeleteSQL.Strings = (
      'delete from kodehargabeli'
      'where'
      '  Angka = :OLD_Angka and'
      '  Kode = :OLD_Kode')
    Left = 268
    Top = 10
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from pengeluaran order by kode desc')
    Left = 169
    Top = 7
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object SDQuery1: TSDQuery
    AutoRefresh = True
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from pengeluaran order by kode desc'
      '')
    Left = 705
    Top = 7
    object SDQuery1Kode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object SDQuery1Tanggal: TDateTimeField
      FieldName = 'Tanggal'
    end
    object SDQuery1Biaya: TCurrencyField
      FieldName = 'Biaya'
    end
    object SDQuery1Deskripsi: TStringField
      FieldName = 'Deskripsi'
      Size = 250
    end
  end
  object DataSource1: TDataSource
    DataSet = SDQuery1
    Left = 740
    Top = 6
  end
end
