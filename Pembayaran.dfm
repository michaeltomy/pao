object PembayaranFm: TPembayaranFm
  Left = 295
  Top = 51
  Width = 1023
  Height = 704
  Caption = 'Pembayaran'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 1007
    Height = 48
    Align = alTop
    TabOrder = 0
  end
  object pnl2: TPanel
    Left = 0
    Top = 589
    Width = 1007
    Height = 58
    Align = alBottom
    TabOrder = 1
    object SaveBtn: TcxButton
      Left = 32
      Top = 18
      Width = 81
      Height = 25
      Caption = 'BAYAR'
      TabOrder = 0
      OnClick = SaveBtnClick
    end
    object ExitBtn: TcxButton
      Left = 124
      Top = 18
      Width = 75
      Height = 25
      Caption = 'EXIT'
      TabOrder = 1
      Visible = False
      OnClick = ExitBtnClick
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 647
    Width = 1007
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 48
    Width = 1007
    Height = 541
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    TabStop = False
    object cxGrid1DBTableView1: TcxGridDBTableView
      DataController.DataSource = ViewDs
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.IncSearch = True
      OptionsSelection.CellSelect = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsSelection.UnselectFocusedRecordOnExit = False
      OptionsView.GroupByBox = False
      object cxGrid1DBTableView1Kode: TcxGridDBColumn
        DataBinding.FieldName = 'Kode'
        Width = 81
      end
      object cxGrid1DBTableView1Tanggal: TcxGridDBColumn
        DataBinding.FieldName = 'Tanggal'
        Width = 121
      end
      object cxGrid1DBTableView1NoUrut: TcxGridDBColumn
        DataBinding.FieldName = 'NoUrut'
        Width = 94
      end
      object cxGrid1DBTableView1Supplier: TcxGridDBColumn
        DataBinding.FieldName = 'NamaSupplier'
        Width = 163
      end
      object cxGrid1DBTableView1Penerima: TcxGridDBColumn
        DataBinding.FieldName = 'NamaPenerima'
        Width = 180
      end
      object cxGrid1DBTableView1HargaTotal: TcxGridDBColumn
        DataBinding.FieldName = 'HargaTotal'
        Width = 111
      end
      object cxGrid1DBTableView1JumlahTerbayar: TcxGridDBColumn
        DataBinding.FieldName = 'Sisa'
        Width = 124
      end
      object cxGrid1DBTableView1Status: TcxGridDBColumn
        DataBinding.FieldName = 'Status'
        Width = 118
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object MasterQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      
        'select a.*, m.nama, m.namaorangtua, m.sekolah, p.nama as namapak' +
        'et, p.jumlahpertemuan, p.masaberlakubulan, p.angsur, p.diskon, p' +
        '.harga, p.jenissekolah, p.tingkat, p.kelas from ambilpaket a, mu' +
        'rid m, paket p')
    UpdateObject = MasterUS
    Left = 649
    Top = 177
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
    end
    object MasterQMurid: TStringField
      FieldName = 'Murid'
      Required = True
    end
    object MasterQPaket: TStringField
      FieldName = 'Paket'
      Required = True
    end
    object MasterQTglAmbilPaket: TDateTimeField
      FieldName = 'TglAmbilPaket'
      Required = True
    end
    object MasterQTglJatuhTempo: TDateTimeField
      FieldName = 'TglJatuhTempo'
      Required = True
    end
    object MasterQLunas: TBooleanField
      FieldName = 'Lunas'
      Required = True
    end
    object MasterQSelesai: TBooleanField
      FieldName = 'Selesai'
      Required = True
    end
    object MasterQnama: TStringField
      FieldName = 'nama'
      Required = True
      Size = 50
    end
    object MasterQnamaorangtua: TStringField
      FieldName = 'namaorangtua'
      Size = 50
    end
    object MasterQsekolah: TStringField
      FieldName = 'sekolah'
      Size = 50
    end
    object MasterQnamapaket: TStringField
      FieldName = 'namapaket'
      Required = True
      Size = 50
    end
    object MasterQjumlahpertemuan: TIntegerField
      FieldName = 'jumlahpertemuan'
      Required = True
    end
    object MasterQmasaberlakubulan: TIntegerField
      FieldName = 'masaberlakubulan'
      Required = True
    end
    object MasterQangsur: TBooleanField
      FieldName = 'angsur'
      Required = True
    end
    object MasterQdiskon: TFloatField
      FieldName = 'diskon'
    end
    object MasterQharga: TCurrencyField
      FieldName = 'harga'
      Required = True
    end
    object MasterQjenissekolah: TStringField
      FieldName = 'jenissekolah'
      Size = 50
    end
    object MasterQtingkat: TStringField
      FieldName = 'tingkat'
      Required = True
      Size = 10
    end
    object MasterQkelas: TStringField
      FieldName = 'kelas'
      Size = 10
    end
    object MasterQJumlahBayar: TCurrencyField
      FieldName = 'JumlahBayar'
      Required = True
    end
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 660
    Top = 134
  end
  object MasterUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, Murid, Paket, JumlahBayar, TglAmbilPaket, TglJatuhT' +
        'empo, Lunas, Selesai'#13#10'from ambilpaket'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update ambilpaket'
      'set'
      '  Kode = :Kode,'
      '  Murid = :Murid,'
      '  Paket = :Paket,'
      '  JumlahBayar = :JumlahBayar,'
      '  TglAmbilPaket = :TglAmbilPaket,'
      '  TglJatuhTempo = :TglJatuhTempo,'
      '  Lunas = :Lunas,'
      '  Selesai = :Selesai'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into ambilpaket'
      
        '  (Kode, Murid, Paket, JumlahBayar, TglAmbilPaket, TglJatuhTempo' +
        ', Lunas, Selesai)'
      'values'
      
        '  (:Kode, :Murid, :Paket, :JumlahBayar, :TglAmbilPaket, :TglJatu' +
        'hTempo, :Lunas, :Selesai)')
    DeleteSQL.Strings = (
      'delete from ambilpaket'
      'where'
      '  Kode = :OLD_Kode')
    Left = 692
    Top = 178
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from ambilpaket order by kode desc')
    Left = 721
    Top = 151
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
    end
  end
  object PegawaiQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from pegawai')
    Left = 289
    Top = 255
    object PegawaiQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object PegawaiQNama: TStringField
      FieldName = 'Nama'
      Size = 250
    end
    object PegawaiQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 250
    end
    object PegawaiQKota: TStringField
      FieldName = 'Kota'
      Size = 100
    end
    object PegawaiQTelp: TStringField
      FieldName = 'Telp'
      Size = 200
    end
    object PegawaiQTglLahir: TDateTimeField
      FieldName = 'TglLahir'
    end
    object PegawaiQJabatan: TStringField
      FieldName = 'Jabatan'
      Size = 100
    end
    object PegawaiQAktif: TBooleanField
      FieldName = 'Aktif'
    end
  end
  object PaketQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from paket')
    Left = 713
    Top = 111
    object PaketQKode: TStringField
      FieldName = 'Kode'
      Required = True
    end
    object PaketQNama: TStringField
      FieldName = 'Nama'
      Required = True
      Size = 50
    end
    object PaketQJumlahPertemuan: TIntegerField
      FieldName = 'JumlahPertemuan'
      Required = True
    end
    object PaketQMasaBerlakuBulan: TIntegerField
      FieldName = 'MasaBerlakuBulan'
      Required = True
    end
    object PaketQAngsur: TBooleanField
      FieldName = 'Angsur'
      Required = True
    end
    object PaketQDiskon: TFloatField
      FieldName = 'Diskon'
    end
    object PaketQHarga: TCurrencyField
      FieldName = 'Harga'
      Required = True
    end
    object PaketQJenisSekolah: TStringField
      FieldName = 'JenisSekolah'
      Size = 50
    end
    object PaketQTingkat: TStringField
      FieldName = 'Tingkat'
      Required = True
      Size = 10
    end
    object PaketQKelas: TStringField
      FieldName = 'Kelas'
      Size = 10
    end
  end
  object SupplierQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from supplier'
      '')
    Left = 249
    Top = 239
    object SupplierQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object SupplierQNama: TStringField
      FieldName = 'Nama'
      Size = 200
    end
    object SupplierQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 200
    end
    object SupplierQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object SupplierQTelp: TStringField
      FieldName = 'Telp'
      Size = 200
    end
    object SupplierQPIC: TStringField
      FieldName = 'PIC'
      Size = 200
    end
    object SupplierQTelpPIC: TStringField
      FieldName = 'TelpPIC'
      Size = 200
    end
  end
  object ViewQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      
        'select *, (hargatotal-jumlahterbayar) as Sisa from penerimaanbah' +
        'an where status ="BELUM LUNAS"')
    Left = 185
    Top = 137
    object ViewQNamaSupplier: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaSupplier'
      LookupDataSet = SupplierQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Supplier'
      Size = 200
      Lookup = True
    end
    object ViewQNamaPenerima: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPenerima'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Penerima'
      Size = 200
      Lookup = True
    end
    object ViewQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object ViewQTanggal: TDateTimeField
      FieldName = 'Tanggal'
    end
    object ViewQNoUrut: TIntegerField
      FieldName = 'NoUrut'
    end
    object ViewQSupplier: TStringField
      FieldName = 'Supplier'
      Size = 10
    end
    object ViewQHargaTotal: TCurrencyField
      FieldName = 'HargaTotal'
    end
    object ViewQPenerima: TStringField
      FieldName = 'Penerima'
      Size = 10
    end
    object ViewQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object ViewQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object ViewQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object ViewQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object ViewQStatus: TStringField
      FieldName = 'Status'
      Size = 50
    end
    object ViewQJumlahTerbayar: TCurrencyField
      FieldName = 'JumlahTerbayar'
    end
    object ViewQsisa: TCurrencyField
      FieldName = 'sisa'
    end
  end
  object ViewDs: TDataSource
    DataSet = ViewQ
    Left = 228
    Top = 142
  end
  object KontrakQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from kontrak'
      ''
      '')
    Left = 737
    Top = 71
    object KontrakQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object KontrakQPelanggan: TStringField
      FieldName = 'Pelanggan'
      Required = True
      Size = 10
    end
    object KontrakQTglMulai: TDateTimeField
      FieldName = 'TglMulai'
      Required = True
    end
    object KontrakQTglSelesai: TDateTimeField
      FieldName = 'TglSelesai'
      Required = True
    end
    object KontrakQStatusRute: TStringField
      FieldName = 'StatusRute'
      Required = True
      Size = 50
    end
    object KontrakQRute: TStringField
      FieldName = 'Rute'
      Size = 10
    end
    object KontrakQJenisBarang: TStringField
      FieldName = 'JenisBarang'
      Size = 50
    end
    object KontrakQTonase: TFloatField
      FieldName = 'Tonase'
    end
    object KontrakQPanjang: TFloatField
      FieldName = 'Panjang'
    end
    object KontrakQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object KontrakQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object KontrakQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object KontrakQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object KontrakQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object KontrakQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object KontrakQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
  end
end
