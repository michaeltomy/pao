object PegawaiDropDownFm: TPegawaiDropDownFm
  Left = 216
  Top = 103
  Width = 1046
  Height = 498
  Caption = 'Pegawai Drop Down'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid1: TcxGrid
    Left = 0
    Top = 0
    Width = 1030
    Height = 460
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object cxGrid1DBTableView1: TcxGridDBTableView
      OnCellDblClick = cxGrid1DBTableView1CellDblClick
      DataController.DataSource = LPBDs
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.IncSearch = True
      OptionsData.Editing = False
      OptionsView.GroupByBox = False
      object cxGrid1DBTableView1Kode: TcxGridDBColumn
        DataBinding.FieldName = 'Kode'
      end
      object cxGrid1DBTableView1Nama: TcxGridDBColumn
        DataBinding.FieldName = 'Nama'
        Width = 257
      end
      object cxGrid1DBTableView1Alamat: TcxGridDBColumn
        DataBinding.FieldName = 'Alamat'
        Width = 204
      end
      object cxGrid1DBTableView1Kota: TcxGridDBColumn
        DataBinding.FieldName = 'Kota'
        Width = 158
      end
      object cxGrid1DBTableView1Telp: TcxGridDBColumn
        DataBinding.FieldName = 'Telp'
        Width = 103
      end
      object cxGrid1DBTableView1TglLahir: TcxGridDBColumn
        DataBinding.FieldName = 'TglLahir'
      end
      object cxGrid1DBTableView1Jabatan: TcxGridDBColumn
        DataBinding.FieldName = 'Jabatan'
        Width = 115
      end
      object cxGrid1DBTableView1Aktif: TcxGridDBColumn
        DataBinding.FieldName = 'Aktif'
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object PegawaiQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from pegawai')
    Left = 216
    Top = 368
    object PegawaiQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object PegawaiQNama: TStringField
      FieldName = 'Nama'
      Size = 250
    end
    object PegawaiQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 250
    end
    object PegawaiQKota: TStringField
      FieldName = 'Kota'
      Size = 100
    end
    object PegawaiQTelp: TStringField
      FieldName = 'Telp'
      Size = 200
    end
    object PegawaiQTglLahir: TDateTimeField
      FieldName = 'TglLahir'
    end
    object PegawaiQJabatan: TStringField
      FieldName = 'Jabatan'
      Size = 100
    end
    object PegawaiQAktif: TBooleanField
      FieldName = 'Aktif'
    end
  end
  object LPBDs: TDataSource
    DataSet = PegawaiQ
    Left = 248
    Top = 368
  end
end
