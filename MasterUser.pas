unit MasterUser;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxPC,
  dxSkinscxPCPainter, cxPCdxBarPopupMenu, dxTabbedMDI,
  dxSkinsdxNavBarPainter, dxNavBar, cxCheckBox, cxDBEdit, cxClasses;

type
  TMasterUserFm = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    MasterQ: TSDQuery;
    MasterDs: TDataSource;
    MasterVGrid: TcxDBVerticalGrid;
    StatusBar: TStatusBar;
    MasterUS: TSDUpdateSQL;
    KodeQ: TSDQuery;
    KodeQkode: TStringField;
    ExitBtn: TcxButton;
    DeleteBtn: TcxButton;
    SaveBtn: TcxButton;
    KodeEdit: TcxButtonEdit;
    lbl1: TLabel;
    SearchBtn: TcxButton;
    PegawaiQ: TSDQuery;
    MasterVGridPassword: TcxDBEditorRow;
    MasterVGridPegawai: TcxDBEditorRow;
    MasterVGridAktif: TcxDBEditorRow;
    MasterVGridNamaPegawai: TcxDBEditorRow;
    MasterVGridAlamatPegawai: TcxDBEditorRow;
    MasterVGridTglLahirPegawai: TcxDBEditorRow;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxDBCheckBox1: TcxDBCheckBox;
    cxDBCheckBox2: TcxDBCheckBox;
    cxDBCheckBox3: TcxDBCheckBox;
    cxDBCheckBox4: TcxDBCheckBox;
    cxDBCheckBox5: TcxDBCheckBox;
    cxDBCheckBox6: TcxDBCheckBox;
    cxDBCheckBox7: TcxDBCheckBox;
    cxDBCheckBox8: TcxDBCheckBox;
    cxDBCheckBox9: TcxDBCheckBox;
    cxDBCheckBox10: TcxDBCheckBox;
    cxDBCheckBox11: TcxDBCheckBox;
    cxDBCheckBox12: TcxDBCheckBox;
    cxDBCheckBox13: TcxDBCheckBox;
    cxDBCheckBox14: TcxDBCheckBox;
    cxDBCheckBox15: TcxDBCheckBox;
    cxDBCheckBox16: TcxDBCheckBox;
    cxDBCheckBox17: TcxDBCheckBox;
    cxDBCheckBox18: TcxDBCheckBox;
    cxDBCheckBox19: TcxDBCheckBox;
    cxDBCheckBox20: TcxDBCheckBox;
    cxDBCheckBox21: TcxDBCheckBox;
    cxDBCheckBox22: TcxDBCheckBox;
    cxDBCheckBox23: TcxDBCheckBox;
    cxDBCheckBox24: TcxDBCheckBox;
    cxDBCheckBox25: TcxDBCheckBox;
    cxDBCheckBox26: TcxDBCheckBox;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxDBCheckBox27: TcxDBCheckBox;
    MasterQKode: TStringField;
    MasterQUsername: TStringField;
    MasterQPassword: TStringField;
    MasterQPegawai: TStringField;
    PegawaiQKode: TStringField;
    PegawaiQNama: TStringField;
    PegawaiQAlamat: TStringField;
    PegawaiQKota: TStringField;
    PegawaiQTelp: TStringField;
    PegawaiQTglLahir: TDateTimeField;
    PegawaiQJabatan: TStringField;
    PegawaiQAktif: TBooleanField;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ExitBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure KodeEditEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SaveBtnClick(Sender: TObject);
    procedure MasterQAfterInsert(DataSet: TDataSet);
    procedure MasterQBeforePost(DataSet: TDataSet);
    procedure DeleteBtnClick(Sender: TObject);
    procedure SearchBtnClick(Sender: TObject);
    procedure MasterVGridEnter(Sender: TObject);
    procedure MasterVGridPegawaiEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent);
  end;

var
  MasterUserFm: TMasterUserFm;

  MasterOriSQL: string;
implementation

uses MenuUtama, DropDown, DM, Math, UserDropDown, PegawaiDropDown;

{$R *.dfm}

{ TMasterArmadaFm }

constructor TMasterUserFm.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
end;

procedure TMasterUserFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TMasterUserFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TMasterUserFm.ExitBtnClick(Sender: TObject);
begin
  Release;
end;

procedure TMasterUserFm.FormCreate(Sender: TObject);
begin
  DMFm:=TDMFm.Create(self);
  MasterOriSQL:=MasterQ.SQL.Text;
  //KodeEdit.Properties.MaxLength:=MasterQKode.Size;
end;

procedure TMasterUserFm.cxButtonEdit1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  KodeEdit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  KodeEditExit(sender);
  MasterVGrid.SetFocus;
end;

procedure TMasterUserFm.KodeEditEnter(Sender: TObject);
begin
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
  MasterVGrid.Enabled:=false;
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  SaveBtn.Enabled:=false;
  DeleteBtn.Enabled:=false;
  MasterQ.Close;
end;

procedure TMasterUserFm.FormShow(Sender: TObject);
begin
  KodeEdit.SetFocus;
  {kodeedit.Properties.Buttons[0].Enabled:=MenuUtamaFm.UserQInsertMasterUser.AsBoolean;
  SaveBtn.Enabled:=menuutamafm.UserQUpdateMasterUser.AsBoolean;
  DeleteBtn.Enabled:=menuutamafm.UserQDeleteMasterUser.AsBoolean; }
  PegawaiQ.Open;
end;

procedure TMasterUserFm.KodeEditExit(Sender: TObject);
begin
  if KodeEdit.Text<>'' then
  begin
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.username like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;   {
      MasterQMasterBarang.AsBoolean:=true;
      MasterQMasterKategori.AsBoolean:=true;
      MasterQMasterPelanggan.AsBoolean:=true;
      MasterQMasterPegawai.AsBoolean:=true;
      MasterQMasterPengguna.AsBoolean:=true;
      MasterQMasterToko.AsBoolean:=true;
      MasterQLihatHargaBeli.AsBoolean:=true;
      MasterQViewBarang.AsBoolean:=true;
      MasterQViewKategori.AsBoolean:=true;
      MasterQViewPelanggan.AsBoolean:=true;
      MasterQViewToko.AsBoolean:=true;
      MasterQViewpegawai.AsBoolean:=true;
      MasterQTransferMasuk.AsBoolean:=true;
      MasterQTransferKeluar.AsBoolean:=true;
      MasterQPenjualan.AsBoolean:=true;
      MasterQPembayaran.AsBoolean:=true;
      MasterQSetorBank.AsBoolean:=true;
      MasterQPengeluaran.AsBoolean:=true;
      MasterQLaporanPenjualan.AsBoolean:=true;
      MasterQLaporanTransferMasuk.AsBoolean:=true;
      MasterQLaporanTransferKeluar.AsBoolean:=true;
      MasterQLaporanPengeluaran.AsBoolean:=true;
      MasterQLaporanSetoranBank.AsBoolean:=true;
      MasterQLaporanKas.AsBoolean:=true;
      MasterQLaporanLabaKotor.AsBoolean:=true;
      MasterQLaporanLabaBersih.AsBoolean:=true;
      MasterQLaporanLabaKotor.AsBoolean:=true;   }
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      DeleteBtn.Enabled:=true;
      MasterQ.Edit;
      DeleteBtn.Enabled:=true;
      //DeleteBtn.Enabled:=menuutamafm.UserQDeleteMasterUser.AsBoolean;
    end;
    SaveBtn.Enabled:=true;
    //SaveBtn.Enabled:=menuutamafm.UserQUpdateMasterUser.AsBoolean;
    MasterVGrid.Enabled:=True;
    MasterVGrid.SetFocus;
  end;
end;

procedure TMasterUserFm.KodeEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_F5) then
  begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
  end;
  if (Key=VK_escape) then
  begin
    ExitBtn.Click;
  end;
end;

procedure TMasterUserFm.SaveBtnClick(Sender: TObject);
begin
  MasterQUsername.AsString:=KodeEdit.Text;
  MasterQ.Post;
  MenuUtamaFm.Database1.StartTransaction;
  try
    MasterQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    ShowMessage('User dengan Kode ' + KodeEdit.Text + ' telah disimpan');
    KodeEdit.SetFocus;
  except
  on E : Exception do
              ShowMessage(E.ClassName+' error raised, with message : '+E.Message);
    {MenuUtamaFm.Database1.Rollback;
    MasterQ.RollbackUpdates;
    ShowMessage('Penyimpanan Gagal'); }
  end;
end;

procedure TMasterUserFm.MasterQAfterInsert(DataSet: TDataSet);
begin
  //DMFm.GetDateQ.Open;
  //MasterQCreateDate.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  //MasterQCreateBy.AsString:=User;
  //DMFm.GetDateQ.Close;
end;

procedure TMasterUserFm.MasterQBeforePost(DataSet: TDataSet);
begin
  //DMFm.GetDateQ.Open;
  //MasterQTglEntry.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  //MasterQOperator.AsString:=User;
  //DMFm.GetDateQ.Close;
end;

procedure TMasterUserFm.DeleteBtnClick(Sender: TObject);
begin
  if MessageDlg('Hapus User '+KodeEdit.Text+' Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     MenuUtamaFm.Database1.StartTransaction;
     try
       MasterQ.Delete;
       MasterQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;
       MessageDlg('User telah dihapus.',mtInformation,[mbOK],0);
     except
       MenuUtamaFm.Database1.Rollback;
       MasterQ.RollbackUpdates;
       MessageDlg('User pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
     end;
     KodeEdit.SetFocus;
  end;
end;

procedure TMasterUserFm.SearchBtnClick(Sender: TObject);
begin
    MasterQ.SQL.Text:=MasterOriSQL;
    UserDropDownFm:=TUserDropdownfm.Create(Self);
    if UserDropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=UserDropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    UserDropDownFm.Release;
end;

procedure TMasterUserFm.MasterVGridEnter(Sender: TObject);
begin
 // MasterVGrid.FocusRow(MasterVGridNama);
end;

procedure TMasterUserFm.MasterVGridPegawaiEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    PegawaiDropDownFm:=TPegawaiDropdownfm.Create(Self);
    if PegawaiDropDownFm.ShowModal=MrOK then
    begin
      Masterq.Edit;
      MasterQPegawai.AsString:=PegawaiDropDownFm.kode;
    end;
    PegawaiDropDownFm.Release;
end;

end.
