object NotaSOFm: TNotaSOFm
  Left = 342
  Top = 129
  Width = 642
  Height = 615
  Caption = 'PENAWARAN / ORDER'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object pnl2: TPanel
    Left = 0
    Top = 525
    Width = 626
    Height = 52
    Align = alBottom
    TabOrder = 0
    object ExitBtn: TcxButton
      Left = 91
      Top = 10
      Width = 75
      Height = 25
      Caption = 'EXIT'
      TabOrder = 0
      OnClick = ExitBtnClick
    end
    object SaveBtn: TcxButton
      Left = 10
      Top = 10
      Width = 75
      Height = 25
      Caption = 'SAVE'
      TabOrder = 1
      OnClick = SaveBtnClick
    end
  end
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 626
    Height = 65
    Align = alTop
    TabOrder = 1
    object lbl1: TLabel
      Left = 12
      Top = 12
      Width = 367
      Height = 37
      Caption = 'PENAWARAN / ORDER'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -32
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object pnl6: TPanel
      Left = 381
      Top = 1
      Width = 244
      Height = 63
      Align = alRight
      Caption = 'pnl6'
      TabOrder = 0
      object cxDBVerticalGrid1: TcxDBVerticalGrid
        Left = 1
        Top = 1
        Width = 242
        Height = 61
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        OptionsView.ScrollBars = ssNone
        OptionsView.RowHeaderWidth = 85
        OptionsBehavior.AlwaysShowEditor = True
        OptionsBehavior.CellHints = False
        OptionsBehavior.GoToNextCellOnEnter = True
        OptionsBehavior.GoToNextCellOnTab = True
        OptionsBehavior.ImmediateEditor = False
        OptionsBehavior.BandSizing = False
        OptionsBehavior.HeaderSizing = False
        OptionsData.CancelOnExit = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Inserting = False
        ParentFont = False
        TabOrder = 0
        DataController.DataSource = MasterDs
        Version = 1
        object cxDBVerticalGrid1Kodenota: TcxDBEditorRow
          Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
          Properties.EditProperties.Buttons = <
            item
              Caption = '+'
              Default = True
              Kind = bkText
            end
            item
              Kind = bkEllipsis
            end>
          Properties.EditProperties.ReadOnly = True
          Properties.EditProperties.OnButtonClick = cxDBVerticalGrid1KodenotaEditPropertiesButtonClick
          Properties.DataBinding.FieldName = 'Kodenota'
          ID = 0
          ParentID = -1
          Index = 0
          Version = 1
        end
        object cxDBVerticalGrid1Tgl: TcxDBEditorRow
          Properties.EditPropertiesClassName = 'TcxDateEditProperties'
          Properties.DataBinding.FieldName = 'Tgl'
          ID = 1
          ParentID = -1
          Index = 1
          Version = 1
        end
      end
    end
  end
  object pnl3: TPanel
    Left = 0
    Top = 65
    Width = 626
    Height = 460
    Align = alClient
    Caption = 'pnl3'
    TabOrder = 2
    object pnl4: TPanel
      Left = 1
      Top = 1
      Width = 624
      Height = 296
      Align = alTop
      Caption = 'pnl4'
      TabOrder = 0
      object MasterVGrid: TcxDBVerticalGrid
        Left = 1
        Top = 1
        Width = 622
        Height = 294
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        OptionsView.ScrollBars = ssVertical
        OptionsView.ShowEditButtons = ecsbFocused
        OptionsView.AutoScaleBands = False
        OptionsBehavior.CellHints = False
        OptionsBehavior.GoToNextCellOnEnter = True
        OptionsBehavior.GoToNextCellOnTab = True
        OptionsBehavior.BandSizing = False
        OptionsBehavior.HeaderSizing = False
        OptionsData.CancelOnExit = False
        OptionsData.Appending = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Inserting = False
        ParentFont = False
        TabOrder = 0
        OnKeyDown = MasterVGridKeyDown
        DataController.DataSource = MasterDs
        Version = 1
        object MasterVGridPelanggan: TcxDBEditorRow
          Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
          Properties.EditProperties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.EditProperties.OnButtonClick = MasterVGridPelangganEditPropertiesButtonClick
          Properties.EditProperties.OnValidate = MasterVGridPelangganEditPropertiesValidate
          Properties.DataBinding.FieldName = 'Pelanggan'
          ID = 0
          ParentID = -1
          Index = 0
          Version = 1
        end
        object MasterVGridNoTelp: TcxDBEditorRow
          Properties.DataBinding.FieldName = 'NoTelp'
          ID = 1
          ParentID = 0
          Index = 0
          Version = 1
        end
        object MasterVGridNama: TcxDBEditorRow
          Properties.DataBinding.FieldName = 'Nama'
          ID = 2
          ParentID = 0
          Index = 1
          Version = 1
        end
        object MasterVGridAlamat: TcxDBEditorRow
          Properties.DataBinding.FieldName = 'Alamat'
          ID = 3
          ParentID = 0
          Index = 2
          Version = 1
        end
        object MasterVGridTglOrder: TcxDBEditorRow
          Properties.DataBinding.FieldName = 'TglOrder'
          ID = 4
          ParentID = -1
          Index = 1
          Version = 1
        end
        object MasterVGridJenisBarang: TcxDBEditorRow
          Properties.DataBinding.FieldName = 'JenisBarang'
          ID = 5
          ParentID = -1
          Index = 2
          Version = 1
        end
        object MasterVGridRute: TcxDBEditorRow
          Expanded = False
          Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
          Properties.EditProperties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.EditProperties.OnButtonClick = MasterVGridRuteEditPropertiesButtonClick
          Properties.DataBinding.FieldName = 'Rute'
          ID = 6
          ParentID = -1
          Index = 3
          Version = 1
        end
        object MasterVGridMuat: TcxDBEditorRow
          Properties.DataBinding.FieldName = 'Muat'
          ID = 7
          ParentID = 6
          Index = 0
          Version = 1
        end
        object MasterVGridBongkar: TcxDBEditorRow
          Properties.DataBinding.FieldName = 'Bongkar'
          ID = 8
          ParentID = 6
          Index = 1
          Version = 1
        end
        object MasterVGridHarga_1: TcxDBEditorRow
          Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.EditProperties.Alignment.Horz = taLeftJustify
          Properties.EditProperties.DisplayFormat = 'Rp.0,00;(Rp.0,00)'
          Properties.DataBinding.FieldName = 'Harga_1'
          ID = 9
          ParentID = 6
          Index = 2
          Version = 1
        end
        object MasterVGridPanjang: TcxDBEditorRow
          Properties.DataBinding.FieldName = 'Panjang'
          ID = 10
          ParentID = -1
          Index = 4
          Version = 1
        end
        object MasterVGridBerat: TcxDBEditorRow
          Properties.DataBinding.FieldName = 'Berat'
          ID = 11
          ParentID = -1
          Index = 5
          Version = 1
        end
        object MasterVGridHarga: TcxDBEditorRow
          Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.EditProperties.Alignment.Horz = taLeftJustify
          Properties.EditProperties.DisplayFormat = 'Rp.0,00;(Rp.0,00)'
          Properties.DataBinding.FieldName = 'Harga'
          ID = 12
          ParentID = -1
          Index = 6
          Version = 1
        end
      end
    end
    object pnl5: TPanel
      Left = 1
      Top = 297
      Width = 624
      Height = 162
      Align = alClient
      TabOrder = 1
      object pnl7: TPanel
        Left = 1
        Top = 1
        Width = 622
        Height = 25
        Align = alTop
        Caption = 'Status'
        TabOrder = 0
      end
      object pnl8: TPanel
        Left = 1
        Top = 26
        Width = 205
        Height = 75
        Align = alLeft
        TabOrder = 1
        object cxCheckBox3: TcxCheckBox
          Left = 75
          Top = 4
          Caption = 'Deal'
          TabOrder = 0
          OnClick = cxCheckBox3Click
          Width = 74
        end
      end
      object pnl9: TPanel
        Left = 422
        Top = 26
        Width = 201
        Height = 75
        Align = alRight
        TabOrder = 2
        object cxCheckBox2: TcxCheckBox
          Left = 74
          Top = 4
          Caption = 'Batal'
          TabOrder = 0
          OnClick = cxCheckBox2Click
          Width = 121
        end
      end
      object pnl10: TPanel
        Left = 206
        Top = 26
        Width = 216
        Height = 75
        Align = alClient
        TabOrder = 3
        object cxCheckBox1: TcxCheckBox
          Left = 80
          Top = 4
          Caption = 'Follow Up'
          TabOrder = 0
          OnClick = cxCheckBox1Click
          Width = 121
        end
        object cxDBVerticalGrid3: TcxDBVerticalGrid
          Left = 1
          Top = 35
          Width = 214
          Height = 39
          Align = alBottom
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          OptionsView.ScrollBars = ssVertical
          OptionsView.RowHeaderWidth = 99
          OptionsData.CancelOnExit = False
          OptionsData.Appending = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Inserting = False
          ParentFont = False
          TabOrder = 1
          DataController.DataSource = MasterDs
          Version = 1
          object cxDBVerticalGrid3TglFollowUp: TcxDBEditorRow
            Properties.DataBinding.FieldName = 'TglFollowUp'
            ID = 0
            ParentID = -1
            Index = 0
            Version = 1
          end
        end
      end
      object pnl11: TPanel
        Left = 1
        Top = 101
        Width = 622
        Height = 60
        Align = alBottom
        Caption = 'pnl11'
        TabOrder = 4
        object cxDBVerticalGrid2: TcxDBVerticalGrid
          Left = 204
          Top = 1
          Width = 417
          Height = 58
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          OptionsView.ScrollBars = ssVertical
          OptionsView.RowHeaderWidth = 116
          OptionsData.CancelOnExit = False
          OptionsData.Appending = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Inserting = False
          ParentFont = False
          TabOrder = 0
          DataController.DataSource = MasterDs
          Version = 1
          object cxDBVerticalGrid2Keterangan: TcxDBEditorRow
            Properties.DataBinding.FieldName = 'Keterangan'
            ID = 0
            ParentID = -1
            Index = 0
            Version = 1
          end
        end
        object pnl12: TPanel
          Left = 1
          Top = 1
          Width = 203
          Height = 58
          Align = alLeft
          TabOrder = 1
        end
      end
    end
  end
  object MasterQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      
        'select * from masterso m, pelanggan p, rute r where m.pelanggan=' +
        'p.kode and m.rute=r.kode')
    UpdateObject = MasterUS
    Left = 330
    Top = 116
    object MasterQKodenota: TStringField
      FieldName = 'Kodenota'
      Required = True
    end
    object MasterQTgl: TDateTimeField
      FieldName = 'Tgl'
      Required = True
    end
    object MasterQPelanggan: TStringField
      FieldName = 'Pelanggan'
      Required = True
      Size = 10
    end
    object MasterQTglOrder: TDateTimeField
      FieldName = 'TglOrder'
    end
    object MasterQJenisBarang: TStringField
      FieldName = 'JenisBarang'
      Size = 100
    end
    object MasterQHarga: TCurrencyField
      FieldName = 'Harga'
      Required = True
    end
    object MasterQRute: TStringField
      FieldName = 'Rute'
      Required = True
      Size = 10
    end
    object MasterQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 100
    end
    object MasterQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object MasterQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object MasterQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object MasterQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object MasterQStatus: TIntegerField
      FieldName = 'Status'
      Required = True
    end
    object MasterQTglFollowUp: TDateTimeField
      FieldName = 'TglFollowUp'
    end
    object MasterQPanjang: TFloatField
      FieldName = 'Panjang'
      Required = True
    end
    object MasterQBerat: TFloatField
      FieldName = 'Berat'
      Required = True
    end
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQNama: TStringField
      FieldName = 'Nama'
      Required = True
      Size = 50
    end
    object MasterQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 100
    end
    object MasterQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 12
    end
    object MasterQCreateDate_1: TDateTimeField
      FieldName = 'CreateDate_1'
    end
    object MasterQCreateBy_1: TStringField
      FieldName = 'CreateBy_1'
      Size = 10
    end
    object MasterQOperator_1: TStringField
      FieldName = 'Operator_1'
      Size = 10
    end
    object MasterQTglEntry_1: TDateTimeField
      FieldName = 'TglEntry_1'
    end
    object MasterQKode_1: TStringField
      FieldName = 'Kode_1'
      Required = True
      Size = 10
    end
    object MasterQMuat: TStringField
      FieldName = 'Muat'
      Required = True
      Size = 50
    end
    object MasterQBongkar: TStringField
      FieldName = 'Bongkar'
      Required = True
      Size = 50
    end
    object MasterQHarga_1: TCurrencyField
      FieldName = 'Harga_1'
      Required = True
    end
    object MasterQCreateDate_2: TDateTimeField
      FieldName = 'CreateDate_2'
    end
    object MasterQCreateBy_2: TStringField
      FieldName = 'CreateBy_2'
      Size = 10
    end
    object MasterQOperator_2: TStringField
      FieldName = 'Operator_2'
      Size = 10
    end
    object MasterQTglEntry_2: TDateTimeField
      FieldName = 'TglEntry_2'
    end
    object MasterQJarak: TIntegerField
      FieldName = 'Jarak'
    end
    object MasterQPoin: TFloatField
      FieldName = 'Poin'
    end
    object MasterQBorongan: TCurrencyField
      FieldName = 'Borongan'
    end
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 386
    Top = 110
  end
  object MasterUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      'select *'
      'from MasterSO'
      'where'
      '  Kodenota = :OLD_Kodenota')
    ModifySQL.Strings = (
      'update MasterSO'
      'set'
      '  Kodenota = :Kodenota,'
      '  Tgl = :Tgl,'
      '  Pelanggan = :Pelanggan,'
      '  TglOrder = :TglOrder,'
      '  JenisBarang = :JenisBarang,'
      '  Harga = :Harga,'
      '  Rute = :Rute,'
      '  Keterangan = :Keterangan,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  TglEntry = :TglEntry,'
      '  Status = :Status,'
      '  TglFollowUp = :TglFollowUp,'
      '  Panjang = :Panjang,'
      '  Berat = :Berat'
      'where'
      '  Kodenota = :OLD_Kodenota')
    InsertSQL.Strings = (
      'insert into MasterSO'
      
        '  (Kodenota, Tgl, Pelanggan, TglOrder, JenisBarang, Harga, Rute,' +
        ' Keterangan, CreateDate, CreateBy, Operator, TglEntry, Status, T' +
        'glFollowUp, Panjang, Berat)'
      'values'
      
        '  (:Kodenota, :Tgl, :Pelanggan, :TglOrder, :JenisBarang, :Harga,' +
        ' :Rute, :Keterangan, :CreateDate, :CreateBy, :Operator, :TglEntr' +
        'y, :Status, :TglFollowUp, :Panjang, :Berat)'
      '')
    DeleteSQL.Strings = (
      'delete from MasterSO'
      'where'
      '  Kodenota = :OLD_Kodenota')
    Left = 448
    Top = 112
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select * from pelanggan where kode like '#39'%'#39' + :text + '#39'%'#39' or nam' +
        'a like '#39'%'#39' + :text + '#39'%'#39)
    Left = 270
    Top = 117
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'text'
        ParamType = ptInput
      end>
    object KodeQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object KodeQNama: TStringField
      FieldName = 'Nama'
      Required = True
      Size = 50
    end
    object KodeQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 100
    end
    object KodeQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 12
    end
  end
  object Kode2Q: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kodenota from masterso order by kodenota desc')
    Left = 326
    Top = 197
    object Kode2Qkodenota: TStringField
      FieldName = 'kodenota'
      Required = True
    end
  end
  object RuteQ: TSDQuery
    AutoRefresh = True
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from rute')
    Left = 406
    Top = 237
    object RuteQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object RuteQMuat: TStringField
      FieldName = 'Muat'
      Required = True
      Size = 50
    end
    object RuteQBongkar: TStringField
      FieldName = 'Bongkar'
      Required = True
      Size = 50
    end
    object RuteQHarga: TCurrencyField
      FieldName = 'Harga'
      Required = True
    end
    object RuteQJarak: TIntegerField
      FieldName = 'Jarak'
    end
    object RuteQPoin: TFloatField
      FieldName = 'Poin'
    end
    object RuteQBorongan: TCurrencyField
      FieldName = 'Borongan'
    end
  end
end
