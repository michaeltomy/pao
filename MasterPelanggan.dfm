object MasterPelangganFm: TMasterPelangganFm
  Left = 619
  Top = 163
  Width = 517
  Height = 480
  AutoSize = True
  BorderIcons = [biSystemMenu]
  Caption = 'Master Pelanggan'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 501
    Height = 48
    Align = alTop
    TabOrder = 0
    object lbl1: TLabel
      Left = 8
      Top = 13
      Width = 25
      Height = 13
      Caption = 'Kode'
    end
    object KodeEdit: TcxButtonEdit
      Left = 40
      Top = 10
      Properties.Buttons = <
        item
          Caption = '+'
          Default = True
          Kind = bkText
        end>
      Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      TabOrder = 0
      OnEnter = KodeEditEnter
      OnExit = KodeEditExit
      OnKeyDown = KodeEditKeyDown
      Width = 121
    end
    object SearchBtn: TcxButton
      Left = 168
      Top = 10
      Width = 57
      Height = 21
      Caption = 'Search'
      TabOrder = 1
      OnClick = SearchBtnClick
    end
  end
  object pnl2: TPanel
    Left = 0
    Top = 372
    Width = 501
    Height = 51
    Align = alBottom
    TabOrder = 2
    object ExitBtn: TcxButton
      Left = 172
      Top = 10
      Width = 75
      Height = 25
      Caption = 'EXIT'
      TabOrder = 2
      OnClick = ExitBtnClick
    end
    object DeleteBtn: TcxButton
      Left = 91
      Top = 10
      Width = 75
      Height = 25
      Caption = 'DELETE'
      TabOrder = 1
      OnClick = DeleteBtnClick
    end
    object SaveBtn: TcxButton
      Left = 8
      Top = 10
      Width = 75
      Height = 25
      Caption = 'SAVE'
      TabOrder = 0
      OnClick = SaveBtnClick
    end
    object cxLabel1: TcxLabel
      Left = 424
      Top = 2
      Caption = '* required fields'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clRed
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = []
      Style.IsFontAssigned = True
    end
  end
  object MasterVGrid: TcxDBVerticalGrid
    Left = 0
    Top = 48
    Width = 501
    Height = 324
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    OptionsView.ScrollBars = ssNone
    OptionsView.RowHeaderWidth = 114
    OptionsBehavior.GoToNextCellOnEnter = True
    OptionsBehavior.GoToNextCellOnTab = True
    OptionsData.CancelOnExit = False
    OptionsData.Appending = False
    OptionsData.Deleting = False
    OptionsData.DeletingConfirmation = False
    OptionsData.Inserting = False
    ParentFont = False
    TabOrder = 1
    DataController.DataSource = MasterDs
    Version = 1
    object MasterVGridNama: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Nama'
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object MasterVGridAlamat: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Alamat'
      ID = 1
      ParentID = -1
      Index = 1
      Version = 1
    end
    object MasterVGridKota: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Kota'
      ID = 2
      ParentID = -1
      Index = 2
      Version = 1
    end
    object MasterVGridTelp: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Telp'
      ID = 3
      ParentID = -1
      Index = 3
      Version = 1
    end
    object MasterVGridHP: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'HP'
      ID = 4
      ParentID = -1
      Index = 4
      Version = 1
    end
    object MasterVGridTglLahir: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxDateEditProperties'
      Properties.EditProperties.DateOnError = deNull
      Properties.EditProperties.ImmediateDropDownWhenActivated = True
      Properties.EditProperties.ImmediateDropDownWhenKeyPressed = True
      Properties.DataBinding.FieldName = 'TglLahir'
      ID = 5
      ParentID = -1
      Index = 5
      Version = 1
    end
    object MasterVGridAktif: TcxDBEditorRow
      Height = 17
      Properties.DataBinding.FieldName = 'Aktif'
      ID = 6
      ParentID = -1
      Index = 6
      Version = 1
    end
    object MasterVGridFoto: TcxDBEditorRow
      Height = 126
      Properties.EditPropertiesClassName = 'TcxImageProperties'
      Properties.EditProperties.GraphicClassName = 'TJPEGImage'
      Properties.DataBinding.FieldName = 'Foto'
      ID = 7
      ParentID = -1
      Index = 7
      Version = 1
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 423
    Width = 501
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object MasterQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select * from pelanggan')
    UpdateObject = MasterUS
    Left = 313
    Top = 9
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQNama: TStringField
      FieldName = 'Nama'
      Size = 250
    end
    object MasterQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 250
    end
    object MasterQKota: TStringField
      FieldName = 'Kota'
      Size = 100
    end
    object MasterQTelp: TStringField
      FieldName = 'Telp'
      Size = 50
    end
    object MasterQHP: TStringField
      FieldName = 'HP'
      Size = 50
    end
    object MasterQTglLahir: TDateTimeField
      FieldName = 'TglLahir'
    end
    object MasterQFoto: TBlobField
      FieldName = 'Foto'
    end
    object MasterQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object MasterQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object MasterQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object MasterQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object MasterQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object MasterQDeleted: TBooleanField
      FieldName = 'Deleted'
    end
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 380
    Top = 14
  end
  object MasterUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, Nama, Alamat, Kota, Telp, HP, TglLahir, Foto, Aktif' +
        ', CreateDate, CreateBy, TglEntry, Operator, Deleted'#13#10'from pelang' +
        'gan'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update pelanggan'
      'set'
      '  Kode = :Kode,'
      '  Nama = :Nama,'
      '  Alamat = :Alamat,'
      '  Kota = :Kota,'
      '  Telp = :Telp,'
      '  HP = :HP,'
      '  TglLahir = :TglLahir,'
      '  Foto = :Foto,'
      '  Aktif = :Aktif,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  TglEntry = :TglEntry,'
      '  Operator = :Operator,'
      '  Deleted = :Deleted'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into pelanggan'
      
        '  (Kode, Nama, Alamat, Kota, Telp, HP, TglLahir, Foto, Aktif, Cr' +
        'eateDate, CreateBy, TglEntry, Operator, Deleted)'
      'values'
      
        '  (:Kode, :Nama, :Alamat, :Kota, :Telp, :HP, :TglLahir, :Foto, :' +
        'Aktif, :CreateDate, :CreateBy, :TglEntry, :Operator, :Deleted)')
    DeleteSQL.Strings = (
      'delete from pelanggan'
      'where'
      '  Kode = :OLD_Kode')
    Left = 452
    Top = 18
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from pelanggan order by kode desc')
    Left = 265
    Top = 7
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
end
