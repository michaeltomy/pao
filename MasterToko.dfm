object MasterTokoFm: TMasterTokoFm
  Left = 390
  Top = 204
  Width = 517
  Height = 362
  BorderIcons = [biSystemMenu]
  Caption = 'Master Toko'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 501
    Height = 48
    Align = alTop
    TabOrder = 0
    object lbl1: TLabel
      Left = 8
      Top = 13
      Width = 25
      Height = 13
      Caption = 'Kode'
    end
    object KodeEdit: TcxButtonEdit
      Left = 40
      Top = 10
      Properties.Buttons = <
        item
          Caption = '+'
          Default = True
          Kind = bkText
        end>
      Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      TabOrder = 0
      OnEnter = KodeEditEnter
      OnExit = KodeEditExit
      OnKeyDown = KodeEditKeyDown
      Width = 121
    end
    object SearchBtn: TcxButton
      Left = 168
      Top = 10
      Width = 57
      Height = 21
      Caption = 'Search'
      TabOrder = 1
      TabStop = False
      OnClick = SearchBtnClick
    end
  end
  object pnl2: TPanel
    Left = 0
    Top = 254
    Width = 501
    Height = 51
    Align = alBottom
    TabOrder = 2
    object ExitBtn: TcxButton
      Left = 172
      Top = 10
      Width = 75
      Height = 25
      Caption = 'EXIT'
      TabOrder = 1
      OnClick = ExitBtnClick
    end
    object DeleteBtn: TcxButton
      Left = 91
      Top = 10
      Width = 75
      Height = 25
      Caption = 'DELETE'
      TabOrder = 2
      TabStop = False
      OnClick = DeleteBtnClick
    end
    object SaveBtn: TcxButton
      Left = 8
      Top = 10
      Width = 75
      Height = 25
      Caption = 'SAVE'
      TabOrder = 0
      OnClick = SaveBtnClick
    end
    object cxLabel1: TcxLabel
      Left = 416
      Top = 2
      Caption = '* required fields'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clRed
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = []
      Style.IsFontAssigned = True
    end
  end
  object MasterVGrid: TcxDBVerticalGrid
    Left = 0
    Top = 48
    Width = 501
    Height = 206
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    OptionsView.ScrollBars = ssNone
    OptionsView.RowHeaderWidth = 183
    OptionsBehavior.GoToNextCellOnEnter = True
    OptionsBehavior.GoToNextCellOnTab = True
    OptionsData.CancelOnExit = False
    OptionsData.Appending = False
    OptionsData.Deleting = False
    OptionsData.DeletingConfirmation = False
    OptionsData.Inserting = False
    ParentFont = False
    TabOrder = 1
    OnEnter = MasterVGridEnter
    OnExit = MasterVGridExit
    DataController.DataSource = MasterDs
    Version = 1
    object MasterVGridNamaToko: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NamaToko'
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object MasterVGridAlamat: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Alamat'
      ID = 1
      ParentID = -1
      Index = 1
      Version = 1
    end
    object MasterVGridKota: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Kota'
      ID = 2
      ParentID = -1
      Index = 2
      Version = 1
    end
    object MasterVGridTelp: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Telp'
      ID = 3
      ParentID = -1
      Index = 3
      Version = 1
    end
    object MasterVGridAktif: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Aktif'
      ID = 4
      ParentID = -1
      Index = 4
      Version = 1
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 305
    Width = 501
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object MasterQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select * from toko')
    UpdateObject = MasterUS
    Left = 345
    Top = 9
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQNamaToko: TStringField
      FieldName = 'NamaToko'
      Size = 250
    end
    object MasterQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 250
    end
    object MasterQKota: TStringField
      FieldName = 'Kota'
      Size = 100
    end
    object MasterQTelp: TStringField
      FieldName = 'Telp'
      Size = 50
    end
    object MasterQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object MasterQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object MasterQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object MasterQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object MasterQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object MasterQDeleted: TBooleanField
      FieldName = 'Deleted'
    end
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 404
    Top = 6
  end
  object MasterUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, NamaToko, Alamat, Kota, Telp, Aktif, CreateDate, Cr' +
        'eateBy, TglEntry, Operator, Deleted'#13#10'from toko'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update toko'
      'set'
      '  Kode = :Kode,'
      '  NamaToko = :NamaToko,'
      '  Alamat = :Alamat,'
      '  Kota = :Kota,'
      '  Telp = :Telp,'
      '  Aktif = :Aktif,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  TglEntry = :TglEntry,'
      '  Operator = :Operator,'
      '  Deleted = :Deleted'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into toko'
      
        '  (Kode, NamaToko, Alamat, Kota, Telp, Aktif, CreateDate, Create' +
        'By, TglEntry, Operator, Deleted)'
      'values'
      
        '  (:Kode, :NamaToko, :Alamat, :Kota, :Telp, :Aktif, :CreateDate,' +
        ' :CreateBy, :TglEntry, :Operator, :Deleted)')
    DeleteSQL.Strings = (
      'delete from toko'
      'where'
      '  Kode = :OLD_Kode')
    Left = 444
    Top = 10
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from toko order by kode desc')
    Left = 289
    Top = 7
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object SopirQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select * from pegawai where jabatan = '#39'PENGEMUDI'#39' and (kode like' +
        ' '#39'%'#39' + :text + '#39'%'#39' or nama like '#39'%'#39' + :text + '#39'%'#39')'
      '')
    Left = 337
    Top = 159
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object SopirQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object SopirQNama: TStringField
      FieldName = 'Nama'
      Required = True
      Size = 50
    end
    object SopirQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 200
    end
    object SopirQNotelp: TStringField
      FieldName = 'Notelp'
      Size = 50
    end
    object SopirQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object SopirQCreateBy: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object SopirQOperator: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
    object SopirQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
  end
  object EkorQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from ekor where kode like '#39'%'#39' + :text + '#39'%'#39' '
      '')
    Left = 369
    Top = 159
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object EkorQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object EkorQPanjang: TStringField
      FieldName = 'Panjang'
      Size = 50
    end
    object EkorQBerat: TStringField
      FieldName = 'Berat'
      Size = 50
    end
    object EkorQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Visible = False
    end
    object EkorQCreateBy: TStringField
      FieldName = 'CreateBy'
      Visible = False
      Size = 10
    end
    object EkorQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Visible = False
    end
    object EkorQOperator: TStringField
      FieldName = 'Operator'
      Visible = False
      Size = 10
    end
  end
  object JenisKendaraanQ: TSDQuery
    AutoRefresh = True
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from jeniskendaraan')
    Left = 304
    Top = 160
    object JenisKendaraanQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object JenisKendaraanQNamaJenis: TStringField
      FieldName = 'NamaJenis'
      Required = True
      Size = 50
    end
    object JenisKendaraanQTipe: TMemoField
      FieldName = 'Tipe'
      Required = True
      BlobType = ftMemo
    end
    object JenisKendaraanQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
  end
end
