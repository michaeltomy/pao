object MasterUserFm: TMasterUserFm
  Left = 192
  Top = 191
  Width = 949
  Height = 498
  Caption = 'Master User'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 933
    Height = 48
    Align = alTop
    TabOrder = 0
    object lbl1: TLabel
      Left = 8
      Top = 13
      Width = 22
      Height = 13
      Caption = 'User'
    end
    object KodeEdit: TcxButtonEdit
      Left = 40
      Top = 10
      Properties.Buttons = <>
      Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      TabOrder = 0
      OnEnter = KodeEditEnter
      OnExit = KodeEditExit
      OnKeyDown = KodeEditKeyDown
      Width = 121
    end
    object SearchBtn: TcxButton
      Left = 168
      Top = 10
      Width = 57
      Height = 21
      Caption = 'Search'
      TabOrder = 1
      TabStop = False
      OnClick = SearchBtnClick
    end
  end
  object pnl2: TPanel
    Left = 0
    Top = 390
    Width = 933
    Height = 51
    Align = alBottom
    TabOrder = 2
    object ExitBtn: TcxButton
      Left = 172
      Top = 10
      Width = 75
      Height = 25
      Caption = 'EXIT'
      TabOrder = 2
      Visible = False
      OnClick = ExitBtnClick
    end
    object DeleteBtn: TcxButton
      Left = 91
      Top = 10
      Width = 75
      Height = 25
      Caption = 'DELETE'
      TabOrder = 0
      OnClick = DeleteBtnClick
    end
    object SaveBtn: TcxButton
      Left = 8
      Top = 10
      Width = 75
      Height = 25
      Caption = 'SAVE'
      TabOrder = 1
      OnClick = SaveBtnClick
    end
  end
  object MasterVGrid: TcxDBVerticalGrid
    Left = 0
    Top = 48
    Width = 393
    Height = 342
    Align = alLeft
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    OptionsView.RowHeaderWidth = 155
    OptionsBehavior.GoToNextCellOnEnter = True
    OptionsBehavior.GoToNextCellOnTab = True
    OptionsData.CancelOnExit = False
    OptionsData.Appending = False
    OptionsData.Deleting = False
    OptionsData.DeletingConfirmation = False
    OptionsData.Inserting = False
    ParentFont = False
    TabOrder = 1
    OnEnter = MasterVGridEnter
    DataController.DataSource = MasterDs
    Version = 1
    object MasterVGridPassword: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Password'
      Styles.Content = cxStyle1
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object MasterVGridPegawai: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.OnButtonClick = MasterVGridPegawaiEditPropertiesButtonClick
      Properties.DataBinding.FieldName = 'Pegawai'
      ID = 1
      ParentID = -1
      Index = 1
      Version = 1
    end
    object MasterVGridNamaPegawai: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NamaPegawai'
      ID = 2
      ParentID = 1
      Index = 0
      Version = 1
    end
    object MasterVGridAlamatPegawai: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'AlamatPegawai'
      ID = 3
      ParentID = 1
      Index = 1
      Version = 1
    end
    object MasterVGridTglLahirPegawai: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'TglLahirPegawai'
      ID = 4
      ParentID = 1
      Index = 2
      Version = 1
    end
    object MasterVGridAktif: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Aktif'
      ID = 5
      ParentID = -1
      Index = 2
      Version = 1
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 441
    Width = 933
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object cxPageControl1: TcxPageControl
    Left = 393
    Top = 48
    Width = 540
    Height = 342
    Align = alClient
    TabOrder = 4
    Properties.ActivePage = cxTabSheet1
    ClientRectBottom = 336
    ClientRectLeft = 3
    ClientRectRight = 534
    ClientRectTop = 26
    object cxTabSheet1: TcxTabSheet
      Caption = 'Otorisasi'
      ImageIndex = 0
      object cxDBCheckBox1: TcxDBCheckBox
        Left = 8
        Top = 8
        Caption = 'Master Barang'
        DataBinding.DataField = 'MasterBarang'
        DataBinding.DataSource = MasterDs
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'MS Sans Serif'
        Style.Font.Style = [fsBold]
        Style.IsFontAssigned = True
        TabOrder = 0
        Transparent = True
        Width = 121
      end
      object cxDBCheckBox2: TcxDBCheckBox
        Left = 8
        Top = 32
        Caption = 'Master Kategori'
        DataBinding.DataField = 'MasterKategori'
        DataBinding.DataSource = MasterDs
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'MS Sans Serif'
        Style.Font.Style = [fsBold]
        Style.IsFontAssigned = True
        TabOrder = 1
        Transparent = True
        Width = 121
      end
      object cxDBCheckBox3: TcxDBCheckBox
        Left = 8
        Top = 56
        Caption = 'Master Pelanggan'
        DataBinding.DataField = 'MasterPelanggan'
        DataBinding.DataSource = MasterDs
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'MS Sans Serif'
        Style.Font.Style = [fsBold]
        Style.IsFontAssigned = True
        TabOrder = 2
        Transparent = True
        Width = 129
      end
      object cxDBCheckBox4: TcxDBCheckBox
        Left = 8
        Top = 80
        Caption = 'Master Pegawai'
        DataBinding.DataField = 'MasterPegawai'
        DataBinding.DataSource = MasterDs
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'MS Sans Serif'
        Style.Font.Style = [fsBold]
        Style.IsFontAssigned = True
        TabOrder = 3
        Transparent = True
        Width = 121
      end
      object cxDBCheckBox5: TcxDBCheckBox
        Left = 8
        Top = 104
        Caption = 'Master Toko'
        DataBinding.DataField = 'MasterToko'
        DataBinding.DataSource = MasterDs
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'MS Sans Serif'
        Style.Font.Style = [fsBold]
        Style.IsFontAssigned = True
        TabOrder = 4
        Transparent = True
        Width = 121
      end
      object cxDBCheckBox6: TcxDBCheckBox
        Left = 8
        Top = 128
        Caption = 'Master Pengguna'
        DataBinding.DataField = 'MasterPengguna'
        DataBinding.DataSource = MasterDs
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'MS Sans Serif'
        Style.Font.Style = [fsBold]
        Style.IsFontAssigned = True
        TabOrder = 5
        Transparent = True
        Width = 121
      end
      object cxDBCheckBox7: TcxDBCheckBox
        Left = 8
        Top = 152
        Caption = 'Lihat Harga Beli'
        DataBinding.DataField = 'LihatHargaBeli'
        DataBinding.DataSource = MasterDs
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'MS Sans Serif'
        Style.Font.Style = [fsBold]
        Style.IsFontAssigned = True
        TabOrder = 6
        Transparent = True
        Width = 121
      end
      object cxDBCheckBox8: TcxDBCheckBox
        Left = 8
        Top = 176
        Caption = 'View Barang'
        DataBinding.DataField = 'ViewBarang'
        DataBinding.DataSource = MasterDs
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'MS Sans Serif'
        Style.Font.Style = [fsBold]
        Style.IsFontAssigned = True
        TabOrder = 7
        Transparent = True
        Width = 121
      end
      object cxDBCheckBox9: TcxDBCheckBox
        Left = 8
        Top = 200
        Caption = 'View Kategori'
        DataBinding.DataField = 'ViewKategori'
        DataBinding.DataSource = MasterDs
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'MS Sans Serif'
        Style.Font.Style = [fsBold]
        Style.IsFontAssigned = True
        TabOrder = 8
        Transparent = True
        Width = 121
      end
      object cxDBCheckBox10: TcxDBCheckBox
        Left = 8
        Top = 224
        Caption = 'View Pegawai'
        DataBinding.DataField = 'ViewPegawai'
        DataBinding.DataSource = MasterDs
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'MS Sans Serif'
        Style.Font.Style = [fsBold]
        Style.IsFontAssigned = True
        TabOrder = 9
        Transparent = True
        Width = 121
      end
      object cxDBCheckBox11: TcxDBCheckBox
        Left = 8
        Top = 248
        Caption = 'View Pelanggan'
        DataBinding.DataField = 'ViewPelanggan'
        DataBinding.DataSource = MasterDs
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'MS Sans Serif'
        Style.Font.Style = [fsBold]
        Style.IsFontAssigned = True
        TabOrder = 10
        Transparent = True
        Width = 121
      end
      object cxDBCheckBox12: TcxDBCheckBox
        Left = 8
        Top = 272
        Caption = 'View Toko'
        DataBinding.DataField = 'ViewToko'
        DataBinding.DataSource = MasterDs
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'MS Sans Serif'
        Style.Font.Style = [fsBold]
        Style.IsFontAssigned = True
        TabOrder = 11
        Transparent = True
        Width = 121
      end
      object cxDBCheckBox13: TcxDBCheckBox
        Left = 168
        Top = 8
        Caption = 'Transfer Masuk'
        DataBinding.DataField = 'TransferMasuk'
        DataBinding.DataSource = MasterDs
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'MS Sans Serif'
        Style.Font.Style = [fsBold]
        Style.IsFontAssigned = True
        TabOrder = 12
        Transparent = True
        Width = 121
      end
      object cxDBCheckBox14: TcxDBCheckBox
        Left = 168
        Top = 32
        Caption = 'Transfer Keluar'
        DataBinding.DataField = 'TransferKeluar'
        DataBinding.DataSource = MasterDs
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'MS Sans Serif'
        Style.Font.Style = [fsBold]
        Style.IsFontAssigned = True
        TabOrder = 13
        Transparent = True
        Width = 121
      end
      object cxDBCheckBox15: TcxDBCheckBox
        Left = 168
        Top = 56
        Caption = 'Penjualan'
        DataBinding.DataField = 'Penjualan'
        DataBinding.DataSource = MasterDs
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'MS Sans Serif'
        Style.Font.Style = [fsBold]
        Style.IsFontAssigned = True
        TabOrder = 14
        Transparent = True
        Width = 121
      end
      object cxDBCheckBox16: TcxDBCheckBox
        Left = 168
        Top = 80
        Caption = 'Pembayaran'
        DataBinding.DataField = 'Pembayaran'
        DataBinding.DataSource = MasterDs
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'MS Sans Serif'
        Style.Font.Style = [fsBold]
        Style.IsFontAssigned = True
        TabOrder = 15
        Transparent = True
        Width = 121
      end
      object cxDBCheckBox17: TcxDBCheckBox
        Left = 168
        Top = 104
        Caption = 'Pengeluaran'
        DataBinding.DataField = 'Pengeluaran'
        DataBinding.DataSource = MasterDs
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'MS Sans Serif'
        Style.Font.Style = [fsBold]
        Style.IsFontAssigned = True
        TabOrder = 16
        Transparent = True
        Width = 121
      end
      object cxDBCheckBox18: TcxDBCheckBox
        Left = 168
        Top = 128
        Caption = 'Setor Bank'
        DataBinding.DataField = 'SetorBank'
        DataBinding.DataSource = MasterDs
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'MS Sans Serif'
        Style.Font.Style = [fsBold]
        Style.IsFontAssigned = True
        TabOrder = 17
        Transparent = True
        Width = 121
      end
      object cxDBCheckBox19: TcxDBCheckBox
        Left = 168
        Top = 152
        Caption = 'Laporan Penjualan'
        DataBinding.DataField = 'LaporanPenjualan'
        DataBinding.DataSource = MasterDs
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'MS Sans Serif'
        Style.Font.Style = [fsBold]
        Style.IsFontAssigned = True
        TabOrder = 18
        Transparent = True
        Width = 129
      end
      object cxDBCheckBox20: TcxDBCheckBox
        Left = 168
        Top = 176
        Caption = 'Laporan Transfer Masuk'
        DataBinding.DataField = 'LaporanTransferMasuk'
        DataBinding.DataSource = MasterDs
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'MS Sans Serif'
        Style.Font.Style = [fsBold]
        Style.IsFontAssigned = True
        TabOrder = 19
        Transparent = True
        Width = 177
      end
      object cxDBCheckBox21: TcxDBCheckBox
        Left = 168
        Top = 200
        Caption = 'Laporan Transfer Keluar'
        DataBinding.DataField = 'LaporanTransferKeluar'
        DataBinding.DataSource = MasterDs
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'MS Sans Serif'
        Style.Font.Style = [fsBold]
        Style.IsFontAssigned = True
        TabOrder = 20
        Transparent = True
        Width = 161
      end
      object cxDBCheckBox22: TcxDBCheckBox
        Left = 168
        Top = 224
        Caption = 'Laporan Pengeluaran'
        DataBinding.DataField = 'LaporanPengeluaran'
        DataBinding.DataSource = MasterDs
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'MS Sans Serif'
        Style.Font.Style = [fsBold]
        Style.IsFontAssigned = True
        TabOrder = 21
        Transparent = True
        Width = 145
      end
      object cxDBCheckBox23: TcxDBCheckBox
        Left = 168
        Top = 248
        Caption = 'Laporan Setoran Bank'
        DataBinding.DataField = 'LaporanSetoranBank'
        DataBinding.DataSource = MasterDs
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'MS Sans Serif'
        Style.Font.Style = [fsBold]
        Style.IsFontAssigned = True
        TabOrder = 22
        Transparent = True
        Width = 169
      end
      object cxDBCheckBox24: TcxDBCheckBox
        Left = 168
        Top = 272
        Caption = 'Laporan Kas'
        DataBinding.DataField = 'LaporanKas'
        DataBinding.DataSource = MasterDs
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'MS Sans Serif'
        Style.Font.Style = [fsBold]
        Style.IsFontAssigned = True
        TabOrder = 23
        Transparent = True
        Width = 121
      end
      object cxDBCheckBox25: TcxDBCheckBox
        Left = 336
        Top = 8
        Caption = 'Laporan Laba Kotor'
        DataBinding.DataField = 'LaporanLabaKotor'
        DataBinding.DataSource = MasterDs
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'MS Sans Serif'
        Style.Font.Style = [fsBold]
        Style.IsFontAssigned = True
        TabOrder = 24
        Transparent = True
        Width = 161
      end
      object cxDBCheckBox26: TcxDBCheckBox
        Left = 336
        Top = 32
        Caption = 'Laporan Laba Bersih'
        DataBinding.DataField = 'LaporanLabaBersih'
        DataBinding.DataSource = MasterDs
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'MS Sans Serif'
        Style.Font.Style = [fsBold]
        Style.IsFontAssigned = True
        TabOrder = 25
        Transparent = True
        Width = 153
      end
      object cxDBCheckBox27: TcxDBCheckBox
        Left = 336
        Top = 56
        Caption = 'Laporan Penjualan Barang'
        DataBinding.DataField = 'LaporanPenjualanBarang'
        DataBinding.DataSource = MasterDs
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'MS Sans Serif'
        Style.Font.Style = [fsBold]
        Style.IsFontAssigned = True
        TabOrder = 26
        Transparent = True
        Visible = False
        Width = 177
      end
    end
  end
  object MasterQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select * from pengguna')
    UpdateObject = MasterUS
    Left = 313
    Top = 9
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQUsername: TStringField
      FieldName = 'Username'
      Size = 50
    end
    object MasterQPassword: TStringField
      FieldName = 'Password'
      Size = 50
    end
    object MasterQPegawai: TStringField
      FieldName = 'Pegawai'
      Size = 10
    end
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 380
    Top = 14
  end
  object MasterUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Username, Password, Pegawai, Aktif, Deleted, MasterBarang' +
        ', MasterKategori, MasterPelanggan, MasterPegawai, MasterPengguna' +
        ', MasterToko, ViewBarang, ViewKategori, ViewPelanggan, ViewToko,' +
        ' ViewPegawai, TransferMasuk, TransferKeluar, Penjualan, Pembayar' +
        'an, Pengeluaran, SetorBank, LaporanPenjualan, LaporanTransferMas' +
        'uk, LaporanTransferKeluar, LaporanPengeluaran, LaporanSetoranBan' +
        'k, LaporanKas, LaporanLabaKotor, LaporanLabaBersih, LihatHargaBe' +
        'li'#13#10'from pengguna'
      'where'
      '  Username = :OLD_Username')
    ModifySQL.Strings = (
      'update pengguna'
      'set'
      '  Username = :Username,'
      '  Password = :Password,'
      '  Pegawai = :Pegawai,'
      '  Aktif = :Aktif,'
      '  Deleted = :Deleted,'
      '  MasterBarang = :MasterBarang,'
      '  MasterKategori = :MasterKategori,'
      '  MasterPelanggan = :MasterPelanggan,'
      '  MasterPegawai = :MasterPegawai,'
      '  MasterPengguna = :MasterPengguna,'
      '  MasterToko = :MasterToko,'
      '  ViewBarang = :ViewBarang,'
      '  ViewKategori = :ViewKategori,'
      '  ViewPelanggan = :ViewPelanggan,'
      '  ViewToko = :ViewToko,'
      '  ViewPegawai = :ViewPegawai,'
      '  TransferMasuk = :TransferMasuk,'
      '  TransferKeluar = :TransferKeluar,'
      '  Penjualan = :Penjualan,'
      '  Pembayaran = :Pembayaran,'
      '  Pengeluaran = :Pengeluaran,'
      '  SetorBank = :SetorBank,'
      '  LaporanPenjualan = :LaporanPenjualan,'
      '  LaporanTransferMasuk = :LaporanTransferMasuk,'
      '  LaporanTransferKeluar = :LaporanTransferKeluar,'
      '  LaporanPengeluaran = :LaporanPengeluaran,'
      '  LaporanSetoranBank = :LaporanSetoranBank,'
      '  LaporanKas = :LaporanKas,'
      '  LaporanLabaKotor = :LaporanLabaKotor,'
      '  LaporanLabaBersih = :LaporanLabaBersih,'
      '  LihatHargaBeli = :LihatHargaBeli'
      'where'
      '  Username = :OLD_Username')
    InsertSQL.Strings = (
      'insert into pengguna'
      
        '  (Username, Password, Pegawai, Aktif, Deleted, MasterBarang, Ma' +
        'sterKategori, MasterPelanggan, MasterPegawai, MasterPengguna, Ma' +
        'sterToko, ViewBarang, ViewKategori, ViewPelanggan, ViewToko, Vie' +
        'wPegawai, TransferMasuk, TransferKeluar, Penjualan, Pembayaran, ' +
        'Pengeluaran, SetorBank, LaporanPenjualan, LaporanTransferMasuk, ' +
        'LaporanTransferKeluar, LaporanPengeluaran, LaporanSetoranBank, L' +
        'aporanKas, LaporanLabaKotor, LaporanLabaBersih, LihatHargaBeli)'
      'values'
      
        '  (:Username, :Password, :Pegawai, :Aktif, :Deleted, :MasterBara' +
        'ng, :MasterKategori, :MasterPelanggan, :MasterPegawai, :MasterPe' +
        'ngguna, :MasterToko, :ViewBarang, :ViewKategori, :ViewPelanggan,' +
        ' :ViewToko, :ViewPegawai, :TransferMasuk, :TransferKeluar, :Penj' +
        'ualan, :Pembayaran, :Pengeluaran, :SetorBank, :LaporanPenjualan,' +
        ' :LaporanTransferMasuk, :LaporanTransferKeluar, :LaporanPengelua' +
        'ran, :LaporanSetoranBank, :LaporanKas, :LaporanLabaKotor, :Lapor' +
        'anLabaBersih, :LihatHargaBeli)')
    DeleteSQL.Strings = (
      'delete from pengguna'
      'where'
      '  Username = :OLD_Username')
    Left = 452
    Top = 10
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from [user] order by kode desc')
    Left = 329
    Top = 71
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object PegawaiQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from pegawai')
    Left = 561
    Top = 7
    object PegawaiQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object PegawaiQNama: TStringField
      FieldName = 'Nama'
      Size = 250
    end
    object PegawaiQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 250
    end
    object PegawaiQKota: TStringField
      FieldName = 'Kota'
      Size = 100
    end
    object PegawaiQTelp: TStringField
      FieldName = 'Telp'
      Size = 200
    end
    object PegawaiQTglLahir: TDateTimeField
      FieldName = 'TglLahir'
    end
    object PegawaiQJabatan: TStringField
      FieldName = 'Jabatan'
      Size = 100
    end
    object PegawaiQAktif: TBooleanField
      FieldName = 'Aktif'
    end
  end
  object cxStyleRepository1: TcxStyleRepository
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svFont]
      Font.Charset = SYMBOL_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Wingdings'
      Font.Style = []
    end
  end
end
