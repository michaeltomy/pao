object NotifikasiFm: TNotifikasiFm
  Left = 318
  Top = 235
  Width = 571
  Height = 476
  Caption = 'Notifikasi'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 555
    Height = 48
    Align = alTop
    TabOrder = 0
  end
  object pnl2: TPanel
    Left = 0
    Top = 368
    Width = 555
    Height = 51
    Align = alBottom
    TabOrder = 1
    object ExitBtn: TcxButton
      Left = 172
      Top = 10
      Width = 75
      Height = 25
      Caption = 'EXIT'
      TabOrder = 2
      Visible = False
      OnClick = ExitBtnClick
    end
    object DeleteBtn: TcxButton
      Left = 91
      Top = 10
      Width = 75
      Height = 25
      Caption = 'DELETE'
      TabOrder = 0
      TabStop = False
      Visible = False
    end
    object SaveBtn: TcxButton
      Left = 8
      Top = 10
      Width = 75
      Height = 25
      Caption = 'REFRESH'
      TabOrder = 1
      OnClick = SaveBtnClick
    end
    object cxListBox3: TcxListBox
      Left = 280
      Top = -8
      Width = 121
      Height = 97
      ItemHeight = 13
      TabOrder = 3
      Visible = False
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 419
    Width = 555
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object cxListBox1: TcxListBox
    Left = 192
    Top = 96
    Width = 399
    Height = 252
    Align = alCustom
    ItemHeight = 29
    ParentFont = False
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -24
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = []
    Style.IsFontAssigned = True
    TabOrder = 3
    Visible = False
    OnDblClick = cxListBox1DblClick
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 48
    Width = 555
    Height = 320
    Align = alClient
    TabOrder = 4
    object cxGrid1TableView1: TcxGridTableView
      OnCellClick = cxGrid1TableView1CellClick
      OnCellDblClick = cxGrid1TableView1CellDblClick
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.IncSearch = True
      OptionsData.Editing = False
      OptionsView.GroupByBox = False
      object cxGrid1TableView1Column1: TcxGridColumn
        Caption = 'Jenis'
        Width = 171
      end
      object cxGrid1TableView1Column2: TcxGridColumn
        Caption = 'Keterangan'
        Width = 348
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1TableView1
    end
  end
  object cxListBox2: TcxListBox
    Left = 56
    Top = 272
    Width = 121
    Height = 97
    ItemHeight = 13
    TabOrder = 5
    Visible = False
  end
  object BarangQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from barang')
    Left = 441
    Top = 9
    object BarangQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object BarangQNama: TStringField
      FieldName = 'Nama'
      Required = True
      Size = 50
    end
    object BarangQHargaBeli: TCurrencyField
      FieldName = 'HargaBeli'
    end
    object BarangQHargaJual: TCurrencyField
      FieldName = 'HargaJual'
    end
    object BarangQJumlah: TIntegerField
      FieldName = 'Jumlah'
    end
    object BarangQSatuan: TStringField
      FieldName = 'Satuan'
      Size = 50
    end
  end
  object SJQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select m.*, sj.kodenota as surat_jalan, p.namapt, r.muat, r.bong' +
        'kar from MasterSO m left outer join'
      
        'MasterSJ sj on m.Kodenota=sj.NoSO, Pelanggan p, Rute r where m.P' +
        'elanggan = p.Kode and m.Rute=r.Kode and m.status='#39'DEAL'#39' and getd' +
        'ate()>=tglorder+1 and sj.kodenota is NULL'
      ' '
      ' ')
    Left = 65
    Top = 65535
    object SJQKodenota: TStringField
      FieldName = 'Kodenota'
      Required = True
      Size = 10
    end
    object SJQTgl: TDateTimeField
      FieldName = 'Tgl'
      Required = True
    end
    object SJQPelanggan: TStringField
      FieldName = 'Pelanggan'
      Required = True
      Size = 10
    end
    object SJQTglOrder: TDateTimeField
      FieldName = 'TglOrder'
      Required = True
    end
    object SJQJenisBarang: TStringField
      FieldName = 'JenisBarang'
      Required = True
      Size = 100
    end
    object SJQHarga: TCurrencyField
      FieldName = 'Harga'
      Required = True
    end
    object SJQRute: TStringField
      FieldName = 'Rute'
      Required = True
      Size = 10
    end
    object SJQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object SJQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object SJQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object SJQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object SJQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object SJQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object SJQTglFollowUp: TDateTimeField
      FieldName = 'TglFollowUp'
    end
    object SJQPanjang: TFloatField
      FieldName = 'Panjang'
      Required = True
    end
    object SJQBerat: TFloatField
      FieldName = 'Berat'
      Required = True
    end
    object SJQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object SJQKontrak: TStringField
      FieldName = 'Kontrak'
      Size = 10
    end
    object SJQDariPT: TMemoField
      FieldName = 'DariPT'
      BlobType = ftMemo
    end
    object SJQDariAlamat: TMemoField
      FieldName = 'DariAlamat'
      BlobType = ftMemo
    end
    object SJQKePT: TMemoField
      FieldName = 'KePT'
      BlobType = ftMemo
    end
    object SJQKeAlamat: TMemoField
      FieldName = 'KeAlamat'
      BlobType = ftMemo
    end
    object SJQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object SJQsurat_jalan: TStringField
      FieldName = 'surat_jalan'
      Size = 10
    end
    object SJQnamapt: TStringField
      FieldName = 'namapt'
      Required = True
      Size = 50
    end
    object SJQmuat: TStringField
      FieldName = 'muat'
      Required = True
      Size = 50
    end
    object SJQbongkar: TStringField
      FieldName = 'bongkar'
      Required = True
      Size = 50
    end
  end
  object FollowUpSOQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select m.*, p.namapt from masterso m, pelanggan p where m.pelang' +
        'gan=p.kode and status='#39'PENDING'#39' and getdate()>=tglorder+3')
    Left = 1
    Top = 65535
    object FollowUpSOQKodenota: TStringField
      FieldName = 'Kodenota'
      Required = True
      Size = 10
    end
    object FollowUpSOQTgl: TDateTimeField
      FieldName = 'Tgl'
      Required = True
    end
    object FollowUpSOQPelanggan: TStringField
      FieldName = 'Pelanggan'
      Required = True
      Size = 10
    end
    object FollowUpSOQTglOrder: TDateTimeField
      FieldName = 'TglOrder'
      Required = True
    end
    object FollowUpSOQJenisBarang: TStringField
      FieldName = 'JenisBarang'
      Required = True
      Size = 100
    end
    object FollowUpSOQHarga: TCurrencyField
      FieldName = 'Harga'
      Required = True
    end
    object FollowUpSOQRute: TStringField
      FieldName = 'Rute'
      Required = True
      Size = 10
    end
    object FollowUpSOQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object FollowUpSOQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object FollowUpSOQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object FollowUpSOQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object FollowUpSOQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object FollowUpSOQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object FollowUpSOQTglFollowUp: TDateTimeField
      FieldName = 'TglFollowUp'
    end
    object FollowUpSOQPanjang: TFloatField
      FieldName = 'Panjang'
      Required = True
    end
    object FollowUpSOQBerat: TFloatField
      FieldName = 'Berat'
      Required = True
    end
    object FollowUpSOQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object FollowUpSOQKontrak: TStringField
      FieldName = 'Kontrak'
      Size = 10
    end
    object FollowUpSOQnamapt: TStringField
      FieldName = 'namapt'
      Required = True
      Size = 50
    end
  end
  object CustQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select top 1 * from masterso where pelanggan like :text order by' +
        ' tgl desc'
      '')
    Left = 505
    Top = 7
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object CustQKodenota: TStringField
      FieldName = 'Kodenota'
      Required = True
      Size = 10
    end
    object CustQTgl: TDateTimeField
      FieldName = 'Tgl'
      Required = True
    end
    object CustQPelanggan: TStringField
      FieldName = 'Pelanggan'
      Required = True
      Size = 10
    end
    object CustQTglOrder: TDateTimeField
      FieldName = 'TglOrder'
      Required = True
    end
    object CustQJenisBarang: TStringField
      FieldName = 'JenisBarang'
      Required = True
      Size = 100
    end
    object CustQHarga: TCurrencyField
      FieldName = 'Harga'
      Required = True
    end
    object CustQRute: TStringField
      FieldName = 'Rute'
      Required = True
      Size = 10
    end
    object CustQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object CustQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object CustQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object CustQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object CustQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object CustQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object CustQTglFollowUp: TDateTimeField
      FieldName = 'TglFollowUp'
    end
    object CustQPanjang: TFloatField
      FieldName = 'Panjang'
      Required = True
    end
    object CustQBerat: TFloatField
      FieldName = 'Berat'
      Required = True
    end
    object CustQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object CustQKontrak: TStringField
      FieldName = 'Kontrak'
      Size = 10
    end
    object CustQDariPT: TMemoField
      FieldName = 'DariPT'
      BlobType = ftMemo
    end
    object CustQDariAlamat: TMemoField
      FieldName = 'DariAlamat'
      BlobType = ftMemo
    end
    object CustQKePT: TMemoField
      FieldName = 'KePT'
      BlobType = ftMemo
    end
    object CustQKeAlamat: TMemoField
      FieldName = 'KeAlamat'
      BlobType = ftMemo
    end
    object CustQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
  end
  object CustomerLamaQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from jeniskendaraan')
    Left = 32
    object CustomerLamaQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object CustomerLamaQNamaJenis: TStringField
      FieldName = 'NamaJenis'
      Required = True
      Size = 50
    end
    object CustomerLamaQTipe: TMemoField
      FieldName = 'Tipe'
      Required = True
      BlobType = ftMemo
    end
    object CustomerLamaQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
  end
  object PelangganQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from pelanggan')
    Left = 473
    Top = 7
    object PelangganQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object PelangganQNamaPT: TStringField
      FieldName = 'NamaPT'
      Required = True
      Size = 50
    end
    object PelangganQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 100
    end
    object PelangganQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object PelangganQNoTelp: TStringField
      FieldName = 'NoTelp'
      Size = 15
    end
    object PelangganQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object PelangganQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object PelangganQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object PelangganQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object PelangganQNamaPIC1: TStringField
      FieldName = 'NamaPIC1'
      Size = 50
    end
    object PelangganQTelpPIC1: TStringField
      FieldName = 'TelpPIC1'
      Size = 15
    end
    object PelangganQJabatanPIC1: TStringField
      FieldName = 'JabatanPIC1'
    end
    object PelangganQNamaPIC2: TStringField
      FieldName = 'NamaPIC2'
      Size = 50
    end
    object PelangganQTelpPIC2: TStringField
      FieldName = 'TelpPIC2'
      Size = 15
    end
    object PelangganQJabatanPIC2: TStringField
      FieldName = 'JabatanPIC2'
    end
    object PelangganQNamaPIC3: TStringField
      FieldName = 'NamaPIC3'
      Size = 50
    end
    object PelangganQTelpPIC3: TStringField
      FieldName = 'TelpPIC3'
      Size = 15
    end
    object PelangganQJabatanPIC3: TStringField
      FieldName = 'JabatanPIC3'
    end
  end
  object BonBarangQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from bonbarang where status='#39'ACCEPTED'#39)
    Left = 97
    Top = 65535
    object BonBarangQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object BonBarangQPeminta: TStringField
      FieldName = 'Peminta'
      Required = True
      Size = 50
    end
    object BonBarangQPenyetuju: TStringField
      FieldName = 'Penyetuju'
      Required = True
      Size = 10
    end
    object BonBarangQPenerima: TStringField
      FieldName = 'Penerima'
      Size = 50
    end
    object BonBarangQLaporanPerbaikan: TStringField
      FieldName = 'LaporanPerbaikan'
      Size = 10
    end
    object BonBarangQLaporanPerawatan: TStringField
      FieldName = 'LaporanPerawatan'
      Size = 10
    end
    object BonBarangQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object BonBarangQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object BonBarangQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object BonBarangQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object BonBarangQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object BonBarangQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object BonBarangQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
  end
  object LPBQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select l.tglterima, d.jumlah, d.barang from lpb l, detaillpb d w' +
        'here getdate()<l.tglterima+1 and d.kodelpb=l.kode')
    Left = 225
    Top = 65535
    object LPBQtglterima: TDateTimeField
      FieldName = 'tglterima'
      Required = True
    end
    object LPBQjumlah: TIntegerField
      FieldName = 'jumlah'
      Required = True
    end
    object LPBQbarang: TStringField
      FieldName = 'barang'
      Required = True
      Size = 10
    end
    object LPBQNamaBarang: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaBarang'
      LookupDataSet = BarangQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'barang'
      Size = 55
      Lookup = True
    end
  end
  object DaftarBeliQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from daftarbeli where status='#39'NEWDB'#39)
    Left = 129
    Top = 65535
    object DaftarBeliQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object DaftarBeliQBarang: TStringField
      FieldName = 'Barang'
      Required = True
      Size = 10
    end
    object DaftarBeliQBonBarang: TStringField
      FieldName = 'BonBarang'
      Required = True
      Size = 10
    end
    object DaftarBeliQHargaMin: TCurrencyField
      FieldName = 'HargaMin'
    end
    object DaftarBeliQHargaMax: TCurrencyField
      FieldName = 'HargaMax'
    end
    object DaftarBeliQHargaLast: TCurrencyField
      FieldName = 'HargaLast'
    end
    object DaftarBeliQLastSupplier: TStringField
      FieldName = 'LastSupplier'
      Size = 10
    end
    object DaftarBeliQJumlahBeli: TIntegerField
      FieldName = 'JumlahBeli'
      Required = True
    end
    object DaftarBeliQHargaSatuan: TCurrencyField
      FieldName = 'HargaSatuan'
    end
    object DaftarBeliQSupplier: TStringField
      FieldName = 'Supplier'
      Size = 10
    end
    object DaftarBeliQCashNCarry: TBooleanField
      FieldName = 'CashNCarry'
    end
    object DaftarBeliQGrandTotal: TCurrencyField
      FieldName = 'GrandTotal'
      Required = True
    end
    object DaftarBeliQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object DaftarBeliQSupplier1: TStringField
      FieldName = 'Supplier1'
      Size = 10
    end
    object DaftarBeliQSupplier2: TStringField
      FieldName = 'Supplier2'
      Size = 10
    end
    object DaftarBeliQSupplier3: TStringField
      FieldName = 'Supplier3'
      Size = 10
    end
    object DaftarBeliQHarga1: TCurrencyField
      FieldName = 'Harga1'
    end
    object DaftarBeliQHarga2: TCurrencyField
      FieldName = 'Harga2'
    end
    object DaftarBeliQHarga3: TCurrencyField
      FieldName = 'Harga3'
    end
    object DaftarBeliQTermin1: TIntegerField
      FieldName = 'Termin1'
    end
    object DaftarBeliQTermin2: TIntegerField
      FieldName = 'Termin2'
    end
    object DaftarBeliQTermin3: TIntegerField
      FieldName = 'Termin3'
    end
    object DaftarBeliQKeterangan1: TMemoField
      FieldName = 'Keterangan1'
      BlobType = ftMemo
    end
    object DaftarBeliQKeterangan2: TMemoField
      FieldName = 'Keterangan2'
      BlobType = ftMemo
    end
    object DaftarBeliQKeterangan3: TMemoField
      FieldName = 'Keterangan3'
      BlobType = ftMemo
    end
    object DaftarBeliQTermin: TIntegerField
      FieldName = 'Termin'
    end
  end
  object PickKendaraanQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select m.*, sj.kodenota as surat_jalan, p.namapt, r.muat, r.bong' +
        'kar from MasterSO m left outer join'
      
        'MasterSJ sj on m.Kodenota=sj.NoSO, Pelanggan p, Rute r where m.P' +
        'elanggan = p.Kode and m.Rute=r.Kode and m.status='#39'DEAL'#39' and m.ar' +
        'mada is NULL'
      ' ')
    Left = 192
    object PickKendaraanQKodenota: TStringField
      FieldName = 'Kodenota'
      Required = True
      Size = 10
    end
    object PickKendaraanQTgl: TDateTimeField
      FieldName = 'Tgl'
      Required = True
    end
    object PickKendaraanQPelanggan: TStringField
      FieldName = 'Pelanggan'
      Required = True
      Size = 10
    end
    object PickKendaraanQTglOrder: TDateTimeField
      FieldName = 'TglOrder'
      Required = True
    end
    object PickKendaraanQJenisBarang: TStringField
      FieldName = 'JenisBarang'
      Required = True
      Size = 100
    end
    object PickKendaraanQHarga: TCurrencyField
      FieldName = 'Harga'
      Required = True
    end
    object PickKendaraanQRute: TStringField
      FieldName = 'Rute'
      Required = True
      Size = 10
    end
    object PickKendaraanQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object PickKendaraanQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object PickKendaraanQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object PickKendaraanQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object PickKendaraanQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object PickKendaraanQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object PickKendaraanQTglFollowUp: TDateTimeField
      FieldName = 'TglFollowUp'
    end
    object PickKendaraanQPanjang: TFloatField
      FieldName = 'Panjang'
      Required = True
    end
    object PickKendaraanQBerat: TFloatField
      FieldName = 'Berat'
      Required = True
    end
    object PickKendaraanQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object PickKendaraanQKontrak: TStringField
      FieldName = 'Kontrak'
      Size = 10
    end
    object PickKendaraanQDariPT: TMemoField
      FieldName = 'DariPT'
      BlobType = ftMemo
    end
    object PickKendaraanQDariAlamat: TMemoField
      FieldName = 'DariAlamat'
      BlobType = ftMemo
    end
    object PickKendaraanQKePT: TMemoField
      FieldName = 'KePT'
      BlobType = ftMemo
    end
    object PickKendaraanQKeAlamat: TMemoField
      FieldName = 'KeAlamat'
      BlobType = ftMemo
    end
    object PickKendaraanQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object PickKendaraanQsurat_jalan: TStringField
      FieldName = 'surat_jalan'
      Size = 10
    end
    object PickKendaraanQnamapt: TStringField
      FieldName = 'namapt'
      Required = True
      Size = 50
    end
    object PickKendaraanQmuat: TStringField
      FieldName = 'muat'
      Required = True
      Size = 50
    end
    object PickKendaraanQbongkar: TStringField
      FieldName = 'bongkar'
      Required = True
      Size = 50
    end
  end
  object PerbaikanQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      
        'select pp.*, lp.kode as laporanperbaikan from permintaanperbaika' +
        'n pp left outer join'
      'laporanperbaikan lp on pp.Kode=lp.pp where lp.kode is null'
      ' ')
    Left = 160
    object PerbaikanQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object PerbaikanQArmada: TStringField
      FieldName = 'Armada'
      Size = 10
    end
    object PerbaikanQEkor: TStringField
      FieldName = 'Ekor'
      Size = 10
    end
    object PerbaikanQPeminta: TStringField
      FieldName = 'Peminta'
      Required = True
      Size = 50
    end
    object PerbaikanQKeluhan: TMemoField
      FieldName = 'Keluhan'
      Required = True
      BlobType = ftMemo
    end
    object PerbaikanQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object PerbaikanQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object PerbaikanQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object PerbaikanQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object PerbaikanQTglCetak: TDateTimeField
      FieldName = 'TglCetak'
    end
    object PerbaikanQlaporanperbaikan: TStringField
      FieldName = 'laporanperbaikan'
      Size = 10
    end
  end
  object MinStokQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from barang where jumlah<=minimumstok')
    Left = 281
    Top = 1
    object MinStokQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MinStokQNama: TStringField
      FieldName = 'Nama'
      Required = True
      Size = 50
    end
    object MinStokQHargaBeli: TCurrencyField
      FieldName = 'HargaBeli'
    end
    object MinStokQHargaJual: TCurrencyField
      FieldName = 'HargaJual'
    end
    object MinStokQJumlah: TIntegerField
      FieldName = 'Jumlah'
    end
    object MinStokQSatuan: TStringField
      FieldName = 'Satuan'
      Size = 50
    end
    object MinStokQMinimumStok: TIntegerField
      FieldName = 'MinimumStok'
    end
  end
  object ArmadaQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from armada')
    Left = 409
    Top = 9
    object ArmadaQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object ArmadaQPlatNo: TStringField
      FieldName = 'PlatNo'
      Required = True
      Size = 10
    end
    object ArmadaQPanjang: TFloatField
      FieldName = 'Panjang'
      Required = True
    end
    object ArmadaQKapasitas: TFloatField
      FieldName = 'Kapasitas'
      Required = True
    end
    object ArmadaQTahunPembuatan: TStringField
      FieldName = 'TahunPembuatan'
      Required = True
      Size = 5
    end
    object ArmadaQJumlahBan: TIntegerField
      FieldName = 'JumlahBan'
      Required = True
    end
    object ArmadaQAktif: TBooleanField
      FieldName = 'Aktif'
      Required = True
    end
    object ArmadaQKmSekarang: TIntegerField
      FieldName = 'KmSekarang'
    end
    object ArmadaQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 50
    end
    object ArmadaQSopir: TStringField
      FieldName = 'Sopir'
      Size = 10
    end
    object ArmadaQEkor: TStringField
      FieldName = 'Ekor'
      Size = 10
    end
    object ArmadaQJenisKendaraan: TStringField
      FieldName = 'JenisKendaraan'
      Required = True
      Size = 10
    end
    object ArmadaQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object ArmadaQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object ArmadaQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object ArmadaQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object ArmadaQSTNKPajakExpired: TDateTimeField
      FieldName = 'STNKPajakExpired'
    end
    object ArmadaQSTNKPerpanjangExpired: TDateTimeField
      FieldName = 'STNKPerpanjangExpired'
    end
    object ArmadaQKirMulai: TDateTimeField
      FieldName = 'KirMulai'
    end
    object ArmadaQKirSelesai: TDateTimeField
      FieldName = 'KirSelesai'
    end
    object ArmadaQNoRangka: TStringField
      FieldName = 'NoRangka'
      Size = 50
    end
    object ArmadaQNoMesin: TStringField
      FieldName = 'NoMesin'
      Size = 50
    end
  end
end
