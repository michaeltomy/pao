unit LaporanKas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus,
  dxSkinsCore, dxSkinsDefaultPainters, DB, SDEngine, UCrpeClasses, UCrpe32,
  StdCtrls, cxButtons, ComCtrls, dxSkinBlack, dxSkinBlue, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinValentine, dxSkinXmas2008Blue;

type
  TLaporanKasFm = class(TForm)
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    lblstatus: TLabel;
    RadioButtonBln: TRadioButton;
    RadioButtonTgl: TRadioButton;
    DateTimePickerDay: TDateTimePicker;
    DateTimePickerDay2: TDateTimePicker;
    DateTimePickerMonth: TDateTimePicker;
    DateTimePickerMonth2: TDateTimePicker;
    cxButton1: TcxButton;
    Crpe1: TCrpe;
    masterq: TSDQuery;
    masterds: TDataSource;
    RadioButton1: TRadioButton;
    DateTimePicker1: TDateTimePicker;
    procedure cxButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  LaporanKasFm: TLaporanKasFm;

implementation
uses DateUtils, MenuUtama, StrUtils, DM;
{$R *.dfm}

procedure TLaporanKasFm.cxButton1Click(Sender: TObject);
var day1, day2 : TDateTime;
    jum_hari:integer;
begin
  lblstatus.Visible:=True;
  Crpe1.Refresh;
    Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'Kas.rpt';


  if RadioButtonTgl.Checked then
    begin
      Crpe1.ParamFields[2].CurrentValue:='date';
      Crpe1.ParamFields[0].CurrentValue:=FormatDateTime('YYYY,MM,DD', DateTimePickerDay.Date);
      Crpe1.ParamFields[1].CurrentValue:=FormatDateTime('YYYY,MM,DD', DateTimePickerDay2.Date);
    end
    else if RadioButtonBln.Checked then
    begin
      Crpe1.ParamFields[2].CurrentValue:='bul';
      DateTimePickerMonth.Date := EncodeDate(YearOf(DateTimePickerMonth.Date), MonthOf(DateTimePickerMonth.date), 1) ;
      jum_hari:= MonthDays[IsLeapYear(YearOf(DateTimePickerMonth2.Date))][MonthOf(DateTimePickerMonth2.Date)];
      DateTimePickerMonth2.Date := EncodeDate(YearOf(DateTimePickerMonth2.Date), MonthOf(DateTimePickerMonth2.date), jum_hari);
      Crpe1.ParamFields[0].CurrentValue:=FormatDateTime('YYYY,MM,DD', DateTimePickerMonth.Date);
      Crpe1.ParamFields[1].CurrentValue:=FormatDateTime('YYYY,MM,DD', DateTimePickerMonth2.Date);
    end
    else if RadioButton1.Checked then
    begin
      Crpe1.ParamFields[2].CurrentValue:='date';
      Crpe1.ParamFields[0].CurrentValue:=FormatDateTime('YYYY,MM,DD', DateTimePicker1.Date);
      Crpe1.ParamFields[1].CurrentValue:=FormatDateTime('YYYY,MM,DD', DateTimePicker1.Date);
    end;

Crpe1.Connect.UserID:=menuutamafm.DataBase1.Params.Values['USER NAME'];
  Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
  Crpe1.Connect.ServerName:=LeftStr(Menuutamafm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);

  //ShowMessage(Crpe1.ReportName);

    Crpe1.Execute;
    lblstatus.Visible:=False;
    self.Visible:=false;
end;

procedure TLaporanKasFm.FormShow(Sender: TObject);
begin
  DMfm := TDMFm.Create(self);
  DMFm.GetDateQ.Open;
  DateTimePickerDay.DateTime:=DMFm.GetDateQNow.AsDateTime;
  DateTimePickerDay2.DateTime:=DMFm.GetDateQNow.AsDateTime;
  DateTimePicker1.DateTime:=DMFm.GetDateQNow.AsDateTime;
  DMFm.GetDateQ.Close;
end;

end.
