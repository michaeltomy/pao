unit MasterSalesOrder;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue;

type
  TMasterSalesOrderFm = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    MasterQ: TSDQuery;
    MasterDs: TDataSource;
    MasterVGrid: TcxDBVerticalGrid;
    StatusBar: TStatusBar;
    MasterUS: TSDUpdateSQL;
    KodeQ: TSDQuery;
    KodeQkode: TStringField;
    ExitBtn: TcxButton;
    DeleteBtn: TcxButton;
    SaveBtn: TcxButton;
    KodeEdit: TcxButtonEdit;
    lbl1: TLabel;
    SearchBtn: TcxButton;
    MasterQKodenota: TStringField;
    MasterQTgl: TDateTimeField;
    MasterQPelanggan: TStringField;
    MasterQTglOrder: TDateTimeField;
    MasterQJenisBarang: TStringField;
    MasterQHarga: TCurrencyField;
    MasterQRute: TStringField;
    MasterQKeterangan: TStringField;
    MasterQCreateDate: TDateTimeField;
    MasterQCreateBy: TStringField;
    MasterQOperator: TStringField;
    MasterQTglEntry: TDateTimeField;
    MasterQStatus: TIntegerField;
    MasterQTglFollowUp: TDateTimeField;
    MasterQMuat: TStringField;
    MasterQBongkar: TStringField;
    MasterQPanjang: TFloatField;
    MasterQBerat: TFloatField;
    MasterVGridTgl: TcxDBEditorRow;
    MasterVGridPelanggan: TcxDBEditorRow;
    MasterVGridTglOrder: TcxDBEditorRow;
    MasterVGridJenisBarang: TcxDBEditorRow;
    MasterVGridHarga: TcxDBEditorRow;
    MasterVGridRute: TcxDBEditorRow;
    MasterVGridKeterangan: TcxDBEditorRow;
    MasterVGridStatus: TcxDBEditorRow;
    MasterVGridTglFollowUp: TcxDBEditorRow;
    MasterVGridMuat: TcxDBEditorRow;
    MasterVGridBongkar: TcxDBEditorRow;
    MasterVGridPanjang: TcxDBEditorRow;
    MasterVGridBerat: TcxDBEditorRow;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ExitBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure KodeEditEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SaveBtnClick(Sender: TObject);
    procedure MasterQAfterInsert(DataSet: TDataSet);
    procedure MasterQBeforePost(DataSet: TDataSet);
    procedure DeleteBtnClick(Sender: TObject);
    procedure SearchBtnClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent);
  end;

var
  MasterSalesOrderFm: TMasterSalesOrderFm;
  MasterOriSQL: string;
implementation

uses MenuUtama, DropDown, DM;

{$R *.dfm}

{ TMasterArmadaFm }

constructor TMasterSalesOrderFm.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
end;

procedure TMasterSalesOrderFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TMasterSalesOrderFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TMasterSalesOrderFm.ExitBtnClick(Sender: TObject);
begin
  Release;
end;

procedure TMasterSalesOrderFm.FormCreate(Sender: TObject);
begin
  DMFm:=TDMFm.Create(self);
  MasterOriSQL:=MasterQ.SQL.Text;
  KodeEdit.Properties.MaxLength:=MasterQKodenota.Size;
end;

procedure TMasterSalesOrderFm.cxButtonEdit1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  KodeEdit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  KodeEditExit(sender);
  MasterVGrid.SetFocus;
end;

procedure TMasterSalesOrderFm.KodeEditEnter(Sender: TObject);
begin
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
  MasterVGrid.Enabled:=false;
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  SaveBtn.Enabled:=false;
  DeleteBtn.Enabled:=false;
  MasterQ.Close;
end;

procedure TMasterSalesOrderFm.FormShow(Sender: TObject);
begin
  KodeEdit.SetFocus;
end;

procedure TMasterSalesOrderFm.KodeEditExit(Sender: TObject);
begin
  if KodeEdit.Text<>'' then
  begin
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kodenota like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      DeleteBtn.Enabled:=True;
    end;
    SaveBtn.Enabled:=True;
    MasterVGrid.Enabled:=True;
  end;
end;

procedure TMasterSalesOrderFm.KodeEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_F5) then
  begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
  end;
  if (Key=VK_escape) then
  begin
    ExitBtn.Click;
  end;
end;

procedure TMasterSalesOrderFm.SaveBtnClick(Sender: TObject);
begin
  if StatusBar.Panels[0].Text= 'Mode : Entry' then
  begin
    MasterQ.Edit;
    KodeQ.Open;
    if KodeQ.IsEmpty then
    begin
      MasterQKodenota.AsString:=DMFm.Fill('1',10,'0',True);
    end
    else
    begin
      MasterQKodenota.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
    end;
    MasterQ.Post;
    KodeQ.Close;
    KodeEdit.Text:=MasterQKodenota.AsString;
  end;
  MenuUtamaFm.Database1.StartTransaction;
  try
    MasterQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    ShowMessage('Sales Order dengan Kode ' + KodeEdit.Text + ' telah disimpan');
    KodeEdit.SetFocus;
  except
    MenuUtamaFm.Database1.Rollback;
    MasterQ.RollbackUpdates;
    ShowMessage('Penyimpanan Gagal');
  end;
end;

procedure TMasterSalesOrderFm.MasterQAfterInsert(DataSet: TDataSet);
begin
  DMFm.GetDateQ.Open;
  MasterQCreateDate.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  MasterQCreateBy.AsString:=User;
  DMFm.GetDateQ.Close;
end;

procedure TMasterSalesOrderFm.MasterQBeforePost(DataSet: TDataSet);
begin
  DMFm.GetDateQ.Open;
  MasterQTglEntry.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  MasterQOperator.AsString:=User;
  DMFm.GetDateQ.Close;
end;

procedure TMasterSalesOrderFm.DeleteBtnClick(Sender: TObject);
begin
  if MessageDlg('Hapus Sales Order '+KodeEdit.Text+' - '+MasterQKodeNota.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     MenuUtamaFm.Database1.StartTransaction;
     try
       MasterQ.Delete;
       MasterQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;
       MessageDlg('Sales Order telah dihapus.',mtInformation,[mbOK],0);
     except
       MenuUtamaFm.Database1.Rollback;
       MasterQ.RollbackUpdates;
       MessageDlg('Sales Order pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
     end;
     KodeEdit.SetFocus;
  end;
end;

procedure TMasterSalesOrderFm.SearchBtnClick(Sender: TObject);
begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
end;

end.
