unit Pembelian;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxCalendar,
  cxCurrencyEdit, cxCalc, dxSkinscxPCPainter, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxGridCustomView, cxGrid,
  cxDropDownEdit, cxLabel, UCrpeClasses, UCrpe32, cxCheckBox;

type
  TPembelianFm = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    MasterQ: TSDQuery;
    MasterDs: TDataSource;
    StatusBar: TStatusBar;
    MasterUS: TSDUpdateSQL;
    KodeQ: TSDQuery;
    ExitBtn: TcxButton;
    DeleteBtn: TcxButton;
    SaveBtn: TcxButton;
    KodeEdit: TcxButtonEdit;
    lbl1: TLabel;
    KodeQkode: TStringField;
    DetailPembelianQ: TSDQuery;
    DataSource1: TDataSource;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    BarangQ: TSDQuery;
    DetailPembelianUS: TSDUpdateSQL;
    BarangUS: TSDUpdateSQL;
    cxCurrencyEdit1: TcxCurrencyEdit;
    cxLabel1: TcxLabel;
    cxLabel3: TcxLabel;
    cxButton1: TcxButton;
    BarangQKode: TStringField;
    BarangQNama: TStringField;
    BarangQHargaBeli: TCurrencyField;
    BarangQHargaJual: TCurrencyField;
    BarangQSatuan: TStringField;
    cxLabel2: TcxLabel;
    SDQuery1: TSDQuery;
    StringField1: TStringField;
    StringField2: TStringField;
    CurrencyField1: TCurrencyField;
    CurrencyField2: TCurrencyField;
    StringField3: TStringField;
    cxLabel5: TcxLabel;
    cxDateEdit1: TcxDateEdit;
    cxLabel6: TcxLabel;
    cxButtonEdit1: TcxButtonEdit;
    SupplierQ: TSDQuery;
    cxCheckBox1: TcxCheckBox;
    cxLabel7: TcxLabel;
    cxCurrencyEdit2: TcxCurrencyEdit;
    cxTextEdit1: TcxTextEdit;
    cxLabel8: TcxLabel;
    cxGrid2: TcxGrid;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2Level1: TcxGridLevel;
    DataSource3: TDataSource;
    ViewQ: TSDQuery;
    MasterQKode: TStringField;
    MasterQNomorNota: TStringField;
    MasterQSupplier: TStringField;
    MasterQTgl: TDateTimeField;
    MasterQGrandTotal: TCurrencyField;
    MasterQStatus: TStringField;
    ViewQKode: TStringField;
    ViewQNomorNota: TStringField;
    ViewQSupplier: TStringField;
    ViewQTgl: TDateTimeField;
    ViewQGrandTotal: TCurrencyField;
    ViewQStatus: TStringField;
    SupplierQKode: TStringField;
    SupplierQNama: TStringField;
    SupplierQAlamat: TMemoField;
    SupplierQKota: TStringField;
    SupplierQTelp1: TStringField;
    SupplierQTelp2: TStringField;
    SupplierQTelp3: TStringField;
    ViewQNamaSupplier: TStringField;
    cxGrid2DBTableView1Column1: TcxGridDBColumn;
    cxGrid2DBTableView1Column2: TcxGridDBColumn;
    cxGrid2DBTableView1Column3: TcxGridDBColumn;
    MasterQNamaSupplier: TStringField;
    DetailPembelianQKodeBeli: TStringField;
    DetailPembelianQBarang: TStringField;
    DetailPembelianQSatuan: TStringField;
    DetailPembelianQSubTotal: TCurrencyField;
    DetailPembelianQNamaBarang: TStringField;
    DetailPembelianQHarga: TCurrencyField;
    DetailPembelianQHargaTerakhir: TCurrencyField;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    cxGrid1DBTableView1Column2: TcxGridDBColumn;
    cxGrid1DBTableView1Column3: TcxGridDBColumn;
    cxGrid1DBTableView1Column4: TcxGridDBColumn;
    cxGrid1DBTableView1Column5: TcxGridDBColumn;
    cxGrid1DBTableView1Column6: TcxGridDBColumn;
    cxGrid1DBTableView1Column7: TcxGridDBColumn;
    cxGrid1DBTableView1Column8: TcxGridDBColumn;
    cxLabel4: TcxLabel;
    cxDateEdit2: TcxDateEdit;
    MasterQTglBayar: TDateTimeField;
    cxGrid2DBTableView1Column4: TcxGridDBColumn;
    DetailPembelianQJumlah: TFloatField;
    DetailPembelianQStok: TFloatField;
    BarangQJumlah: TFloatField;
    SDQuery1Jumlah: TFloatField;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ExitBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure KodeEditEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SaveBtnClick(Sender: TObject);
    procedure DeleteBtnClick(Sender: TObject);
    procedure SearchBtnClick(Sender: TObject);
    procedure MasterVGridKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MasterVGridBarangEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MasterVGridJumlahEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure MasterVGridHargaSatuanEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxGrid1DBTableView1Column1PropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure SDQuery1BeforePost(DataSet: TDataSet);
    procedure cxGrid1Exit(Sender: TObject);
    procedure cxGrid1DBTableView1Column6PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxGrid1DBTableView1Column8PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure SDQuery1AfterPost(DataSet: TDataSet);
    procedure DetailQAfterPost(DataSet: TDataSet);
    procedure DetailQBeforePost(DataSet: TDataSet);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxGrid1DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SaveBtnKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxButton1Click(Sender: TObject);
    procedure cxGrid1DBTableView1Column7PropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure KodeEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxDateEdit1PropertiesChange(Sender: TObject);
    procedure cxGrid1DBTableView1Column5PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxCheckBox1PropertiesChange(Sender: TObject);
    procedure cxTextEdit1PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure cxGrid2DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxGrid1DBTableView1Column7PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure DetailPembelianQAfterDelete(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent);
  end;

var
  PembelianFm: TPembelianFm;

  MasterOriSQL: string;
  BarangOriSQL, DetailPembelianOriSQL:string;
implementation

uses MenuUtama, DropDown, DM, StrUtils, DateUtils;

{$R *.dfm}

{ TMasterArmadaFm }

constructor TPembelianFm.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
end;

procedure TPembelianFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TPembelianFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TPembelianFm.ExitBtnClick(Sender: TObject);
begin
  Release;
end;

procedure TPembelianFm.FormCreate(Sender: TObject);
begin
  DMFm:=TDMFm.Create(self);
  MasterOriSQL:=MasterQ.SQL.Text;
  DetailPembelianOriSQL:=DetailPembelianQ.SQL.Text;
  BarangOriSQL:=BarangQ.SQL.Text;
  KodeEdit.Properties.MaxLength:=MasterQKode.Size;
end;

procedure TPembelianFm.cxButtonEdit1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    SupplierQ.Close;
    SupplierQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,SupplierQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      MasterQSupplier.AsString:=SupplierQKode.AsString;
      cxButtonEdit1.Text:=SupplierQNama.AsString;
    end;
    DropDownFm.Release;
end;

procedure TPembelianFm.KodeEditEnter(Sender: TObject);
begin
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  SaveBtn.Enabled:=false;
  DeleteBtn.Enabled:=false;
  MasterQ.Close;
end;

procedure TPembelianFm.FormShow(Sender: TObject);
begin
  KodeEdit.SetFocus;
end;

procedure TPembelianFm.KodeEditExit(Sender: TObject);
begin
  if KodeEdit.Text<>'' then
  begin
    //kodeedit.Text:=DMfm.Fill(kodeedit.Text,10,'0',true);
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
      MasterQ.Edit;
      cxDateEdit1.Date:=Today;
     // MasterQJumlahDibayar.AsInteger:=0;
      DetailPembelianQ.Close;
      DetailPembelianQ.ParamByName('text').AsString:='a';
      DetailPembelianQ.Open;
      cxGrid1.Enabled:=true;
    end
    else
    begin
      DetailPembelianQ.Close;
      DetailPembelianQ.ParamByName('text').AsString:=KodeEdit.Text;
      DetailPembelianQ.Open;
      DetailPembelianQ.Edit;
      masterq.Edit;
      cxDateEdit1.Date:=masterqtgl.AsDateTime;
      if MasterQTglBayar.AsDateTime < 1/1/2000 then cxDateEdit2.Text:='NULL' else
      cxdateedit2.Date:=Masterqtglbayar.AsDateTime;
      cxTextEdit1.Text:=MasterQNomorNota.AsString;
      cxButtonEdit1.Text:=MasterQNamaSupplier.asstring;
      cxCurrencyEdit1.Text:=MasterQGrandTotal.AsString;
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      cxGrid1.Enabled:=false;
      DeleteBtn.Enabled:=True;
      cxGrid1.Enabled:=true;
    end;
    SaveBtn.Enabled:=True;
    //MasterVGrid.Enabled:=True;
  end;
end;

procedure TPembelianFm.KodeEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    if key=VK_F6 then
  begin
     SaveBtn.Click;
  end
  else if key=VK_F7 then
  begin
    DeleteBtn.Click;
  end
  else if key=VK_F8 then
  begin
    ExitBtn.Click;
  end
  else if key=VK_F5 then
  begin
    cxButtonEdit1PropertiesButtonClick(self,0);
  end
  else if key=VK_F4 then
  begin
    //ShowMessage('asd');
    cxGrid1DBTableView1.NavigatorButtons.Insert.Click;
    cxGrid1.SetFocus;
  end;
  if (Key=VK_escape) then
  begin
    ExitBtn.Click;
  end;
end;

procedure TPembelianFm.SaveBtnClick(Sender: TObject);
var kodeprint:string;
begin

    if StatusBar.Panels[0].Text= 'Mode : Entry' then
    begin
      MasterQ.Edit;
      KodeQ.Close;
      KodeQ.Open;
      if KodeQ.IsEmpty then
      begin
        MasterQKode.AsString:=DMFm.Fill('1',10,'0',True);
      end
      else
      begin
        MasterQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
      end;
      //Dmfm.GetDateQ.Open;
      //MasterQTgl.AsDateTime:=cxDateEdit1.Date;
      //MasterQTglBayar.AsDateTime:=cxdateedit2.Date;
      //dmfm.GetDateQ.Close;
      MasterQGrandTotal.AsInteger:=strtoint(cxlabel2.Caption);
      //if cxCheckBox1.Checked then MasterQJumlahDibayar.AsInteger:=MasterQGrandTotal.AsInteger
      //else MasterQJumlahDibayar.AsInteger:=cxCurrencyEdit2.EditValue;
      //MasterQGrandTotal.AsInteger:=0;

      KodeQ.Close;
      KodeEdit.Text:=MasterQKode.AsString;

    end
    else
    begin
      MasterQGrandTotal.AsCurrency:=cxCurrencyEdit1.Value;
    end;
    MasterQTgl.AsDateTime:=cxDateEdit1.Date;
    if cxDateEdit2.text<>'' then
    begin
        MasterQTglBayar.AsDateTime:=cxdateedit2.Date;
        MasterQStatus.AsString:='LUNAS';
    end
    else
    begin
        masterqstatus.AsString:='BELUM LUNAS';
        MasterQTglBayar.AsVariant:=NULL;
    end;
      MasterQGrandTotal.AsInteger:=strtoint(cxCurrencyEdit1.EditValue);
      MasterQ.Post;
    MenuUtamaFm.Database1.StartTransaction;
    try
      MasterQ.ApplyUpdates;
      DetailPembelianQ.ApplyUpdates;
      MenuUtamaFm.Database1.Commit;
      MasterQ.CommitUpdates;
      DetailPembelianQ.CommitUpdates;
      //DetailPenjualanQ.First;
      {while DetailPenjualanQ.Eof=false do
      begin
        SDQuery1.Close;
        SDQuery1.SQL.Text:='update barang set jumlah=jumlah-'+ DetailPenjualanQJumlah.AsString+' where kode="' + DetailPenjualanQBarang.AsString + '"';
        SDQuery1.ExecSQL;
        DetailPenjualanQ.Next;
      end;}
      ShowMessage('Penyimpanan Berhasil');
    except
    on E : Exception do
    begin
              ShowMessage(E.ClassName+' error raised, with message : '+E.Message);

      MenuUtamaFm.Database1.Rollback;
      MasterQ.RollbackUpdates;
      DetailPembelianQ.RollbackUpdates;
      end;
    end;
    SaveBtn.Enabled:=false;
    cxCheckBox1.Checked:=false;
    cxCurrencyEdit1.EditValue:=null;
    cxCurrencyEdit2.EditValue:=0;
    cxButtonEdit1.Text:='';
    ViewQ.Close;
    ViewQ.Open;
    viewq.Refresh;
    barangq.Refresh;
    
end;

procedure TPembelianFm.DeleteBtnClick(Sender: TObject);
begin
  if MessageDlg('Hapus Penjualan '+KodeEdit.Text+' - '+MasterQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     MenuUtamaFm.Database1.StartTransaction;
     try
       SDQuery1.SQL.Text:='delete from detailpembelian where kodebeli='+MasterQKode.AsString;
       SDQuery1.ExecSQL;
       MasterQ.Delete;
       //DetailPenjualanQ.ApplyUpdates;
       //PembayaranQ.ApplyUpdates;
       MasterQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;
       MessageDlg('Penjualan telah dihapus.',mtInformation,[mbOK],0);
     except
       MenuUtamaFm.Database1.Rollback;
       MasterQ.RollbackUpdates;
       DetailPembelianQ.RollbackUpdates;
       MessageDlg('Pembelian pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
     end;
     DeleteBtn.Enabled:=false;
     KodeEdit.SetFocus;
  end;
end;

procedure TPembelianFm.SearchBtnClick(Sender: TObject);
begin
    cxGrid1DBTableView1.NavigatorButtons.Insert.Click;
    cxGrid1.SetFocus;
    cxGrid1DBTableView1.Columns[0].Focused:=true;
end;

procedure TPembelianFm.MasterVGridKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
if key=VK_F6 then
begin
   SaveBtn.Click;
end
else if key=VK_F7 then
begin
  DeleteBtn.Click;
end
else if key=VK_F8 then
begin
  ExitBtn.Click;
end
  else if key=VK_F4 then
  begin
    cxGrid1DBTableView1.NavigatorButtons.Insert.Click;
    cxGrid1.SetFocus;
  end;
end;

procedure TPembelianFm.MasterVGridBarangEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
    DropDownFm:=TDropdownfm.Create(Self,BarangQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      {Masterqbarang.AsString:=barangqkode.asstring;
      masterqsupplier.asstring:=barangqsupplier.asstring;
      masterqnama.AsString:=barangqnama.asstring;
      masterqsatuan.asstring:=barangqsatuan.asstring;
      masterqjenis.AsString:=barangqjenis.asstring;
      MasterVGrid.SetFocus;}
    end;
    DropDownFm.Release;
end;

procedure TPembelianFm.MasterVGridJumlahEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
  //MasterQJumlah.AsInteger:=DisplayValue;
  //MasterQGrandTotal.AsInteger:=MasterQJumlah.AsInteger * MasterQHargaSatuan.AsInteger;
end;

procedure TPembelianFm.MasterVGridHargaSatuanEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin

  //MasterQHargaSatuan.AsCurrency:=DisplayValue;
  //MasterQGrandTotal.AsInteger:=MasterQJumlah.AsInteger * MasterQHargaSatuan.AsInteger;

end;

procedure TPembelianFm.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  //KodeEdit.Text:=SDQuery1Kode.AsString;
  //KodeEditExit(self);
  masterq.Close;
  //MasterQ.SQL.Text:= 'select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+SDQuery1Kode.AsString+'%');
  //masterq.ParamByName('text').AsString:=SDQuery1Kode.AsString;
  masterq.Open;
  SaveBtn.Enabled:=true;
  DeleteBtn.Enabled:=true;
end;

procedure TPembelianFm.cxGrid1DBTableView1Column1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  //ShowMessage(inttostr(abuttonindex));
    DetailPembelianQ.Open;
    DetailPembelianQ.Insert;
    DropDownFm:=TDropdownfm.Create(Self,BarangQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      DetailPembelianQBarang.AsString:=barangqkode.asstring;
      DetailPembelianQSatuan.AsString:=BarangQSatuan.AsString;
    end;
    DropDownFm.Release;
    //cxGrid1DBTableView1Column7.Focused:=true;
end;

procedure TPembelianFm.SDQuery1BeforePost(DataSet: TDataSet);
var i : integer;
begin
    
end;

procedure TPembelianFm.cxGrid1Exit(Sender: TObject);
begin
  cxGrid1DBTableView1.NavigatorButtons.Post.Click;
  cxButton1.SetFocus;
  //SaveBtn.SetFocus;
end;

procedure TPembelianFm.cxGrid1DBTableView1Column6PropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
  DetailPembelianQJumlah.AsFloat:=DisplayValue;
  DetailPembelianQSubTotal.AsCurrency:=DetailPembelianQHarga.AsCurrency*DetailPembelianQJumlah.AsFloat;
  //DetailQHargaSatuan.AsString:=DisplayValue;
  //DetailQSubTotal.AsInteger:=DetailQHargaSatuan.AsInteger * DetailQJumlah.AsInteger - DetailQDiskon.AsInteger;
  //cxLabel1.Caption:=inttostr();
end;

procedure TPembelianFm.cxGrid1DBTableView1Column8PropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
  //DetailPembelianQDiskon.AsString:=DisplayValue;
  //DetailPembelianQSubTotal.AsInteger:=DetailPembelianQHargaSatuan.AsInteger * DetailPembelianQJumlah.AsInteger - DetailPembelianQDiskon.AsInteger;

end;

procedure TPembelianFm.SDQuery1AfterPost(DataSet: TDataSet);
var i,temp:integer;
begin

end;

procedure TPembelianFm.DetailQAfterPost(DataSet: TDataSet);
var temp,i:integer;
begin
    temp:=0;
    DetailPembelianQ.First;
    for i:= 1 to DetailPembelianQ.RecordCount do
    begin
      //ShowMessage(DetailQSubTotal.AsString);
      temp:=temp+ DetailPembelianQSubTotal.AsInteger;
      DetailPembelianQ.Next;
    end;
    cxCurrencyEdit1.Text:=inttostr(temp);
    cxlabel2.Caption:=inttostr(temp);
    cxCurrencyEdit1.SetFocus;
    {BarangQ.Edit;
    //ShowMessage(BarangQStok.AsString);
    BarangQStok.AsInteger:=BarangQStok.AsInteger-DetailQJumlah.AsInteger;
    //ShowMessage(BarangQStok.AsString);
    BarangQ.Post;}
    DetailPembelianQ.First;
      cxGrid1DBTableView1.NavigatorButtons.Post.Click;
  cxGrid1DBTableView1.NavigatorButtons.Insert.Click;
  cxGrid1.SetFocus;
  cxGrid1DBTableView1.Columns[7].Focused:=true;
end;

procedure TPembelianFm.DetailQBeforePost(DataSet: TDataSet);
begin
    if KodeEdit.Text='INSERT BARU' then
    begin
      KodeQ.Open;
      if KodeQ.IsEmpty then
      begin
        DetailPembelianQKodeBeli.AsString:=DMFm.Fill('1',10,'0',True);
      end
      else
      begin
        DetailPembelianQKodeBeli.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
      end;
    end
    else
    begin
      DetailPembelianQKodeBeli.AsString:=KodeEdit.Text;
    end;
    //ShowMessage(SDQuery1KodeJual.AsString);
end;

procedure TPembelianFm.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
  var temp:boolean;
begin
    if key=VK_F6 then
  begin
     SaveBtn.Click;
  end
  else if key=VK_F7 then
  begin
    DeleteBtn.Click;
  end
  else if key=VK_F8 then
  begin
    ExitBtn.Click;
  end
  else if key=VK_F5 then
  begin
    cxButtonEdit1PropertiesButtonClick(self,0);
  end
  else if key=VK_F4 then
  begin
    //ShowMessage('asd');
    cxGrid1DBTableView1.NavigatorButtons.Insert.Click;
    cxGrid1.SetFocus;
  end;
end;

procedure TPembelianFm.cxGrid1DBTableView1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    if key=VK_F6 then
  begin
     SaveBtn.Click;
  end
  else if key=VK_F7 then
  begin
    DeleteBtn.Click;
  end
  else if key=VK_F8 then
  begin
    ExitBtn.Click;
  end
  else if key=VK_F5 then
  begin
    cxButtonEdit1PropertiesButtonClick(self,0);
  end
  else if key=VK_F4 then
  begin
    //ShowMessage('asd');
    cxGrid1DBTableView1.NavigatorButtons.Insert.Click;
    cxGrid1.SetFocus;
    cxGrid1DBTableView1Column1.Focused:=true;
  end;
end;

procedure TPembelianFm.SaveBtnKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
           if key=VK_F6 then
  begin
     SaveBtn.Click;
  end
  else if key=VK_F7 then
  begin
    DeleteBtn.Click;
  end
  else if key=VK_F8 then
  begin
    ExitBtn.Click;
  end
  else if key=VK_F5 then
  begin
    cxButtonEdit1PropertiesButtonClick(self,0);
  end
  else if key=VK_F4 then
  begin
    //ShowMessage('asd');
    cxGrid1DBTableView1.NavigatorButtons.Insert.Click;
    cxGrid1.SetFocus;
    cxGrid1DBTableView1Column1.Focused:=true;
  end;
end;

procedure TPembelianFm.cxButton1Click(Sender: TObject);
begin
  cxGrid1DBTableView1.NavigatorButtons.Insert.Click;
  cxGrid1.SetFocus;
  cxGrid1DBTableView1Column1.Focused:=true;
end;

procedure TPembelianFm.cxGrid1DBTableView1Column7PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  BarangQ.Close;
  BarangQ.Open;
    DropDownFm:=TDropdownfm.Create(Self,BarangQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      DetailPembelianQ.Insert;
      DetailPembelianQ.Edit;
      DetailPembelianQbarang.AsString:=barangqkode.asstring;
      //DetailPembelianQDiskon.AsInteger:=0;
      DetailPembelianQKodeBeli.AsString:=MasterQKode.AsString;
    end;
    DropDownFm.Release;
    cxGrid1DBTableView1.Columns[5].Focused:=true;
end;

procedure TPembelianFm.KodeEditPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  if kodeedit.Text ='' then kodeedit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  MasterQ.Open;
  MasterQ.Insert;
  MasterQ.Edit;
  KodeEditExit(sender);
  DetailPembelianQ.Close;
  DetailPembelianQ.SQL.Text:=DetailPembelianOriSQL;
  DetailPembelianQ.Open;
  cxButtonEdit1.Text:='';
  cxTextEdit1.Text:='';
  cxDateEdit2.Text:='';
  {cxGrid1.SetFocus;
  cxGrid1DBTableView1.NavigatorButtons.Post.Click;
  cxGrid1DBTableView1.NavigatorButtons.Insert.Click;
  cxGrid1.SetFocus;
  cxGrid1DBTableView1.Columns[0].Focused:=true;   }
end;

procedure TPembelianFm.cxDateEdit1PropertiesChange(Sender: TObject);
begin
  MasterQTgl.AsDateTime:=cxDateEdit1.Date;
end;

procedure TPembelianFm.cxGrid1DBTableView1Column5PropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
  {if DisplayValue> DetailPembelianQStok.AsInteger then
  begin
    ShowMessage('stok tidak cukup');
    DetailPembelianQJumlah.AsInteger:=0;
  end
  else
  begin
    DetailPembelianQJumlah.AsInteger:=DisplayValue;
    DetailPembelianQSubTotal.AsInteger:=DetailPembelianQHargaSatuan.AsInteger * DetailPembelianQJumlah.AsInteger - DetailPembelianQDiskon.AsInteger;
  end; }
end;

procedure TPembelianFm.cxCheckBox1PropertiesChange(Sender: TObject);
begin
  if cxCheckBox1.Checked then
  begin
    cxLabel7.Visible:=false;
    cxCurrencyEdit2.Visible:=false;
  end
  else
  begin
    cxlabel7.Visible:=true;
    cxCurrencyEdit2.Visible:=true;
  end;
end;

procedure TPembelianFm.cxTextEdit1PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
begin
  MasterQNomorNota.AsString := cxTextEdit1.Text;
end;

procedure TPembelianFm.cxGrid2DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  KodeEdit.Text:=ViewQKode.AsString;
  KodeEditExit(self);
  cxCurrencyEdit1.SetFocus;
end;

procedure TPembelianFm.cxGrid1DBTableView1Column7PropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
  DetailPembelianQHarga.AsCurrency:=DisplayValue;
  DetailPembelianQSubTotal.AsCurrency:=DetailPembelianQHarga.AsCurrency*DetailPembelianQJumlah.AsFloat;
  
end;

procedure TPembelianFm.DetailPembelianQAfterDelete(DataSet: TDataSet);
var temp, i : integer;
begin
    temp:=0;
    DetailPembelianQ.First;
    for i:= 1 to DetailPembelianQ.RecordCount do
    begin
      temp:=temp+ DetailPembelianQSubTotal.AsInteger;
      DetailPembelianQ.Next;
    end;
    cxCurrencyEdit1.Text:=inttostr(temp);
    cxlabel2.Caption:=inttostr(temp);
    cxCurrencyEdit1.SetFocus;
end;

end.
