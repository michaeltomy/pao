unit DaftarBarang;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, SDEngine, Grids, DBGrids,
  cxGridBandedTableView, cxGridDBBandedTableView, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxGridCustomPopupMenu,
  cxGridPopupMenu, cxContainer, cxTextEdit, cxMaskEdit, cxDropDownEdit,
  StdCtrls;

type
  TDaftarBarangFm = class(TForm)
    pnl1: TPanel;
    DataSource1: TDataSource;
    cxGridViewRepository1: TcxGridViewRepository;
    cxGridViewRepository1DBBandedTableView1: TcxGridDBBandedTableView;
    cxGridPopupMenu1: TcxGridPopupMenu;
    cxGrid1: TcxGrid;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxGrid1Level1: TcxGridLevel;
    MasterQ: TSDQuery;
    Panel1: TPanel;
    cxTextEdit1: TcxTextEdit;
    cxComboBox1: TcxComboBox;
    MasterQKode: TStringField;
    MasterQNama: TStringField;
    MasterQHargaBeli: TCurrencyField;
    MasterQHargaJual: TCurrencyField;
    MasterQSatuan: TStringField;
    cxGridViewRepository1DBBandedTableView1Column1: TcxGridDBBandedColumn;
    cxGridViewRepository1DBBandedTableView1Column2: TcxGridDBBandedColumn;
    cxGridViewRepository1DBBandedTableView1Column3: TcxGridDBBandedColumn;
    cxGridViewRepository1DBBandedTableView1Column4: TcxGridDBBandedColumn;
    cxGridViewRepository1DBBandedTableView1Column5: TcxGridDBBandedColumn;
    cxGridViewRepository1DBBandedTableView1Column6: TcxGridDBBandedColumn;
    MasterQMinimumStok: TIntegerField;
    cxGridViewRepository1DBBandedTableView1Column7: TcxGridDBBandedColumn;
    SDUpdateSQL1: TSDUpdateSQL;
    Button1: TButton;
    MasterQJumlah: TFloatField;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormCreate(Sender: TObject);
    procedure dbgrd1DblClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cxTextEdit1PropertiesChange(Sender: TObject);
    procedure cxComboBox1PropertiesChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
    q :TSDQuery;
  public
    { Public declarations }
    kode:string;
//    constructor Create(aOwner: TComponent;Query1:TSDQuery);overload;
  end;

var
  DaftarBarangFm: TDaftarBarangFm;
  masterorisql:string;

implementation

uses MenuUtama;

{$R *.dfm}

{ TDropDownFm }


procedure TDaftarBarangFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TDaftarBarangFm.FormCreate(Sender: TObject);
var i : integer;
begin
  MasterQ.Open;
  masterorisql:=masterq.sql.text;
  masterq.Edit;
end;

procedure TDaftarBarangFm.dbgrd1DblClick(Sender: TObject);
begin
 // kode:=q.Fields[0].AsString;
 // ModalResult:=mrOk;
end;

procedure TDaftarBarangFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
release;
end;

procedure TDaftarBarangFm.cxTextEdit1PropertiesChange(Sender: TObject);
begin
  if cxComboBox1.ItemIndex=0 then
  begin
    masterq.Close;
    masterq.SQL.Text:= 'select * from barang where nama like '+ QuotedStr('%'+cxTextEdit1.Text+'%');
    masterq.Open;
  end
  else if cxComboBox1.ItemIndex=1 then
  begin
    masterq.Close;
    masterq.SQL.Text:= 'select * from barang where kode like '+ QuotedStr('%'+cxTextEdit1.Text+'%');
    masterq.Open;
  end;
end;

procedure TDaftarBarangFm.cxComboBox1PropertiesChange(Sender: TObject);
begin
cxTextEdit1.Clear;
end;

procedure TDaftarBarangFm.FormShow(Sender: TObject);
begin
  cxTextEdit1.SetFocus;
end;

procedure TDaftarBarangFm.Button1Click(Sender: TObject);
begin
  MenuUtamaFm.Database1.StartTransaction;
  try
    MasterQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    ShowMessage('Update Berhasil');
   // KodeEdit.SetFocus;
  except
    MenuUtamaFm.Database1.Rollback;
    MasterQ.RollbackUpdates;
    ShowMessage('Penyimpanan Gagal');
  end;

end;

end.
