object KartuGaransiDropDownFm: TKartuGaransiDropDownFm
  Left = 277
  Top = 136
  Width = 903
  Height = 498
  Caption = 'Kartu Garansi Drop Down'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid1: TcxGrid
    Left = 0
    Top = 0
    Width = 887
    Height = 460
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object cxGrid1DBTableView1: TcxGridDBTableView
      OnCellDblClick = cxGrid1DBTableView1CellDblClick
      DataController.DataSource = LPBDs
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.IncSearch = True
      OptionsData.Editing = False
      OptionsView.GroupByBox = False
      object cxGrid1DBTableView1Tanggal: TcxGridDBColumn
        DataBinding.FieldName = 'Tanggal'
        Width = 115
      end
      object cxGrid1DBTableView1NamaPelanggan: TcxGridDBColumn
        DataBinding.FieldName = 'NamaPelanggan'
        Width = 204
      end
      object cxGrid1DBTableView1Resep: TcxGridDBColumn
        DataBinding.FieldName = 'Resep'
        Width = 226
      end
      object cxGrid1DBTableView1Lensa: TcxGridDBColumn
        DataBinding.FieldName = 'Lensa'
        Width = 216
      end
      object cxGrid1DBTableView1Frame: TcxGridDBColumn
        DataBinding.FieldName = 'Frame'
        Width = 208
      end
      object cxGrid1DBTableView1PD: TcxGridDBColumn
        DataBinding.FieldName = 'PD'
        Width = 140
      end
      object cxGrid1DBTableView1SPHki: TcxGridDBColumn
        DataBinding.FieldName = 'SPHki'
        Width = 124
      end
      object cxGrid1DBTableView1SPHka: TcxGridDBColumn
        DataBinding.FieldName = 'SPHka'
        Width = 124
      end
      object cxGrid1DBTableView1CYLki: TcxGridDBColumn
        DataBinding.FieldName = 'CYLki'
        Width = 104
      end
      object cxGrid1DBTableView1CYLka: TcxGridDBColumn
        DataBinding.FieldName = 'CYLka'
        Width = 143
      end
      object cxGrid1DBTableView1AXki: TcxGridDBColumn
        DataBinding.FieldName = 'AXki'
        Width = 120
      end
      object cxGrid1DBTableView1AXka: TcxGridDBColumn
        DataBinding.FieldName = 'AXka'
        Width = 118
      end
      object cxGrid1DBTableView1ADDki: TcxGridDBColumn
        DataBinding.FieldName = 'ADDki'
        Width = 131
      end
      object cxGrid1DBTableView1ADDka: TcxGridDBColumn
        DataBinding.FieldName = 'ADDka'
        Width = 113
      end
      object cxGrid1DBTableView1Jauh: TcxGridDBColumn
        DataBinding.FieldName = 'Jauh'
      end
      object cxGrid1DBTableView1Dekat: TcxGridDBColumn
        DataBinding.FieldName = 'Dekat'
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object MasterQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from kartugaransi')
    Left = 216
    Top = 368
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQTanggal: TDateTimeField
      FieldName = 'Tanggal'
    end
    object MasterQResep: TStringField
      FieldName = 'Resep'
      Size = 200
    end
    object MasterQID: TStringField
      FieldName = 'ID'
      Size = 200
    end
    object MasterQLensa: TStringField
      FieldName = 'Lensa'
      Size = 200
    end
    object MasterQFrame: TStringField
      FieldName = 'Frame'
      Size = 200
    end
    object MasterQPelanggan: TStringField
      FieldName = 'Pelanggan'
      Size = 10
    end
    object MasterQPD: TStringField
      FieldName = 'PD'
      Size = 50
    end
    object MasterQSPHki: TStringField
      FieldName = 'SPHki'
      Size = 50
    end
    object MasterQSPHka: TStringField
      FieldName = 'SPHka'
      Size = 50
    end
    object MasterQCYLki: TStringField
      FieldName = 'CYLki'
      Size = 50
    end
    object MasterQCYLka: TStringField
      FieldName = 'CYLka'
      Size = 50
    end
    object MasterQAXki: TStringField
      FieldName = 'AXki'
      Size = 50
    end
    object MasterQAXka: TStringField
      FieldName = 'AXka'
      Size = 50
    end
    object MasterQADDki: TStringField
      FieldName = 'ADDki'
      Size = 50
    end
    object MasterQADDka: TStringField
      FieldName = 'ADDka'
      Size = 50
    end
    object MasterQJauh: TBooleanField
      FieldName = 'Jauh'
    end
    object MasterQDekat: TBooleanField
      FieldName = 'Dekat'
    end
    object MasterQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object MasterQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object MasterQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object MasterQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object MasterQNamaPelanggan: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPelanggan'
      LookupDataSet = PelangganQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Pelanggan'
      Size = 150
      Lookup = True
    end
  end
  object LPBDs: TDataSource
    DataSet = MasterQ
    Left = 248
    Top = 368
  end
  object PelangganQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from pelanggan')
    Left = 112
    Top = 328
  end
end
