unit PrepDropDown;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, SDEngine, Grids, DBGrids,
  cxGridBandedTableView, cxGridDBBandedTableView, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxGridCustomPopupMenu,
  cxGridPopupMenu;

type
  TPrepDropDownFm = class(TForm)
    pnl1: TPanel;
    DataSource1: TDataSource;
    SupplierQ: TSDQuery;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    SupplierQKode: TStringField;
    SupplierQBahan: TStringField;
    BahanQ: TSDQuery;
    BahanQKode: TStringField;
    BahanQNamaBahan: TStringField;
    BahanQJumlah: TFloatField;
    BahanQSatuan: TStringField;
    BahanQLokasi: TStringField;
    BahanQKategori: TStringField;
    SupplierQNamaBahan: TStringField;
    SupplierQSatuan: TStringField;
    cxGrid1DBTableView1Kode: TcxGridDBColumn;
    cxGrid1DBTableView1NamaBahan: TcxGridDBColumn;
    cxGrid1DBTableView1Satuan: TcxGridDBColumn;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure dbgrd1DblClick(Sender: TObject);
    procedure cxGridViewRepository1DBBandedTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxGridViewRepository1DBBandedTableView1KeyDown(
      Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    kode,nama,jenissup,SupOriSQL:string;
    constructor Create(aOwner: TComponent;kd:string); overload;
    constructor Create(aOwner: TComponent); overload;
  end;

var
  PrepDropDownFm: TPrepDropDownFm;

implementation

{$R *.dfm}

{ TDropDownFm }

constructor TPrepDropDownFm.Create(aOwner: TComponent;kd:string);
begin
  inherited Create(aOwner);
  jenissup:=kd;
end;

constructor TPrepDropDownFm.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
end;

procedure TPrepDropDownFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TPrepDropDownFm.dbgrd1DblClick(Sender: TObject);
begin
  kode:=SupplierQ.Fields[0].AsString;
 // nama:=SupplierQNamaToko.AsString;
  ModalResult:=mrOk;
end;

procedure TPrepDropDownFm.cxGridViewRepository1DBBandedTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  kode:=SupplierQ.Fields[0].AsString;
 // nama:=SupplierQNamaToko.AsString;
  ModalResult:=mrOk;
end;

procedure TPrepDropDownFm.cxGridViewRepository1DBBandedTableView1KeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
if Key=13 then
begin
  kode:=SupplierQ.Fields[0].AsString;
 // nama:=SupplierQNamaToko.AsString;
  ModalResult:=mrOk;
end;
end;

procedure TPrepDropDownFm.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  kode:=SupplierQ.Fields[0].AsString;
  //nama:=SupplierQNamaToko.AsString;
  ModalResult:=mrOk;
end;

procedure TPrepDropDownFm.FormShow(Sender: TObject);
begin
  bahanq.Open;
  SupOriSQL:=SupplierQ.SQL.Text;
  if jenissup='po' then
    begin
      SupplierQ.Close;
      SupplierQ.SQL.Text:='SELECT DISTINCT s.* FROM daftarbeli db, supplier s where db.status='+QuotedStr('ON PROCESS')+' and db.supplier=s.kode and  db.CashNCarry=0 and db.supplier<>'+QuotedStr('0000000000')+' order by s.namatoko';
      SupplierQ.ExecSQL;
      SupplierQ.Open;
    end
  else
    begin
      SupplierQ.Close;
      SupplierQ.SQL.Text:=SupOriSQL;
      SupplierQ.ExecSQL;
      SupplierQ.Open;
    end;
end;

end.
