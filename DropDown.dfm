object DropDownFm: TDropDownFm
  Left = 17
  Top = 71
  Width = 1241
  Height = 630
  AutoSize = True
  Caption = 'DropDownFm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PrintScale = poPrintToFit
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 567
    Width = 1225
    Height = 25
    Align = alBottom
    TabOrder = 0
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 41
    Width = 1225
    Height = 526
    Align = alClient
    TabOrder = 1
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGridViewRepository1DBBandedTableView1
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1225
    Height = 41
    Align = alTop
    TabOrder = 2
    object cxComboBox1: TcxComboBox
      Left = 152
      Top = 8
      Properties.Items.Strings = (
        'Nama')
      Properties.OnChange = cxComboBox1PropertiesChange
      TabOrder = 0
      Text = 'Nama'
      Width = 121
    end
    object cxTextEdit1: TcxTextEdit
      Left = 16
      Top = 8
      Properties.OnChange = cxTextEdit1PropertiesChange
      TabOrder = 1
      Width = 121
    end
  end
  object DataSource1: TDataSource
    Left = 296
    Top = 372
  end
  object cxGridViewRepository1: TcxGridViewRepository
    Left = 432
    Top = 384
    object cxGridViewRepository1DBBandedTableView1: TcxGridDBBandedTableView
      DragMode = dmAutomatic
      OnDblClick = cxGridViewRepository1DBBandedTableView1DblClick
      OnKeyDown = cxGridViewRepository1DBBandedTableView1KeyDown
      Navigator.Buttons.PriorPage.Visible = True
      Navigator.Buttons.Prior.Visible = True
      Navigator.Buttons.Insert.Visible = True
      Navigator.Buttons.Append.Visible = True
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = True
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.InfoPanel.Visible = True
      Navigator.Visible = True
      OnCellDblClick = cxGridViewRepository1DBBandedTableView1CellDblClick
      OnEditKeyDown = cxGridViewRepository1DBBandedTableView1EditKeyDown
      DataController.DataSource = DataSource1
      DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoSortByDisplayText, dcoFocusTopRowAfterSorting, dcoGroupsAlwaysExpanded, dcoImmediatePost, dcoInsertOnNewItemRowFocusing]
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      Filtering.ColumnFilteredItemsList = True
      OptionsBehavior.ImmediateEditor = False
      OptionsBehavior.IncSearch = True
      OptionsBehavior.ShowLockedStateImageOptions.BestFit = lsimImmediate
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Appending = True
      OptionsSelection.HideFocusRectOnExit = False
      OptionsView.CellEndEllipsis = True
      OptionsView.ColumnAutoWidth = True
      OptionsView.Indicator = True
      OptionsView.BandCaptionsInColumnAlternateCaption = True
      OptionsView.BandHeaderEndEllipsis = True
      Styles.Selection = cxStyle1
      Bands = <
        item
          Width = 90
        end>
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 552
    Top = 400
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 640
    Top = 392
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor]
      Color = clHighlight
    end
    object cxStyle2: TcxStyle
    end
  end
end
