object MasterInstansiFm: TMasterInstansiFm
  Left = 366
  Top = 224
  Width = 437
  Height = 195
  Caption = 'Master Instansi'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 421
    Height = 48
    Align = alTop
    TabOrder = 0
    object lbl1: TLabel
      Left = 8
      Top = 13
      Width = 25
      Height = 13
      Caption = 'Kode'
    end
    object KodeEdit: TcxButtonEdit
      Left = 40
      Top = 10
      Properties.Buttons = <
        item
          Caption = '+'
          Default = True
          Kind = bkText
        end>
      Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      TabOrder = 0
      OnEnter = KodeEditEnter
      OnExit = KodeEditExit
      OnKeyDown = KodeEditKeyDown
      Width = 121
    end
    object SearchBtn: TcxButton
      Left = 168
      Top = 10
      Width = 57
      Height = 21
      Caption = 'Search'
      TabOrder = 1
      OnClick = SearchBtnClick
    end
  end
  object pnl2: TPanel
    Left = 0
    Top = 87
    Width = 421
    Height = 51
    Align = alBottom
    TabOrder = 2
    object ExitBtn: TcxButton
      Left = 172
      Top = 10
      Width = 75
      Height = 25
      Caption = 'EXIT [F8]'
      TabOrder = 1
      OnClick = ExitBtnClick
    end
    object DeleteBtn: TcxButton
      Left = 91
      Top = 10
      Width = 75
      Height = 25
      Caption = 'DELETE [F7]'
      TabOrder = 2
      OnClick = DeleteBtnClick
    end
    object SaveBtn: TcxButton
      Left = 8
      Top = 10
      Width = 75
      Height = 25
      Caption = 'SAVE [F6]'
      TabOrder = 0
      OnClick = SaveBtnClick
    end
  end
  object MasterVGrid: TcxDBVerticalGrid
    Left = 0
    Top = 48
    Width = 421
    Height = 39
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    OptionsView.RowHeaderWidth = 149
    OptionsBehavior.GoToNextCellOnEnter = True
    OptionsBehavior.GoToNextCellOnTab = True
    OptionsData.CancelOnExit = False
    OptionsData.Appending = False
    OptionsData.Deleting = False
    OptionsData.DeletingConfirmation = False
    OptionsData.Inserting = False
    ParentFont = False
    TabOrder = 1
    TabStop = False
    OnKeyDown = MasterVGridKeyDown
    DataController.DataSource = MasterDs
    Version = 1
    object MasterVGridNama: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Nama'
      ID = 51
      ParentID = -1
      Index = 0
      Version = 1
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 138
    Width = 421
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object MasterQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from instansi')
    UpdateObject = MasterUS
    Left = 265
    Top = 9
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQNama: TStringField
      FieldName = 'Nama'
      Size = 200
    end
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 300
    Top = 6
  end
  object MasterUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      'select Kode, Nama'#13#10'from instansi'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update instansi'
      'set'
      '  Kode = :Kode,'
      '  Nama = :Nama'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into instansi'
      '  (Kode, Nama)'
      'values'
      '  (:Kode, :Nama)')
    DeleteSQL.Strings = (
      'delete from instansi'
      'where'
      '  Kode = :OLD_Kode')
    Left = 332
    Top = 10
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from instansi order by kode desc')
    Left = 233
    Top = 7
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object KategoriQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from kategori')
    Left = 176
    Top = 280
    object KategoriQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object KategoriQNamaKategori: TStringField
      FieldName = 'NamaKategori'
      Size = 50
    end
    object KategoriQMinStok: TFloatField
      FieldName = 'MinStok'
    end
    object KategoriQMaxStok: TFloatField
      FieldName = 'MaxStok'
    end
    object KategoriQDeleted: TBooleanField
      FieldName = 'Deleted'
    end
  end
end
