unit MasterMenuSet;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxCalendar,
  cxCurrencyEdit, cxCalc, cxRadioGroup, cxMemo, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxGridCustomView, cxGrid, cxLabel, cxCheckBox;

type
  TMasterMenuSetFm = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    MasterQ: TSDQuery;
    MasterDs: TDataSource;
    StatusBar: TStatusBar;
    KodeQ: TSDQuery;
    KodeQkode: TStringField;
    ExitBtn: TcxButton;
    DeleteBtn: TcxButton;
    SaveBtn: TcxButton;
    KodeEdit: TcxButtonEdit;
    lbl1: TLabel;
    SearchBtn: TcxButton;
    MenuSetDs: TDataSource;
    MenuSetUS: TSDUpdateSQL;
    cxLabel1: TcxLabel;
    MasterQKode: TStringField;
    MasterQNamaMenu: TStringField;
    MasterQKeterangan: TMemoField;
    MasterQHarga: TCurrencyField;
    DetailMenuQ: TSDQuery;
    ExecuteDeleteDetailMenuQ: TSDQuery;
    Panel1: TPanel;
    Panel2: TPanel;
    MasterVGrid: TcxDBVerticalGrid;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    MasterQKategori: TStringField;
    KategoriQ: TSDQuery;
    KategoriQKode: TStringField;
    KategoriQNama: TStringField;
    MasterQNamaKategori: TStringField;
    MasterVGridNamaMenu: TcxDBEditorRow;
    MasterVGridKeterangan: TcxDBEditorRow;
    MasterVGridHarga: TcxDBEditorRow;
    MasterVGridNamaKategori: TcxDBEditorRow;
    MasterQInisial: TStringField;
    MasterVGridInisial: TcxDBEditorRow;
    DetailMenuQKodeMenuSet: TStringField;
    DetailMenuQKodeMenu: TStringField;
    DetailMenuQJumlah: TStringField;
    DetailMenuQNamaMenu: TStringField;
    cxGrid1DBTableView1Jumlah: TcxGridDBColumn;
    cxGrid1DBTableView1NamaMenu: TcxGridDBColumn;
    MenuQ: TSDQuery;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure KodeEditEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SaveBtnClick(Sender: TObject);
    procedure MasterQAfterInsert(DataSet: TDataSet);
    procedure MasterQBeforePost(DataSet: TDataSet);
    procedure DeleteBtnClick(Sender: TObject);
    procedure SearchBtnClick(Sender: TObject);
    procedure MasterVGridExit(Sender: TObject);
    procedure MasterVGridEnter(Sender: TObject);
    procedure cxGrid1DBTableView1NavigatorButtonsButtonClick(
      Sender: TObject; AButtonIndex: Integer; var ADone: Boolean);
    procedure cxGrid1Exit(Sender: TObject);
    procedure cxGrid1DBTableView1Column1PropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure MasterVGridJenisKendaraanEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure ExitBtnClick(Sender: TObject);
    procedure cxGrid1DBTableView1NamaMenuPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure MasterVGridNamaMenuEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent);
  end;

var
  MasterMenuSetFm: TMasterMenuSetFm;

  MasterOriSQL, DetailOriSQL: string;
implementation

uses MenuUtama, DropDown, DM, 
  BarangDropDown, PrepDropDown, MenuDropDown, MenuSetDropDown;

{$R *.dfm}

{ TMasterArmadaFm }

constructor TMasterMenuSetFm.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
end;

procedure TMasterMenuSetFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TMasterMenuSetFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TMasterMenuSetFm.FormCreate(Sender: TObject);
begin
  DMFm:=TDMFm.Create(self);
  MasterOriSQL:=MasterQ.SQL.Text;
  DetailOriSQL:=DetailMenuQ.SQL.Text;
  KodeEdit.Properties.MaxLength:=MasterQKode.Size;
end;

procedure TMasterMenuSetFm.cxButtonEdit1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
{  KodeEdit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  MasterQ.Open;
  MasterQ.Append;
  detailq.Close;
  KodeEditExit(sender);  }
  MasterVGrid.Enabled:=True;
  MasterVGrid.SetFocus;
end;

procedure TMasterMenuSetFm.KodeEditEnter(Sender: TObject);
begin
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
  MasterVGrid.Enabled:=false;
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  SaveBtn.Enabled:=false;
  DeleteBtn.Enabled:=false;
  MasterQ.Close;
end;

procedure TMasterMenuSetFm.FormShow(Sender: TObject);
begin
  KodeEdit.SetFocus;
  KategoriQ.Open;
  MenuQ.Open;
//  MasterQ.Open;
 // kodeedit.Properties.Buttons[0].Enabled:=MenuUtamaFm.UserQInsertMasterStandardPerawatan.AsBoolean;
 // SaveBtn.Enabled:=menuutamafm.UserQUpdateMasterStandardPerawatan.AsBoolean;
 // DeleteBtn.Enabled:=menuutamafm.UserQDeleteMasterStandardPerawatan.AsBoolean;
end;

procedure TMasterMenuSetFm.KodeEditExit(Sender: TObject);
begin
  if KodeEdit.Text<>'' then
  begin
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Append;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      deletebtn.Enabled:=true;
      //DeleteBtn.Enabled:=menuutamafm.UserQDeleteMasterStandardPerawatan.AsBoolean;
    end;
    savebtn.Enabled:=true;
    //SaveBtn.Enabled:=menuutamafm.UserQUpdateMasterStandardPerawatan.AsBoolean;
//    MasterVGrid.Enabled:=True;
    DetailMenuQ.Close;
    DetailMenuQ.ParamByName('text').AsString:='';
    DetailMenuQ.Open;
  end;
end;

procedure TMasterMenuSetFm.KodeEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_F5) then
  begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
  end;
  if (Key=VK_escape) then
  begin
    ExitBtn.Click;
  end;
end;

procedure TMasterMenuSetFm.SaveBtnClick(Sender: TObject);
begin
//  MasterQ.Post;
  MenuUtamaFm.Database1.StartTransaction;
  try
//    MasterQ.ApplyUpdates;
    DetailMenuQ.Open;
    DetailMenuQ.Edit;
    DetailMenuQ.First;
    while DetailMenuQ.Eof=FALSE do
    begin
        DetailMenuQ.Edit;
        DetailMenuQKodeMenuSet.AsString:=MasterQKode.AsString;
        DetailMenuQ.Post;
        DetailMenuQ.Next;
    end;
    DetailMenuQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
//    MasterQ.CommitUpdates;
    DetailMenuQ.CommitUpdates;
    ShowMessage('Menu Set dengan Kode ' + KodeEdit.Text + ' telah disimpan');
    FormShow(self);
  except
    on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
//      MasterQ.RollbackUpdates;
      DetailMenuQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
    end;
  end;
  DetailMenuQ.Close;
  DetailMenuQ.ParamByName('text').AsString:='';
  DetailMenuQ.Open;
  DetailMenuQ.Close;
end;

procedure TMasterMenuSetFm.MasterQAfterInsert(DataSet: TDataSet);
begin
  //DMFm.GetDateQ.Open;
  //MasterQCreateDate.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  //MasterQCreateBy.AsString:=User;
  //DMFm.GetDateQ.Close;
end;

procedure TMasterMenuSetFm.MasterQBeforePost(DataSet: TDataSet);
begin
  //DMFm.GetDateQ.Open;
  //MasterQTglEntry.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  //MasterQOperator.AsString:=User;
  //DMFm.GetDateQ.Close;

end;

procedure TMasterMenuSetFm.DeleteBtnClick(Sender: TObject);
begin
  if MessageDlg('Hapus Menu Set '+KodeEdit.Text+' - '+MasterQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     MenuUtamaFm.Database1.StartTransaction;
     try
        ExecuteDeleteDetailMenuQ.SQL.Text:=('delete from MenuSet where KodeMenuSet='+QuotedStr(KodeEdit.Text));
        ExecuteDeleteDetailMenuQ.ExecSQL;
        MessageDlg('Menu Set telah dihapus.',mtInformation,[mbOK],0);
     except
       on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MessageDlg('Menu Set pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
    end;
     end;
     KodeEdit.SetFocus;
  end;
  DetailMenuQ.Close;
  DetailMenuQ.ParamByName('text').AsString:='';
  DetailMenuQ.Open;
end;

procedure TMasterMenuSetFm.SearchBtnClick(Sender: TObject);
begin
    //MasterQ.SQL.Text:=MasterOriSQL;
    MenuSetDropDownFm:=TMenuSetDropdownfm.Create(Self);
    if MenuSetDropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=MenuSetDropDownFm.kode;
      KodeEditExit(Sender);
      DetailMenuQ.Close;
      DetailMenuQ.ParamByName('text').AsString := MasterQKode.AsString;
      DetailMenuQ.Open;
    end;
    MenuDropDownFm.Release;
end;

procedure TMasterMenuSetFm.MasterVGridExit(Sender: TObject);
begin
//  SaveBtn.SetFocus;
end;

procedure TMasterMenuSetFm.MasterVGridEnter(Sender: TObject);
begin
 // mastervgrid.FocusRow(MasterVGridJenisKendaraan);
end;

procedure TMasterMenuSetFm.cxGrid1DBTableView1NavigatorButtonsButtonClick(
  Sender: TObject; AButtonIndex: Integer; var ADone: Boolean);
begin
  if abuttonindex=10 then
  begin

  end;
end;

procedure TMasterMenuSetFm.cxGrid1Exit(Sender: TObject);
begin
  cxGrid1DBTableView1.Navigator.Buttons.Post.Click;
end;

procedure TMasterMenuSetFm.cxGrid1DBTableView1Column1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
 { detailq.ParamByName('text').AsString:=masterqkode.AsString;
  detailq.Open;
  detailq.Insert;

  BarangDropDownFm:=TBarangDropDownFm.Create(self);
  if BarangDropDownFm.ShowModal=MrOK then
  begin
    DetailQBarang.AsString:= BarangDropDownFm.kode;
  end;
  DetailQKodeStandard.AsString:=masterqkode.AsString;
  BarangDropDownFm.Release;           }
{
  DropDownFm:=TDropdownfm.Create(Self,BarangQ);
  if DropDownFm.ShowModal=MrOK then
  begin
    cxGrid1DBTableView1.DataController.setvalue(cxGrid1DBTableView1.DataController.FocusedDataRowIndex,0,barangqkode.AsString);
    detailqbarang.AsString:=barangqkode.AsString;
    cxGrid1DBTableView1.DataController.setvalue(cxGrid1DBTableView1.DataController.FocusedDataRowIndex,1,barangqnama.AsString);
    detailqnama.AsString:=barangqnama.AsString;
  end;
  DetailQKodeStandard.AsString:=masterqkode.AsString;
  DropDownFm.Release;    }
end;

procedure TMasterMenuSetFm.MasterVGridJenisKendaraanEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  //DropDownFm:=TDropdownfm.Create(Self,JenisQ);
  //if DropDownFm.ShowModal=MrOK then
  //begin
    {JenisQ.Close;
    //KategoriQ.ParamByName('text').AsString:=VarToStr(displayvalue);
    JenisQ.ExecSQL;
    JenisQ.Open;}
    {JenisKendaraanDropDownFm:=TJenisKendaraanDropdownfm.Create(Self);
    if JenisKendaraanDropDownFm.ShowModal=MrOK then
    begin
      MasterQ.Open;
      MasterQ.Edit;
      MasterQJenisKendaraan.AsString:=JenisKendaraanDropDownFm.kode;
      //MasterQDetailKendaraan.AsString:=JenisQNamaJenis.AsString;
      //MasterVGrid.SetFocus;
      //end:
  end;
  JenisKendaraanDropDownFm.Release;        }
end;

procedure TMasterMenuSetFm.ExitBtnClick(Sender: TObject);
begin
  release;
end;

procedure TMasterMenuSetFm.cxGrid1DBTableView1NamaMenuPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  MenuDropDownFm:=TMenuDropdownfm.Create(Self);
  if MenuDropDownFm.ShowModal=MrOK then
  begin
    DetailMenuQ.Edit;
    DetailMenuQKodeMenu.AsString:=MenuDropDownFm.kode;
    DetailMenuQKodeMenuSet.AsString:=MasterQKode.AsString;
  end;
  MenuDropDownFm.Release;
end;

procedure TMasterMenuSetFm.MasterVGridNamaMenuEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  MenuDropDownFm:=TMenuDropdownfm.Create(Self);
  if MenuDropDownFm.ShowModal=MrOK then
  begin
    KodeEdit.Text:=MenuDropDownFm.kode;
    KodeEditExit(Sender);
    DetailMenuQ.Close;
    DetailMenuQ.ParamByName('text').AsString := MasterQKode.AsString;
    DetailMenuQ.Open;
  end;
  MenuSetDropDownFm.Release;
end;

end.
