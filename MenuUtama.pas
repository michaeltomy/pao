unit MenuUtama;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, DB, SDEngine, StdCtrls, cxControls, cxContainer, cxEdit,
  cxTextEdit, cxMemo, cxListBox, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, dxGDIPlusClasses, cxImage, jpeg, dxSkinsForm,
  dxLayoutLookAndFeels, cxClasses, cxStyles, dxSkinsdxBarPainter, dxBar;

type
  TMenuUtamaFm = class(TForm)
    DataBase1: TSDDatabase;
    UserQ: TSDQuery;
    cxImage1: TcxImage;
    cxLookAndFeelController1: TcxLookAndFeelController;
    cxStyleRepository1: TcxStyleRepository;
    dxLayoutLookAndFeelList1: TdxLayoutLookAndFeelList;
    dxSkinController1: TdxSkinController;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarSubItem1: TdxBarSubItem;
    dxBarButton1: TdxBarButton;
    dxBarButton2: TdxBarButton;
    dxBarButton3: TdxBarButton;
    dxBarButton4: TdxBarButton;
    dxBarButton5: TdxBarButton;
    dxBarButton6: TdxBarButton;
    dxBarSubItem2: TdxBarSubItem;
    dxBarSubItem3: TdxBarSubItem;
    dxBarButton7: TdxBarButton;
    dxBarButton8: TdxBarButton;
    dxBarButton9: TdxBarButton;
    dxBarButton10: TdxBarButton;
    dxBarButton11: TdxBarButton;
    dxBarButton12: TdxBarButton;
    dxBarButton13: TdxBarButton;
    dxBarSubItem4: TdxBarSubItem;
    dxBarButton14: TdxBarButton;
    dxBarButton15: TdxBarButton;
    dxBarButton16: TdxBarButton;
    dxBarButton17: TdxBarButton;
    dxBarButton18: TdxBarButton;
    dxBarSubItem5: TdxBarSubItem;
    dxBarButton19: TdxBarButton;
    dxBarButton20: TdxBarButton;
    dxBarSubItem6: TdxBarSubItem;
    dxBarButton21: TdxBarButton;
    dxBarButton22: TdxBarButton;
    dxBarButton23: TdxBarButton;
    dxBarButton24: TdxBarButton;
    dxBarButton25: TdxBarButton;
    dxBarButton26: TdxBarButton;
    dxBarButton27: TdxBarButton;
    dxBarButton28: TdxBarButton;
    dxBarButton29: TdxBarButton;
    dxBarButton30: TdxBarButton;
    dxBarButton31: TdxBarButton;
    dxBarButton32: TdxBarButton;
    dxBarButton33: TdxBarButton;
    dxBarButton34: TdxBarButton;
    dxBarButton35: TdxBarButton;
    dxBarButton36: TdxBarButton;
    dxBarButton37: TdxBarButton;
    dxBarButton38: TdxBarButton;
    UserQKode: TStringField;
    UserQUsername: TStringField;
    UserQPassword: TStringField;
    UserQPegawai: TStringField;
    dxBarButton39: TdxBarButton;
    dxBarButton40: TdxBarButton;
    dxBarButton41: TdxBarButton;
    dxBarButton42: TdxBarButton;
    dxBarButton43: TdxBarButton;
    dxBarButton44: TdxBarButton;
    dxBarButton45: TdxBarButton;
    dxBarButton46: TdxBarButton;
    dxBarButton47: TdxBarButton;
    dxBarButton48: TdxBarButton;
    dxBarButton49: TdxBarButton;
    procedure Armada1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SO1Click(Sender: TObject);
    procedure SuratJalan1Click(Sender: TObject);
    procedure JadwalArmada1Click(Sender: TObject);
    procedure SuratJalan2Click(Sender: TObject);
    procedure SalesOrder1Click(Sender: TObject);
    procedure Pelanggan1Click(Sender: TObject);
    procedure Rute1Click(Sender: TObject);
    procedure Sopir1Click(Sender: TObject);
    procedure User1Click(Sender: TObject);
    procedure Setting1Click(Sender: TObject);
    procedure LaporanPenjualan1Click(Sender: TObject);
    procedure LaporanPembelian1Click(Sender: TObject);
    procedure LaporanLaba1Click(Sender: TObject);
    procedure Supplier1Click(Sender: TObject);
    procedure DaftarBarang1Click(Sender: TObject);
    procedure DaftarSupplier1Click(Sender: TObject);
    procedure DaftarPelanggan1Click(Sender: TObject);
    procedure Pengeluaran1Click(Sender: TObject);
    procedure Pembayaran1Click(Sender: TObject);
    procedure LaporanPembelianPerSupplier1Click(Sender: TObject);
    procedure LaporanPengeluaran1Click(Sender: TObject);
    procedure LaporanPembayaran1Click(Sender: TObject);
    procedure LaporanPemenuhanPembayaran1Click(Sender: TObject);
    procedure LaporanKasMasukKeluar1Click(Sender: TObject);
    procedure ampilkan1Click(Sender: TObject);
    procedure Setting3Click(Sender: TObject);
    procedure LaporanHistoriPenjualan1Click(Sender: TObject);
    procedure LaporanHistoriPembelian1Click(Sender: TObject);
    procedure dxBarButton1Click(Sender: TObject);
    procedure dxBarButton4Click(Sender: TObject);
    procedure dxBarButton5Click(Sender: TObject);
    procedure dxBarButton3Click(Sender: TObject);
    procedure dxBarButton2Click(Sender: TObject);
    procedure dxBarButton6Click(Sender: TObject);
    procedure dxBarButton7Click(Sender: TObject);
    procedure dxBarButton14Click(Sender: TObject);
    procedure dxBarButton16Click(Sender: TObject);
    procedure dxBarButton17Click(Sender: TObject);
    procedure dxBarButton18Click(Sender: TObject);
    procedure dxBarButton15Click(Sender: TObject);
    procedure dxBarButton19Click(Sender: TObject);
    procedure dxBarButton20Click(Sender: TObject);
    procedure dxBarButton22Click(Sender: TObject);
    procedure dxBarButton23Click(Sender: TObject);
    procedure dxBarButton21Click(Sender: TObject);
    procedure dxBarButton31Click(Sender: TObject);
    procedure dxBarButton24Click(Sender: TObject);
    procedure dxBarButton25Click(Sender: TObject);
    procedure dxBarButton26Click(Sender: TObject);
    procedure dxBarButton27Click(Sender: TObject);
    procedure dxBarButton28Click(Sender: TObject);
    procedure dxBarButton29Click(Sender: TObject);
    procedure dxBarButton30Click(Sender: TObject);
    procedure dxBarButton33Click(Sender: TObject);
    procedure dxBarButton34Click(Sender: TObject);
    procedure dxBarButton35Click(Sender: TObject);
    procedure dxBarButton36Click(Sender: TObject);
    procedure dxBarButton37Click(Sender: TObject);
    procedure dxBarButton38Click(Sender: TObject);
    procedure dxBarButton39Click(Sender: TObject);
    procedure dxBarButton40Click(Sender: TObject);
    procedure dxBarButton41Click(Sender: TObject);
    procedure dxBarButton42Click(Sender: TObject);
    procedure dxBarButton43Click(Sender: TObject);
    procedure dxBarButton44Click(Sender: TObject);
    procedure dxBarButton45Click(Sender: TObject);
    procedure dxBarButton46Click(Sender: TObject);
    procedure dxBarButton47Click(Sender: TObject);
    procedure dxBarButton48Click(Sender: TObject);
    procedure dxBarButton49Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  MenuUtamaFm: TMenuUtamaFm;
  User:string;
implementation

uses MasterArmada,
     Login, NotaSO, NotaSJ, JadwalArmada, MasterSuratJalan,
  MasterSalesOrder, MasterPelanggan, MasterRute, MasterSopir, MasterUser,
  MasterBarang, DaftarKontrol, Pembelian, Penjualan, Setting,
  LaporanPenjualan, LaporanPembelian, LaporanLaba, MasterSupplier,
  DaftarBarang, DaftarSupplier, DaftarPelanggan, Pengeluaran, Pembayaran,
  LaporanPembelianPerSupplier, LaporanPengeluaran, LaporanPembayaran,
  LaporanPemenuhanPembayaran, LaporanKas, Notifikasi,
  LaporanHistoriPenjualan, LaporanHistoriPembelian, MasterKategoriBarang,
  MasterPegawai, MasterToko, BarangDropDown, KategoriBarangDropDown,
  PelangganDropDown, TokoDropDown, PegawaiDropDown, TransferMasuk,
  TransferKeluar, Setoran, LaporanTransferMasuk, LaporanTransferKeluar,
  LapPengeluaran, LapSetoran, LapKas, LapLabaKotor, LapLabaBersih,
  KodeHargaBeli, Kwitansi, MasterInstansi, KwitansiJamkesda,
  RekapitulasiJamkesda, KwitansiJamkes, MasterPrep, MasterMenu,
  PenerimaanBahan, PengeluaranBahan, Persiapan, MasterKategoriMenu,
  PenutupanDataHarian, MasterMenuSet;

{$R *.dfm}

procedure TMenuUtamaFm.Armada1Click(Sender: TObject);
begin
  MasterBarangFm:= TMasterBarangFm.create(self);
end;


procedure TMenuUtamaFm.FormCreate(Sender: TObject);
var   F: Textfile;
  str,pass: string;
  temp :integer;
  masuk,keluar :boolean;
begin
  //timer1.Enabled:=true;
  AssignFile(f, ExtractFilePath(Application.ExeName)+'dt.dat'); {Assigns the Filename}
  Reset(f); {Opens the file for reading}
  Readln(f, str);
  pass:=str;

  //Database1.Params.Values['PASSWORD']:=str;
  Closefile(f);
  AssignFile(f, ExtractFilePath(Application.ExeName)+'bg.dat'); {Assigns the Filename}
  Reset(f);
  Readln(f, str);
  cxImage1.Picture.LoadFromFile(str);
  closefile(f);
  AssignFile(f, 'database.txt'); {Assigns the Filename}
  Reset(f); {Opens the file for reading}
  Readln(f, str);
  DataBase1.RemoteDatabase:=str;
  Readln(f, str);
  Database1.Params.Values['USER NAME']:=str;
  Readln(f, str);
  Database1.Params.Values['PASSWORD']:=str;
  Closefile(f);
  Database1.Open;

  Masuk:=false;
  while masuk=false do
  begin
    LoginFm:=TLoginFm.Create(Self);
    temp := LoginFm.ShowModal;
    if temp=mrOk then
    begin
      DataBase1.Open;
      UserQ.ParamByName('username').AsString:=loginFm.UserName;
      UserQ.ParamByName('password').AsString:=loginFm.password;
      UserQ.Open;
      User:=UserQkode.AsString;
      UserQ.Close;
      if user='' then
      begin
        ShowMessage('login gagal');
      end
      else masuk:=true;
    end
    else if temp=mrCancel then
    begin
      masuk:=true;
      Application.Terminate;

    end;
  end;
  if keluar then application.Terminate;
  //cxImage1.Picture.LoadFromFile('att.gif');
  userq.Open;
end;

procedure TMenuUtamaFm.SO1Click(Sender: TObject);
begin
  PenjualanFm:= TPenjualanFm.create(self);
end;

procedure TMenuUtamaFm.SuratJalan1Click(Sender: TObject);
begin
  PembelianFm:= TPembelianFm.create(self);
end;

procedure TMenuUtamaFm.JadwalArmada1Click(Sender: TObject);
begin
  DaftarKontrolFm:= TDaftarKontrolFm.create(self);
end;

procedure TMenuUtamaFm.SuratJalan2Click(Sender: TObject);
begin
    MasterSuratJalanFm:= TMasterSuratJalanFm.create(self);
end;

procedure TMenuUtamaFm.SalesOrder1Click(Sender: TObject);
begin
    MasterSalesOrderFm:= TMasterSalesOrderFm.create(self);
end;

procedure TMenuUtamaFm.Pelanggan1Click(Sender: TObject);
begin
  MasterPelangganFm:= TMasterPelangganFm.create(self);
end;

procedure TMenuUtamaFm.Rute1Click(Sender: TObject);
begin
  MasterRuteFm:= TMasterRuteFm.create(self);
end;

procedure TMenuUtamaFm.Sopir1Click(Sender: TObject);
begin
  MasterSopirFm:= TMasterSopirFm.create(self);
end;

procedure TMenuUtamaFm.User1Click(Sender: TObject);
begin
  MasterUserFm:= TMasterUserFm.create(self);
end;

procedure TMenuUtamaFm.Setting1Click(Sender: TObject);
begin
// SettingFm :=TSettingFm.create(self);
end;

procedure TMenuUtamaFm.LaporanPenjualan1Click(Sender: TObject);
begin
  LaporanPenjualanFm :=TLaporanPenjualanFm.create(self)
end;

procedure TMenuUtamaFm.LaporanPembelian1Click(Sender: TObject);
begin
  LaporanPembelianFm := TLaporanPembelianFm.create(self);
end;

procedure TMenuUtamaFm.LaporanLaba1Click(Sender: TObject);
begin
  LaporanLabaFm := TLaporanLabaFm.create(self);
end;

procedure TMenuUtamaFm.Supplier1Click(Sender: TObject);
begin
  MasterSupplierfm := TMasterSupplierFm.create(self);
end;

procedure TMenuUtamaFm.DaftarBarang1Click(Sender: TObject);
begin
  DaftarBarangFm := TDaftarBarangFm.create(self);
end;

procedure TMenuUtamaFm.DaftarSupplier1Click(Sender: TObject);
begin
  DaftarSupplierFm:= TDaftarSupplierFm.create(self);
end;

procedure TMenuUtamaFm.DaftarPelanggan1Click(Sender: TObject);
begin
  DaftarPelangganFm :=TDaftarPelangganFm.create(self);
end;

procedure TMenuUtamaFm.Pengeluaran1Click(Sender: TObject);
begin
  PengeluaranFm := TPengeluaranFm.create(self);
  
end;

procedure TMenuUtamaFm.Pembayaran1Click(Sender: TObject);
begin
  PembayaranFm :=TPembayaranFm.create(self,'');
end;

procedure TMenuUtamaFm.LaporanPembelianPerSupplier1Click(Sender: TObject);
begin
  LaporanPembelianPerSupplierFm :=TLaporanPembelianPerSupplierFm.create(self);
end;

procedure TMenuUtamaFm.LaporanPengeluaran1Click(Sender: TObject);
begin
  LaporanPengeluaranFm :=TLaporanPengeluaranFm.create(self);
end;

procedure TMenuUtamaFm.LaporanPembayaran1Click(Sender: TObject);
begin
  LaporanPembayaranFm :=TLaporanPembayaranFm.create(self);
end;

procedure TMenuUtamaFm.LaporanPemenuhanPembayaran1Click(Sender: TObject);
begin
  LaporanPemenuhanPembayaranFm := TLaporanPemenuhanPembayaranFm.create(self);
end;

procedure TMenuUtamaFm.LaporanKasMasukKeluar1Click(Sender: TObject);
begin
  LaporanKasFm := TLaporanKasFm.create(self);
end;

procedure TMenuUtamaFm.ampilkan1Click(Sender: TObject);
begin
  NotifikasiFm :=TNotifikasiFm.create(self);
end;

procedure TMenuUtamaFm.Setting3Click(Sender: TObject);
begin
  SettingFm := TSettingFm.create(self);
end;

procedure TMenuUtamaFm.LaporanHistoriPenjualan1Click(Sender: TObject);
begin
  LaporanHistoriPenjualanFm := TLaporanHistoriPenjualanFm.create(self);
end;

procedure TMenuUtamaFm.LaporanHistoriPembelian1Click(Sender: TObject);
begin
  LaporanHistoriPembelianFm := TLaporanHistoriPembelianFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton1Click(Sender: TObject);
begin
  MasterBarangFm:=TMasterBarangFm.Create(self);
  MasterBarangFm.ShowModal;
end;

procedure TMenuUtamaFm.dxBarButton4Click(Sender: TObject);
begin
  MasterKategoriBarangFm:=TMasterKategoriBarangFm.create(self);
  MasterKategoriBarangFm.showmodal;
end;

procedure TMenuUtamaFm.dxBarButton5Click(Sender: TObject);
begin
  MasterPegawaiFm:=TMasterPegawaiFm.create(self,'');
  MasterPegawaiFm.showmodal;
end;

procedure TMenuUtamaFm.dxBarButton3Click(Sender: TObject);
begin
  MasterPelangganFm:=TMasterPelangganFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton2Click(Sender: TObject);
begin
  MasterUserFm:=TMasterUserFm.create(self);
  MasterUserFm.ShowModal;
end;

procedure TMenuUtamaFm.dxBarButton6Click(Sender: TObject);
begin
  MasterTokoFm:=TMasterTokoFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton7Click(Sender: TObject);
begin
  PengeluaranFm:=TPengeluaranFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton14Click(Sender: TObject);
begin
  BarangDropDownFm:=TBarangDropDownFm.create(self);
  BarangDropDownFm.showModal;
end;

procedure TMenuUtamaFm.dxBarButton16Click(Sender: TObject);
begin
  KategoriBarangDropDownFm:=TKategoriBarangDropDownFm.create(self);
  KategoriBarangDropDownFm.showmodal;
end;

procedure TMenuUtamaFm.dxBarButton17Click(Sender: TObject);
begin
  PelangganDropDownFm:=TPelangganDropDownFm.create(self);
  PelangganDropDownFm.showmodal;
end;

procedure TMenuUtamaFm.dxBarButton18Click(Sender: TObject);
begin
  TokoDropDownFm:=TTokoDropDownFm.create(self);
  TokoDropDownFm.showmodal;
end;

procedure TMenuUtamaFm.dxBarButton15Click(Sender: TObject);
begin
  PegawaiDropDownFm:=TPegawaiDropDownFm.create(self);
  PegawaiDropDownFm.showmodal;
end;

procedure TMenuUtamaFm.dxBarButton19Click(Sender: TObject);
begin
  TransferMasukFm:=TTransferMasukFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton20Click(Sender: TObject);
begin
  TransferKeluarFm:=TTransferKeluarFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton22Click(Sender: TObject);
begin
  PenjualanFm:=TPenjualanFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton23Click(Sender: TObject);
begin
  SetoranFm:=TSetoranFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton21Click(Sender: TObject);
begin
  LaporanPenjualanFm:=TLaporanPenjualanFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton31Click(Sender: TObject);
begin
  PembayaranFm:=TPembayaranFm.create(self,'');
end;

procedure TMenuUtamaFm.dxBarButton24Click(Sender: TObject);
begin
  LaporanTransferMasukFm:=TLaporanTransferMasukFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton25Click(Sender: TObject);
begin
  LaporanTransferKeluarFm:=TLaporanTransferKeluarFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton26Click(Sender: TObject);
begin
  LapPengeluaranFm:=TLapPengeluaranFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton27Click(Sender: TObject);
begin
  LapSetoranFm:=TLapSetoranFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton28Click(Sender: TObject);
begin
  LapKasFm:=TLapKasFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton29Click(Sender: TObject);
begin
  LapLabaKotorFm:=TLapLabaKotorFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton30Click(Sender: TObject);
begin
  LapLabaBersihFm:=Tlaplababersihfm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton33Click(Sender: TObject);
begin
  KodeHargaBeliFm:=TKodeHargaBeliFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton34Click(Sender: TObject);
begin
  KwitansiFm:=TKwitansiFm.create(self,'');
  KwitansiFm.ShowModal;
end;

procedure TMenuUtamaFm.dxBarButton35Click(Sender: TObject);
begin
  MasterInstansiFm:=TMasterInstansiFm.create(self);
  
end;

procedure TMenuUtamaFm.dxBarButton36Click(Sender: TObject);
begin
RekapitulasiJamkesdaFm:=TRekapitulasiJamkesdaFm.create(self);  
end;

procedure TMenuUtamaFm.dxBarButton37Click(Sender: TObject);
begin
  KwitansiJamkesdaFm:=TKwitansiJamkesdaFm.create(self);
end;

procedure TMenuUtamaFm.dxBarButton38Click(Sender: TObject);
begin
  KwitansiJamkesFm:=TKwitansiJamkesFm.create(self,'');
  KwitansiJamkesFm.showmodal;
end;

procedure TMenuUtamaFm.dxBarButton39Click(Sender: TObject);
begin
  MasterSupplierFm:=TMasterSupplierFm.create(self);
  MasterSupplierFm.showmodal;
end;

procedure TMenuUtamaFm.dxBarButton40Click(Sender: TObject);
begin
  MasterPrepFm:=TMasterPrepFm.create(self);
  MasterPrepFm.showmodal;
end;

procedure TMenuUtamaFm.dxBarButton41Click(Sender: TObject);
begin
  MasterMenuFm:=TMasterMenuFm.create(self);
  MasterMenuFm.showmodal;
end;

procedure TMenuUtamaFm.dxBarButton42Click(Sender: TObject);
begin
  PenerimaanBahanFm:=TPenerimaanBahanFm.create(self);
  PenerimaanBahanFm.showmodal;
end;

procedure TMenuUtamaFm.dxBarButton43Click(Sender: TObject);
begin
  PengeluaranBahanFm:=TPengeluaranBahanFm.create(self);
  PengeluaranBahanFm.showmodal;
end;

procedure TMenuUtamaFm.dxBarButton44Click(Sender: TObject);
begin
  PersiapanFm:=TPersiapanFm.create(self);
  PersiapanFm.showmodal;
end;

procedure TMenuUtamaFm.dxBarButton45Click(Sender: TObject);
begin
  PembayaranFm:=TPembayaranFm.create(self,'');
end;

procedure TMenuUtamaFm.dxBarButton46Click(Sender: TObject);
begin
  MasterKategoriMenuFm:=TMasterKategoriMenuFm.create(self);
  MasterKategoriMenuFm.showmodal;
end;

procedure TMenuUtamaFm.dxBarButton47Click(Sender: TObject);
begin
  SettingFm:=TSettingFm.create(self);
  SettingFm.showmodal;
end;

procedure TMenuUtamaFm.dxBarButton48Click(Sender: TObject);
begin
  PenutupanDataHarianFm:=TPenutupanDataHarianFm.create(self);
  PenutupanDataHarianFm.showmodal;
end;

procedure TMenuUtamaFm.dxBarButton49Click(Sender: TObject);
begin
  MasterMenuSetFm:=TMasterMenuSetFm.create(self);
  MasterMenuSetFm.showmodal;
end;

end.
