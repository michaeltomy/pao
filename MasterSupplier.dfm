object MasterSupplierFm: TMasterSupplierFm
  Left = 498
  Top = 237
  Width = 387
  Height = 390
  AutoSize = True
  Caption = 'Master Supplier'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 371
    Height = 48
    Align = alTop
    TabOrder = 0
    object lbl1: TLabel
      Left = 8
      Top = 13
      Width = 25
      Height = 13
      Caption = 'Kode'
    end
    object KodeEdit: TcxButtonEdit
      Left = 40
      Top = 10
      Properties.Buttons = <
        item
          Caption = '+'
          Default = True
          Kind = bkText
        end>
      Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      TabOrder = 0
      OnEnter = KodeEditEnter
      OnExit = KodeEditExit
      OnKeyDown = KodeEditKeyDown
      Width = 121
    end
    object SearchBtn: TcxButton
      Left = 168
      Top = 10
      Width = 57
      Height = 21
      Caption = 'Search'
      TabOrder = 1
      OnClick = SearchBtnClick
    end
  end
  object pnl2: TPanel
    Left = 0
    Top = 282
    Width = 371
    Height = 51
    Align = alBottom
    TabOrder = 2
    object ExitBtn: TcxButton
      Left = 172
      Top = 10
      Width = 75
      Height = 25
      Caption = 'EXIT [F8]'
      TabOrder = 1
      Visible = False
      OnClick = ExitBtnClick
    end
    object DeleteBtn: TcxButton
      Left = 91
      Top = 10
      Width = 75
      Height = 25
      Caption = 'DELETE'
      TabOrder = 2
      OnClick = DeleteBtnClick
    end
    object SaveBtn: TcxButton
      Left = 8
      Top = 10
      Width = 75
      Height = 25
      Caption = 'SAVE'
      TabOrder = 0
      OnClick = SaveBtnClick
    end
  end
  object MasterVGrid: TcxDBVerticalGrid
    Left = 0
    Top = 48
    Width = 371
    Height = 234
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    OptionsView.RowHeaderWidth = 112
    OptionsBehavior.GoToNextCellOnEnter = True
    OptionsBehavior.GoToNextCellOnTab = True
    OptionsData.CancelOnExit = False
    OptionsData.Appending = False
    OptionsData.Deleting = False
    OptionsData.DeletingConfirmation = False
    OptionsData.Inserting = False
    ParentFont = False
    TabOrder = 1
    TabStop = False
    OnKeyDown = MasterVGridKeyDown
    DataController.DataSource = MasterDs
    Version = 1
    object MasterVGridNama: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Nama'
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object MasterVGridAlamat: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Alamat'
      ID = 1
      ParentID = -1
      Index = 1
      Version = 1
    end
    object MasterVGridKota: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Kota'
      ID = 2
      ParentID = -1
      Index = 2
      Version = 1
    end
    object MasterVGridTelp: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Telp'
      ID = 3
      ParentID = -1
      Index = 3
      Version = 1
    end
    object MasterVGridPIC: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'PIC'
      ID = 4
      ParentID = -1
      Index = 4
      Version = 1
    end
    object MasterVGridTelpPIC: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'TelpPIC'
      ID = 5
      ParentID = -1
      Index = 5
      Version = 1
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 333
    Width = 371
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object MasterQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select * from supplier')
    UpdateObject = MasterUS
    Left = 265
    Top = 9
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQNama: TStringField
      FieldName = 'Nama'
      Size = 200
    end
    object MasterQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 200
    end
    object MasterQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object MasterQTelp: TStringField
      FieldName = 'Telp'
      Size = 200
    end
    object MasterQPIC: TStringField
      FieldName = 'PIC'
      Size = 200
    end
    object MasterQTelpPIC: TStringField
      FieldName = 'TelpPIC'
      Size = 200
    end
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 300
    Top = 6
  end
  object MasterUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, Nama, Alamat, Kota, Telp, PIC, TelpPIC'#13#10'from suppli' +
        'er'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update supplier'
      'set'
      '  Kode = :Kode,'
      '  Nama = :Nama,'
      '  Alamat = :Alamat,'
      '  Kota = :Kota,'
      '  Telp = :Telp,'
      '  PIC = :PIC,'
      '  TelpPIC = :TelpPIC'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into supplier'
      '  (Kode, Nama, Alamat, Kota, Telp, PIC, TelpPIC)'
      'values'
      '  (:Kode, :Nama, :Alamat, :Kota, :Telp, :PIC, :TelpPIC)')
    DeleteSQL.Strings = (
      'delete from supplier'
      'where'
      '  Kode = :OLD_Kode')
    Left = 332
    Top = 10
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from supplier order by kode desc')
    Left = 233
    Top = 7
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
end
