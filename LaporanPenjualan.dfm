inherited LaporanPenjualanFm: TLaporanPenjualanFm
  Left = 145
  Top = 70
  Caption = 'Laporan Penjualan'
  ClientWidth = 1167
  OldCreateOrder = True
  Visible = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited lbDescrip: TLabel
    Width = 1167
    Caption = 
      'This example demonstates the ExpressQuantumGrid printing capabil' +
      'ities.'
    Visible = False
  end
  inherited sbMain: TStatusBar
    Width = 1167
  end
  inherited ToolBar1: TToolBar
    Width = 1167
    object tbtnFullCollapse: TToolButton
      Left = 123
      Top = 0
      Action = actFullCollapse
      ParentShowHint = False
      ShowHint = True
    end
    object tbtnFullExpand: TToolButton
      Left = 146
      Top = 0
      Action = actFullExpand
      ParentShowHint = False
      ShowHint = True
    end
  end
  object Panel1: TPanel [3]
    Left = 0
    Top = 41
    Width = 1167
    Height = 40
    Align = alTop
    TabOrder = 2
    object DateTimePicker1: TDateTimePicker
      Left = 24
      Top = 8
      Width = 113
      Height = 21
      Date = 41416.587301469910000000
      Time = 41416.587301469910000000
      TabOrder = 0
    end
    object DateTimePicker2: TDateTimePicker
      Left = 144
      Top = 8
      Width = 113
      Height = 21
      Date = 41416.587307858800000000
      Time = 41416.587307858800000000
      TabOrder = 1
    end
    object Button1: TButton
      Left = 264
      Top = 8
      Width = 81
      Height = 22
      Caption = 'REFRESH'
      TabOrder = 2
      OnClick = Button1Click
    end
  end
  object Panel2: TPanel [4]
    Left = 0
    Top = 81
    Width = 1167
    Height = 388
    Align = alClient
    Caption = 'Panel2'
    TabOrder = 3
    object cxGrid: TcxGrid
      Left = 1
      Top = 1
      Width = 1165
      Height = 386
      Align = alClient
      TabOrder = 0
      object tvPlanets: TcxGridTableView
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsBehavior.IncSearch = True
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsSelection.MultiSelect = True
        OptionsView.ColumnAutoWidth = True
        OptionsView.HeaderAutoHeight = True
        Styles.StyleSheet = tvssDevExpress
        object tvPlanetsNAME: TcxGridColumn
          Caption = 'Name'
          HeaderAlignmentHorz = taCenter
          Width = 100
        end
        object tvPlanetsNO: TcxGridColumn
          Caption = '#'
          RepositoryItem = edrepCenterText
          HeaderAlignmentHorz = taCenter
          Width = 40
        end
        object tvPlanetsORBITS: TcxGridColumn
          Caption = 'Orbits'
          RepositoryItem = edrepCenterText
          GroupIndex = 0
          HeaderAlignmentHorz = taCenter
          SortIndex = 0
          SortOrder = soAscending
        end
        object tvPlanetsDISTANCE: TcxGridColumn
          Caption = 'Distance (000km)'
          RepositoryItem = edrepRightText
          HeaderAlignmentHorz = taCenter
          SortIndex = 1
          SortOrder = soAscending
          Width = 80
        end
        object tvPlanetsPERIOD: TcxGridColumn
          Caption = 'Period (days)'
          RepositoryItem = edrepRightText
          HeaderAlignmentHorz = taCenter
          Width = 80
        end
        object tvPlanetsDISCOVERER: TcxGridColumn
          Caption = 'Discoverer'
          RepositoryItem = edrepCenterText
          HeaderAlignmentHorz = taCenter
        end
        object tvPlanetsDATE: TcxGridColumn
          Caption = 'Date'
          RepositoryItem = edrepCenterText
          HeaderAlignmentHorz = taCenter
        end
        object tvPlanetsRADIUS: TcxGridColumn
          Caption = 'Radius (km)'
          RepositoryItem = edrepRightText
          HeaderAlignmentHorz = taCenter
        end
      end
      object cxGridDBBandedTableView1: TcxGridDBBandedTableView
        DataController.DataSource = MasterDs
        DataController.Summary.DefaultGroupSummaryItems = <
          item
            Kind = skSum
            FieldName = 'GrandTotal'
            Column = cxGridDBBandedTableView1GrandTotal
            DisplayText = 'SumGranTotal'
          end
          item
            Kind = skSum
            FieldName = 'JumlahDibayar'
            Column = cxGridDBBandedTableView1JumlahDibayar
            DisplayText = 'SumJumlahDibayar'
          end>
        DataController.Summary.FooterSummaryItems = <
          item
            Kind = skSum
            FieldName = 'GrandTotal'
            Column = cxGridDBBandedTableView1GrandTotal
          end
          item
            Kind = skSum
            FieldName = 'JumlahDibayar'
            Column = cxGridDBBandedTableView1JumlahDibayar
          end
          item
            Kind = skSum
            FieldName = 'InsentifJual'
            Column = cxGridDBBandedTableView1InsentifJual
          end
          item
            Kind = skSum
            FieldName = 'InsentifPeriksa'
            Column = cxGridDBBandedTableView1InsentifPeriksa
          end>
        DataController.Summary.SummaryGroups = <
          item
            Links = <
              item
                Column = cxGridDBBandedTableView1NamaBarang
              end>
            SummaryItems = <
              item
                Kind = skSum
                FieldName = 'SubTotal'
                Column = cxGridDBBandedTableView1SubTotal
              end>
          end>
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsView.ExpandButtonsForEmptyDetails = False
        OptionsView.Footer = True
        OptionsView.GroupByHeaderLayout = ghlHorizontal
        OptionsView.GroupFooterMultiSummaries = True
        OptionsView.GroupFooters = gfVisibleWhenExpanded
        OptionsView.GroupRowStyle = grsOffice11
        OptionsView.GroupSummaryLayout = gslAlignWithColumnsAndDistribute
        OptionsView.BandCaptionsInColumnAlternateCaption = True
        OptionsView.BandHeaderEndEllipsis = True
        Bands = <
          item
            Width = 1148
          end>
        object cxGridDBBandedTableView1Kode: TcxGridDBBandedColumn
          DataBinding.FieldName = 'Kode'
          Width = 56
          Position.BandIndex = 0
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object cxGridDBBandedTableView1Tanggal: TcxGridDBBandedColumn
          DataBinding.FieldName = 'Tanggal'
          Width = 75
          Position.BandIndex = 0
          Position.ColIndex = 1
          Position.RowIndex = 0
        end
        object cxGridDBBandedTableView1Pelanggan: TcxGridDBBandedColumn
          DataBinding.FieldName = 'NamaPelanggan'
          Width = 94
          Position.BandIndex = 0
          Position.ColIndex = 2
          Position.RowIndex = 0
        end
        object cxGridDBBandedTableView1Diskon: TcxGridDBBandedColumn
          DataBinding.FieldName = 'Diskon'
          Width = 62
          Position.BandIndex = 0
          Position.ColIndex = 4
          Position.RowIndex = 0
        end
        object cxGridDBBandedTableView1GrandTotal: TcxGridDBBandedColumn
          DataBinding.FieldName = 'GrandTotal'
          Width = 76
          Position.BandIndex = 0
          Position.ColIndex = 3
          Position.RowIndex = 0
        end
        object cxGridDBBandedTableView1JumlahDibayar: TcxGridDBBandedColumn
          DataBinding.FieldName = 'JumlahDibayar'
          Width = 82
          Position.BandIndex = 0
          Position.ColIndex = 5
          Position.RowIndex = 0
        end
        object cxGridDBBandedTableView1Status: TcxGridDBBandedColumn
          DataBinding.FieldName = 'Status'
          Width = 66
          Position.BandIndex = 0
          Position.ColIndex = 6
          Position.RowIndex = 0
        end
        object cxGridDBBandedTableView1Keterangan: TcxGridDBBandedColumn
          DataBinding.FieldName = 'Keterangan'
          Width = 75
          Position.BandIndex = 0
          Position.ColIndex = 11
          Position.RowIndex = 0
        end
        object cxGridDBBandedTableView1Pemeriksa: TcxGridDBBandedColumn
          DataBinding.FieldName = 'NamaPemeriksa'
          Width = 84
          Position.BandIndex = 0
          Position.ColIndex = 12
          Position.RowIndex = 0
        end
        object cxGridDBBandedTableView1Penjual: TcxGridDBBandedColumn
          DataBinding.FieldName = 'NamaPenjual'
          Width = 69
          Position.BandIndex = 0
          Position.ColIndex = 13
          Position.RowIndex = 0
        end
        object cxGridDBBandedTableView1Quantity: TcxGridDBBandedColumn
          DataBinding.FieldName = 'Quantity'
          Width = 53
          Position.BandIndex = 0
          Position.ColIndex = 8
          Position.RowIndex = 0
        end
        object cxGridDBBandedTableView1HargaJual: TcxGridDBBandedColumn
          DataBinding.FieldName = 'HargaJual'
          Width = 60
          Position.BandIndex = 0
          Position.ColIndex = 9
          Position.RowIndex = 0
        end
        object cxGridDBBandedTableView1SubTotal: TcxGridDBBandedColumn
          DataBinding.FieldName = 'SubTotal'
          Width = 73
          Position.BandIndex = 0
          Position.ColIndex = 10
          Position.RowIndex = 0
        end
        object cxGridDBBandedTableView1NamaBarang: TcxGridDBBandedColumn
          DataBinding.FieldName = 'NamaBarang'
          Width = 73
          Position.BandIndex = 0
          Position.ColIndex = 7
          Position.RowIndex = 0
        end
        object cxGridDBBandedTableView1InsentifJual: TcxGridDBBandedColumn
          DataBinding.FieldName = 'InsentifJual'
          Width = 69
          Position.BandIndex = 0
          Position.ColIndex = 14
          Position.RowIndex = 0
        end
        object cxGridDBBandedTableView1InsentifPeriksa: TcxGridDBBandedColumn
          DataBinding.FieldName = 'InsentifPeriksa'
          Width = 81
          Position.BandIndex = 0
          Position.ColIndex = 15
          Position.RowIndex = 0
        end
      end
      object cxGridDBTableView1: TcxGridDBTableView
        DataController.DataSource = DataSource1
        DataController.DetailKeyFieldNames = 'KodePenjualan'
        DataController.MasterKeyFieldNames = 'Kode'
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsView.GroupByBox = False
        object cxGridDBTableView1KodePenjualan: TcxGridDBColumn
          DataBinding.FieldName = 'KodePenjualan'
        end
        object cxGridDBTableView1KodeBarang: TcxGridDBColumn
          DataBinding.FieldName = 'NamaBarang'
          Width = 142
        end
        object cxGridDBTableView1Quantity: TcxGridDBColumn
          DataBinding.FieldName = 'Quantity'
        end
        object cxGridDBTableView1HargaBeli: TcxGridDBColumn
          DataBinding.FieldName = 'HargaBeli'
          Width = 84
        end
        object cxGridDBTableView1HargaJual: TcxGridDBColumn
          DataBinding.FieldName = 'HargaJual'
          Width = 86
        end
        object cxGridDBTableView1Diskon: TcxGridDBColumn
          DataBinding.FieldName = 'Diskon'
          Width = 81
        end
        object cxGridDBTableView1SubTotal: TcxGridDBColumn
          DataBinding.FieldName = 'SubTotal'
          Width = 89
        end
        object cxGridDBTableView1Deleted: TcxGridDBColumn
          DataBinding.FieldName = 'Deleted'
          Width = 46
        end
      end
      object cxGridLevel1: TcxGridLevel
        GridView = cxGridDBBandedTableView1
      end
    end
  end
  inherited mmMain: TMainMenu
    Left = 32
    Top = 187
    inherited miOptions: TMenuItem
      object miFullCollapsing: TMenuItem [0]
        Action = actFullCollapse
      end
      object miFullExpand: TMenuItem [1]
        Action = actFullExpand
      end
      object N3: TMenuItem [2]
        Caption = '-'
      end
    end
    inherited miHelp: TMenuItem
      Visible = False
    end
  end
  inherited sty: TActionList
    Left = 72
    Top = 179
    object actFullExpand: TAction
      Category = 'Options'
      Caption = 'Full &Expand'
      Hint = 'Full expand'
      ImageIndex = 8
      OnExecute = actFullExpandExecute
    end
    object actFullCollapse: TAction
      Category = 'Options'
      Caption = 'Full &Collapse'
      Hint = 'Full collapse'
      ImageIndex = 7
      OnExecute = actFullCollapseExecute
    end
  end
  inherited dxComponentPrinter: TdxComponentPrinter
    CurrentLink = dxComponentPrinterLink1
    Left = 720
    Top = 64
    object dxComponentPrinterLink1: TdxGridReportLink
      Active = True
      Component = cxGrid
      PrinterPage.DMPaper = 1
      PrinterPage.Footer = 5080
      PrinterPage.GrayShading = True
      PrinterPage.Header = 2540
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.PageSize.X = 215900
      PrinterPage.PageSize.Y = 279400
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 41475.497984814810000000
      BuiltInReportLink = True
    end
  end
  inherited dxPSEngineController1: TdxPSEngineController
    Left = 752
    Top = 64
  end
  inherited ilMain: TcxImageList
    FormatVersion = 1
    DesignInfo = 11010160
  end
  inherited XPManifest1: TXPManifest
    Left = 784
    Top = 56
  end
  object edrepMain: TcxEditRepository
    Left = 184
    Top = 179
    object edrepCenterText: TcxEditRepositoryTextItem
      Properties.Alignment.Horz = taCenter
    end
    object edrepRightText: TcxEditRepositoryTextItem
      Properties.Alignment.Horz = taRightJustify
    end
  end
  object StyleRepository: TcxStyleRepository
    Left = 144
    Top = 179
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle3: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle4: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle5: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 14811135
      TextColor = clBlack
    end
    object cxStyle6: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clNavy
    end
    object cxStyle7: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = 14872561
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
    end
    object cxStyle8: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle9: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle10: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle11: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle12: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle13: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16777088
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clBlue
    end
    object cxStyle14: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 12937777
      TextColor = clWhite
    end
    object tvssDevExpress: TcxGridTableViewStyleSheet
      Caption = 'DevExpress'
      Styles.Background = cxStyle1
      Styles.Content = cxStyle2
      Styles.ContentEven = cxStyle3
      Styles.ContentOdd = cxStyle4
      Styles.FilterBox = cxStyle5
      Styles.Inactive = cxStyle10
      Styles.IncSearch = cxStyle11
      Styles.Selection = cxStyle14
      Styles.Footer = cxStyle6
      Styles.Group = cxStyle7
      Styles.GroupByBox = cxStyle8
      Styles.Header = cxStyle9
      Styles.Indicator = cxStyle12
      Styles.Preview = cxStyle13
      BuiltIn = True
    end
  end
  object SDQuery1: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from detailpenjualan')
    Left = 488
    Top = 280
    object SDQuery1KodePenjualan: TStringField
      FieldName = 'KodePenjualan'
      Required = True
      Size = 10
    end
    object SDQuery1KodeBarang: TStringField
      FieldName = 'KodeBarang'
      Required = True
      Size = 10
    end
    object SDQuery1Quantity: TFloatField
      FieldName = 'Quantity'
    end
    object SDQuery1HargaBeli: TCurrencyField
      FieldName = 'HargaBeli'
    end
    object SDQuery1HargaJual: TCurrencyField
      FieldName = 'HargaJual'
    end
    object SDQuery1Diskon: TCurrencyField
      FieldName = 'Diskon'
    end
    object SDQuery1SubTotal: TCurrencyField
      FieldName = 'SubTotal'
    end
    object SDQuery1Deleted: TBooleanField
      FieldName = 'Deleted'
    end
    object SDQuery1NamaBarang: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaBarang'
      LookupDataSet = BarangQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'KodeBarang'
      Size = 200
      Lookup = True
    end
  end
  object DataSource1: TDataSource
    DataSet = SDQuery1
    Left = 544
    Top = 280
  end
  object MasterQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from penjualan p, detailpenjualan dp'
      
        'where tanggal>= :text1 and tanggal<= :text2 and dp.kodepenjualan' +
        '=p.kode')
    Left = 8
    Top = 48
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'text1'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'text2'
        ParamType = ptInput
      end>
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQTanggal: TDateTimeField
      FieldName = 'Tanggal'
    end
    object MasterQPelanggan: TStringField
      FieldName = 'Pelanggan'
      Size = 10
    end
    object MasterQDiskon: TCurrencyField
      FieldName = 'Diskon'
    end
    object MasterQGrandTotal: TCurrencyField
      FieldName = 'GrandTotal'
    end
    object MasterQJumlahDibayar: TCurrencyField
      FieldName = 'JumlahDibayar'
    end
    object MasterQStatus: TStringField
      FieldName = 'Status'
      Size = 50
    end
    object MasterQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object MasterQPemeriksa: TStringField
      FieldName = 'Pemeriksa'
      Size = 10
    end
    object MasterQPenjual: TStringField
      FieldName = 'Penjual'
      Size = 10
    end
    object MasterQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object MasterQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object MasterQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object MasterQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object MasterQDeleted: TBooleanField
      FieldName = 'Deleted'
    end
    object MasterQNamaPelanggan: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPelanggan'
      LookupDataSet = PelangganQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Pelanggan'
      Size = 200
      Lookup = True
    end
    object MasterQNamaPemeriksa: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPemeriksa'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Pemeriksa'
      Size = 200
      Lookup = True
    end
    object MasterQNamaPenjual: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPenjual'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Penjual'
      Size = 200
      Lookup = True
    end
    object MasterQDP: TCurrencyField
      FieldName = 'DP'
    end
    object MasterQKodePenjualan: TStringField
      FieldName = 'KodePenjualan'
      Required = True
      Size = 10
    end
    object MasterQKodeBarang: TStringField
      FieldName = 'KodeBarang'
      Required = True
      Size = 10
    end
    object MasterQQuantity: TFloatField
      FieldName = 'Quantity'
    end
    object MasterQHargaBeli: TCurrencyField
      FieldName = 'HargaBeli'
    end
    object MasterQHargaJual: TCurrencyField
      FieldName = 'HargaJual'
    end
    object MasterQDiskon_1: TCurrencyField
      FieldName = 'Diskon_1'
    end
    object MasterQSubTotal: TCurrencyField
      FieldName = 'SubTotal'
    end
    object MasterQDeleted_1: TBooleanField
      FieldName = 'Deleted_1'
    end
    object MasterQNamaBarang: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaBarang'
      LookupDataSet = BarangQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'KodeBarang'
      Size = 200
      Lookup = True
    end
    object MasterQInsentifJual: TCurrencyField
      FieldKind = fkLookup
      FieldName = 'InsentifJual'
      LookupDataSet = BarangQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'InsentifJual'
      KeyFields = 'KodeBarang'
      Lookup = True
    end
    object MasterQInsentifPeriksa: TCurrencyField
      FieldKind = fkLookup
      FieldName = 'InsentifPeriksa'
      LookupDataSet = BarangQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'InsentifPeriksa'
      KeyFields = 'KodeBarang'
      Lookup = True
    end
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 40
    Top = 56
  end
  object MasterUs: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kodenota, Tgl, NoSO, Sopir, Sopir2, Crew, TitipKwitansi, ' +
        'NominalKwitansi, NoKwitansi, Keterangan, Kir, STNK, Pajak, TglKe' +
        'mbali, Pendapatan, Pengeluaran, SisaDisetor, SPBUAYaniLiter, SPB' +
        'UAYaniUang, SPBUAYaniJam, SPBULuarLiter, SPBULuarUang, SPBULuarU' +
        'angDiberi, SPBULuarDetail, Status, CreateDate, CreateBy, Operato' +
        'r, TglEntry, Laporan, TglRealisasi, Awal, Akhir, TglCetak, Claim' +
        'Sopir, KeteranganClaimSopir, PremiSopir, PremiSopir2, PremiKerne' +
        't, TabunganSopir, TabunganSopir2, Tol, UangJalan, BiayaLainLain,' +
        ' KeteranganBiayaLainLain, UangMakan, UangInap, UangParkir, Other'
      'from MasterSJ'
      'where'
      '  Kodenota = :OLD_Kodenota')
    ModifySQL.Strings = (
      'update MasterSJ'
      'set'
      '  Kodenota = :Kodenota,'
      '  Tgl = :Tgl,'
      '  NoSO = :NoSO,'
      '  Sopir = :Sopir,'
      '  Sopir2 = :Sopir2,'
      '  Crew = :Crew,'
      '  TitipKwitansi = :TitipKwitansi,'
      '  NominalKwitansi = :NominalKwitansi,'
      '  NoKwitansi = :NoKwitansi,'
      '  Keterangan = :Keterangan,'
      '  Kir = :Kir,'
      '  STNK = :STNK,'
      '  Pajak = :Pajak,'
      '  TglKembali = :TglKembali,'
      '  Pendapatan = :Pendapatan,'
      '  Pengeluaran = :Pengeluaran,'
      '  SisaDisetor = :SisaDisetor,'
      '  SPBUAYaniLiter = :SPBUAYaniLiter,'
      '  SPBUAYaniUang = :SPBUAYaniUang,'
      '  SPBUAYaniJam = :SPBUAYaniJam,'
      '  SPBULuarLiter = :SPBULuarLiter,'
      '  SPBULuarUang = :SPBULuarUang,'
      '  SPBULuarUangDiberi = :SPBULuarUangDiberi,'
      '  SPBULuarDetail = :SPBULuarDetail,'
      '  Status = :Status,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  TglEntry = :TglEntry,'
      '  Laporan = :Laporan,'
      '  TglRealisasi = :TglRealisasi,'
      '  Awal = :Awal,'
      '  Akhir = :Akhir,'
      '  TglCetak = :TglCetak,'
      '  ClaimSopir = :ClaimSopir,'
      '  KeteranganClaimSopir = :KeteranganClaimSopir,'
      '  PremiSopir = :PremiSopir,'
      '  PremiSopir2 = :PremiSopir2,'
      '  PremiKernet = :PremiKernet,'
      '  TabunganSopir = :TabunganSopir,'
      '  TabunganSopir2 = :TabunganSopir2,'
      '  Tol = :Tol,'
      '  UangJalan = :UangJalan,'
      '  BiayaLainLain = :BiayaLainLain,'
      '  KeteranganBiayaLainLain = :KeteranganBiayaLainLain,'
      '  UangMakan = :UangMakan,'
      '  UangInap = :UangInap,'
      '  UangParkir = :UangParkir,'
      '  Other = :Other'
      'where'
      '  Kodenota = :OLD_Kodenota')
    InsertSQL.Strings = (
      'insert into MasterSJ'
      
        '  (Kodenota, Tgl, NoSO, Sopir, Sopir2, Crew, TitipKwitansi, Nomi' +
        'nalKwitansi, NoKwitansi, Keterangan, Kir, STNK, Pajak, TglKembal' +
        'i, Pendapatan, Pengeluaran, SisaDisetor, SPBUAYaniLiter, SPBUAYa' +
        'niUang, SPBUAYaniJam, SPBULuarLiter, SPBULuarUang, SPBULuarUangD' +
        'iberi, SPBULuarDetail, Status, CreateDate, CreateBy, Operator, T' +
        'glEntry, Laporan, TglRealisasi, Awal, Akhir, TglCetak, ClaimSopi' +
        'r, KeteranganClaimSopir, PremiSopir, PremiSopir2, PremiKernet, T' +
        'abunganSopir, TabunganSopir2, Tol, UangJalan, BiayaLainLain, Ket' +
        'eranganBiayaLainLain, UangMakan, UangInap, UangParkir, Other)'
      'values'
      
        '  (:Kodenota, :Tgl, :NoSO, :Sopir, :Sopir2, :Crew, :TitipKwitans' +
        'i, :NominalKwitansi, :NoKwitansi, :Keterangan, :Kir, :STNK, :Paj' +
        'ak, :TglKembali, :Pendapatan, :Pengeluaran, :SisaDisetor, :SPBUA' +
        'YaniLiter, :SPBUAYaniUang, :SPBUAYaniJam, :SPBULuarLiter, :SPBUL' +
        'uarUang, :SPBULuarUangDiberi, :SPBULuarDetail, :Status, :CreateD' +
        'ate, :CreateBy, :Operator, :TglEntry, :Laporan, :TglRealisasi, :' +
        'Awal, :Akhir, :TglCetak, :ClaimSopir, :KeteranganClaimSopir, :Pr' +
        'emiSopir, :PremiSopir2, :PremiKernet, :TabunganSopir, :TabunganS' +
        'opir2, :Tol, :UangJalan, :BiayaLainLain, :KeteranganBiayaLainLai' +
        'n, :UangMakan, :UangInap, :UangParkir, :Other)')
    DeleteSQL.Strings = (
      'delete from MasterSJ'
      'where'
      '  Kodenota = :OLD_Kodenota')
    Left = 64
    Top = 56
  end
  object PelangganQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from pelanggan')
    UpdateObject = RealisasiAnjemUs
    Left = 136
    Top = 88
    object PelangganQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object PelangganQNama: TStringField
      FieldName = 'Nama'
      Size = 250
    end
    object PelangganQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 250
    end
    object PelangganQKota: TStringField
      FieldName = 'Kota'
      Size = 100
    end
    object PelangganQTelp: TStringField
      FieldName = 'Telp'
      Size = 50
    end
    object PelangganQHP: TStringField
      FieldName = 'HP'
      Size = 50
    end
    object PelangganQTglLahir: TDateTimeField
      FieldName = 'TglLahir'
    end
    object PelangganQFoto: TBlobField
      FieldName = 'Foto'
    end
    object PelangganQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object PelangganQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object PelangganQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object PelangganQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object PelangganQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object PelangganQDeleted: TBooleanField
      FieldName = 'Deleted'
    end
  end
  object PegawaiQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from pegawai')
    Left = 184
    Top = 88
    object PegawaiQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object PegawaiQNama: TStringField
      FieldName = 'Nama'
      Size = 250
    end
    object PegawaiQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 250
    end
    object PegawaiQTglLahir: TDateTimeField
      FieldName = 'TglLahir'
    end
    object PegawaiQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object PegawaiQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object PegawaiQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object PegawaiQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object PegawaiQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object PegawaiQDeleted: TBooleanField
      FieldName = 'Deleted'
    end
  end
  object RealisasiAnjemUs: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, Kontrak, TanggalMulai, TanggalSelesai, Armada, Peng' +
        'emudi, KilometerAwal, KilometerAkhir, Pendapatan, Pengeluaran, S' +
        'isaDisetor, SPBUAYaniLiter, SPBUAYaniUang, SPBUAYaniJam, SPBULua' +
        'rLiter, SPBULuarUang, SPBULuarUangDiberi, SPBULuarDetail, PremiS' +
        'opir, PremiKernet, TabunganSopir, Tol, BiayaLainLain, Keterangan' +
        'BiayaLainLain, CreateBy, Operator, CreateDate, TglEntry'
      'from RealisasiAnjem'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update RealisasiAnjem'
      'set'
      '  Kode = :Kode,'
      '  Kontrak = :Kontrak,'
      '  TanggalMulai = :TanggalMulai,'
      '  TanggalSelesai = :TanggalSelesai,'
      '  Armada = :Armada,'
      '  Pengemudi = :Pengemudi,'
      '  KilometerAwal = :KilometerAwal,'
      '  KilometerAkhir = :KilometerAkhir,'
      '  Pendapatan = :Pendapatan,'
      '  Pengeluaran = :Pengeluaran,'
      '  SisaDisetor = :SisaDisetor,'
      '  SPBUAYaniLiter = :SPBUAYaniLiter,'
      '  SPBUAYaniUang = :SPBUAYaniUang,'
      '  SPBUAYaniJam = :SPBUAYaniJam,'
      '  SPBULuarLiter = :SPBULuarLiter,'
      '  SPBULuarUang = :SPBULuarUang,'
      '  SPBULuarUangDiberi = :SPBULuarUangDiberi,'
      '  SPBULuarDetail = :SPBULuarDetail,'
      '  PremiSopir = :PremiSopir,'
      '  PremiKernet = :PremiKernet,'
      '  TabunganSopir = :TabunganSopir,'
      '  Tol = :Tol,'
      '  BiayaLainLain = :BiayaLainLain,'
      '  KeteranganBiayaLainLain = :KeteranganBiayaLainLain,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  CreateDate = :CreateDate,'
      '  TglEntry = :TglEntry'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into RealisasiAnjem'
      
        '  (Kode, Kontrak, TanggalMulai, TanggalSelesai, Armada, Pengemud' +
        'i, KilometerAwal, KilometerAkhir, Pendapatan, Pengeluaran, SisaD' +
        'isetor, SPBUAYaniLiter, SPBUAYaniUang, SPBUAYaniJam, SPBULuarLit' +
        'er, SPBULuarUang, SPBULuarUangDiberi, SPBULuarDetail, PremiSopir' +
        ', PremiKernet, TabunganSopir, Tol, BiayaLainLain, KeteranganBiay' +
        'aLainLain, CreateBy, Operator, CreateDate, TglEntry)'
      'values'
      
        '  (:Kode, :Kontrak, :TanggalMulai, :TanggalSelesai, :Armada, :Pe' +
        'ngemudi, :KilometerAwal, :KilometerAkhir, :Pendapatan, :Pengelua' +
        'ran, :SisaDisetor, :SPBUAYaniLiter, :SPBUAYaniUang, :SPBUAYaniJa' +
        'm, :SPBULuarLiter, :SPBULuarUang, :SPBULuarUangDiberi, :SPBULuar' +
        'Detail, :PremiSopir, :PremiKernet, :TabunganSopir, :Tol, :BiayaL' +
        'ainLain, :KeteranganBiayaLainLain, :CreateBy, :Operator, :Create' +
        'Date, :TglEntry)')
    DeleteSQL.Strings = (
      'delete from RealisasiAnjem'
      'where'
      '  Kode = :OLD_Kode')
    Left = 88
    Top = 232
  end
  object BarangQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from barang')
    UpdateObject = MasterUs
    Left = 32
    Top = 304
    object BarangQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object BarangQBarcode: TStringField
      FieldName = 'Barcode'
      Size = 100
    end
    object BarangQNama: TStringField
      FieldName = 'Nama'
      Size = 250
    end
    object BarangQKategori: TStringField
      FieldName = 'Kategori'
      Size = 10
    end
    object BarangQTipe: TStringField
      FieldName = 'Tipe'
      Size = 100
    end
    object BarangQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object BarangQJumlah: TFloatField
      FieldName = 'Jumlah'
    end
    object BarangQSatuan: TStringField
      FieldName = 'Satuan'
      Size = 50
    end
    object BarangQInsentifJual: TCurrencyField
      FieldName = 'InsentifJual'
    end
    object BarangQInsentifPeriksa: TCurrencyField
      FieldName = 'InsentifPeriksa'
    end
    object BarangQHargaJual: TCurrencyField
      FieldName = 'HargaJual'
    end
    object BarangQHargaBeli: TCurrencyField
      FieldName = 'HargaBeli'
    end
    object BarangQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object BarangQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object BarangQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object BarangQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object BarangQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object BarangQDeleted: TBooleanField
      FieldName = 'Deleted'
    end
  end
end
