object MasterMenuSetFm: TMasterMenuSetFm
  Left = 517
  Top = 141
  AutoScroll = False
  BorderIcons = [biSystemMenu]
  Caption = 'Master Menu Set'
  ClientHeight = 264
  ClientWidth = 615
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 615
    Height = 48
    Align = alTop
    TabOrder = 0
    object lbl1: TLabel
      Left = 8
      Top = 13
      Width = 25
      Height = 13
      Caption = 'Kode'
    end
    object KodeEdit: TcxButtonEdit
      Left = 40
      Top = 10
      TabStop = False
      Properties.Buttons = <
        item
          Caption = '+'
          Default = True
          Kind = bkText
        end>
      Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      TabOrder = 0
      OnEnter = KodeEditEnter
      OnExit = KodeEditExit
      OnKeyDown = KodeEditKeyDown
      Width = 121
    end
    object SearchBtn: TcxButton
      Left = 168
      Top = 10
      Width = 57
      Height = 21
      Caption = 'Search'
      TabOrder = 1
      TabStop = False
      OnClick = SearchBtnClick
    end
  end
  object pnl2: TPanel
    Left = 0
    Top = 194
    Width = 615
    Height = 51
    Align = alBottom
    TabOrder = 1
    object ExitBtn: TcxButton
      Left = 172
      Top = 10
      Width = 75
      Height = 25
      Caption = 'EXIT'
      TabOrder = 2
      Visible = False
      OnClick = ExitBtnClick
    end
    object DeleteBtn: TcxButton
      Left = 91
      Top = 10
      Width = 75
      Height = 25
      Caption = 'DELETE'
      TabOrder = 0
      TabStop = False
      OnClick = DeleteBtnClick
    end
    object SaveBtn: TcxButton
      Left = 8
      Top = 10
      Width = 75
      Height = 25
      Caption = 'SAVE'
      TabOrder = 1
      OnClick = SaveBtnClick
    end
    object cxLabel1: TcxLabel
      Left = 336
      Top = 2
      Caption = '* required fields'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clRed
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = []
      Style.IsFontAssigned = True
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 245
    Width = 615
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 345
    Height = 146
    Align = alLeft
    TabOrder = 3
    object MasterVGrid: TcxDBVerticalGrid
      Left = 1
      Top = 1
      Width = 343
      Height = 144
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      OptionsView.RowHeaderWidth = 146
      OptionsBehavior.GoToNextCellOnTab = True
      OptionsData.CancelOnExit = False
      OptionsData.Appending = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Inserting = False
      ParentFont = False
      TabOrder = 0
      OnEnter = MasterVGridEnter
      OnExit = MasterVGridExit
      DataController.DataSource = MasterDs
      Version = 1
      object MasterVGridNamaMenu: TcxDBEditorRow
        Properties.Caption = 'NamaMenuSet'
        Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
        Properties.EditProperties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.EditProperties.OnButtonClick = MasterVGridNamaMenuEditPropertiesButtonClick
        Properties.DataBinding.FieldName = 'NamaMenu'
        ID = 0
        ParentID = -1
        Index = 0
        Version = 1
      end
      object MasterVGridInisial: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'Inisial'
        Properties.Options.Editing = False
        ID = 1
        ParentID = -1
        Index = 1
        Version = 1
      end
      object MasterVGridHarga: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'Harga'
        Properties.Options.Editing = False
        ID = 2
        ParentID = -1
        Index = 2
        Version = 1
      end
      object MasterVGridNamaKategori: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'NamaKategori'
        Properties.Options.Editing = False
        ID = 3
        ParentID = -1
        Index = 3
        Version = 1
      end
      object MasterVGridKeterangan: TcxDBEditorRow
        Height = 56
        Properties.DataBinding.FieldName = 'Keterangan'
        Properties.Options.Editing = False
        ID = 4
        ParentID = -1
        Index = 4
        Version = 1
      end
    end
  end
  object Panel2: TPanel
    Left = 345
    Top = 48
    Width = 270
    Height = 146
    Align = alClient
    TabOrder = 4
    object cxGrid1: TcxGrid
      Left = 1
      Top = 1
      Width = 268
      Height = 144
      Align = alClient
      TabOrder = 0
      OnExit = cxGrid1Exit
      object cxGrid1DBTableView1: TcxGridDBTableView
        Navigator.Buttons.OnButtonClick = cxGrid1DBTableView1NavigatorButtonsButtonClick
        Navigator.Buttons.First.Visible = False
        Navigator.Buttons.PriorPage.Visible = False
        Navigator.Buttons.Prior.Visible = False
        Navigator.Buttons.Next.Visible = False
        Navigator.Buttons.NextPage.Visible = False
        Navigator.Buttons.Last.Visible = False
        Navigator.Buttons.Insert.Visible = False
        Navigator.Buttons.Append.Visible = False
        Navigator.Buttons.Edit.Visible = True
        Navigator.Buttons.Post.Visible = True
        Navigator.Buttons.Cancel.Visible = True
        Navigator.Buttons.SaveBookmark.Visible = False
        Navigator.Buttons.GotoBookmark.Visible = False
        Navigator.Buttons.Filter.Visible = False
        Navigator.Visible = True
        DataController.DataModeController.SmartRefresh = True
        DataController.DataSource = MenuSetDs
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        NewItemRow.Visible = True
        OptionsBehavior.AlwaysShowEditor = True
        OptionsBehavior.FocusCellOnTab = True
        OptionsBehavior.FocusFirstCellOnNewRecord = True
        OptionsBehavior.GoToNextCellOnEnter = True
        OptionsData.Appending = True
        OptionsData.DeletingConfirmation = False
        OptionsSelection.MultiSelect = True
        OptionsView.GroupByBox = False
        object cxGrid1DBTableView1NamaMenu: TcxGridDBColumn
          DataBinding.FieldName = 'NamaMenu'
          Width = 182
        end
        object cxGrid1DBTableView1Jumlah: TcxGridDBColumn
          DataBinding.FieldName = 'Jumlah'
          Width = 62
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
        Options.DetailTabsPosition = dtpLeft
      end
    end
  end
  object MasterQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select * from menu')
    Left = 273
    Top = 9
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQNamaMenu: TStringField
      FieldName = 'NamaMenu'
      Size = 200
    end
    object MasterQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object MasterQHarga: TCurrencyField
      FieldName = 'Harga'
    end
    object MasterQKategori: TStringField
      FieldName = 'Kategori'
      Size = 10
    end
    object MasterQNamaKategori: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaKategori'
      LookupDataSet = KategoriQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Kategori'
      Size = 200
      Lookup = True
    end
    object MasterQInisial: TStringField
      FieldName = 'Inisial'
      Size = 100
    end
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 356
    Top = 6
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from menu order by kode desc')
    Left = 241
    Top = 7
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object MenuSetDs: TDataSource
    DataSet = DetailMenuQ
    Left = 492
    Top = 14
  end
  object MenuSetUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      'select KodeMenuSet, KodeMenu, Jumlah'#13#10'from menuset'
      'where'
      '  KodeMenuSet = :OLD_KodeMenuSet and'
      '  KodeMenu = :OLD_KodeMenu')
    ModifySQL.Strings = (
      'update menuset'
      'set'
      '  KodeMenuSet = :KodeMenuSet,'
      '  KodeMenu = :KodeMenu,'
      '  Jumlah = :Jumlah'
      'where'
      '  KodeMenuSet = :OLD_KodeMenuSet and'
      '  KodeMenu = :OLD_KodeMenu')
    InsertSQL.Strings = (
      'insert into menuset'
      '  (KodeMenuSet, KodeMenu, Jumlah)'
      'values'
      '  (:KodeMenuSet, :KodeMenu, :Jumlah)')
    DeleteSQL.Strings = (
      'delete from menuset'
      'where'
      '  KodeMenuSet = :OLD_KodeMenuSet and'
      '  KodeMenu = :OLD_KodeMenu')
    Left = 524
    Top = 10
  end
  object DetailMenuQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from menuset where kodemenuset= :text')
    UpdateObject = MenuSetUS
    Left = 457
    Top = 9
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object DetailMenuQKodeMenuSet: TStringField
      FieldName = 'KodeMenuSet'
      Size = 10
    end
    object DetailMenuQKodeMenu: TStringField
      FieldName = 'KodeMenu'
      Required = True
      Size = 10
    end
    object DetailMenuQJumlah: TStringField
      FieldName = 'Jumlah'
    end
    object DetailMenuQNamaMenu: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaMenu'
      LookupDataSet = MenuQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NamaMenu'
      KeyFields = 'KodeMenu'
      Size = 100
      Lookup = True
    end
  end
  object ExecuteDeleteDetailMenuQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      '')
    Left = 352
    Top = 216
  end
  object KategoriQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from kategori')
    Left = 321
    Top = 7
    object KategoriQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object KategoriQNama: TStringField
      FieldName = 'Nama'
      Size = 200
    end
  end
  object MenuQ: TSDQuery
    DatabaseName = 'Data'
    Options = []
    SQL.Strings = (
      'select * from menu')
    Left = 424
    Top = 8
  end
end
