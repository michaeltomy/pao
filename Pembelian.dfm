object PembelianFm: TPembelianFm
  Left = 267
  Top = 29
  Width = 910
  Height = 724
  Caption = 'Pembelian'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 894
    Height = 97
    Align = alTop
    TabOrder = 0
    object lbl1: TLabel
      Left = 8
      Top = 13
      Width = 25
      Height = 13
      Caption = 'Kode'
    end
    object KodeEdit: TcxButtonEdit
      Left = 40
      Top = 10
      Properties.Buttons = <
        item
          Caption = '+'
          Default = True
          Kind = bkText
        end>
      Properties.OnButtonClick = KodeEditPropertiesButtonClick
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      TabOrder = 0
      OnEnter = KodeEditEnter
      OnExit = KodeEditExit
      OnKeyDown = KodeEditKeyDown
      Width = 121
    end
    object cxCurrencyEdit1: TcxCurrencyEdit
      Left = 689
      Top = 21
      ParentFont = False
      Properties.ReadOnly = True
      Properties.UseDisplayFormatWhenEditing = True
      Properties.UseThousandSeparator = True
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -16
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsBold]
      Style.IsFontAssigned = True
      TabOrder = 1
      Width = 177
    end
    object cxLabel1: TcxLabel
      Left = 560
      Top = 24
      Caption = 'Grand Total :'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -16
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsBold]
      Style.IsFontAssigned = True
    end
    object cxLabel5: TcxLabel
      Left = 8
      Top = 64
      Caption = 'Supplier'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -16
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsBold]
      Style.IsFontAssigned = True
    end
    object cxDateEdit1: TcxDateEdit
      Left = 276
      Top = 36
      Properties.ShowTime = False
      Properties.OnChange = cxDateEdit1PropertiesChange
      TabOrder = 4
      Width = 121
    end
    object cxLabel6: TcxLabel
      Left = 200
      Top = 35
      Caption = 'Tanggal'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -16
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsBold]
      Style.IsFontAssigned = True
    end
    object cxButtonEdit1: TcxButtonEdit
      Left = 112
      Top = 64
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.ReadOnly = True
      Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
      TabOrder = 6
      Width = 121
    end
    object cxCheckBox1: TcxCheckBox
      Left = 546
      Top = 60
      Caption = 'LUNAS'
      Properties.OnChange = cxCheckBox1PropertiesChange
      TabOrder = 7
      Visible = False
      Width = 79
    end
    object cxLabel7: TcxLabel
      Left = 630
      Top = 56
      Caption = 'DP :'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -16
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsBold]
      Style.IsFontAssigned = True
      Visible = False
    end
    object cxCurrencyEdit2: TcxCurrencyEdit
      Left = 689
      Top = 53
      ParentFont = False
      Properties.Nullstring = '0'
      Properties.ReadOnly = False
      Properties.UseDisplayFormatWhenEditing = True
      Properties.UseThousandSeparator = True
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -16
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsBold]
      Style.IsFontAssigned = True
      TabOrder = 9
      Visible = False
      Width = 177
    end
    object cxTextEdit1: TcxTextEdit
      Left = 63
      Top = 37
      Properties.OnValidate = cxTextEdit1PropertiesValidate
      TabOrder = 10
      Width = 121
    end
    object cxLabel8: TcxLabel
      Left = 8
      Top = 35
      Caption = 'Nota'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -16
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsBold]
      Style.IsFontAssigned = True
    end
    object cxLabel4: TcxLabel
      Left = 247
      Top = 61
      AutoSize = False
      Caption = 'Tanggal Bayar'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -16
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsBold]
      Style.IsFontAssigned = True
      Height = 24
      Width = 122
    end
    object cxDateEdit2: TcxDateEdit
      Left = 373
      Top = 62
      Properties.ShowTime = False
      Properties.OnChange = cxDateEdit1PropertiesChange
      TabOrder = 13
      Width = 121
    end
  end
  object pnl2: TPanel
    Left = 0
    Top = 616
    Width = 894
    Height = 51
    Align = alBottom
    TabOrder = 1
    object ExitBtn: TcxButton
      Left = 268
      Top = 10
      Width = 75
      Height = 25
      Caption = 'EXIT [F8]'
      TabOrder = 1
      OnClick = ExitBtnClick
    end
    object DeleteBtn: TcxButton
      Left = 187
      Top = 10
      Width = 75
      Height = 25
      Caption = 'DELETE [F7]'
      TabOrder = 2
      OnClick = DeleteBtnClick
    end
    object SaveBtn: TcxButton
      Left = 104
      Top = 10
      Width = 75
      Height = 25
      Caption = 'SAVE [F6]'
      TabOrder = 0
      OnClick = SaveBtnClick
      OnKeyDown = SaveBtnKeyDown
    end
    object cxLabel3: TcxLabel
      Left = 376
      Top = 8
      Caption = 'New Nota [F5], Add Item [F4]'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -21
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = []
      Style.IsFontAssigned = True
    end
    object cxButton1: TcxButton
      Left = 16
      Top = 10
      Width = 83
      Height = 25
      Caption = 'ADD ITEM [F4]'
      TabOrder = 4
      OnClick = cxButton1Click
      OnKeyDown = SaveBtnKeyDown
    end
    object cxLabel2: TcxLabel
      Left = 376
      Top = 32
      Caption = 'cxLabel2'
      Visible = False
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 667
    Width = 894
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 97
    Width = 894
    Height = 312
    Align = alTop
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    OnExit = cxGrid1Exit
    object cxGrid1DBTableView1: TcxGridDBTableView
      OnKeyDown = cxGrid1DBTableView1KeyDown
      Navigator.Buttons.First.Visible = False
      Navigator.Buttons.PriorPage.Visible = False
      Navigator.Buttons.Prior.Visible = False
      Navigator.Buttons.Next.Visible = False
      Navigator.Buttons.NextPage.Visible = False
      Navigator.Buttons.Last.Visible = False
      Navigator.Buttons.Insert.Visible = True
      Navigator.Buttons.Post.Visible = True
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.Visible = True
      OnCellDblClick = cxGrid1DBTableView1CellDblClick
      DataController.DataSource = DataSource1
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      NewItemRow.Visible = True
      OptionsBehavior.FocusCellOnTab = True
      OptionsBehavior.NavigatorHints = True
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnGrouping = False
      OptionsData.Appending = True
      OptionsData.DeletingConfirmation = False
      OptionsView.GroupByBox = False
      object cxGrid1DBTableView1Column1: TcxGridDBColumn
        DataBinding.FieldName = 'Barang'
        PropertiesClassName = 'TcxButtonEditProperties'
        Properties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.OnButtonClick = cxGrid1DBTableView1Column1PropertiesButtonClick
        Width = 91
      end
      object cxGrid1DBTableView1Column2: TcxGridDBColumn
        DataBinding.FieldName = 'NamaBarang'
        Width = 182
      end
      object cxGrid1DBTableView1Column3: TcxGridDBColumn
        DataBinding.FieldName = 'Stok'
      end
      object cxGrid1DBTableView1Column4: TcxGridDBColumn
        DataBinding.FieldName = 'Satuan'
        Width = 94
      end
      object cxGrid1DBTableView1Column5: TcxGridDBColumn
        DataBinding.FieldName = 'HargaTerakhir'
        Width = 106
      end
      object cxGrid1DBTableView1Column6: TcxGridDBColumn
        DataBinding.FieldName = 'Jumlah'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.OnValidate = cxGrid1DBTableView1Column6PropertiesValidate
        Width = 71
      end
      object cxGrid1DBTableView1Column7: TcxGridDBColumn
        DataBinding.FieldName = 'Harga'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.OnValidate = cxGrid1DBTableView1Column7PropertiesValidate
        Width = 128
      end
      object cxGrid1DBTableView1Column8: TcxGridDBColumn
        DataBinding.FieldName = 'SubTotal'
        Width = 141
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object cxGrid2: TcxGrid
    Left = 0
    Top = 416
    Width = 894
    Height = 200
    Align = alBottom
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
    object cxGrid2DBTableView1: TcxGridDBTableView
      OnCellDblClick = cxGrid2DBTableView1CellDblClick
      DataController.DataSource = DataSource3
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.ImmediateEditor = False
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      object cxGrid2DBTableView1Column1: TcxGridDBColumn
        DataBinding.FieldName = 'Tgl'
      end
      object cxGrid2DBTableView1Column2: TcxGridDBColumn
        DataBinding.FieldName = 'NamaSupplier'
        Width = 294
      end
      object cxGrid2DBTableView1Column3: TcxGridDBColumn
        DataBinding.FieldName = 'GrandTotal'
        Width = 183
      end
      object cxGrid2DBTableView1Column4: TcxGridDBColumn
        DataBinding.FieldName = 'Status'
        Width = 202
      end
    end
    object cxGrid2Level1: TcxGridLevel
      GridView = cxGrid2DBTableView1
    end
  end
  object MasterQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from pembelian')
    UpdateObject = MasterUS
    Left = 49
    Top = 193
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQNomorNota: TStringField
      FieldName = 'NomorNota'
      Size = 50
    end
    object MasterQSupplier: TStringField
      FieldName = 'Supplier'
      Required = True
      Size = 10
    end
    object MasterQTgl: TDateTimeField
      FieldName = 'Tgl'
      Required = True
    end
    object MasterQGrandTotal: TCurrencyField
      FieldName = 'GrandTotal'
      Required = True
    end
    object MasterQStatus: TStringField
      FieldName = 'Status'
      Size = 50
    end
    object MasterQNamaSupplier: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaSupplier'
      LookupDataSet = SupplierQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Supplier'
      Size = 55
      Lookup = True
    end
    object MasterQTglBayar: TDateTimeField
      FieldName = 'TglBayar'
    end
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 84
    Top = 190
  end
  object MasterUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select GrandTotal, Kode, NomorNota, Status, Supplier, Tgl, TglBa' +
        'yar'#13#10'from pembelian'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update pembelian'
      'set'
      '  GrandTotal = :GrandTotal,'
      '  Kode = :Kode,'
      '  NomorNota = :NomorNota,'
      '  Status = :Status,'
      '  Supplier = :Supplier,'
      '  Tgl = :Tgl,'
      '  TglBayar = :TglBayar'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into pembelian'
      '  (GrandTotal, Kode, NomorNota, Status, Supplier, Tgl, TglBayar)'
      'values'
      
        '  (:GrandTotal, :Kode, :NomorNota, :Status, :Supplier, :Tgl, :Tg' +
        'lBayar)')
    DeleteSQL.Strings = (
      'delete from pembelian'
      'where'
      '  Kode = :OLD_Kode')
    Left = 124
    Top = 194
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from pembelian order by kode desc')
    Left = 17
    Top = 191
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object DetailPembelianQ: TSDQuery
    AutoRefresh = True
    DatabaseName = 'data'
    Options = []
    BeforePost = DetailQBeforePost
    AfterPost = DetailQAfterPost
    AfterDelete = DetailPembelianQAfterDelete
    SQL.Strings = (
      'select * from detailpembelian where kodebeli=:text'
      '')
    UpdateObject = DetailPembelianUS
    Left = 193
    Top = 191
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object DetailPembelianQKodeBeli: TStringField
      FieldName = 'KodeBeli'
      Required = True
      Size = 10
    end
    object DetailPembelianQBarang: TStringField
      FieldName = 'Barang'
      Required = True
      Size = 10
    end
    object DetailPembelianQNamaBarang: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaBarang'
      LookupDataSet = BarangQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Barang'
      Size = 55
      Lookup = True
    end
    object DetailPembelianQStok: TFloatField
      FieldKind = fkLookup
      FieldName = 'Stok'
      LookupDataSet = BarangQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Jumlah'
      KeyFields = 'Barang'
      Lookup = True
    end
    object DetailPembelianQSatuan: TStringField
      FieldName = 'Satuan'
      Required = True
      Size = 50
    end
    object DetailPembelianQHargaTerakhir: TCurrencyField
      FieldKind = fkLookup
      FieldName = 'HargaTerakhir'
      LookupDataSet = BarangQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'HargaBeli'
      KeyFields = 'Barang'
      Lookup = True
    end
    object DetailPembelianQJumlah: TFloatField
      FieldName = 'Jumlah'
      Required = True
    end
    object DetailPembelianQHarga: TCurrencyField
      FieldName = 'Harga'
      Required = True
    end
    object DetailPembelianQSubTotal: TCurrencyField
      FieldName = 'SubTotal'
      Required = True
    end
  end
  object DataSource1: TDataSource
    DataSet = DetailPembelianQ
    Left = 228
    Top = 198
  end
  object BarangQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from barang'
      '')
    UpdateObject = BarangUS
    Left = 321
    Top = 199
    object BarangQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object BarangQNama: TStringField
      FieldName = 'Nama'
      Required = True
      Size = 50
    end
    object BarangQHargaBeli: TCurrencyField
      FieldName = 'HargaBeli'
    end
    object BarangQHargaJual: TCurrencyField
      FieldName = 'HargaJual'
    end
    object BarangQJumlah: TFloatField
      FieldName = 'Jumlah'
    end
    object BarangQSatuan: TStringField
      FieldName = 'Satuan'
      Size = 50
    end
  end
  object DetailPembelianUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select KodeBeli, Barang, Jumlah, Satuan, Harga, SubTotal'#13#10'from d' +
        'etailpembelian'
      'where'
      '  KodeBeli = :OLD_KodeBeli and'
      '  Barang = :OLD_Barang')
    ModifySQL.Strings = (
      'update detailpembelian'
      'set'
      '  KodeBeli = :KodeBeli,'
      '  Barang = :Barang,'
      '  Jumlah = :Jumlah,'
      '  Satuan = :Satuan,'
      '  Harga = :Harga,'
      '  SubTotal = :SubTotal'
      'where'
      '  KodeBeli = :OLD_KodeBeli and'
      '  Barang = :OLD_Barang')
    InsertSQL.Strings = (
      'insert into detailpembelian'
      '  (KodeBeli, Barang, Jumlah, Satuan, Harga, SubTotal)'
      'values'
      '  (:KodeBeli, :Barang, :Jumlah, :Satuan, :Harga, :SubTotal)')
    DeleteSQL.Strings = (
      'delete from detailpembelian'
      'where'
      '  KodeBeli = :OLD_KodeBeli and'
      '  Barang = :OLD_Barang')
    Left = 260
    Top = 194
  end
  object BarangUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, Nama, HargaBeli, HargaJual, Jumlah, Satuan'#13#10'from ba' +
        'rang'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update barang'
      'set'
      '  Kode = :Kode,'
      '  Nama = :Nama,'
      '  HargaBeli = :HargaBeli,'
      '  HargaJual = :HargaJual,'
      '  Jumlah = :Jumlah,'
      '  Satuan = :Satuan'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into barang'
      '  (Kode, Nama, HargaBeli, HargaJual, Jumlah, Satuan)'
      'values'
      '  (:Kode, :Nama, :HargaBeli, :HargaJual, :Jumlah, :Satuan)')
    DeleteSQL.Strings = (
      'delete from barang'
      'where'
      '  Kode = :OLD_Kode')
    Left = 372
    Top = 202
  end
  object SDQuery1: TSDQuery
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from barang'
      '')
    Left = 529
    Top = 207
    object StringField1: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object StringField2: TStringField
      FieldName = 'Nama'
      Required = True
      Size = 50
    end
    object CurrencyField1: TCurrencyField
      FieldName = 'HargaBeli'
    end
    object CurrencyField2: TCurrencyField
      FieldName = 'HargaJual'
    end
    object SDQuery1Jumlah: TFloatField
      FieldName = 'Jumlah'
    end
    object StringField3: TStringField
      FieldName = 'Satuan'
      Size = 50
    end
  end
  object SupplierQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from supplier'
      '')
    Left = 161
    Top = 191
    object SupplierQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object SupplierQNama: TStringField
      FieldName = 'Nama'
      Required = True
      Size = 50
    end
    object SupplierQAlamat: TMemoField
      FieldName = 'Alamat'
      BlobType = ftMemo
    end
    object SupplierQKota: TStringField
      FieldName = 'Kota'
      Size = 50
    end
    object SupplierQTelp1: TStringField
      FieldName = 'Telp1'
      Required = True
      Size = 50
    end
    object SupplierQTelp2: TStringField
      FieldName = 'Telp2'
      Size = 50
    end
    object SupplierQTelp3: TStringField
      FieldName = 'Telp3'
      Size = 50
    end
  end
  object DataSource3: TDataSource
    DataSet = ViewQ
    Left = 52
    Top = 246
  end
  object ViewQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from pembelian'
      '')
    Left = 17
    Top = 247
    object ViewQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object ViewQNomorNota: TStringField
      FieldName = 'NomorNota'
      Size = 50
    end
    object ViewQSupplier: TStringField
      FieldName = 'Supplier'
      Required = True
      Size = 10
    end
    object ViewQTgl: TDateTimeField
      FieldName = 'Tgl'
      Required = True
    end
    object ViewQGrandTotal: TCurrencyField
      FieldName = 'GrandTotal'
      Required = True
    end
    object ViewQStatus: TStringField
      FieldName = 'Status'
      Required = True
      Size = 50
    end
    object ViewQNamaSupplier: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaSupplier'
      LookupDataSet = SupplierQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'Supplier'
      Size = 55
      Lookup = True
    end
  end
end
