object MasterSuratJalanFm: TMasterSuratJalanFm
  Left = 426
  Top = 370
  Width = 607
  Height = 258
  Caption = 'Master Surat Jalan'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 591
    Height = 48
    Align = alTop
    TabOrder = 0
    object lbl1: TLabel
      Left = 8
      Top = 13
      Width = 25
      Height = 13
      Caption = 'Kode'
    end
    object KodeEdit: TcxButtonEdit
      Left = 40
      Top = 10
      Properties.Buttons = <
        item
          Caption = '+'
          Default = True
          Kind = bkText
        end>
      Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      TabOrder = 0
      OnEnter = KodeEditEnter
      OnExit = KodeEditExit
      OnKeyDown = KodeEditKeyDown
      Width = 121
    end
    object SearchBtn: TcxButton
      Left = 168
      Top = 10
      Width = 57
      Height = 21
      Caption = 'Search'
      TabOrder = 1
      OnClick = SearchBtnClick
    end
  end
  object pnl2: TPanel
    Left = 0
    Top = 150
    Width = 591
    Height = 51
    Align = alBottom
    TabOrder = 2
    object ExitBtn: TcxButton
      Left = 172
      Top = 10
      Width = 75
      Height = 25
      Caption = 'EXIT'
      TabOrder = 1
      OnClick = ExitBtnClick
    end
    object DeleteBtn: TcxButton
      Left = 91
      Top = 10
      Width = 75
      Height = 25
      Caption = 'DELETE'
      TabOrder = 2
      OnClick = DeleteBtnClick
    end
    object SaveBtn: TcxButton
      Left = 8
      Top = 10
      Width = 75
      Height = 25
      Caption = 'SAVE'
      TabOrder = 0
      OnClick = SaveBtnClick
    end
  end
  object MasterVGrid: TcxDBVerticalGrid
    Left = 0
    Top = 48
    Width = 591
    Height = 102
    Align = alClient
    OptionsBehavior.GoToNextCellOnTab = True
    OptionsData.CancelOnExit = False
    OptionsData.Appending = False
    OptionsData.Deleting = False
    OptionsData.DeletingConfirmation = False
    OptionsData.Inserting = False
    TabOrder = 1
    TabStop = False
    DataController.DataSource = MasterDs
    Version = 1
    object MasterVGridTgl: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxDateEditProperties'
      Properties.DataBinding.FieldName = 'Tgl'
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object MasterVGridNoSO: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.EditProperties.ReadOnly = True
      Properties.DataBinding.FieldName = 'NoSO'
      ID = 1
      ParentID = -1
      Index = 1
      Version = 1
    end
    object MasterVGridRute: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Rute'
      ID = 2
      ParentID = -1
      Index = 2
      Version = 1
    end
    object MasterVGridPelanggan: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Pelanggan'
      ID = 3
      ParentID = -1
      Index = 3
      Version = 1
    end
    object MasterVGridArmada: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Armada'
      ID = 4
      ParentID = -1
      Index = 4
      Version = 1
    end
    object MasterVGridEkor: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Ekor'
      ID = 5
      ParentID = -1
      Index = 5
      Version = 1
    end
    object MasterVGridSopir: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Sopir'
      ID = 6
      ParentID = -1
      Index = 6
      Version = 1
    end
    object MasterVGridCrew: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Crew'
      ID = 7
      ParentID = -1
      Index = 7
      Version = 1
    end
    object MasterVGridKeterangan: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Keterangan'
      ID = 8
      ParentID = -1
      Index = 8
      Version = 1
    end
    object MasterVGridKirKepala: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'KirKepala'
      ID = 9
      ParentID = -1
      Index = 9
      Version = 1
    end
    object MasterVGridKirEkor: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'KirEkor'
      ID = 10
      ParentID = -1
      Index = 10
      Version = 1
    end
    object MasterVGridSTNK: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'STNK'
      ID = 11
      ParentID = -1
      Index = 11
      Version = 1
    end
    object MasterVGridPajak: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Pajak'
      ID = 12
      ParentID = -1
      Index = 12
      Version = 1
    end
    object MasterVGridTali: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Tali'
      ID = 13
      ParentID = -1
      Index = 13
      Version = 1
    end
    object MasterVGridRantai: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Rantai'
      ID = 14
      ParentID = -1
      Index = 14
      Version = 1
    end
    object MasterVGridKayu: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Kayu'
      ID = 15
      ParentID = -1
      Index = 15
      Version = 1
    end
    object MasterVGridxNama: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'xNama'
      ID = 16
      ParentID = -1
      Index = 16
      Version = 1
    end
    object MasterVGridxTgl: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'xTgl'
      ID = 17
      ParentID = -1
      Index = 17
      Version = 1
    end
    object MasterVGridxNoSJ: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'xNoSJ'
      ID = 18
      ParentID = -1
      Index = 18
      Version = 1
    end
    object MasterVGridNamaMuat: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NamaMuat'
      ID = 19
      ParentID = -1
      Index = 19
      Version = 1
    end
    object MasterVGridTelpMuat: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'TelpMuat'
      ID = 20
      ParentID = -1
      Index = 20
      Version = 1
    end
    object MasterVGridNamaBongkar: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NamaBongkar'
      ID = 21
      ParentID = -1
      Index = 21
      Version = 1
    end
    object MasterVGridTelpBongkar: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'TelpBongkar'
      ID = 22
      ParentID = -1
      Index = 22
      Version = 1
    end
    object MasterVGridLaporan: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Laporan'
      ID = 23
      ParentID = -1
      Index = 23
      Version = 1
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 201
    Width = 591
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object MasterQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    AfterInsert = MasterQAfterInsert
    BeforePost = MasterQBeforePost
    SQL.Strings = (
      'select * from mastersj')
    UpdateObject = MasterUS
    Left = 377
    Top = 9
    object MasterQKodenota: TStringField
      FieldName = 'Kodenota'
      Required = True
    end
    object MasterQTgl: TDateTimeField
      FieldName = 'Tgl'
      Required = True
    end
    object MasterQNoSO: TStringField
      FieldName = 'NoSO'
      Required = True
    end
    object MasterQRute: TStringField
      FieldName = 'Rute'
      Required = True
      Size = 10
    end
    object MasterQPelanggan: TStringField
      FieldName = 'Pelanggan'
      Required = True
      Size = 10
    end
    object MasterQArmada: TStringField
      FieldName = 'Armada'
      Required = True
      Size = 10
    end
    object MasterQEkor: TStringField
      FieldName = 'Ekor'
      Size = 10
    end
    object MasterQSopir: TStringField
      FieldName = 'Sopir'
      Required = True
      Size = 10
    end
    object MasterQCrew: TStringField
      FieldName = 'Crew'
      Size = 10
    end
    object MasterQKeterangan: TStringField
      FieldName = 'Keterangan'
      Size = 100
    end
    object MasterQKirKepala: TBooleanField
      FieldName = 'KirKepala'
      Required = True
    end
    object MasterQKirEkor: TBooleanField
      FieldName = 'KirEkor'
      Required = True
    end
    object MasterQSTNK: TBooleanField
      FieldName = 'STNK'
      Required = True
    end
    object MasterQPajak: TBooleanField
      FieldName = 'Pajak'
      Required = True
    end
    object MasterQTali: TIntegerField
      FieldName = 'Tali'
      Required = True
    end
    object MasterQRantai: TIntegerField
      FieldName = 'Rantai'
      Required = True
    end
    object MasterQKayu: TIntegerField
      FieldName = 'Kayu'
      Required = True
    end
    object MasterQxNama: TStringField
      FieldName = 'xNama'
      Size = 50
    end
    object MasterQxTgl: TDateTimeField
      FieldName = 'xTgl'
    end
    object MasterQxNoSJ: TStringField
      FieldName = 'xNoSJ'
    end
    object MasterQNamaMuat: TStringField
      FieldName = 'NamaMuat'
      Size = 50
    end
    object MasterQTelpMuat: TStringField
      FieldName = 'TelpMuat'
      Size = 12
    end
    object MasterQNamaBongkar: TStringField
      FieldName = 'NamaBongkar'
      Size = 50
    end
    object MasterQTelpBongkar: TStringField
      FieldName = 'TelpBongkar'
      Size = 12
    end
    object MasterQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
      Required = True
    end
    object MasterQCreateBy: TStringField
      FieldName = 'CreateBy'
      Required = True
      Size = 10
    end
    object MasterQOperator: TStringField
      FieldName = 'Operator'
      Required = True
      Size = 10
    end
    object MasterQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
      Required = True
    end
    object MasterQLaporan: TStringField
      FieldName = 'Laporan'
      Size = 100
    end
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 460
    Top = 14
  end
  object MasterUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kodenota, Tgl, NoSO, Rute, Pelanggan, Armada, Ekor, Sopir' +
        ', Crew, Keterangan, KirKepala, KirEkor, STNK, Pajak, Tali, Ranta' +
        'i, Kayu, xNama, xTanggal, xNoSJ, NamaMuat, TelpMuat, NamaBongkar' +
        ', TelpBongkar, CreateDate, CreateBy, Operator, TglEntry, Laporan'
      'from mastersj'
      'where'
      '  Kodenota = :OLD_Kodenota')
    ModifySQL.Strings = (
      'update sopir'
      'set'
      '  Kodenota = :Kodenota,'
      '  Tgl = :Tgl,'
      '  NoSO = :NoSO,'
      '  Rute = :Rute,'
      '  Pelanggan = :Pelanggan,'
      '  Armada = :Armada,'
      '  Ekor = :Ekor,'
      '  Sopir = :Sopir,'
      '  Crew = :Crew,'
      '  Keterangan = :Keterangan,'
      '  KirKepala = :KirKepala,'
      '  KirEkor = :KirEkor,'
      '  STNK = :STNK,'
      '  Pajak = :Pajak,'
      '  Tali = :Tali,'
      '  Rantai = :Rantai,'
      '  Kayu = :Kayu,'
      '  xNama = :xNama,'
      '  xTgl = :xTgl,'
      '  xNoSJ = :xNoSJ,'
      '  NamaMuat = :NamaMuat,'
      '  TelpMuat = :TelpMuat,'
      '  NamaBongkar = :NamaBongkar,'
      '  TelpBongkar = :TelpBongkar,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  Operator = :Operator,'
      '  TglEntry = :TglEntry,'
      '  Laporan = :Laporan'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into mastersj'
      
        '  (Kodenota, Tgl, NoSO, Rute, Pelanggan, Armada, Ekor, Sopir, Cr' +
        'ew, Keterangan, KirKepala, KirEkor, STNK, Pajak, Tali, Rantai, K' +
        'ayu, xNama, xTgl, xNoSJ, NamaMuat, TelpMuat, NamaBongkar, TelpBo' +
        'ngkar, CreateDate, CreateBy, Operator, TglEntry, Laporan)'
      'values'
      
        '  (:Kodenota, :Tgl, :NoSO, :Rute, :Pelanggan, :Armada, :Ekor, :S' +
        'opir, :Crew, :Keterangan, :KirKepala, :KirEkor, :STNK, :Pajak, :' +
        'Tali, :Rantai, :Kayu, :xNama, :xTgl, :xNoSJ, :NamaMuat, :TelpMua' +
        't, :NamaBongkar, :TelpBongkar, :CreateDate, :CreateBy, :Operator' +
        ', :TglEntry, :Laporan)')
    DeleteSQL.Strings = (
      'delete from mastersj'
      'where'
      '  Kodenota = :OLD_Kodenota')
    Left = 500
    Top = 10
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kodenota from mastersj order by kodenota desc')
    Left = 417
    Top = 15
    object KodeQkodenota: TStringField
      FieldName = 'kodenota'
      Required = True
    end
  end
end
