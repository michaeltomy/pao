unit Notifikasi;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxCalendar,
  cxCurrencyEdit, cxCalc, cxRadioGroup, cxListBox, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxGridCustomTableView,
  cxGridTableView, cxGridCustomView, cxClasses, cxGridLevel, cxGrid, DateUtils;

type
  TNotifikasiFm = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    BarangQ: TSDQuery;
    StatusBar: TStatusBar;
    SJQ: TSDQuery;
    ExitBtn: TcxButton;
    DeleteBtn: TcxButton;
    SaveBtn: TcxButton;
    FollowUpSOQ: TSDQuery;
    CustQ: TSDQuery;
    CustomerLamaQ: TSDQuery;
    CustomerLamaQKode: TStringField;
    CustomerLamaQNamaJenis: TStringField;
    CustomerLamaQTipe: TMemoField;
    CustomerLamaQKeterangan: TMemoField;
    cxListBox1: TcxListBox;
    FollowUpSOQKodenota: TStringField;
    FollowUpSOQTgl: TDateTimeField;
    FollowUpSOQPelanggan: TStringField;
    FollowUpSOQTglOrder: TDateTimeField;
    FollowUpSOQJenisBarang: TStringField;
    FollowUpSOQHarga: TCurrencyField;
    FollowUpSOQRute: TStringField;
    FollowUpSOQKeterangan: TMemoField;
    FollowUpSOQCreateDate: TDateTimeField;
    FollowUpSOQCreateBy: TStringField;
    FollowUpSOQOperator: TStringField;
    FollowUpSOQTglEntry: TDateTimeField;
    FollowUpSOQStatus: TStringField;
    FollowUpSOQTglFollowUp: TDateTimeField;
    FollowUpSOQPanjang: TFloatField;
    FollowUpSOQBerat: TFloatField;
    FollowUpSOQArmada: TStringField;
    FollowUpSOQKontrak: TStringField;
    FollowUpSOQnamapt: TStringField;
    cxListBox3: TcxListBox;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1TableView1: TcxGridTableView;
    cxGrid1TableView1Column1: TcxGridColumn;
    cxGrid1TableView1Column2: TcxGridColumn;
    PelangganQ: TSDQuery;
    PelangganQKode: TStringField;
    PelangganQNamaPT: TStringField;
    PelangganQAlamat: TStringField;
    PelangganQKota: TStringField;
    PelangganQNoTelp: TStringField;
    PelangganQCreateDate: TDateTimeField;
    PelangganQCreateBy: TStringField;
    PelangganQOperator: TStringField;
    PelangganQTglEntry: TDateTimeField;
    PelangganQNamaPIC1: TStringField;
    PelangganQTelpPIC1: TStringField;
    PelangganQJabatanPIC1: TStringField;
    PelangganQNamaPIC2: TStringField;
    PelangganQTelpPIC2: TStringField;
    PelangganQJabatanPIC2: TStringField;
    PelangganQNamaPIC3: TStringField;
    PelangganQTelpPIC3: TStringField;
    PelangganQJabatanPIC3: TStringField;
    CustQKodenota: TStringField;
    CustQTgl: TDateTimeField;
    CustQPelanggan: TStringField;
    CustQTglOrder: TDateTimeField;
    CustQJenisBarang: TStringField;
    CustQHarga: TCurrencyField;
    CustQRute: TStringField;
    CustQKeterangan: TMemoField;
    CustQCreateDate: TDateTimeField;
    CustQCreateBy: TStringField;
    CustQOperator: TStringField;
    CustQTglEntry: TDateTimeField;
    CustQStatus: TStringField;
    CustQTglFollowUp: TDateTimeField;
    CustQPanjang: TFloatField;
    CustQBerat: TFloatField;
    CustQArmada: TStringField;
    CustQKontrak: TStringField;
    CustQDariPT: TMemoField;
    CustQDariAlamat: TMemoField;
    CustQKePT: TMemoField;
    CustQKeAlamat: TMemoField;
    CustQTglCetak: TDateTimeField;
    SJQKodenota: TStringField;
    SJQTgl: TDateTimeField;
    SJQPelanggan: TStringField;
    SJQTglOrder: TDateTimeField;
    SJQJenisBarang: TStringField;
    SJQHarga: TCurrencyField;
    SJQRute: TStringField;
    SJQKeterangan: TMemoField;
    SJQCreateDate: TDateTimeField;
    SJQCreateBy: TStringField;
    SJQOperator: TStringField;
    SJQTglEntry: TDateTimeField;
    SJQStatus: TStringField;
    SJQTglFollowUp: TDateTimeField;
    SJQPanjang: TFloatField;
    SJQBerat: TFloatField;
    SJQArmada: TStringField;
    SJQKontrak: TStringField;
    SJQDariPT: TMemoField;
    SJQDariAlamat: TMemoField;
    SJQKePT: TMemoField;
    SJQKeAlamat: TMemoField;
    SJQTglCetak: TDateTimeField;
    SJQsurat_jalan: TStringField;
    SJQnamapt: TStringField;
    SJQmuat: TStringField;
    SJQbongkar: TStringField;
    BonBarangQ: TSDQuery;
    BonBarangQKode: TStringField;
    BonBarangQPeminta: TStringField;
    BonBarangQPenyetuju: TStringField;
    BonBarangQPenerima: TStringField;
    BonBarangQLaporanPerbaikan: TStringField;
    BonBarangQLaporanPerawatan: TStringField;
    BonBarangQStatus: TStringField;
    BonBarangQCreateDate: TDateTimeField;
    BonBarangQCreateBy: TStringField;
    BonBarangQOperator: TStringField;
    BonBarangQTglEntry: TDateTimeField;
    BonBarangQArmada: TStringField;
    BonBarangQTglCetak: TDateTimeField;
    LPBQ: TSDQuery;
    DaftarBeliQ: TSDQuery;
    PickKendaraanQ: TSDQuery;
    DaftarBeliQKode: TStringField;
    DaftarBeliQBarang: TStringField;
    DaftarBeliQBonBarang: TStringField;
    DaftarBeliQHargaMin: TCurrencyField;
    DaftarBeliQHargaMax: TCurrencyField;
    DaftarBeliQHargaLast: TCurrencyField;
    DaftarBeliQLastSupplier: TStringField;
    DaftarBeliQJumlahBeli: TIntegerField;
    DaftarBeliQHargaSatuan: TCurrencyField;
    DaftarBeliQSupplier: TStringField;
    DaftarBeliQCashNCarry: TBooleanField;
    DaftarBeliQGrandTotal: TCurrencyField;
    DaftarBeliQStatus: TStringField;
    DaftarBeliQSupplier1: TStringField;
    DaftarBeliQSupplier2: TStringField;
    DaftarBeliQSupplier3: TStringField;
    DaftarBeliQHarga1: TCurrencyField;
    DaftarBeliQHarga2: TCurrencyField;
    DaftarBeliQHarga3: TCurrencyField;
    DaftarBeliQTermin1: TIntegerField;
    DaftarBeliQTermin2: TIntegerField;
    DaftarBeliQTermin3: TIntegerField;
    DaftarBeliQKeterangan1: TMemoField;
    DaftarBeliQKeterangan2: TMemoField;
    DaftarBeliQKeterangan3: TMemoField;
    DaftarBeliQTermin: TIntegerField;
    LPBQtglterima: TDateTimeField;
    LPBQjumlah: TIntegerField;
    LPBQbarang: TStringField;
    LPBQNamaBarang: TStringField;
    PickKendaraanQKodenota: TStringField;
    PickKendaraanQTgl: TDateTimeField;
    PickKendaraanQPelanggan: TStringField;
    PickKendaraanQTglOrder: TDateTimeField;
    PickKendaraanQJenisBarang: TStringField;
    PickKendaraanQHarga: TCurrencyField;
    PickKendaraanQRute: TStringField;
    PickKendaraanQKeterangan: TMemoField;
    PickKendaraanQCreateDate: TDateTimeField;
    PickKendaraanQCreateBy: TStringField;
    PickKendaraanQOperator: TStringField;
    PickKendaraanQTglEntry: TDateTimeField;
    PickKendaraanQStatus: TStringField;
    PickKendaraanQTglFollowUp: TDateTimeField;
    PickKendaraanQPanjang: TFloatField;
    PickKendaraanQBerat: TFloatField;
    PickKendaraanQArmada: TStringField;
    PickKendaraanQKontrak: TStringField;
    PickKendaraanQDariPT: TMemoField;
    PickKendaraanQDariAlamat: TMemoField;
    PickKendaraanQKePT: TMemoField;
    PickKendaraanQKeAlamat: TMemoField;
    PickKendaraanQTglCetak: TDateTimeField;
    PickKendaraanQsurat_jalan: TStringField;
    PickKendaraanQnamapt: TStringField;
    PickKendaraanQmuat: TStringField;
    PickKendaraanQbongkar: TStringField;
    PerbaikanQ: TSDQuery;
    PerbaikanQKode: TStringField;
    PerbaikanQArmada: TStringField;
    PerbaikanQEkor: TStringField;
    PerbaikanQPeminta: TStringField;
    PerbaikanQKeluhan: TMemoField;
    PerbaikanQCreateDate: TDateTimeField;
    PerbaikanQCreateBy: TStringField;
    PerbaikanQOperator: TStringField;
    PerbaikanQTglEntry: TDateTimeField;
    PerbaikanQTglCetak: TDateTimeField;
    PerbaikanQlaporanperbaikan: TStringField;
    MinStokQ: TSDQuery;
    ArmadaQ: TSDQuery;
    ArmadaQKode: TStringField;
    ArmadaQPlatNo: TStringField;
    ArmadaQPanjang: TFloatField;
    ArmadaQKapasitas: TFloatField;
    ArmadaQTahunPembuatan: TStringField;
    ArmadaQJumlahBan: TIntegerField;
    ArmadaQAktif: TBooleanField;
    ArmadaQKmSekarang: TIntegerField;
    ArmadaQKeterangan: TStringField;
    ArmadaQSopir: TStringField;
    ArmadaQEkor: TStringField;
    ArmadaQJenisKendaraan: TStringField;
    ArmadaQCreateDate: TDateTimeField;
    ArmadaQCreateBy: TStringField;
    ArmadaQOperator: TStringField;
    ArmadaQTglEntry: TDateTimeField;
    ArmadaQSTNKPajakExpired: TDateTimeField;
    ArmadaQSTNKPerpanjangExpired: TDateTimeField;
    ArmadaQKirMulai: TDateTimeField;
    ArmadaQKirSelesai: TDateTimeField;
    ArmadaQNoRangka: TStringField;
    ArmadaQNoMesin: TStringField;
    cxListBox2: TcxListBox;
    BarangQKode: TStringField;
    BarangQNama: TStringField;
    BarangQHargaBeli: TCurrencyField;
    BarangQHargaJual: TCurrencyField;
    BarangQJumlah: TIntegerField;
    BarangQSatuan: TStringField;
    MinStokQKode: TStringField;
    MinStokQNama: TStringField;
    MinStokQHargaBeli: TCurrencyField;
    MinStokQHargaJual: TCurrencyField;
    MinStokQJumlah: TIntegerField;
    MinStokQSatuan: TStringField;
    MinStokQMinimumStok: TIntegerField;
    procedure cek();
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ExitBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxListBox1DblClick(Sender: TObject);
    procedure cxGrid1TableView1CellDblClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxGrid1TableView1CellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure SaveBtnClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent);
  end;

var
  NotifikasiFm: TNotifikasiFm;
  jumnotif :integer;
  MasterOriSQL: string;
implementation

uses MenuUtama, DropDown, DM, notaSO, Math;

{$R *.dfm}

{ TMasterArmadaFm }

constructor TNotifikasiFm.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
end;

procedure TNotifikasiFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TNotifikasiFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TNotifikasiFm.ExitBtnClick(Sender: TObject);
begin
  Release;
end;

procedure TNotifikasiFm.cek();
var i:integer;
begin

  //MasterOriSQL:=MasterQ.SQL.Text;
  jumnotif:=0;
  MinStokQ.Open;
  minstokq.First;
  cxGrid1TableView1.DataController.RecordCount:=0;
  for i:=1 to MinStokQ.RecordCount do
  begin
    jumnotif:=jumnotif + 1;
    cxGrid1TableView1.DataController.RecordCount:=cxGrid1TableView1.DataController.RecordCount+1;
    cxGrid1TableView1.DataController.Values[jumnotif-1,0]:='Min Stok';
    cxGrid1TableView1.DataController.Values[jumnotif-1,1]:=MinStokQNama.AsString + ' stok = ' + MinStokQJumlah.AsString + ' (MinStok=' + MinStokQMinimumStok.AsString + ')';
    cxListBox1.Items.Add('Min Stok');
    cxlistbox2.Items.Add('Min Stok');
    cxlistbox3.Items.Add('Min Stok');
    MinStokQ.Next;

  end;
end;

procedure TNotifikasiFm.FormCreate(Sender: TObject);
var i : integer;
begin
  DMFm:=TDMFm.Create(self);
  cek();
end;




procedure TNotifikasiFm.cxListBox1DblClick(Sender: TObject);
begin
  if cxListBox2.Items.Strings[cxListBox1.itemindex] = 'SO' then
  begin
    NotaSOFm:= TNotaSOFm.create(self,cxListBox3.Items.Strings[cxListBox1.itemindex]);
  end
  else if cxListBox2.Items.Strings[cxListBox1.ItemIndex] = 'Kontrak' then
  begin
    //NotaSOFm:= TNotaSOFm.create(self,cxListBox3.Items.Strings[cxListBox1.itemindex]);
  end;
  //ShowMessage(cxListBox3.Items.Strings[cxListBox1.ItemIndex]);
end;

procedure TNotifikasiFm.cxGrid1TableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  if cxListBox2.Items.Strings[Sender.DataController.GetFocusedRowIndex] = 'SO' then
  begin
    NotaSOFm:= TNotaSOFm.create(self,cxListBox3.Items.Strings[Acellviewinfo.RecordViewInfo.Index]);
  end
  else if cxListBox2.Items.Strings[Sender.DataController.GetFocusedRowIndex] = 'Inactive' then
  begin
   // DaftarPesertaFm :=TDaftarPesertaFm.create(self,cxlistbox3.items.strings[acellviewinfo.recordviewinfo.index]);
  end
  else if cxListBox2.Items.Strings[Sender.DataController.GetFocusedRowIndex] = 'SJ' then
  begin
   // NotaSJFm := TNotaSJFm.create(self);
    //DaftarPesertaFm :=TDaftarPesertaFm.create(self,cxlistbox3.items.strings[acellviewinfo.recordviewinfo.index]);
  end
  else if cxListBox2.Items.Strings[Sender.DataController.GetFocusedRowIndex] = 'Bon Barang' then
  begin
    //BonBarangFm := TBonBarangFm.create(self);
    //DaftarPesertaFm :=TDaftarPesertaFm.create(self,cxlistbox3.items.strings[acellviewinfo.recordviewinfo.index]);
  end
  else if cxListBox2.Items.Strings[Sender.DataController.GetFocusedRowIndex] = 'Daftar Beli' then
  begin
    //DaftarBeliFm:=TDaftarBeliFm.create(self);
    //DaftarPesertaFm :=TDaftarPesertaFm.create(self,cxlistbox3.items.strings[acellviewinfo.recordviewinfo.index]);
  end
  else if cxListBox2.Items.Strings[Sender.DataController.GetFocusedRowIndex] = 'Perbaikan' then
  begin
    //LaporanPermintaanPerbaikanFm := TLaporanPermintaanPerbaikanFm.create(self);
    //DaftarPesertaFm :=TDaftarPesertaFm.create(self,cxlistbox3.items.strings[acellviewinfo.recordviewinfo.index]);
  end
  else if cxListBox2.Items.Strings[Sender.DataController.GetFocusedRowIndex] = 'Pick Kendaraan' then
  begin
    //PickOrderFm := TPickOrderFm.create(self);
    //DaftarPesertaFm :=TDaftarPesertaFm.create(self,cxlistbox3.items.strings[acellviewinfo.recordviewinfo.index]);
  end;
end;

procedure TNotifikasiFm.cxGrid1TableView1CellClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  //ShowMessage(inttostr(Sender.DataController.GetFocusedRowIndex));
end;

procedure TNotifikasiFm.SaveBtnClick(Sender: TObject);
begin
  cek();
  if jumnotif=0 then
  begin
    //MenuUtamaFm.cxImage1.Visible:=false;
    //MenuUtamaFm.cxLabel1.Visible:=false;
  end;
end;

end.
