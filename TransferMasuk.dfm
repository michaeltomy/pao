object TransferMasukFm: TTransferMasukFm
  Left = 193
  Top = 87
  Width = 1060
  Height = 724
  Caption = 'Transfer Masuk'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 1044
    Height = 97
    Align = alTop
    TabOrder = 0
    object lbl1: TLabel
      Left = 8
      Top = 13
      Width = 25
      Height = 13
      Caption = 'Kode'
      Transparent = True
    end
    object KodeEdit: TcxButtonEdit
      Left = 40
      Top = 10
      Properties.Buttons = <
        item
          Caption = '+'
          Default = True
          Kind = bkText
        end>
      Properties.OnButtonClick = KodeEditPropertiesButtonClick
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      TabOrder = 0
      OnEnter = KodeEditEnter
      OnExit = KodeEditExit
      OnKeyDown = KodeEditKeyDown
      Width = 121
    end
    object cxCurrencyEdit1: TcxCurrencyEdit
      Left = 681
      Top = 53
      ParentFont = False
      Properties.ReadOnly = True
      Properties.UseDisplayFormatWhenEditing = True
      Properties.UseThousandSeparator = True
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -16
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsBold]
      Style.IsFontAssigned = True
      TabOrder = 1
      Width = 177
    end
    object cxLabel1: TcxLabel
      Left = 560
      Top = 56
      Caption = 'Grand Total :'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -16
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsBold]
      Style.IsFontAssigned = True
      Transparent = True
    end
    object cxLabel5: TcxLabel
      Left = 8
      Top = 60
      Caption = 'PIC'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -16
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsBold]
      Style.IsFontAssigned = True
      Transparent = True
    end
    object cxLabel6: TcxLabel
      Left = 280
      Top = 35
      Caption = 'Tanggal'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -16
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsBold]
      Style.IsFontAssigned = True
      Transparent = True
    end
    object cxLabel8: TcxLabel
      Left = 8
      Top = 35
      Caption = 'Toko Asal'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -16
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsBold]
      Style.IsFontAssigned = True
      Transparent = True
    end
    object cxLabel4: TcxLabel
      Left = 279
      Top = 61
      AutoSize = False
      Caption = 'Status'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -16
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsBold]
      Style.IsFontAssigned = True
      Transparent = True
      Height = 24
      Width = 122
    end
  end
  object pnl2: TPanel
    Left = 0
    Top = 616
    Width = 1044
    Height = 51
    Align = alBottom
    TabOrder = 1
    object ExitBtn: TcxButton
      Left = 268
      Top = 10
      Width = 75
      Height = 25
      Caption = 'EXIT [F8]'
      TabOrder = 1
      OnClick = ExitBtnClick
    end
    object DeleteBtn: TcxButton
      Left = 187
      Top = 10
      Width = 75
      Height = 25
      Caption = 'DELETE [F7]'
      TabOrder = 2
      OnClick = DeleteBtnClick
    end
    object SaveBtn: TcxButton
      Left = 104
      Top = 10
      Width = 75
      Height = 25
      Caption = 'SAVE [F6]'
      TabOrder = 0
      OnClick = SaveBtnClick
      OnKeyDown = SaveBtnKeyDown
    end
    object cxButton1: TcxButton
      Left = 16
      Top = 10
      Width = 83
      Height = 25
      Caption = 'ADD ITEM [F4]'
      TabOrder = 3
      OnClick = cxButton1Click
      OnKeyDown = SaveBtnKeyDown
    end
    object cxLabel2: TcxLabel
      Left = 384
      Top = 16
      Caption = 'cxLabel2'
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 667
    Width = 1044
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 97
    Width = 1044
    Height = 319
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    OnExit = cxGrid1Exit
    object cxGrid1DBTableView1: TcxGridDBTableView
      OnKeyDown = cxGrid1DBTableView1KeyDown
      Navigator.Buttons.First.Visible = False
      Navigator.Buttons.PriorPage.Visible = False
      Navigator.Buttons.Prior.Visible = False
      Navigator.Buttons.Next.Visible = False
      Navigator.Buttons.NextPage.Visible = False
      Navigator.Buttons.Last.Visible = False
      Navigator.Buttons.Insert.Visible = True
      Navigator.Buttons.Post.Visible = True
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.Visible = True
      OnCellDblClick = cxGrid1DBTableView1CellDblClick
      DataController.DataSource = DataSource1
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      NewItemRow.Visible = True
      OptionsBehavior.FocusCellOnTab = True
      OptionsBehavior.NavigatorHints = True
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnGrouping = False
      OptionsData.Appending = True
      OptionsData.DeletingConfirmation = False
      OptionsView.GroupByBox = False
      object cxGrid1DBTableView1BarcodeBarang: TcxGridDBColumn
        DataBinding.FieldName = 'BarcodeBarang'
        Width = 142
      end
      object cxGrid1DBTableView1KodeBarang: TcxGridDBColumn
        DataBinding.FieldName = 'KodeBarang'
        PropertiesClassName = 'TcxButtonEditProperties'
        Properties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.OnButtonClick = cxGrid1DBTableView1KodeBarangPropertiesButtonClick
        Width = 130
      end
      object cxGrid1DBTableView1NamaBarang: TcxGridDBColumn
        DataBinding.FieldName = 'NamaBarang'
        Width = 167
      end
      object cxGrid1DBTableView1TipeBarang: TcxGridDBColumn
        DataBinding.FieldName = 'TipeBarang'
        Width = 94
      end
      object cxGrid1DBTableView1Satuan: TcxGridDBColumn
        DataBinding.FieldName = 'Satuan'
        Width = 84
      end
      object cxGrid1DBTableView1Quantity: TcxGridDBColumn
        DataBinding.FieldName = 'Quantity'
        PropertiesClassName = 'TcxCalcEditProperties'
        Properties.OnValidate = cxGrid1DBTableView1QuantityPropertiesValidate
      end
      object cxGrid1DBTableView1HargaPokok: TcxGridDBColumn
        DataBinding.FieldName = 'HargaPokok'
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.OnValidate = cxGrid1DBTableView1HargaPokokPropertiesValidate
        Width = 87
      end
      object cxGrid1DBTableView1SubTotal: TcxGridDBColumn
        DataBinding.FieldName = 'SubTotal'
        PropertiesClassName = 'TcxCurrencyEditProperties'
      end
      object cxGrid1DBTableView1Keterangan: TcxGridDBColumn
        DataBinding.FieldName = 'Keterangan'
        Width = 152
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object cxGrid2: TcxGrid
    Left = 0
    Top = 416
    Width = 1044
    Height = 200
    Align = alBottom
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
    object cxGrid2DBTableView1: TcxGridDBTableView
      OnCellDblClick = cxGrid2DBTableView1CellDblClick
      DataController.DataSource = DataSource3
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.ImmediateEditor = False
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      object cxGrid2DBTableView1Kode: TcxGridDBColumn
        DataBinding.FieldName = 'Kode'
        Width = 109
      end
      object cxGrid2DBTableView1Tanggal: TcxGridDBColumn
        DataBinding.FieldName = 'Tanggal'
      end
      object cxGrid2DBTableView1TokoAsal: TcxGridDBColumn
        DataBinding.FieldName = 'TokoAsal'
        Width = 176
      end
      object cxGrid2DBTableView1PenanggungJawab: TcxGridDBColumn
        DataBinding.FieldName = 'PenanggungJawab'
        Width = 143
      end
      object cxGrid2DBTableView1grandtotal: TcxGridDBColumn
        Caption = 'Grand Total'
        DataBinding.FieldName = 'grandtotal'
        Width = 198
      end
    end
    object cxGrid2Level1: TcxGridLevel
      GridView = cxGrid2DBTableView1
    end
  end
  object cxDBComboBox1: TcxDBComboBox
    Left = 372
    Top = 63
    DataBinding.DataField = 'Status'
    DataBinding.DataSource = MasterDs
    Enabled = False
    Properties.Items.Strings = (
      'LUNAS'
      'BELUM LUNAS')
    TabOrder = 5
    Width = 121
  end
  object cxDBDateEdit1: TcxDBDateEdit
    Left = 373
    Top = 37
    DataBinding.DataField = 'Tanggal'
    DataBinding.DataSource = MasterDs
    Enabled = False
    TabOrder = 6
    Width = 121
  end
  object cxButtonEdit1: TcxButtonEdit
    Left = 116
    Top = 38
    Enabled = False
    Properties.Buttons = <
      item
        Default = True
        Kind = bkEllipsis
      end>
    Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
    TabOrder = 7
    Width = 121
  end
  object cxButtonEdit2: TcxButtonEdit
    Left = 116
    Top = 63
    Enabled = False
    Properties.Buttons = <
      item
        Default = True
        Kind = bkEllipsis
      end>
    Properties.OnButtonClick = cxButtonEdit2PropertiesButtonClick
    TabOrder = 8
    Width = 121
  end
  object MasterQ: TSDQuery
    AutoRefresh = True
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from transfermasuk')
    UpdateObject = MasterUS
    Left = 41
    Top = 137
    object MasterQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object MasterQTanggal: TDateTimeField
      FieldName = 'Tanggal'
    end
    object MasterQTokoAsal: TStringField
      FieldName = 'TokoAsal'
      Size = 10
    end
    object MasterQPenanggungJawab: TStringField
      FieldName = 'PenanggungJawab'
      Size = 10
    end
    object MasterQGrandTotal: TCurrencyField
      FieldName = 'GrandTotal'
    end
    object MasterQTerbayar: TCurrencyField
      FieldName = 'Terbayar'
    end
    object MasterQStatus: TStringField
      FieldName = 'Status'
      Size = 50
    end
    object MasterQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object MasterQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object MasterQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object MasterQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object MasterQDeleted: TBooleanField
      FieldName = 'Deleted'
    end
    object MasterQNamaTokoAsal: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaTokoAsal'
      LookupDataSet = TokoQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'NamaToko'
      KeyFields = 'TokoAsal'
      Size = 200
      Lookup = True
    end
    object MasterQNamaPenanggungJawab: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaPenanggungJawab'
      LookupDataSet = PegawaiQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'PenanggungJawab'
      Size = 200
      Lookup = True
    end
  end
  object MasterDs: TDataSource
    DataSet = MasterQ
    Left = 76
    Top = 150
  end
  object MasterUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, Tanggal, TokoAsal, PenanggungJawab, GrandTotal, Ter' +
        'bayar, Status, CreateDate, CreateBy, TglEntry, Operator, Deleted' +
        #13#10'from transfermasuk'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update transfermasuk'
      'set'
      '  Kode = :Kode,'
      '  Tanggal = :Tanggal,'
      '  TokoAsal = :TokoAsal,'
      '  PenanggungJawab = :PenanggungJawab,'
      '  GrandTotal = :GrandTotal,'
      '  Terbayar = :Terbayar,'
      '  Status = :Status,'
      '  CreateDate = :CreateDate,'
      '  CreateBy = :CreateBy,'
      '  TglEntry = :TglEntry,'
      '  Operator = :Operator,'
      '  Deleted = :Deleted'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into transfermasuk'
      
        '  (Kode, Tanggal, TokoAsal, PenanggungJawab, GrandTotal, Terbaya' +
        'r, Status, CreateDate, CreateBy, TglEntry, Operator, Deleted)'
      'values'
      
        '  (:Kode, :Tanggal, :TokoAsal, :PenanggungJawab, :GrandTotal, :T' +
        'erbayar, :Status, :CreateDate, :CreateBy, :TglEntry, :Operator, ' +
        ':Deleted)')
    DeleteSQL.Strings = (
      'delete from transfermasuk'
      'where'
      '  Kode = :OLD_Kode')
    Left = 116
    Top = 162
  end
  object KodeQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select top 1 kode from transfermasuk order by kode desc')
    Left = 9
    Top = 143
    object KodeQkode: TStringField
      FieldName = 'kode'
      Required = True
      Size = 10
    end
  end
  object DetailTransferQ: TSDQuery
    AutoRefresh = True
    DatabaseName = 'data'
    Options = []
    BeforePost = DetailQBeforePost
    AfterPost = DetailQAfterPost
    AfterDelete = DetailTransferQAfterDelete
    SQL.Strings = (
      'select * from detailtransfermasuk where kodetransfermasuk= :text'
      '')
    UpdateObject = DetailTransferUS
    Left = 97
    Top = 279
    ParamData = <
      item
        DataType = ftString
        Name = 'text'
        ParamType = ptInput
      end>
    object DetailTransferQKodeTransferMasuk: TStringField
      FieldName = 'KodeTransferMasuk'
      Required = True
      Size = 10
    end
    object DetailTransferQKodeBarang: TStringField
      FieldName = 'KodeBarang'
      Required = True
      Size = 10
    end
    object DetailTransferQQuantity: TFloatField
      FieldName = 'Quantity'
    end
    object DetailTransferQHargaPokok: TCurrencyField
      FieldName = 'HargaPokok'
      Required = True
    end
    object DetailTransferQSubTotal: TCurrencyField
      FieldName = 'SubTotal'
    end
    object DetailTransferQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object DetailTransferQNamaBarang: TStringField
      FieldKind = fkLookup
      FieldName = 'NamaBarang'
      LookupDataSet = BarangQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Nama'
      KeyFields = 'KodeBarang'
      Size = 200
      Lookup = True
    end
    object DetailTransferQBarcodeBarang: TStringField
      FieldKind = fkLookup
      FieldName = 'BarcodeBarang'
      LookupDataSet = BarangQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Barcode'
      KeyFields = 'kodebarang'
      Size = 200
      Lookup = True
    end
    object DetailTransferQKategoriBarang: TStringField
      FieldKind = fkLookup
      FieldName = 'KategoriBarang'
      LookupDataSet = BarangQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Kategori'
      KeyFields = 'KodeBarang'
      Size = 200
      Lookup = True
    end
    object DetailTransferQTipeBarang: TStringField
      FieldKind = fkLookup
      FieldName = 'TipeBarang'
      LookupDataSet = BarangQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Tipe'
      KeyFields = 'KodeBarang'
      Size = 200
      Lookup = True
    end
    object DetailTransferQSatuan: TStringField
      FieldKind = fkLookup
      FieldName = 'Satuan'
      LookupDataSet = BarangQ
      LookupKeyFields = 'Kode'
      LookupResultField = 'Satuan'
      KeyFields = 'KodeBarang'
      Size = 100
      Lookup = True
    end
  end
  object DataSource1: TDataSource
    DataSet = DetailTransferQ
    Left = 132
    Top = 278
  end
  object BarangQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from barang'
      '')
    UpdateObject = BarangUS
    Left = 225
    Top = 207
    object BarangQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object BarangQBarcode: TStringField
      FieldName = 'Barcode'
      Size = 100
    end
    object BarangQNama: TStringField
      FieldName = 'Nama'
      Size = 250
    end
    object BarangQKategori: TStringField
      FieldName = 'Kategori'
      Size = 10
    end
    object BarangQTipe: TStringField
      FieldName = 'Tipe'
      Size = 100
    end
    object BarangQKeterangan: TMemoField
      FieldName = 'Keterangan'
      BlobType = ftMemo
    end
    object BarangQJumlah: TFloatField
      FieldName = 'Jumlah'
    end
    object BarangQSatuan: TStringField
      FieldName = 'Satuan'
      Size = 50
    end
    object BarangQInsentifJual: TCurrencyField
      FieldName = 'InsentifJual'
    end
    object BarangQInsentifPeriksa: TCurrencyField
      FieldName = 'InsentifPeriksa'
    end
    object BarangQHargaJual: TCurrencyField
      FieldName = 'HargaJual'
    end
    object BarangQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object BarangQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object BarangQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object BarangQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object BarangQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object BarangQDeleted: TBooleanField
      FieldName = 'Deleted'
    end
    object BarangQHargaBeli: TCurrencyField
      FieldName = 'HargaBeli'
    end
  end
  object DetailTransferUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select KodeTransferMasuk, KodeBarang, Quantity, HargaPokok, SubT' +
        'otal, Keterangan'#13#10'from detailtransfermasuk'
      'where'
      '  KodeTransferMasuk = :OLD_KodeTransferMasuk and'
      '  KodeBarang = :OLD_KodeBarang')
    ModifySQL.Strings = (
      'update detailtransfermasuk'
      'set'
      '  KodeTransferMasuk = :KodeTransferMasuk,'
      '  KodeBarang = :KodeBarang,'
      '  Quantity = :Quantity,'
      '  HargaPokok = :HargaPokok,'
      '  SubTotal = :SubTotal,'
      '  Keterangan = :Keterangan'
      'where'
      '  KodeTransferMasuk = :OLD_KodeTransferMasuk and'
      '  KodeBarang = :OLD_KodeBarang')
    InsertSQL.Strings = (
      'insert into detailtransfermasuk'
      
        '  (KodeTransferMasuk, KodeBarang, Quantity, HargaPokok, SubTotal' +
        ', Keterangan)'
      'values'
      
        '  (:KodeTransferMasuk, :KodeBarang, :Quantity, :HargaPokok, :Sub' +
        'Total, :Keterangan)')
    DeleteSQL.Strings = (
      'delete from detailtransfermasuk'
      'where'
      '  KodeTransferMasuk = :OLD_KodeTransferMasuk and'
      '  KodeBarang = :OLD_KodeBarang')
    Left = 164
    Top = 274
  end
  object BarangUS: TSDUpdateSQL
    RefreshSQL.Strings = (
      
        'select Kode, Nama, HargaBeli, HargaJual, Jumlah, Satuan'#13#10'from ba' +
        'rang'
      'where'
      '  Kode = :OLD_Kode')
    ModifySQL.Strings = (
      'update barang'
      'set'
      '  Kode = :Kode,'
      '  Nama = :Nama,'
      '  HargaBeli = :HargaBeli,'
      '  HargaJual = :HargaJual,'
      '  Jumlah = :Jumlah,'
      '  Satuan = :Satuan'
      'where'
      '  Kode = :OLD_Kode')
    InsertSQL.Strings = (
      'insert into barang'
      '  (Kode, Nama, HargaBeli, HargaJual, Jumlah, Satuan)'
      'values'
      '  (:Kode, :Nama, :HargaBeli, :HargaJual, :Jumlah, :Satuan)')
    DeleteSQL.Strings = (
      'delete from barang'
      'where'
      '  Kode = :OLD_Kode')
    Left = 612
    Top = 202
  end
  object SDQuery1: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from barang'
      '')
    Left = 641
    Top = 199
    object StringField1: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object StringField2: TStringField
      FieldName = 'Nama'
      Required = True
      Size = 50
    end
    object CurrencyField1: TCurrencyField
      FieldName = 'HargaBeli'
    end
    object CurrencyField2: TCurrencyField
      FieldName = 'HargaJual'
    end
    object SDQuery1Jumlah: TFloatField
      FieldName = 'Jumlah'
    end
    object StringField3: TStringField
      FieldName = 'Satuan'
      Size = 50
    end
  end
  object TokoQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    SQL.Strings = (
      'select * from toko'
      '')
    Left = 49
    Top = 215
    object TokoQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object TokoQNamaToko: TStringField
      FieldName = 'NamaToko'
      Size = 250
    end
    object TokoQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 250
    end
    object TokoQKota: TStringField
      FieldName = 'Kota'
      Size = 100
    end
    object TokoQTelp: TStringField
      FieldName = 'Telp'
      Size = 50
    end
    object TokoQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object TokoQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object TokoQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object TokoQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object TokoQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object TokoQDeleted: TBooleanField
      FieldName = 'Deleted'
    end
  end
  object DataSource3: TDataSource
    DataSet = ViewQ
    Left = 52
    Top = 454
  end
  object ViewQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      
        'select tm.Kode, tm.Tanggal, t.namatoko as TokoAsal, p.nama as Pe' +
        'nanggungJawab, tm.grandtotal from TransferMasuk tm, pegawai p, t' +
        'oko t '
      'where t.kode=tm.tokoasal and tm.penanggungjawab=p.kode'
      ''
      '')
    Left = 17
    Top = 447
    object ViewQTanggal: TDateTimeField
      FieldName = 'Tanggal'
    end
    object ViewQTokoAsal: TStringField
      FieldName = 'TokoAsal'
      Size = 250
    end
    object ViewQPenanggungJawab: TStringField
      FieldName = 'PenanggungJawab'
      Size = 250
    end
    object ViewQgrandtotal: TCurrencyField
      FieldName = 'grandtotal'
    end
    object ViewQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
  end
  object PegawaiQ: TSDQuery
    DatabaseName = 'data'
    Options = []
    Active = True
    SQL.Strings = (
      'select * from pegawai')
    Left = 96
    Top = 216
    object PegawaiQKode: TStringField
      FieldName = 'Kode'
      Required = True
      Size = 10
    end
    object PegawaiQNama: TStringField
      FieldName = 'Nama'
      Size = 250
    end
    object PegawaiQAlamat: TStringField
      FieldName = 'Alamat'
      Size = 250
    end
    object PegawaiQTglLahir: TDateTimeField
      FieldName = 'TglLahir'
    end
    object PegawaiQAktif: TBooleanField
      FieldName = 'Aktif'
    end
    object PegawaiQCreateDate: TDateTimeField
      FieldName = 'CreateDate'
    end
    object PegawaiQCreateBy: TStringField
      FieldName = 'CreateBy'
      Size = 10
    end
    object PegawaiQTglEntry: TDateTimeField
      FieldName = 'TglEntry'
    end
    object PegawaiQOperator: TStringField
      FieldName = 'Operator'
      Size = 10
    end
    object PegawaiQDeleted: TBooleanField
      FieldName = 'Deleted'
    end
  end
end
