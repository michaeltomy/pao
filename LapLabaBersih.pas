unit LapLabaBersih;

{$I cxVer.inc}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DemoBasicMain, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd,
  dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd,
  dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPSCore, ActnList, ImgList, Menus, ComCtrls,
  ToolWin, StdCtrls, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridCustomView, cxGrid, cxClasses,
  cxEditRepositoryItems, dxPScxCommon, dxPScxGridLnk, XPMan, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary,
  dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin,
  dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver, dxSkinSpringTime,
  dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, dxSkinscxPCPainter, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxGridLayoutViewLnk, dxSkinsdxBarPainter, DB,
  cxDBData, cxGridBandedTableView, cxGridDBBandedTableView, SDEngine,
  ExtCtrls, cxCalendar;

type
  TLapLabaBersihFm = class(TDemoBasicMainForm)
    edrepMain: TcxEditRepository;
    edrepCenterText: TcxEditRepositoryTextItem;
    edrepRightText: TcxEditRepositoryTextItem;
    StyleRepository: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    cxStyle14: TcxStyle;
    tvssDevExpress: TcxGridTableViewStyleSheet;
    dxComponentPrinterLink1: TdxGridReportLink;
    SDQuery1: TSDQuery;
    DataSource1: TDataSource;
    SDQuery1Kodenota: TStringField;
    SDQuery1Tgl: TDateTimeField;
    SDQuery1Pelanggan: TStringField;
    SDQuery1Berangkat: TDateTimeField;
    SDQuery1Tiba: TDateTimeField;
    SDQuery1Harga: TCurrencyField;
    SDQuery1PPN: TCurrencyField;
    SDQuery1PembayaranAwal: TCurrencyField;
    SDQuery1TglPembayaranAwal: TDateTimeField;
    SDQuery1CaraPembayaranAwal: TStringField;
    SDQuery1KeteranganCaraPembayaranAwal: TStringField;
    SDQuery1NoKwitansiPembayaranAwal: TStringField;
    SDQuery1NominalKwitansiPembayaranAwal: TCurrencyField;
    SDQuery1PenerimaPembayaranAwal: TStringField;
    SDQuery1Pelunasan: TCurrencyField;
    SDQuery1TglPelunasan: TDateTimeField;
    SDQuery1CaraPembayaranPelunasan: TStringField;
    SDQuery1KetCaraPembayaranPelunasan: TStringField;
    SDQuery1NoKwitansiPelunasan: TStringField;
    SDQuery1NominalKwitansiPelunasan: TCurrencyField;
    SDQuery1PenerimaPelunasan: TStringField;
    SDQuery1Extend: TBooleanField;
    SDQuery1TglKembaliExtend: TDateTimeField;
    SDQuery1BiayaExtend: TCurrencyField;
    SDQuery1PPNExtend: TCurrencyField;
    SDQuery1KapasitasSeat: TIntegerField;
    SDQuery1AC: TBooleanField;
    SDQuery1Toilet: TBooleanField;
    SDQuery1AirSuspension: TBooleanField;
    SDQuery1Rute: TStringField;
    SDQuery1TglFollowUp: TDateTimeField;
    SDQuery1Armada: TStringField;
    SDQuery1Kontrak: TStringField;
    SDQuery1PICJemput: TMemoField;
    SDQuery1JamJemput: TDateTimeField;
    SDQuery1NoTelpPICJemput: TStringField;
    SDQuery1AlamatJemput: TMemoField;
    SDQuery1Status: TStringField;
    SDQuery1StatusPembayaran: TStringField;
    SDQuery1ReminderPending: TDateTimeField;
    SDQuery1PenerimaPending: TStringField;
    SDQuery1Keterangan: TMemoField;
    SDQuery1CreateDate: TDateTimeField;
    SDQuery1CreateBy: TStringField;
    SDQuery1Operator: TStringField;
    SDQuery1TglEntry: TDateTimeField;
    SDQuery1TglCetak: TDateTimeField;
    Panel1: TPanel;
    Panel2: TPanel;
    cxGrid: TcxGrid;
    tvPlanets: TcxGridTableView;
    tvPlanetsNAME: TcxGridColumn;
    tvPlanetsNO: TcxGridColumn;
    tvPlanetsORBITS: TcxGridColumn;
    tvPlanetsDISTANCE: TcxGridColumn;
    tvPlanetsPERIOD: TcxGridColumn;
    tvPlanetsDISCOVERER: TcxGridColumn;
    tvPlanetsDATE: TcxGridColumn;
    tvPlanetsRADIUS: TcxGridColumn;
    cxGridDBBandedTableView1: TcxGridDBBandedTableView;
    cxGridLevel1: TcxGridLevel;
    DateTimePicker1: TDateTimePicker;
    DateTimePicker2: TDateTimePicker;
    Button1: TButton;
    MasterQ: TSDQuery;
    MasterDs: TDataSource;
    MasterUs: TSDUpdateSQL;
    TokoQ: TSDQuery;
    PegawaiQ: TSDQuery;
    RealisasiAnjemUs: TSDUpdateSQL;
    PegawaiQKode: TStringField;
    PegawaiQNama: TStringField;
    PegawaiQAlamat: TStringField;
    PegawaiQTglLahir: TDateTimeField;
    PegawaiQAktif: TBooleanField;
    PegawaiQCreateDate: TDateTimeField;
    PegawaiQCreateBy: TStringField;
    PegawaiQTglEntry: TDateTimeField;
    PegawaiQOperator: TStringField;
    PegawaiQDeleted: TBooleanField;
    TokoQKode: TStringField;
    TokoQNamaToko: TStringField;
    TokoQAlamat: TStringField;
    TokoQKota: TStringField;
    TokoQTelp: TStringField;
    TokoQAktif: TBooleanField;
    TokoQCreateDate: TDateTimeField;
    TokoQCreateBy: TStringField;
    TokoQTglEntry: TDateTimeField;
    TokoQOperator: TStringField;
    TokoQDeleted: TBooleanField;
    BarangQ: TSDQuery;
    BarangQKode: TStringField;
    BarangQBarcode: TStringField;
    BarangQNama: TStringField;
    BarangQKategori: TStringField;
    BarangQTipe: TStringField;
    BarangQKeterangan: TMemoField;
    BarangQJumlah: TFloatField;
    BarangQSatuan: TStringField;
    BarangQInsentifJual: TCurrencyField;
    BarangQInsentifPeriksa: TCurrencyField;
    BarangQHargaJual: TCurrencyField;
    BarangQHargaBeli: TCurrencyField;
    BarangQAktif: TBooleanField;
    BarangQCreateDate: TDateTimeField;
    BarangQCreateBy: TStringField;
    BarangQTglEntry: TDateTimeField;
    BarangQOperator: TStringField;
    BarangQDeleted: TBooleanField;
    MasterQTanggal: TDateTimeField;
    MasterQReferensi: TStringField;
    MasterQLABARUGI: TCurrencyField;
    MasterQKeterangan: TStringField;
    cxGridDBBandedTableView1Tanggal: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1Referensi: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1LABARUGI: TcxGridDBBandedColumn;
    cxGridDBBandedTableView1Keterangan: TcxGridDBBandedColumn;
    procedure actFullExpandExecute(Sender: TObject);
    procedure actFullCollapseExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure actShowDemoDescriptionExecute(Sender: TObject);
  private
    procedure CustomizeColumns;
  end;

var
  LapLabaBersihFm: TLapLabaBersihFm;

implementation

{$R *.dfm}
uses DateUtils;

procedure TLapLabaBersihFm.actFullExpandExecute(Sender: TObject);
begin
  inherited;
  cxGridDBBandedTableView1.DataController.Groups.FullExpand;
  //tvPlanets.DataController.Groups.FullExpand;
end;

procedure TLapLabaBersihFm.actFullCollapseExecute(Sender: TObject);
begin
  inherited;
  cxGridDBBandedTableView1.DataController.Groups.FullCollapse;
  //tvPlanets.DataController.Groups.FullCollapse;
end;

procedure TLapLabaBersihFm.CustomizeColumns;
const
  cDistance = 3;
  cPeriod = 4;
  cRadius = 7;
var
  I: Integer;
begin
  DecimalSeparator := '.';
  with tvPlanets do
  for I := 0 to ColumnCount - 1 do
    if I in [cDistance, cRadius] then
      Columns[I].DataBinding.ValueTypeClass := TcxIntegerValueType
    else
      if I in [cPeriod] then
      Columns[I].DataBinding.ValueTypeClass := TcxFloatValueType
      else
       Columns[I].DataBinding.ValueTypeClass := TcxStringValueType;
end;

procedure TLapLabaBersihFm.FormCreate(Sender: TObject);
begin
  inherited;
  CustomizeColumns;
end;

procedure TLapLabaBersihFm.FormShow(Sender: TObject);
begin
  tvPlanets.DataController.Groups.FullExpand;
  DateTimePicker1.DateTime:=Today;
DateTimePicker2.DateTime:=Today;
end;

procedure TLapLabaBersihFm.Button1Click(Sender: TObject);
var total:currency;
begin
  inherited;
  MasterQ.Close;
  MasterQ.ParamByName('text1').AsDate:= DateTimePicker1.Date;
  MasterQ.ParamByName('text2').AsDate:=DateTimePicker2.Date;
  MasterQ.Open;
  MasterQ.Edit;
  MasterQ.First;
  while MasterQ.Eof=false do
  begin
      MasterQ.Edit;
      //MasterQRupiah.AsCurrency:=MasterQSPBUAYaniLiter.AsFloat*4500;
      MasterQ.Next;
  end;
end;

procedure TLapLabaBersihFm.actShowDemoDescriptionExecute(Sender: TObject);
begin
  //inherited;

end;

end.
