unit JadwalArmada;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, Grids;

type
  TJadwalArmadaFm = class(TForm)
    pnl1: TPanel;
    StringGrid1: TStringGrid;
    pnl2: TPanel;
    pnl3: TPanel;
    MasterVGrid: TcxDBVerticalGrid;
    pnl4: TPanel;
    DetailJadwalQ: TSDQuery;
    DetailJadwalQRute: TStringField;
    DetailJadwalQNoSJ: TStringField;
    DetailJadwalQPelanggan: TStringField;
    DetailJadwalQNoSO: TStringField;
    DetailJadwalDs: TDataSource;
    MasterVGridRute: TcxDBEditorRow;
    MasterVGridNoSJ: TcxDBEditorRow;
    MasterVGridPelanggan: TcxDBEditorRow;
    MasterVGridNoSO: TcxDBEditorRow;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent);
  end;

var
  JadwalArmadaFm: TJadwalArmadaFm;

  MasterOriSQL: string;
implementation

uses MenuUtama, DropDown, DM;

{$R *.dfm}

{ TMasterArmadaFm }

constructor TJadwalArmadaFm.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
end;

procedure TJadwalArmadaFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TJadwalArmadaFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

end.
