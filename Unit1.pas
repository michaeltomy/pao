unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, SDEngine, frxClass, frxDBSet;

type
  TForm1 = class(TForm)
    frxReport1: TfrxReport;
    frxDBDataset1: TfrxDBDataset;
    SDQuery1: TSDQuery;
    DataSource1: TDataSource;
    SDQuery1Kode: TStringField;
    SDQuery1PlatNo: TStringField;
    SDQuery1Panjang: TFloatField;
    SDQuery1Kapasitas: TFloatField;
    SDQuery1Aktif: TBooleanField;
    SDQuery1Keterangan: TStringField;
    SDQuery1CreateDate: TDateTimeField;
    SDQuery1CreateBy: TStringField;
    SDQuery1Operator: TStringField;
    SDQuery1TglEntry: TDateTimeField;
    SDQuery1Sopir: TStringField;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
begin
frxReport1.PrepareReport;
frxReport1.ShowPreparedReport;

end;

end.
