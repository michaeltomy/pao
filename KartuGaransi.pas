unit KartuGaransi;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, dxLayoutControl, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxLabel,
  cxCalendar, cxCurrencyEdit, UCrpeClasses, UCrpe32;

type
  TKartuGaransiFm = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    MasterQ: TSDQuery;
    MasterDs: TDataSource;
    MasterVGrid: TcxDBVerticalGrid;
    StatusBar: TStatusBar;
    MasterUS: TSDUpdateSQL;
    KodeQ: TSDQuery;
    KodeQkode: TStringField;
    DeleteBtn: TcxButton;
    SaveBtn: TcxButton;
    KodeEdit: TcxButtonEdit;
    lbl1: TLabel;
    SearchBtn: TcxButton;
    cxLabel1: TcxLabel;
    Crpe1: TCrpe;
    MasterVGridTanggal: TcxDBEditorRow;
    MasterVGridResep: TcxDBEditorRow;
    MasterVGridLensa: TcxDBEditorRow;
    MasterVGridFrame: TcxDBEditorRow;
    MasterVGridPelanggan: TcxDBEditorRow;
    MasterVGridPD: TcxDBEditorRow;
    MasterVGridSPHki: TcxDBEditorRow;
    MasterVGridSPHka: TcxDBEditorRow;
    MasterVGridCYLki: TcxDBEditorRow;
    MasterVGridCYLka: TcxDBEditorRow;
    MasterVGridAXki: TcxDBEditorRow;
    MasterVGridAXka: TcxDBEditorRow;
    MasterVGridADDki: TcxDBEditorRow;
    MasterVGridADDka: TcxDBEditorRow;
    MasterVGridJauh: TcxDBEditorRow;
    MasterVGridDekat: TcxDBEditorRow;
    PelangganQ: TSDQuery;
    PelangganQKode: TStringField;
    PelangganQNama: TStringField;
    PelangganQAlamat: TStringField;
    PelangganQKota: TStringField;
    PelangganQTelp: TStringField;
    PelangganQHP: TStringField;
    PelangganQTglLahir: TDateTimeField;
    PelangganQFoto: TBlobField;
    PelangganQAktif: TBooleanField;
    PelangganQCreateDate: TDateTimeField;
    PelangganQCreateBy: TStringField;
    PelangganQTglEntry: TDateTimeField;
    PelangganQOperator: TStringField;
    PelangganQDeleted: TBooleanField;
    MasterQKode: TStringField;
    MasterQTanggal: TDateTimeField;
    MasterQResep: TStringField;
    MasterQID: TStringField;
    MasterQLensa: TStringField;
    MasterQFrame: TStringField;
    MasterQPelanggan: TStringField;
    MasterQPD: TStringField;
    MasterQSPHki: TStringField;
    MasterQSPHka: TStringField;
    MasterQCYLki: TStringField;
    MasterQCYLka: TStringField;
    MasterQAXki: TStringField;
    MasterQAXka: TStringField;
    MasterQADDki: TStringField;
    MasterQADDka: TStringField;
    MasterQJauh: TBooleanField;
    MasterQDekat: TBooleanField;
    MasterQCreateBy: TStringField;
    MasterQCreateDate: TDateTimeField;
    MasterQOperator: TStringField;
    MasterQTglEntry: TDateTimeField;
    MasterVGridNamaPelanggan: TcxDBEditorRow;
    MasterQNamaPelanggan: TStringField;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ExitBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure KodeEditEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure KodeEditExit(Sender: TObject);
    procedure KodeEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SaveBtnClick(Sender: TObject);
    procedure DeleteBtnClick(Sender: TObject);
    procedure SearchBtnClick(Sender: TObject);
    procedure MasterVGridEnter(Sender: TObject);
    procedure MasterQAfterInsert(DataSet: TDataSet);
    procedure MasterQBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(aOwner: TComponent;kd:string);
  end;

var
  KartuGaransiFm: TKartuGaransiFm;
  paramkode:string;
  MasterOriSQL: string;
implementation

uses MenuUtama, DropDown, DM, PegawaiDropDown, KwitansiDropDown, StrUtils;

{$R *.dfm}

{ TMasterArmadaFm }

constructor TKartuGaransiFm.Create(aOwner: TComponent;kd:string);
begin
  inherited Create(aOwner);
  paramkode:=kd;
end;

procedure TKartuGaransiFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TKartuGaransiFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TKartuGaransiFm.ExitBtnClick(Sender: TObject);
begin
  Release;
end;

procedure TKartuGaransiFm.FormCreate(Sender: TObject);
begin
//  DMFm:=TDMFm.Create(self);
  MasterOriSQL:=MasterQ.SQL.Text;
  KodeEdit.Properties.MaxLength:=MasterQKode.Size;
end;

procedure TKartuGaransiFm.cxButtonEdit1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  KodeEdit.Text:='INSERT BARU';
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style + [fsItalic];
  KodeEditExit(sender);
  MasterVGrid.SetFocus;
  savebtn.Enabled:=true;
end;

procedure TKartuGaransiFm.KodeEditEnter(Sender: TObject);
begin
  KodeEdit.Style.Font.Style:=KodeEdit.Style.Font.Style - [fsItalic];
  MasterVGrid.Enabled:=false;
  KodeEdit.Clear;
  StatusBar.Panels[0].Text:= 'Mode : ???';
  SaveBtn.Enabled:=false;
  DeleteBtn.Enabled:=false;
  MasterQ.Close;
end;

procedure TKartuGaransiFm.FormShow(Sender: TObject);
begin
  KodeEdit.SetFocus;
  if paramkode<>'' then
  begin
    KodeEdit.Text:=paramkode;
    KodeEditExit(sender);
  end;
  cxButtonEdit1PropertiesButtonClick(self,0);
  {kodeedit.Properties.Buttons[0].Enabled:=MenuUtamaFm.UserQInsertMasterPegawai.AsBoolean;
  SaveBtn.Enabled:=menuutamafm.UserQUpdateMasterPegawai.AsBoolean;
  DeleteBtn.Enabled:=menuutamafm.UserQDeleteMasterPegawai.AsBoolean;  }
end;

procedure TKartuGaransiFm.KodeEditExit(Sender: TObject);
begin
  if KodeEdit.Text<>'' then
  begin
    MasterQ.SQL.Clear;
    MasterQ.SQL.Add('select * from ('+ MasterOriSQL +')x where x.kode like '+ QuotedStr('%'+KodeEdit.Text+'%'));
    MasterQ.Open;
    if MasterQ.IsEmpty then
    begin
      StatusBar.Panels[0].Text:= 'Mode : Entry';
      MasterQ.Insert;
      MasterQ.Edit;
      MasterQJauh.AsBoolean:=false;
      MasterqDekat.AsBoolean:=false;
      MasterQTanggal.AsDateTime:=Now;
      MasterQPelanggan.AsString:=paramkode;
    end
    else
    begin
      StatusBar.Panels[0].Text:= 'Mode : Edit';
      DeleteBtn.Enabled:=true;
      MasterQ.Edit;
      //DeleteBtn.Enabled:=menuutamafm.UserQDeleteMasterPegawai.AsBoolean;
    end;
    SaveBtn.Enabled:=True;
    //SaveBtn.Enabled:=menuutamafm.UserQUpdateMasterPegawai.AsBoolean;
    MasterVGrid.Enabled:=True;
  end;
end;

procedure TKartuGaransiFm.KodeEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_F5) then
  begin
    MasterQ.SQL.Text:=MasterOriSQL;
    DropDownFm:=TDropdownfm.Create(Self,MasterQ);
    if DropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=DropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    DropDownFm.Release;
  end;

end;

procedure TKartuGaransiFm.SaveBtnClick(Sender: TObject);
var kodeprint :string;
begin
  if StatusBar.Panels[0].Text= 'Mode : Entry' then
  begin
    MasterQ.Edit;
    KodeQ.Open;
    if KodeEdit.Text = 'INSERT BARU' then
    begin
      if KodeQ.IsEmpty then
      begin
        MasterQKode.AsString:=DMFm.Fill('1',10,'0',True);
      end
      else
      begin
        MasterQKode.AsString:=DMFm.Fill(InttoStr(StrtoInt(KodeQkode.AsString)+1),10,'0',True);
      end;
    end
    else
    begin
      MasterQKode.AsString:=KodeEdit.Text;
    end;

    KodeQ.Close;
    KodeEdit.Text:=MasterQKode.AsString;
  end;
  kodeprint:='';
  if StatusBar.Panels[0].Text= 'Mode : Entry' then kodeprint:=masterqkode.asstring;
  MasterQ.Post;
  MenuUtamaFm.Database1.StartTransaction;
  try
    MasterQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    ShowMessage('Kartu Garansi dengan Kode ' + KodeEdit.Text + ' telah disimpan');
    KodeEdit.SetFocus;
    if kodeprint<>'' then
    begin
      if MessageDlg('Cetak Kartu Garansi '+kodeprint+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then
      begin
        Crpe1.Refresh;
        Crpe1.ReportName:=ExtractFilePath(Application.ExeName)+'kartugaransi.rpt';
        Crpe1.ParamFields[0].CurrentValue:=kodeprint;
        Crpe1.Connect.UserID:=menuutamafm.DataBase1.Params.Values['USER NAME'];
        Crpe1.Connect.Password:=menuutamafm.DataBase1.Params.Values['PASSWORD'];
        Crpe1.Connect.ServerName:=LeftStr(MenuutamaFm.DataBase1.RemoteDatabase,pos(':',MenuutamaFm.database1.remotedatabase)-1);
        Crpe1.Execute;
      end;
    end;
  except
    MenuUtamaFm.Database1.Rollback;
    MasterQ.RollbackUpdates;
    ShowMessage('Penyimpanan Gagal');
  end;
end;

procedure TKartuGaransiFm.DeleteBtnClick(Sender: TObject);
begin
  if MessageDlg('Hapus Kartu Garansi '+KodeEdit.Text+' - '+MasterQKode.AsString+', Anda yakin ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
     MenuUtamaFm.Database1.StartTransaction;
     try
       MasterQ.Delete;
       MasterQ.ApplyUpdates;
       MenuUtamaFm.Database1.Commit;
       MessageDlg('Kartu Garansi telah dihapus.',mtInformation,[mbOK],0);
     except
       MenuUtamaFm.Database1.Rollback;
       MasterQ.RollbackUpdates;
       MessageDlg('Kartu Garansi pernah/sedang bertransaksi tidak boleh dihapus.',mtConfirmation,[mbOK],0);
     end;
     KodeEdit.SetFocus;
  end;
end;

procedure TKartuGaransiFm.SearchBtnClick(Sender: TObject);
begin
    MasterQ.SQL.Text:=MasterOriSQL;
    KwitansiDropDownFm:=TKwitansiDropDownfm.Create(Self);
    if KwitansiDropDownFm.ShowModal=MrOK then
    begin
      KodeEdit.Text:=KwitansiDropDownFm.kode;
      KodeEditExit(Sender);
      MasterVGrid.SetFocus;
    end;
    KwitansiDropDownFm.Release;
end;

procedure TKartuGaransiFm.MasterVGridEnter(Sender: TObject);
begin
  MasterVGrid.FocusRow(MasterVGridTanggal);
end;

procedure TKartuGaransiFm.MasterQAfterInsert(DataSet: TDataSet);
begin
  MasterQCreateBy.AsString:=User;
end;

procedure TKartuGaransiFm.MasterQBeforePost(DataSet: TDataSet);
begin
  MasterQOperator.AsString:=User;
end;

end.
