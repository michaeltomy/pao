unit KartuGaransiDropDown;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, dxSkinsCore, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, SDEngine, dxSkinBlack,
  dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinValentine, dxSkinXmas2008Blue;

type
  TKartuGaransiDropDownFm = class(TForm)
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    MasterQ: TSDQuery;
    LPBDs: TDataSource;
    PelangganQ: TSDQuery;
    MasterQKode: TStringField;
    MasterQTanggal: TDateTimeField;
    MasterQResep: TStringField;
    MasterQID: TStringField;
    MasterQLensa: TStringField;
    MasterQFrame: TStringField;
    MasterQPelanggan: TStringField;
    MasterQPD: TStringField;
    MasterQSPHki: TStringField;
    MasterQSPHka: TStringField;
    MasterQCYLki: TStringField;
    MasterQCYLka: TStringField;
    MasterQAXki: TStringField;
    MasterQAXka: TStringField;
    MasterQADDki: TStringField;
    MasterQADDka: TStringField;
    MasterQJauh: TBooleanField;
    MasterQDekat: TBooleanField;
    MasterQCreateBy: TStringField;
    MasterQCreateDate: TDateTimeField;
    MasterQOperator: TStringField;
    MasterQTglEntry: TDateTimeField;
    cxGrid1DBTableView1Tanggal: TcxGridDBColumn;
    cxGrid1DBTableView1Resep: TcxGridDBColumn;
    cxGrid1DBTableView1Lensa: TcxGridDBColumn;
    cxGrid1DBTableView1Frame: TcxGridDBColumn;
    cxGrid1DBTableView1PD: TcxGridDBColumn;
    cxGrid1DBTableView1SPHki: TcxGridDBColumn;
    cxGrid1DBTableView1SPHka: TcxGridDBColumn;
    cxGrid1DBTableView1CYLki: TcxGridDBColumn;
    cxGrid1DBTableView1CYLka: TcxGridDBColumn;
    cxGrid1DBTableView1AXki: TcxGridDBColumn;
    cxGrid1DBTableView1AXka: TcxGridDBColumn;
    cxGrid1DBTableView1ADDki: TcxGridDBColumn;
    cxGrid1DBTableView1ADDka: TcxGridDBColumn;
    cxGrid1DBTableView1Jauh: TcxGridDBColumn;
    cxGrid1DBTableView1Dekat: TcxGridDBColumn;
    MasterQNamaPelanggan: TStringField;
    cxGrid1DBTableView1NamaPelanggan: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    kode:string;
  end;

var
  KartuGaransiDropDownFm: TKartuGaransiDropDownFm;

implementation

{$R *.dfm}

procedure TKartuGaransiDropDownFm.FormCreate(Sender: TObject);
begin
  MasterQ.Open;
end;

procedure TKartuGaransiDropDownFm.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  kode:=MasterQKode.AsString;
  ModalResult:=mrOK;
end;

end.
