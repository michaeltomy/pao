unit Setting;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, dxSkinsCore, dxSkinsDefaultPainters, Menus, DB,
  SDEngine, StdCtrls, cxButtons, cxLabel, cxTextEdit, cxCurrencyEdit,
  cxDBEdit;

type
  TSettingFm = class(TForm)
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    cxLabel3: TcxLabel;
    cxLabel4: TcxLabel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    SettingQ: TSDQuery;
    SettingUS: TSDUpdateSQL;
    SettingQJumlahMeja: TIntegerField;
    SettingQKode: TStringField;
    SettingQPajak: TFloatField;
    SettingQServis: TFloatField;
    SettingQBCA: TFloatField;
    SettingQVisaMaster: TFloatField;
    cxDBCEServis: TcxDBCurrencyEdit;
    cxDBCEPajak: TcxDBCurrencyEdit;
    cxDBCEBCA: TcxDBCurrencyEdit;
    cxDBCEVisaMaster: TcxDBCurrencyEdit;
    SettingDs: TDataSource;
    procedure FormShow(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  SettingFm: TSettingFm;

implementation

uses MenuUtama;

{$R *.dfm}

procedure TSettingFm.FormShow(Sender: TObject);
begin
  SettingQ.Open;
//  cxCEServis.Value:=SettingQServis.AsFloat;
//  cxCEPajak.Value:=SettingQPajak.AsFloat;
//  cxCEBCA.Value:=SettingQBCA.AsFloat;
//  cxCEVisa.Value:=SettingQVisaMaster.AsFloat;
end;

procedure TSettingFm.cxButton1Click(Sender: TObject);
begin
  SettingQ.Edit;
  SettingQKode.AsString:='0000000001';
  SettingQ.Post;
  MenuUtamaFm.Database1.StartTransaction;
  try
    SettingQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    SettingQ.CommitUpdates;
    ShowMessage('Setting baru telah disimpan');
  except
    on E : Exception do begin
      ShowMessage(E.ClassName + 'error raised, with message : ' + E.Message);
      MenuUtamaFm.Database1.Rollback;
      SettingQ.RollbackUpdates;
      ShowMessage('Penyimpanan Gagal');
    end;
  end;
end;

end.
