unit NotaSJ;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxGraphics, cxEdit, cxControls, cxInplaceContainer,
  cxVGrid, ExtCtrls, cxContainer, cxTextEdit, cxMaskEdit,
  cxButtonEdit, DBTables, DB, SDEngine, cxDBVGrid, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxDBData, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxClasses, cxGridLevel, cxGrid, cxPC, cxCheckBox,
  cxLookAndFeels, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue;

type
  TNotaSJFm = class(TForm)
    pnl2: TPanel;
    MasterQ: TSDQuery;
    MasterDs: TDataSource;
    MasterUS: TSDUpdateSQL;
    KodeQ: TSDQuery;
    KodeQkode: TStringField;
    ExitBtn: TcxButton;
    SaveBtn: TcxButton;
    pnl1: TPanel;
    pnl3: TPanel;
    pnl6: TPanel;
    cxDBVerticalGrid1: TcxDBVerticalGrid;
    lbl1: TLabel;
    pnl4: TPanel;
    cxDBVerticalGrid2: TcxDBVerticalGrid;
    cxDBVerticalGrid2Laporan: TcxDBEditorRow;
    pnl5: TPanel;
    pnl7: TPanel;
    pnl8: TPanel;
    pnl9: TPanel;
    pnl10: TPanel;
    pnl11: TPanel;
    cxDBVerticalGrid3: TcxDBVerticalGrid;
    cxDBVerticalGrid3NamaMuat: TcxDBEditorRow;
    cxDBVerticalGrid3TelpMuat: TcxDBEditorRow;
    cxDBVerticalGrid4: TcxDBVerticalGrid;
    cxDBVerticalGrid4NamaBongkar: TcxDBEditorRow;
    cxDBVerticalGrid4TelpBongkar: TcxDBEditorRow;
    pnl12: TPanel;
    pnl13: TPanel;
    pnl14: TPanel;
    cxDBVerticalGrid5: TcxDBVerticalGrid;
    cxDBVerticalGrid5xNama: TcxDBEditorRow;
    cxDBVerticalGrid5xTgl: TcxDBEditorRow;
    cxDBVerticalGrid5xNoSJ: TcxDBEditorRow;
    cxDBVerticalGrid6: TcxDBVerticalGrid;
    pnl16: TPanel;
    cxDBVerticalGrid7: TcxDBVerticalGrid;
    MasterQKodenota: TStringField;
    MasterQTgl: TDateTimeField;
    MasterQNoSO: TStringField;
    MasterQRute: TStringField;
    MasterQPelanggan: TStringField;
    MasterQArmada: TStringField;
    MasterQEkor: TStringField;
    MasterQSopir: TStringField;
    MasterQCrew: TStringField;
    MasterQKeterangan: TStringField;
    MasterQKirKepala: TBooleanField;
    MasterQKirEkor: TBooleanField;
    MasterQSTNK: TBooleanField;
    MasterQPajak: TBooleanField;
    MasterQTali: TIntegerField;
    MasterQRantai: TIntegerField;
    MasterQKayu: TIntegerField;
    MasterQxNama: TStringField;
    MasterQxTgl: TDateTimeField;
    MasterQxNoSJ: TStringField;
    MasterQNamaMuat: TStringField;
    MasterQTelpMuat: TStringField;
    MasterQNamaBongkar: TStringField;
    MasterQTelpBongkar: TStringField;
    MasterQCreateDate: TDateTimeField;
    MasterQCreateBy: TStringField;
    MasterQOperator: TStringField;
    MasterQTglEntry: TDateTimeField;
    MasterQLaporan: TStringField;
    MasterQmuat: TStringField;
    MasterQbongkar: TStringField;
    MasterQnama: TStringField;
    MasterQnama_1: TStringField;
    cxDBVerticalGrid1Kodenota: TcxDBEditorRow;
    cxDBVerticalGrid1Tgl: TcxDBEditorRow;
    cxDBVerticalGrid6NoSO: TcxDBEditorRow;
    cxDBVerticalGrid6Rute: TcxDBEditorRow;
    cxDBVerticalGrid6Pelanggan: TcxDBEditorRow;
    cxDBVerticalGrid6Sopir: TcxDBEditorRow;
    cxDBVerticalGrid6Crew: TcxDBEditorRow;
    cxDBVerticalGrid6Keterangan: TcxDBEditorRow;
    cxDBVerticalGrid6muat: TcxDBEditorRow;
    cxDBVerticalGrid6bongkar: TcxDBEditorRow;
    cxDBVerticalGrid6nama: TcxDBEditorRow;
    cxDBVerticalGrid6nama_1: TcxDBEditorRow;
    cxDBVerticalGrid7Armada: TcxDBEditorRow;
    cxDBVerticalGrid7Ekor: TcxDBEditorRow;
    cxDBVerticalGrid7KirKepala: TcxDBEditorRow;
    cxDBVerticalGrid7KirEkor: TcxDBEditorRow;
    cxDBVerticalGrid7STNK: TcxDBEditorRow;
    cxDBVerticalGrid7Pajak: TcxDBEditorRow;
    cxDBVerticalGrid7Tali: TcxDBEditorRow;
    cxDBVerticalGrid7Rantai: TcxDBEditorRow;
    cxDBVerticalGrid7Kayu: TcxDBEditorRow;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ExitBtnClick(Sender: TObject);
    procedure SaveBtnClick(Sender: TObject);
    procedure MasterQAfterInsert(DataSet: TDataSet);
    procedure MasterQBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
    kodenota:string;
  public
    { Public declarations }
    constructor Create(aOwner: TComponent;kd:string);
  end;

var
  NotaSJFm: TNotaSJFm;
  MasterOriSQL: string;
implementation

uses MenuUtama, DropDown, DM;

{$R *.dfm}

{ TMasterArmadaFm }

constructor TNotaSJFm.Create(aOwner: TComponent;kd:string);
begin
  inherited Create(aOwner);
  kodenota:='';
end;

procedure TNotaSJFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TNotaSJFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  release;
end;

procedure TNotaSJFm.ExitBtnClick(Sender: TObject);
var clear:boolean;
begin
  clear := true;
  if sender = ExitBtn then
  begin
    if (messagedlg('Anda yakin mau Batal ?',mtConfirmation,[mbYes,mbNo],0)=IDNo) then clear:=false;
  end;
  if clear then
  begin
    Release;
  end;
end;

procedure TNotaSJFm.SaveBtnClick(Sender: TObject);
begin
  MenuUtamaFm.Database1.StartTransaction;
  try
    MasterQ.ApplyUpdates;
    MenuUtamaFm.Database1.Commit;
    MasterQ.CommitUpdates;
    ShowMessage('Pesanan Penjualan dengan Kode ' + MasterQKodenota.AsString + ' telah disimpan');
    if kodenota='' then TNotaSJFm.Create(Self.Parent,'');
    ExitBtnClick(sender);
  except
    MenuUtamaFm.Database1.Rollback;
    MasterQ.RollbackUpdates;
    ShowMessage('Penyimpanan Gagal');
  end;
end;

procedure TNotaSJFm.MasterQAfterInsert(DataSet: TDataSet);
begin
  DMFm.GetDateQ.Open;
  MasterQCreateDate.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  MasterQCreateBy.AsString:=User;
  DMFm.GetDateQ.Close;
end;

procedure TNotaSJFm.MasterQBeforePost(DataSet: TDataSet);
begin
  DMFm.GetDateQ.Open;
  MasterQTglEntry.AsDateTime:=DMFm.GetDateQNow.AsDateTime;
  MasterQOperator.AsString:=User;
  DMFm.GetDateQ.Close;
end;

end.
