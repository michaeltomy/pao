object JadwalArmadaFm: TJadwalArmadaFm
  Left = 288
  Top = 204
  Width = 753
  Height = 436
  Caption = 'JADWAL ARMADA'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 745
    Height = 264
    Align = alTop
    Caption = 'pnl1'
    TabOrder = 0
    object StringGrid1: TStringGrid
      Left = 1
      Top = 30
      Width = 743
      Height = 233
      Align = alClient
      ColCount = 30
      RowCount = 10
      TabOrder = 0
    end
    object pnl2: TPanel
      Left = 1
      Top = 1
      Width = 743
      Height = 29
      Align = alTop
      Caption = 'Tabel Jadwal Armada'
      TabOrder = 1
    end
  end
  object pnl3: TPanel
    Left = 0
    Top = 264
    Width = 745
    Height = 145
    Align = alClient
    Caption = 'pnl3'
    TabOrder = 1
    object MasterVGrid: TcxDBVerticalGrid
      Left = 1
      Top = 28
      Width = 743
      Height = 116
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      OptionsData.CancelOnExit = False
      OptionsData.Editing = False
      OptionsData.Appending = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Inserting = False
      ParentFont = False
      TabOrder = 0
      DataController.DataSource = DetailJadwalDs
      object MasterVGridNoSJ: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'NoSJ'
      end
      object MasterVGridNoSO: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'NoSO'
      end
      object MasterVGridPelanggan: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'Pelanggan'
      end
      object MasterVGridRute: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'Rute'
      end
    end
    object pnl4: TPanel
      Left = 1
      Top = 1
      Width = 743
      Height = 27
      Align = alTop
      Caption = 'Detail Jadwal Armada'
      TabOrder = 1
    end
  end
  object DetailJadwalQ: TSDQuery
    DatabaseName = 'data'
    SQL.Strings = (
      'select '#39#39' as Rute, '#39#39' as NoSJ, '#39#39' as Pelanggan, '#39#39' as NoSO')
    Left = 185
    Top = 158
    object DetailJadwalQRute: TStringField
      FieldName = 'Rute'
      Required = True
      Size = 1
    end
    object DetailJadwalQNoSJ: TStringField
      FieldName = 'NoSJ'
      Required = True
      Size = 1
    end
    object DetailJadwalQPelanggan: TStringField
      FieldName = 'Pelanggan'
      Required = True
      Size = 1
    end
    object DetailJadwalQNoSO: TStringField
      FieldName = 'NoSO'
      Required = True
      Size = 1
    end
  end
  object DetailJadwalDs: TDataSource
    DataSet = DetailJadwalQ
    Left = 259
    Top = 162
  end
end
