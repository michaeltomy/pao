unit DaftarSupplier;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, SDEngine, Grids, DBGrids,
  cxGridBandedTableView, cxGridDBBandedTableView, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxGridCustomPopupMenu,
  cxGridPopupMenu, cxContainer, cxTextEdit, cxMaskEdit, cxDropDownEdit;

type
  TDaftarSupplierFm = class(TForm)
    pnl1: TPanel;
    DataSource1: TDataSource;
    cxGridViewRepository1: TcxGridViewRepository;
    cxGridViewRepository1DBBandedTableView1: TcxGridDBBandedTableView;
    cxGridPopupMenu1: TcxGridPopupMenu;
    cxGrid1: TcxGrid;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxGrid1Level1: TcxGridLevel;
    MasterQ: TSDQuery;
    Panel1: TPanel;
    cxTextEdit1: TcxTextEdit;
    cxComboBox1: TcxComboBox;
    MasterQKode: TStringField;
    MasterQNama: TStringField;
    MasterQAlamat: TMemoField;
    MasterQKota: TStringField;
    MasterQTelp1: TStringField;
    MasterQTelp2: TStringField;
    MasterQTelp3: TStringField;
    cxGridViewRepository1DBBandedTableView1Column1: TcxGridDBBandedColumn;
    cxGridViewRepository1DBBandedTableView1Column2: TcxGridDBBandedColumn;
    cxGridViewRepository1DBBandedTableView1Column3: TcxGridDBBandedColumn;
    cxGridViewRepository1DBBandedTableView1Column4: TcxGridDBBandedColumn;
    cxGridViewRepository1DBBandedTableView1Column5: TcxGridDBBandedColumn;
    cxGridViewRepository1DBBandedTableView1Column6: TcxGridDBBandedColumn;
    cxGridViewRepository1DBBandedTableView1Column7: TcxGridDBBandedColumn;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormCreate(Sender: TObject);
    procedure dbgrd1DblClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cxTextEdit1PropertiesChange(Sender: TObject);
    procedure cxComboBox1PropertiesChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    q :TSDQuery;
  public
    { Public declarations }
    kode:string;
//    constructor Create(aOwner: TComponent;Query1:TSDQuery);overload;
  end;

var
  DaftarSupplierFm: TDaftarSupplierFm;
  masterorisql:string;

implementation

{$R *.dfm}

{ TDropDownFm }

procedure TDaftarSupplierFm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TDaftarSupplierFm.FormCreate(Sender: TObject);
var i : integer;
begin
  MasterQ.Open;
  masterorisql:=masterq.sql.text;
end;

procedure TDaftarSupplierFm.dbgrd1DblClick(Sender: TObject);
begin
  kode:=q.Fields[0].AsString;
  ModalResult:=mrOk;
end;

procedure TDaftarSupplierFm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
release;
end;

procedure TDaftarSupplierFm.cxTextEdit1PropertiesChange(Sender: TObject);
begin
  if cxComboBox1.ItemIndex=0 then
  begin
    masterq.Close;
    masterq.SQL.Text:= 'select * from supplier where nama like '+ QuotedStr('%'+cxTextEdit1.Text+'%');
    masterq.Open;
  end
  else if cxComboBox1.ItemIndex=1 then
  begin
    masterq.Close;
    masterq.SQL.Text:= 'select * from supplier where kode like '+ QuotedStr('%'+cxTextEdit1.Text+'%');
    masterq.Open;
  end
  else if cxComboBox1.ItemIndex=2 then
  begin
    masterq.Close;
    masterq.SQL.Text:= 'select * from supllier where kota like '+ QuotedStr('%'+cxTextEdit1.Text+'%');
    masterq.Open;
  end;
end;

procedure TDaftarSupplierFm.cxComboBox1PropertiesChange(Sender: TObject);
begin
cxTextEdit1.Clear;
end;

procedure TDaftarSupplierFm.FormShow(Sender: TObject);
begin
  cxTextEdit1.SetFocus;
end;

end.
